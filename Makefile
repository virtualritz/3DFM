
.PHONY: clean cleantree allversions
.PHONY: src mel 3dfmXGen osl xmltemplates

all: src mel 3dfmXGen osl xmltemplates

src:
	@$(MAKE) --silent -C ${DEVROOT}/src

mel:
	@$(MAKE) --silent -C ${DEVROOT}/mel

3dfmShave:
ifneq "$(SKIP_SHAVE)" "1"
	@$(MAKE) --silent -C ${DEVROOT}/3dfmShave
endif

3dfmHair:
	@$(MAKE) --silent -C ${DEVROOT}/3dfmHair

3dfmXGen:
	@$(MAKE) --silent -C ${DEVROOT}/3dfmXGen

osl:
	@$(MAKE) --silent -C ${DEVROOT}/osl

xmltemplates:
	@$(MAKE) --silent -C ${DEVROOT}/xmltemplates

clean:
	rm -rf build/$(PLAT)$(PLAT_SUFFIX)

cleantree:
	rm -rf build

ALLVERSIONS := 2018 2019 2020 2022 2023

allversions:
	@for version in $(ALLVERSIONS); do \
		if [ -e $(HOME_3DELIGHT)/building/mayalibs/$(PLAT)/maya$$version/include/maya/MStatus.h ] ; then \
			if [ -e $(HOME_3DELIGHT)/building/shave/$(PLAT)/maya$$version/include/maya/shaveAPI.h ] ; then \
				export MAYA_VERSION=$$version ; $(MAKE) --silent ; \
			else \
				export MAYA_VERSION=$$version ; $(MAKE) --silent SKIP_SHAVE=1 ; \
			fi ; \
		fi ; \
	done

