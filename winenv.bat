@echo off

rem ----- User environment -----

rem if HOME_3DELIGHT is not set, map a Q: drive letter to ../3delight-resources
rem and set HOME_3DELIGHT to that
if not "%HOME_3DELIGHT%" == "" goto home_3delight_defined
for /f "usebackq tokens=*" %%a in (`%ComSpec% /C "subst | grep Q:"`) do set QDRIVE=%%a

if not "%QDRIVE%" == "" goto qdrive_mapped
for /f "usebackq tokens=*" %%a in (`%ComSpec% /C "cygpath -aw %0 | sed -e 's,winenv\\(\\.bat\\)\\?,,'"`) do subst Q: %%a..\3delight-resources

:qdrive_mapped
set QDRIVE=
set HOME_3DELIGHT=q:

:home_3delight_defined

rem Use short paths to avoid errors in patsubst due to spaces in paths
rem Do NOT use double quotes in MAYA_LOCATION or DELIGHT values
if "%MAYA_LOCATION%" == "" goto maya_location_undefined
for /f "usebackq tokens=*" %%a in (`cygpath -d "%MAYA_LOCATION%"`) do set WIN_MAYA_LOCATION=%%a
:maya_location_undefined

if "%DELIGHT%" == '' goto delight_undefined
for /f "usebackq tokens=*" %%a in (`cygpath -d -m "%DELIGHT%"`) do set DELIGHT=%%a
:delight_undefined

rem ----- Compiler environment -----

set INCLUDE=

if "%~1" == "/VS2012_64" goto Set_VS2012_64
if "%~1" == "/VS2015_64" goto Set_VS2015_64
if "%~1" == "/VS2017" goto Set_VS2017
if "%~1" == "/VS2019" goto Set_VS2019

echo "usage: winenv.bat [ /VS2012_64 ]"
echo "usage: winenv.bat [ /VS2015_64 ]"
echo "usage: winenv.bat [ /VS2017 ]"
echo "usage: winenv.bat [ /VS2019 ]"
echo "usage: winenv.bat [ /VS2022 ]"
goto end

:Set_VS2012_64
rem This requires /V:ON switch to cmd or equivalent registry entry (see cmd /?).
call "%VS110COMNTOOLS%\..\..\VC\vcvarsall.bat" x86_amd64
set PLAT=win64-x64
goto finish_compiler

:Set_VS2015_64
rem This requires /V:ON switch to cmd or equivalent registry entry (see cmd /?).
call "%VS140COMNTOOLS%\..\..\VC\vcvarsall.bat" x86_amd64
set PLAT=win64-x64
goto finish_compiler

:Set_VS2017
rem This requires /V:ON switch to cmd or equivalent registry entry (see cmd /?).
call "%ProgramFiles(x86)%\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x86_amd64
set PLAT=win64-x64
goto finish_compiler

:Set_VS2019
rem Try the way things were installed on a first machine. Then how they ended
rem up on a second machine. I have no idea if I did something different in the
rem setup or if it's just from a slightly different version of VS2019.
if exist "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" (
	call "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" amd64
) else (
	call "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\Common7\Tools\VsDevCmd.bat" -arch=amd64
)
set PLAT=win64-x64
goto finish_compiler

:Set_VS2022
rem Try the way things were installed on a first machine. Then how they ended
rem up on a second machine. I have no idea if I did something different in the
rem setup or if it's just from a slightly different version of VS2019.
if exist "%ProgramFiles(x86)%\Microsoft Visual Studio\2022\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" (
	call "%ProgramFiles(x86)%\Microsoft Visual Studio\2022\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" amd64
) else (
	call "%ProgramFiles(x86)%\Microsoft Visual Studio\2022\Community\Common7\Tools\VsDevCmd.bat" -arch=amd64
)
set PLAT=win64-x64
goto finish_compiler

:finish_compiler

rem ----- DirectX SDK -----
if exist %HOME_3DELIGHT%\building\DirectX11 (
	echo Using DirectX SDK patched for intel compiler
	set "INCLUDE=%INCLUDE%;%HOME_3DELIGHT%\building\DirectX11\include"
	set "LIB=%LIB%;%HOME_3DELIGHT%\building\DirectX11\lib\%PLAT%"
) else (
	echo Using installed DirectX SDK, if any
	if exist "%DXSDK_DIR%\Utilities\bin\dx_setenv.cmd" call "%DXSDK_DIR%\Utilities\bin\dx_setenv.cmd"
)

rem ----- 3Delight environment -----

set OS=WINDOWS

set PATH=%PATH%;%DELIGHT%/bin;%DELIGHT%/lib

set MAKEFLAGS=--unix
set MAKE_MODE=opt

:end

rem Restore default colors. This fails in a remote shell, hence the || to reset errorlevel
color || %ComSpec% /C "exit 0"

rem Remove the double ; from the PATH variable as they mess up some part of
rem cygwin (make complains about not finding echo). They are inserted by the
rem crappy SetEnv from SDK 7.1
for /f "usebackq tokens=*" %%a in (`%ComSpec% /C "echo %PATH%| sed -e 's,;;,;,g'"`) do set PATH=%%a

rem Set this to avoid some warnings about a few dos paths in our makefiles.
set CYGWIN=nodosfilewarning

setup.bat
