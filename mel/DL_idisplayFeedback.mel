/*
  Copyright (c) 2014 soho vfx inc.
  Copyright (c) 2014 The 3Delight Team.
*/

// This file contains callbacks to be called from the i-display application
// and associated utility functions.

global proc
DIF_multiplyLayerColor(string $attributes[], float $values[], float $mult[])
{
  for($i = 0; $i < size($attributes); $i++)
  {
    if(objExists($attributes[$i]))
    {
      float $color[3];
      for($j = 0; $j < 3; $j++)
      {
        $color[$j] = $values[$i*3+$j] * $mult[$j];
      }
      setAttr $attributes[$i] -type float3 $color[0] $color[1] $color[2];
    }
  }
}

global proc
DIF_scaleLayerIntensity(string $attributes[], float $values[], float $scale)
{
  for($i = 0; $i < size($attributes); $i++)
  {
    if(objExists($attributes[$i]))
    {
      setAttr $attributes[$i] ($values[$i] * $scale);
    }
  }
}


global proc
DIF_selectLayer(string $attributes[])
{
  string $nodes[];
  for($i = 0; $i < size($attributes); $i++)
  {
    string $node = plugNode($attributes[$i]);
    if(objExists($node))
    {
      $nodes[size($nodes)] = $node;
    }
  }

  select $nodes;
}


global proc string
DIF_getLayerDescriptor(string $light_group, string $all_related_passes[])
{
  string $obj = "{";

  // Name
  string $nameAttr = "\"name\":\"" + RS_getLightGroupLabel($light_group) + "\"";

  // Light category type
  string $type;
  if(DL_isLight($light_group))
  {
    $type = "single light";
  }
  else
  {
    $type = "light group";
  }
  string $typeAttr = "\"type\":\"" + $type + "\"";

  // Intensity attributes
  string $attributes[];

  string $lights[] = DL_getLightsInGroup($light_group);
  for($light in $lights)
  {
    if(objExists($light + ".intensity"))
	{
	  $attributes[size($attributes)] = $light + ".intensity";
	}
  }

  string $quotedAttributes =
    "\"" + stringArrayToString($attributes, "\",\"") + "\"";
  if(size($attributes) == 0)
  {
    $quotedAttributes = "";
  }
  string $attributesAttr = "\"attributes\":[" + $quotedAttributes + "]";

  /*
    Current intensities.  Lib Jansson is peculiar in that it distinguishes
    integers from floats (unlike JSON, which doesn't).  Technically, this should
    not be a problem, because a JSON array can contains objects of differing
    types.  But mixing floats and integers in the same array seems to confuse
    it.  So, we create our array attribute string by hand, making sure that even
    numbers with zero fractional part are encoded as floats.
  */
  string $valuesAttr = "\"values\":[";
  string $sep = "";
  for($attribute in $attributes)
  {
    $valuesAttr += $sep;
    $sep = ",";

    string $value = `getAttr $attribute`;

    /*
      Encode plain integers as floats.  Using fmod to check if a number is an
      integer is not sufficient, since big numbers with no fractional part are
      still encoded as floats (in scientific notation).
    */
    if(!gmatch($value, "*[.e]*")) $value += ".0";

    $valuesAttr += $value;
  }
  $valuesAttr += "]";

  // Color attributes
  string $colorAttributes[];
  for($light in $lights)
  {
    if(objExists($light + ".color"))
      $colorAttributes[size($colorAttributes)] = $light + ".color";
    else if(objExists($light + ".tint"))
      $colorAttributes[size($colorAttributes)] = $light + ".tint";
    else if(objExists($light + ".colorLight"))
      $colorAttributes[size($colorAttributes)] = $light + ".colorLight";
  }

  string $quotedColorAttributes =
    "\"" + stringArrayToString($colorAttributes, "\",\"") + "\"";
  if(size($colorAttributes) == 0)
  {
    $quotedColorAttributes = "";
  }
  string $colorAttributesAttr = "\"color_attributes\":[" + $quotedColorAttributes + "]";

  /*
    Current colors.  Lib Jansson is peculiar in that it distinguishes
    integers from floats (unlike JSON, which doesn't).  Technically, this should
    not be a problem, because a JSON array can contains objects of differing
    types.  But mixing floats and integers in the same array seems to confuse
    it.  So, we create our array attribute string by hand, making sure that even
    numbers with zero fractional part are encoded as floats.
  */
  string $colorValuesAttr = "\"color_values\":[";
  $sep = "";
  for($colorAttribute in $colorAttributes)
  {
    $colorValuesAttr += $sep;
    $sep = ",";

    float $value[] = `getAttr $colorAttribute`;
    string $value0 = $value[0];
    string $value1 = $value[1];
    string $value2 = $value[2];

    /*
      Encode plain integers as floats.  Using fmod to check if a number is an
      integer is not sufficient, since big numbers with no fractional part are
      still encoded as floats (in scientific notation).
    */
    if(!gmatch($value0, "*[.e]*")) $value0 += ".0";
    if(!gmatch($value1, "*[.e]*")) $value1 += ".0";
    if(!gmatch($value2, "*[.e]*")) $value2 += ".0";

    $colorValuesAttr += "[" + $value0 + "," + $value1 + "," + $value2 + "]";
  }
  $colorValuesAttr += "]";

  return "{" + $nameAttr + "," + $typeAttr + "," + $attributesAttr + "," + $valuesAttr + "," + $colorAttributesAttr + "," + $colorValuesAttr + "}";
}

