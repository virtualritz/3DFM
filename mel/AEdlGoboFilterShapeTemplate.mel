/*
  Copyright (c) The 3Delight Team.
*/

global proc
AE_GFS_map_Changed(string $plug)
{
}

global proc int
AE_GFS_assignMapCB(
    string $plug,
    string $filename,
    string $file_type)
{
  setAttr -type "string" $plug $filename;

  AE_GFS_map_Changed($plug);

  return true;
}

global proc
AE_GFS_fileBrowser(string $plug)
{
  string $callback = "AE_GFS_assignMapCB " + $plug;
  fileBrowser($callback, "Select Map file", "Best Guess", 0);
}

global proc
AE_GFS_map_New(string $plug)
{
  AEfileButtonControl("Map", "map_Field", "map_BrowseButton", "");

  AE_GFS_map_Replace($plug);
}

global proc
AE_GFS_map_Replace(string $plug)
{
  connectControl map_Field $plug;

  string $cmd = "AE_GFS_fileBrowser " + $plug;
  button -e -c $cmd map_BrowseButton;

  textField
    -edit
    -changeCommand ("AE_GFS_map_Changed " + $plug)
    "map_Field";
}

proc string wrapModeToMenuItem( string $mode )
{
  string $item;

  switch( $mode )
  {
    case "black": $item = "Black"; break;
    case "mirror": $item = "Mirror"; break;
    case "periodic": $item = "Periodic"; break;
    case "clamp":
    default:
      $item = "Clamp";
  }

  return $item;
}

proc string menuItemToWrapMode( string $item )
{
  string $mode;

  switch( $item )
  {
    case "Black": $mode = "black"; break;
    case "Mirror": $mode = "mirror"; break;
    case "Periodic": $mode = "periodic"; break;
    case "Clamp":
    default:
      $mode = "clamp";
  }

  return $mode;
}

/*
  Callback on optionMenu changed events. Update the related attribute value.
*/
global proc
AE_GFS_optionMenuChangedCB( string $plug, string $menu )
{
  string $item = `optionMenuGrp -q -value $menu`;
  string $mode = menuItemToWrapMode( $item );
  setAttr -type "string" $plug $mode;
}

/*
  Callback on the attribute changed event to keep the option menu in sync
*/
global proc
AE_GFS_wrapAttributeChangedCB( string $plug, string $menu )
{
  if( !objExists( $plug ) )
  {
    return;
  }

  string $value = getAttr( $plug );
  string $item = wrapModeToMenuItem( $value );
  optionMenuGrp -edit -value $item $menu;
}

proc createOptionMenuWrap(string $label, string $menu)
{
  optionMenuGrp -label $label $menu;
  menuItem -label "Clamp";
  menuItem -label "Black";
  menuItem -label "Mirror";
  menuItem -label "Periodic";
  setParent ..;
}

global proc
AE_GFS_swrapNew(string $plug)
{
  string $menu = AE_controlNameFromPlug( $plug );
  createOptionMenuWrap("S Wrap Mode", $menu);

  AE_GFS_wrapReplace($plug);
}

global proc
AE_GFS_twrapNew(string $plug)
{
  string $menu = AE_controlNameFromPlug( $plug );
  createOptionMenuWrap("T Wrap Mode", $menu);

  AE_GFS_wrapReplace($plug);
}

global proc
AE_GFS_wrapReplace(string $plug)
{
  string $menu = AE_controlNameFromPlug( $plug );
  $menu = `optionMenuGrp -q -fullPathName $menu`;

  string $cmd = "AE_GFS_optionMenuChangedCB ";

  // Set the menu change command
  string $currCmd = $cmd + $plug + " " + $menu;
  optionMenuGrp -edit -changeCommand $currCmd $menu;

  string $sync_menu_cb = "AE_GFS_wrapAttributeChangedCB " +
    "\"" + $plug + "\" " +
    "\"" + $menu + "\"";

  // Install a script job to keep the menu in sync with attribute changes
  scriptJob
    -replacePrevious
    -parent $menu
    -attributeChange $plug
    $sync_menu_cb;

  // Update the menu label to the current attr value right now
  eval( $sync_menu_cb );
}

global proc AEdlGoboFilterShapeTemplate(string $node)
{
  editorTemplate -beginScrollLayout;

  AE_addTopSeparatorWithLabel( "goboSep", "Gobo", 30 );

  editorTemplate
    -callCustom "AE_GFS_map_New" "AE_GFS_map_Replace" "textureName";

  AE_addSpacer( 8 );
  AE_addControl( "density" );
  AE_addControl( "invert" );

  AE_addSpacer( 12 );

  AE_addControl( "scale" );
  AE_addControl( "offset" );

  AE_addSpacer( 8 );

  editorTemplate
    -callCustom "AE_GFS_swrapNew" "AE_GFS_wrapReplace" "swrap";
  editorTemplate
    -callCustom "AE_GFS_twrapNew" "AE_GFS_wrapReplace" "twrap";

  AE_addSpacer( 12 );
  AE_addControl( "useFilterCoordinateSystem" );
  editorTemplate -addExtraControls -extraControlsLabel "";

  editorTemplate -suppress bboxMin;
  editorTemplate -suppress bboxMax;

  editorTemplate -suppress motionBlur;
  editorTemplate -suppress visibleInRefractions;
  editorTemplate -suppress visibleInReflections;
  editorTemplate -suppress castsShadows;
  editorTemplate -suppress receiveShadows;
  editorTemplate -suppress doubleSided;
  editorTemplate -suppress opposite;
  editorTemplate -suppress smoothShading;

  editorTemplate -suppress lastReadFilename;
  editorTemplate -suppress version;
  editorTemplate -suppress caching;
  editorTemplate -suppress nodeState;
  editorTemplate -suppress visibility;
  editorTemplate -suppress intermediateObject;
  editorTemplate -suppress template;
  editorTemplate -suppress ghosting;
  editorTemplate -suppress instObjGroups;
  editorTemplate -suppress useObjectColor;
  editorTemplate -suppress objectColor;
  editorTemplate -suppress drawOverride;
  editorTemplate -suppress lodVisibility;
  editorTemplate -suppress renderInfo;
  editorTemplate -suppress blackBox;
  editorTemplate -suppress renderLayerInfo;
  editorTemplate -suppress ghostingControl;
  editorTemplate -suppress ghostCustomSteps;
  editorTemplate -suppress ghostFrames;
  editorTemplate -suppress ghostRangeStart;
  editorTemplate -suppress ghostRangeEnd;
  editorTemplate -suppress ghostDriver;
  editorTemplate -suppress ghostColorPreA;
  editorTemplate -suppress ghostColorPre;
  editorTemplate -suppress ghostColorPreR;
  editorTemplate -suppress ghostColorPreG;
  editorTemplate -suppress ghostColorPreB;
  editorTemplate -suppress ghostColorPostA;
  editorTemplate -suppress ghostColorPost;
  editorTemplate -suppress ghostColorPostR;
  editorTemplate -suppress ghostColorPostG;
  editorTemplate -suppress ghostColorPostB;
  editorTemplate -suppress maxVisibilitySamplesOverride;
  editorTemplate -suppress maxVisibilitySamples;
  editorTemplate -suppress geometryAntialiasingOverride;
  editorTemplate -suppress antialiasingLevel;
  editorTemplate -suppress shadingSamplesOverride;
  editorTemplate -suppress shadingSamples;
  editorTemplate -suppress maxShadingSamples;
  editorTemplate -suppress volumeSamplesOverride;
  editorTemplate -suppress volumeSamples;
  editorTemplate -suppress depthJitter;
  editorTemplate -suppress ignoreSelfShadowing;
  editorTemplate -suppress primaryVisibility;
  editorTemplate -suppress compInstObjGroups;
  editorTemplate -suppress tweak;
  editorTemplate -suppress relativeTweak;
  editorTemplate -suppress controlPoints;
  editorTemplate -suppress weights;
  editorTemplate -suppress uvSet;
  editorTemplate -suppress currentUVSet;
  editorTemplate -suppress displayImmediate;
  editorTemplate -suppress displayColors;
  editorTemplate -suppress displayColorChannel;
  editorTemplate -suppress currentColorSet;
  editorTemplate -suppress colorSet;
  editorTemplate -suppress ignoreHwShader;
  editorTemplate -suppress featureDisplacement;
  editorTemplate -suppress initialSampleRate;
  editorTemplate -suppress extraSampleRate;
  editorTemplate -suppress textureThreshold;
  editorTemplate -suppress normalThreshold;
  editorTemplate -suppress collisionOffsetVelocityIncrement;
  editorTemplate -suppress collisionDepthVelocityIncrement;
  editorTemplate -suppress collisionDepthVelocityMultiplier;
  editorTemplate -suppress collisionOffsetVelocityMultiplier;
  editorTemplate -suppress boundingBoxScale;
  editorTemplate -suppress rmbCommand;
  editorTemplate -suppress templateName;
  editorTemplate -suppress templatePath;
  editorTemplate -suppress viewName;
  editorTemplate -suppress iconName;
  editorTemplate -suppress viewMode;
  editorTemplate -suppress templateVersion;
  editorTemplate -suppress uiTreatment;
  editorTemplate -suppress customTreatment;
  editorTemplate -suppress creator;
  editorTemplate -suppress creationDate;
  editorTemplate -suppress containerType;
  editorTemplate -suppress objectColorRGB;
  editorTemplate -suppress selectionChildHighlighting;
  editorTemplate -suppress hiddenInOutliner;
  editorTemplate -suppress wireColorRGB;
  editorTemplate -suppress time;
  editorTemplate -suppress sc_gamma;
  editorTemplate -suppress fc_gamma;
  editorTemplate -suppress frozen;
  editorTemplate -suppress useOutlinerColor;
  editorTemplate -suppress outlinerColor;
  editorTemplate -suppress hardwareFogMultiplier;
  editorTemplate -suppress holdOut;

  editorTemplate -endScrollLayout;
}