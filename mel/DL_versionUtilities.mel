/*
  Copyright (c) 2014 soho vfx inc.
  Copyright (c) 2014 The 3Delight Team.
*/

global int $g_DL_pluginVersionWarningWasDisplayed;
global string $g_DL_lockedOutdatedNodes;

global proc
DL_setVersionAttr(string $node, int $version[])
{
  //
  // Description:
  //  This procedure is called during the creation of nodes of 3Delight node
  //  types (such as delightRenderPass, etc).
  //  This procedure adds a dynamic attribute called "version" to the
  //  specified node and sets its value to the oldest version of the 3Delight
  //  plugin that is able to correctly handle it. This makes it possible for
  //  code to determine if the node needs to be updated or is more recent than
  //  what the plugin can handle.
  //

  if (!objExists($node + ".version"))
  {
    addAttr -longName version -dataType "string" -hidden true $node;
  }

  string $version_str = $version[0] + "." + $version[1] + "." + $version[2];
  setAttr -type "string" ($node + ".version") $version_str;
}

//
// Converts ancient float "version" attribute to its current string form.
//
global proc
DL_updateVersionAttr(string $node)
{
  string $plug = $node + ".version";

  if (objExists($plug))
  {
    string $node_version = "";

    if (`getAttr -type ($plug)` == "float")
    {
      float $old_version = getAttr($plug);
      deleteAttr $plug;
      addAttr -longName version -dataType "string" $node;
      setAttr -type "string" $plug $old_version;
      $node_version = $old_version;
    }
  }
}

global proc int[]
DL_getVersionAttrAsInts(string $node)
{
  int $version_numbers[3] = { 0, 0, 0 };

  string $version_attr = $node + ".version";
  if (!objExists($version_attr))
    return $version_numbers;

  string $version_string = getAttr($version_attr);

  string $tokens[];
  int $num_tokens = tokenize($version_string, ".", $tokens);

  if ($num_tokens > 3)
    $num_tokens = 3;

  int $i;
  for($i = 0; $i < $num_tokens; $i++)
  {
    $version_numbers[$i] = $tokens[$i];
  }

  return $version_numbers;
}

global proc int
DL_isNodeVersionOlderThan(
  string $i_node,
  int $i_major,
  int $i_minor,
  int $i_patch)
{
  int $ref_number[] = { $i_major, $i_minor, $i_patch };
  int $res = DL_checkNodeVersion($i_node, $ref_number);

  return ($res < 0);
}

//
// Compares a version number to a reference number and returns -1, 0 or 1 if
// the version number is older than, equal to, or newer than the reference
// number, respectively.
//
proc int
compareVersions(int $version[], int $reference[])
{
  // Anything from 6.x.y and above is considered a remnant of the previous
  // versions. This is a migration measure for the version number reset to
  // 1.x.
  //
  if( $version[0] > $reference[0] && $version[0] > 5 )
    return -1;

  // Consider as new, every version below 6.x.y (e.g 1.3.22) when getting
  // compared with a reference above 5.x.y (e.g 9.0.1).
  //
  if($reference[0] > 5 && $version[0] < 6)
  {
	return 1;
  }

  for($i = 0; $i < size($version); $i++)
  {
    if ($version[$i] < $reference[$i])
      return -1;
    if ($version[$i] > $reference[$i])
      return 1;
  }

  return 0;
}

global proc int
DL_checkNodeVersion(string $node, int $oldest_compatible_version[])
{
  int $too_old = -1;
  int $up_to_date = 0;
  int $too_new = 1;

  string $version_plug = $node + ".version";

  if (!objExists($version_plug))
    return $too_old;

  if (`getAttr -type ($version_plug)` == "float")
    return $too_old;

  int $node_version[] = DL_getVersionAttrAsInts($node);
  if (compareVersions($node_version, $oldest_compatible_version) < 0)
    return $too_old;

  int $new_reference[] = `delightAbout -rendererVersionIntArray`;
  if (compareVersions($node_version, $new_reference) > 0)
    return $too_new;

  return $up_to_date;
}

global proc
delightSetPluginVersion(string $node)
{
  // Kept for compatibility
  // 7.0.12 is the last value returned by retired command
  // `delightAbout -oldestCompatibleVersionString`.
  //
  DL_setVersionAttr($node, { 7, 0, 12 });
}

global proc
DL_displayPluginVersionWarning(string $required_version)
{
  global int $g_DL_pluginVersionWarningWasDisplayed;
  if (!$g_DL_pluginVersionWarningWasDisplayed)
  {
    string $msg = "This scene requires 3Delight for Maya " + $required_version
      + " or newer. It may not render correctly with this version.";

    warning($msg);
    $g_DL_pluginVersionWarningWasDisplayed = 1;
  }
}

proc int
isNodeLocked(string $node)
{
  int $values[] = `lockNode -q $node`;
  return $values[0];
}

//
// Returns wether or not the given node is updatable.
//
// Referenced (i.e. read-only) nodes will be considered updatable when Maya
// is ran without a GUI; this is most likely in a rendering context where the
// scene will not be saved. Saving attribute addition & removal for read-only
// nodes is likely to cause issues and should be avoided.
//
proc int
isNodeUpdatable(string $node, int $gui)
{
  if ($gui && `referenceQuery -isNodeReferenced $node`)
  {
    string $ref_file = `referenceQuery -filename $node`;
    delightTable -setValueInt "delightReferencesTable" $ref_file 1;
    return false;
  }

  if (isNodeLocked($node))
  {
    global string $g_DL_lockedOutdatedNodes;
    $g_DL_lockedOutdatedNodes = $g_DL_lockedOutdatedNodes + $node + " ";
    return false;
  }

  return true;
}

//
// For every node of the specified type, call the specified update procedure if
// its version is older than the supplied version number.
//
proc
updateNodes(
  string $node_type,
  string $attrs_update_proc,
  string $user_update_proc,
  int $oldest_compatible_version[])
{
  // Enforce node update even when there is a GUI.
  // As mentioned in isNodeUpdateable comment, saving node updates on
  // referenced nodes may cause issues, but not doing it makes for a very
  // irritating workflow.
  //
  // See commit comment for details.
  //
  int $gui = 0;
  string $nodes[] = `ls -type $node_type`;

  for ($node in $nodes)
  {
    int $version_status =
      DL_checkNodeVersion($node, $oldest_compatible_version);

    if ($version_status > 0)
    {
      DL_displayPluginVersionWarning(getAttr($node + ".version"));
    }

    if ($version_status < 0)
    {
      if (!isNodeUpdatable($node, $gui))
        continue;

      if ($attrs_update_proc != "")
      {
        eval($attrs_update_proc +" \"" + $node + "\"");
      }
    }

    if ($user_update_proc != "")
    {
      eval($user_update_proc +" \"" + $node + "\"");
    }
  }
}

proc
createWarningWindow(string $window_name, string $labels[], int $num_references)
{
  // $labels is expected to be of size 3 by the rest of the code here; this is
  // simply to share the same text between the dialog and the warnings
  // displayed in the script editor.
  //
  int $labels_and_button_height = DL_switchOsInt(116, 111, 117);
  int $line_height = DL_switchOsInt(14, 12, 15);
  int $scrollfield_offset = 12;
  int $scrollbar_height = 17;

  int $scrollfield_height = $num_references * $line_height +
    $scrollbar_height + $scrollfield_offset;

  int $win_height =  $scrollfield_height + $labels_and_button_height;
  if ($win_height > 700)
    $win_height = 700;

  string $window = `window
    -title "3Delight for Maya Warning"
    -width 500
    -h $win_height
    $window_name`;

  string $form = `formLayout`;

  int $num_labels = size($labels);

  string $t1 = `text -label ("<h3>" + $labels[0] + "</h3>")`;
  string $t2 = `text -label $labels[1]`;
  string $t3 = `text -label $labels[2]`;

  string $scroll = `scrollField
    -editable false
    -h $scrollfield_height
    ($window_name + "ScrollField")`;

  string $button = `button -label "Close" -command ("deleteUI "+ $window)`;

  setParent ..;  // from columnLayout
  setParent ..;  // from formLayout

  formLayout
    -edit
    -attachForm $t1 "top" 10
    -attachForm $t1 "left" 10
    -attachForm $t1 "right" 10
    -attachNone $t1 "bottom"

    -attachControl $t2 "top" 10 $t1
    -attachForm $t2 "left" 10
    -attachForm $t2 "right" 10
    -attachNone $t2 "bottom"

    -attachControl $t3 "top" 0 $t2
    -attachForm $t3 "left" 10
    -attachForm $t3 "right" 10
    -attachNone $t3 "bottom"

    -attachControl $scroll "top" 10 $t3
    -attachForm $scroll "left" 10
    -attachForm $scroll "right" 10
    -attachForm $scroll "bottom" 45

    -attachControl $button "top" 10 $scroll
    -attachPosition $button "left" -40 50
    -attachPosition $button "right" -40 50
    -attachForm $button "bottom" 10

    $form;
}

proc
displayOutdatedReferencedNodesWarning(string $labels[], string $references[])
{
  string $warning_window = "_3DelightWarningWindow";

  string $all_windows[] = `lsUI -windows`;
  if (!stringArrayContains($warning_window, $all_windows))
  {
    createWarningWindow($warning_window, $labels, size($references));
  }

  string $prev_parent = `setParent -q`;

  setParent $warning_window;

  string $scroll_field = ($warning_window + "ScrollField");

  if (!`scrollField -q -exists $scroll_field`)
    return;

  string $scroll_text = `scrollField -q -text $scroll_field`;
  string $references_to_display[] = $references;

  if ($scroll_text != "")
  {
    string $existing_refs[];
    tokenize($scroll_text, "\n", $existing_refs);

    DL_stringArrayAppend($references_to_display, $existing_refs);

    $references_to_display =
      DL_stringArrayRemoveDuplicates($references_to_display);
  }

  $scroll_text = `stringArrayToString $references_to_display "\n"`;

  scrollField -e -text $scroll_text $scroll_field;

  showWindow $warning_window;

  setParent $prev_parent;
}

/*
  delightUpdateOldNodes

  Updates all the 3Delight for Maya nodes to ensure they have all the
  attributes of the latest version.

  This is usually called upon plugin load and via a C++ callback on scene &
  references events.
*/
global proc
delightUpdateOldNodes()
{
  global int $g_DL_pluginVersionWarningWasDisplayed;
  $g_DL_pluginVersionWarningWasDisplayed = 0;

  string $ref_table = "delightReferencesTable";
  if (`delightTable -tableExists $ref_table`)
    delightTable -clearTable $ref_table;
  else
    delightTable -addTable $ref_table;

  global string $g_DL_lockedOutdatedNodes;
  $g_DL_lockedOutdatedNodes = "";

  updateNodes(
    "dlRenderSettings",
    "RS_updateOldAttributes",
    "",
    RS_getOldestCompatibleVersion());

  updateNodes(
    "dlRenderGlobals",
    "RG_updateOldAttributes",
    "",
    RG_getOldestCompatibleVersion());

  string $refs[] = `delightTable -getAllKeys $ref_table`;
  if (size($refs) > 0)
  {
    string $warning_text[] =
    {
      "References contain outdated 3Delight for Maya nodes.",
      "This may cause errors or render incorrectly.",
      "Open and then save the following referenced scenes to update them:"
    };

    warning(stringArrayToString($warning_text, " "));
    for ($curr_ref in $refs)
    {
      warning $curr_ref;
    }

    displayOutdatedReferencedNodesWarning($warning_text, $refs);
  }

  // Not very likely, but possible case
  //
  if ($g_DL_lockedOutdatedNodes != "")
  {
    warning("Some outdated 3Delight for Maya nodes are locked. They may " +
      "cause errors or render incorrectly. Unlock the following nodes and " +
      "run `delightUpdateOldNodes`:");
    warning($g_DL_lockedOutdatedNodes);
  }

  delightTable -deleteTable $ref_table;
}

