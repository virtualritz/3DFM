/*
  Copyright (c) The 3Delight Team.
*/

global proc int[]
DLM_dlColorCorrection_getOldestCompatibleVersion()
{
  return { 9, 0, 0 };
}

global proc dlColorCorrectionInit( string $node )
{
  if(!objExists($node))
  {
    return;
  }

  setAttr( $node + ".colorR" ) 1;
  setAttr( $node + ".colorG" ) 1;
  setAttr( $node + ".colorB" ) 1;

  if(!objExists($node + ".mask"))
  {
    addAttr -longName mask -attributeType "float"
      -minValue 0
      -maxValue 1
      -keyable true
      -dv 1;
  }

  if( !objExists($node + ".gamma") )
  {
    addAttr -longName gamma -attributeType "float"
    -minValue 0
    -maxValue 5
    -keyable true
    -dv 1;
  }

  if( !objExists($node + ".hueShift") )
  {
    addAttr -longName hueShift -attributeType "float"
    -minValue 0
    -maxValue 1
    -keyable true
    -dv 0;
  }

  if(!objExists($node + ".saturation"))
  {
    addAttr -longName saturation -attributeType "float"
    -minValue 0
    -maxValue 5
    -keyable true
    -dv 1;
  }

  if(!objExists($node + ".vibrance"))
  {
    addAttr -longName vibrance -attributeType "float"
    -minValue 0
    -maxValue 5
    -keyable true
    -dv 1;
  }

  if(!objExists($node + ".contrast"))
  {
    addAttr -longName contrast -attributeType "float"
    -minValue 0
    -maxValue 5
    -keyable true
    -dv 1;
  }

  if(!objExists($node + ".contrastPivot"))
  {
    addAttr -longName contrastPivot -attributeType "float"
    -minValue 0
    -maxValue 1
    -keyable true
    -dv 0.18;
  }

  if(!objExists($node + ".exposure"))
  {
    addAttr -longName exposure -attributeType "float"
    -minValue -10
    -maxValue 10
    -keyable true
    -dv 0;
  }

  if(!objExists($node + ".gain"))
  {
    addAttr -longName gain -attributeType float3 -usedAsColor $node;
    addAttr
      -longName gainR -attributeType "float" -dv 1 -parent gain $node;
    addAttr
      -longName gainG -attributeType "float" -dv 1 -parent gain $node;
    addAttr
      -longName gainB -attributeType "float" -dv 1 -parent gain $node;
  }

  if(!objExists($node + ".offset"))
  {
    addAttr -longName offset -attributeType float3 -usedAsColor $node;
    addAttr -longName offsetR -attributeType "float" -dv 0 -parent offset $node;
    addAttr -longName offsetG -attributeType "float" -dv 0 -parent offset $node;
    addAttr -longName offsetB -attributeType "float" -dv 0 -parent offset $node;
  }

  if(!objExists($node + ".invert"))
  {
    addAttr -longName invert -attributeType bool -dv 0 $node;
  }
}

global proc
DLM_dlColorCorrection_outputShaderAttributes( string $node )
{
}

global proc
DLM_dlColorCorrection_updateOldAttributes(string $node)
{
  // When adding new procedure calls above, don't forget to update
  // DLM_dlColorCorrection_getOldestCompatibleVersion(), at the beginning of the
  // file.
  DL_setVersionAttr($node, DLM_dlColorCorrection_getOldestCompatibleVersion());
}


