/*
  Copyright (c) 2013 soho vfx inc.
  Copyright (c) 2013 The 3Delight Team.
*/


// This file contains the implementation of the AOV Selector dialog.
// The main function is DL_aovSelector, which is defined further below.


// Type IDs for list items.  Those are prefixed to the items.

proc int DL_PredefinedAovID()    { return 0; }
proc int DL_PredefinedSubAovID() { return 1; }
proc int DL_CustomAovID()        { return 2; }
proc int DL_MaskID()             { return 3; }
proc int DL_TitleID()            { return 4; }
proc int DL_EmptyItemID()        { return 5; }
proc int DL_UnknownItemID()      { return 6; }

proc string
addID(int $id, string $item)
{
  return $id + $item;
}

proc string
removeID(string $item)
{
  return substring($item, 2, size($item));
}

proc int
getID(string $item)
{
  return int(startString($item, 1));
}


// Various lists contents

proc string[]
GetShadingItems()
{
  return stringArrayAddPrefix(DOV_predefinedShadingAovs(), DL_PredefinedAovID());
}

proc int
CountSubItems(string $items[])
{
  int $n = 0;
  for($item in $items)
  {
    if(getID($item) == DL_PredefinedSubAovID())
    {
      $n++;
    }
  }

  return $n;
}

proc string[]
GetAuxiliaryItems()
{
  return stringArrayAddPrefix(DOV_predefinedAuxiliaryAovs(), DL_PredefinedAovID());
/*
  DL_stringArrayAppend($items, GetMaskItems());

  return $items;
*/
}

proc
stringArrayAddPrefixAndAppend(
  string $target[],
  string $source[],
  string $newPrefix)
{
  for($item in $source)
  {
    $target[size($target)] = $newPrefix + $item;
  }
}

proc string[]
GetCustomItems()
{
  return stringArrayAddPrefix(DOV_customAovs(), DL_CustomAovID());
}

/*
  Pops a modal dialog allowing the user to change which AOVs are selected for
  output.
  Returns true if any change were made, false otherwise.
*/
global proc int
DL_aovSelector(string $aovs[])
{
  // Build a huge string to pass as an argument to the dialog, which will use it
  // to initialize its contents.

  string $typed_selection[];

  string $shading_items[] = GetShadingItems();
  string $aux_items[] = GetAuxiliaryItems();
  string $custom_items[] = GetCustomItems();

  for($aov in $aovs)
  {
    string $type;
    if(stringArrayContains(DL_PredefinedSubAovID() + $aov, $shading_items))
    {
      $type = DL_PredefinedSubAovID();
    }
    else if(stringArrayContains(DL_PredefinedAovID() + $aov, $shading_items))
    {
      $type = DL_PredefinedAovID();
    }
    else if(stringArrayContains(DL_PredefinedAovID() + $aov, $aux_items))
    {
      $type = DL_PredefinedAovID();
    }
    else if(stringArrayContains(DL_MaskID() + $aov, $aux_items))
    {
      $type = DL_MaskID();
    }
    else if(stringArrayContains(DL_CustomAovID() + $aov, $custom_items))
    {
      $type = DL_CustomAovID();
    }
    else
    {
      $type = DL_UnknownItemID();
    }

    $typed_selection[size($typed_selection)] = $type + $aov;
  }

  string $selection_string = stringArrayToString($typed_selection, "\t");

  string $question = ("DL_fillAovSelector \"" + $selection_string + "\"");


  // Display the dialog and wait for the user to close it

  string $answer = `layoutDialog -title "AOV Selector" -uiScript $question`;


  // Parse the returned string, as it might contain changes to the selection

  if($answer == "Cancel" || $answer == "dismiss")
  {
    return false;
  }

  string $new_selection[];
  if($answer != "")
  {
    tokenize($answer, "\t", $new_selection);
  }

  int $modified = false;
  for($i = size($typed_selection)-1; $i >=0; $i--)
  {
    if(getID($typed_selection[$i]) == DL_UnknownItemID())
    {
      continue;
    }

    int $pos = DL_stringArrayFind($typed_selection[$i], $new_selection);
    if($pos < 0)
    {
      stringArrayRemoveAtIndex($i, $typed_selection);
      $modified = true;
    }
    else
    {
      stringArrayRemoveAtIndex($pos, $new_selection);
    }
  }

  if(size($new_selection) > 0)
  {
    appendStringArray($typed_selection, $new_selection, size($new_selection));
    clear($new_selection);
    $modified = true;
  }

  clear($aovs);

  for($item in $typed_selection)
  {
    int $type = getID($item);
    string $var = removeID($item);
    $aovs[size($aovs)] = $var;
  }

  return $modified;
}

global proc int
IgnoreDummyItems(string $item, int $selected)
{
  return getID($item) != DL_EmptyItemID() && getID($item) != DL_TitleID();
}

global proc int
IgnoreDummyAndLeafItems(string $item, int $selected)
{
  return
    IgnoreDummyItems($item, $selected) &&
    getID($item) != DL_PredefinedSubAovID();
}

proc string
CreateAOVList(
  string $name,
  string $items[],
  string $selected_items[],
  int $extraLines,
  int $expanded)
{
  string $selectionCommand = "IgnoreDummyAndLeafItems";
  if($expanded)
  {
    $selectionCommand = "IgnoreDummyItems";
  }

  string $list = DSL_createScrollList($name, $selectionCommand);

  int $c;
  int $nbItems = size($items);
  for($c = 0; $c < $nbItems; $c++)
  {
      string $item = $items[$c];
      string $item_id = getID($item);

      DSL_addItem(
        $list,
        $item,
        DOV_uiNameField(removeID($item)),
        stringArrayContains($item, $selected_items),
        $item_id != DL_PredefinedSubAovID() || $expanded,
        $item_id != DL_TitleID());
  }

  for($c = 0; $c < $extraLines; $c++)
  {
    DSL_addItem(
      $list,
      string(DL_EmptyItemID()) + $c,
      "",
      false,
      !$expanded,
      false);
  }

  return $list;
}

global proc
DL_expandAovList(string $list)
{
  string $items[] = DSL_getAllItems($list);
  for($item in $items)
  {
    DSL_setItemVisibility($list, $item, (getID($item) != DL_EmptyItemID()));
  }

  DSL_setSelectionCallback($list, "IgnoreDummyItems");
}

global proc
DL_collapseAovList(string $list)
{
  string $items[] = DSL_getAllItems($list);
  for($item in $items)
  {
    DSL_setItemVisibility($list, $item, (getID($item) != DL_PredefinedSubAovID()));
  }

  /*
    The tree view selects even invisible items when the user selects a range of
    item using Shift+click.  This validation callback prevents this.
  */
  DSL_setSelectionCallback($list, "IgnoreDummyAndLeafItems");
}

proc
DL_fillSelection(string $list, string $selection[])
{
  string $selected[] = DSL_getSelectedItems($list);
  appendStringArray($selection, $selected, size($selected));
}

global proc
DL_dismissAovSelector(string $shading_list, string $aux_list, string $custom_list)
{
  string $selection[];
  DL_fillSelection($shading_list, $selection);
  DL_fillSelection($aux_list, $selection);
  DL_fillSelection($custom_list, $selection);

  string $answer = stringArrayToString($selection, "\t");

  layoutDialog -dismiss $answer;
}

global proc
DL_fillAovSelector(string $selection_string)
{
  string $shading_items[] = GetShadingItems();
  int $nb_shading_sub_items = CountSubItems($shading_items);
  string $auxiliary_items[] = GetAuxiliaryItems();
  string $custom_items[] = GetCustomItems();

  string $selection[];
  if($selection_string != "")
  {
    tokenize($selection_string, "\t", $selection);
  }

  int $max_lines = max(size($shading_items), size($auxiliary_items));

  string $dialog = `setParent -q`;

  string $shading_label =
    `text -label "Shading Components" -font "boldLabelFont" -enable false`;
  int $expand = CountSubItems($selection) > 0;
  string $shading_list =
    CreateAOVList(
      "shading",
      $shading_items,
      $selection,
      $max_lines - size($shading_items),
      $expand);

  string $expand_check =
    `checkBox
      -label "Additional Sub-Components"
      -visible false
      -value $expand
      -onCommand ("DL_expandAovList " + $shading_list)
      -offCommand ("DL_collapseAovList " + $shading_list)`;

  string $aux_label =
    `text -label "Auxiliary Variables" -font "boldLabelFont" -enable false`;
  string $aux_list =
    CreateAOVList(
      "auxiliary",
      $auxiliary_items,
      $selection,
      $max_lines - size($auxiliary_items),
      false);
  string $custom_label =
    `text -label "Custom Variables" -font "boldLabelFont" -enable false`;
  string $custom_list =
    CreateAOVList(
      "custom",
      $custom_items,
      $selection,
      $max_lines - size($custom_items),
      false);

  string $font = "tinyBoldLabelFont";
  if(getApplicationVersionAsFloat() < 2011)
  {
    $font = "smallPlainLabelFont";
  }
  string $custom_info = `text -align "left" -font $font -enable false`;
  if(DOV_useCustomUserFunctions())
  {
    text
      -edit
      -label "Custom Variables were\nuser-defined using the \n3Delight API."
      $custom_info;
  }
  else
  {
    text
      -edit
      -label
        ("Custom Variables are defined\nin file \"customAOVs.txt\",\n" +
        "located in your 3Delight\ninstallation directory.")
      $custom_info;
  }

  string $ok =
    `button
      -label "OK"
      -command
        ("DL_dismissAovSelector " + $shading_list + " " + $aux_list + " " +
        $custom_list)`;
  string $cancel =
    `button
      -label "Cancel"
      -command "layoutDialog -dismiss \"Cancel\""`;

  int $horizontal_spacing = 30;
  int $vertical_spacing = 8;
  int $check_width = DL_switchOsInt(165, 175, 155);
  int $list_width = $check_width;
  int $list_height = DSL_listHeight($max_lines);
  int $info_width = DL_switchOsInt(136, 140, 150);

  layout -edit -height $list_height $shading_list;
  layout -edit -height $list_height $aux_list;
  layout -edit -height $list_height $custom_list;

  formLayout
    -edit
    -width (3 * $list_width + 4 * $horizontal_spacing)
    -height 285

    -attachForm $ok "bottom" $vertical_spacing
    -attachPosition $ok "right" ($horizontal_spacing / 2) 50

    -attachForm $cancel "bottom" $vertical_spacing
    -attachPosition $cancel "left" ($horizontal_spacing / 2) 50

    -attachForm $shading_label "top" (2 * $vertical_spacing)
    -attachPosition $shading_label "left" (-$horizontal_spacing - 3 * $list_width / 2) 50
    -attachPosition $shading_label "right" ($horizontal_spacing + $list_width / 2) 50

    -attachForm $aux_label "top" (2 * $vertical_spacing)
    -attachPosition $aux_label "left" (-$list_width / 2) 50
    -attachPosition $aux_label "right" (-$list_width / 2) 50

    -attachForm $custom_label "top" (2 * $vertical_spacing)
    -attachPosition $custom_label "left" ($horizontal_spacing + $list_width / 2) 50
    -attachPosition $custom_label "right" (-$horizontal_spacing - 3 * $list_width / 2) 50

    -attachControl $shading_list "top" $vertical_spacing $shading_label
    -attachOppositeControl $shading_list "left" 0 $shading_label
    -attachOppositeControl $shading_list "right" 0 $shading_label

    -attachControl $aux_list "top" $vertical_spacing $aux_label
    -attachOppositeControl $aux_list "left" 0 $aux_label
    -attachOppositeControl $aux_list "right" 0 $aux_label

    -attachControl $custom_list "top" $vertical_spacing $custom_label
    -attachOppositeControl $custom_list "left" 0 $custom_label
    -attachOppositeControl $custom_list "right" 0 $custom_label

    -attachControl $expand_check "top" $vertical_spacing $shading_list
    -attachPosition
      $expand_check
      "left"
      (-$list_width - $horizontal_spacing - $check_width / 2)
      50

    -attachControl $custom_info "top" $vertical_spacing $aux_list
    -attachPosition
      $custom_info
      "left"
      ($list_width + $horizontal_spacing - $info_width / 2)
      50

    $dialog;
}

