/*
	Copyright (c) 2006 soho vfx inc.
	Copyright (c) 2006 The 3Delight Team.
*/

/*
  This is the image layers UI portion of the dlRenderSettings AE template.
*/

/* Obsolete - should not be used */
proc string AE_getUIPrefix(string $plug)
{
  //
  // Returns a prefix to use for naming UI elements related to the given node
  //
  string $node = plugNode($plug);
  string $node_type_prefix = DL_getNodeTypePrefix($node);

  string $prefix = "";
  if (delightExists($node_type_prefix + "getPrefix"))
    $prefix = eval($node_type_prefix + "getPrefix(\"" + $node + "\")");

  if ($prefix == "")
    $prefix = "_" + $node_type_prefix;

  return $prefix;
}

proc
layerFilenameTextReplace(
  string $prefix,
  string $node,
  int $Layer_index)
{
  string $parent_layout = $prefix + "overridesColumnLayout";
  if (! `layout -q -exists $parent_layout`)
    return;

  AE_RS_updateCustomFilename($prefix, $node);

  string $filenameTextField = $prefix + "filenameTextField";
  textFieldGrp
    -edit
    -changeCommand
      ("AE_RS_layerFilenameTextFieldChanged " + $filenameTextField + " " +
        $node)
    $filenameTextField;

  scriptJob
    -attributeChange
      ($node + ".layerFilenames")
      ("AE_RS_updateCustomFilename " + $prefix + " " + $node)
    -disregardIndex
    -compressUndo true
    -replacePrevious
    -parent $filenameTextField;
  scriptJob
    -attributeChange
      ($node + ".layerDefaultFilename")
      ("AE_RS_updateCustomFilename " + $prefix + " " + $node)
    -compressUndo true
    -parent $filenameTextField;
}

global proc int
AE_RS_selectLight(
  string $node,
  string $item,
  int $selected)
{
  int $layers[] = RS_getSelectedLayersIndices($node);

  if($selected)
  {
    RS_addLightGroup($node, $item);
  }
  else
  {
    RS_removeLightGroup($node, $item);
  }

  return true;
}

global proc
AE_RS_updateLightsList(string $node, string $list, string $expand_toggle)
{
  DSL_removeAllItems($list);

  string $groups[] = RS_getLightGroups($node);

  string $lights[] = eval(DL_getListLightNodesCmd() + " -l");

  string $single_lights[];
  string $sets[] = DL_getAllLightSets($lights, $single_lights);

  if(size($sets) > 0)
  {
    DSL_addSubtitle($list, "Groups");
  }

  for($set in $sets)
  {
    DSL_addItem(
      $list,
      $set,
      ("    " + RS_getLightGroupLabel($set)),
      stringArrayContains($set, $groups),
      true,
      true);
  }

  if(!`checkBox -q -value $expand_toggle`)
  {
    $lights = $single_lights;
  }

  if(size($lights) > 0)
  {
    DSL_addSubtitle($list, "Lights");
  }

  for($light in $lights)
  {
    DSL_addItem(
      $list,
      $light,
      ("    " + RS_getLightGroupLabel($light)),
      stringArrayContains($light, $groups),
      true,
      true);
  }

  int $nb_lines = 10;
  int $nb_items =
    (size($sets) > 0) + size($sets) +
    (size($lights) > 0) + size($lights);

  while($nb_items < $nb_lines)
  {
    DSL_addItem($list, ("+" + $nb_items), "", false, true, false);
    $nb_items++;
  }

  control -edit -height (DSL_listHeight($nb_lines)) $list;
}

global proc
AE_RS_updateLightGroupsCallback(
  string $node,
  string $list,
  string $expand)
{
  string $update_cmd =
    "AE_RS_updateLightsList " + $node + " " + $list + " " + $expand;
  string $categories_attr = $node + ".lightGroups";
  scriptJob
    -attributeChange $categories_attr $update_cmd
    -disregardIndex
    -compressUndo true
    -replacePrevious
    -parent $list;
}

proc
layerLightsGadgetsCreate(string $prefix)
{
  string $frame = $prefix + "lightsFrameLayout";
  string $form = `formLayout -parent $frame ($prefix + "lightsFormLayout")`;

  $list = DSL_createScrollList($prefix + "lightsList", "");

  string $expand =
    `checkBox -height 20 -label "Display All Lights" ($prefix + "expandToggle")`;
  int $expandWidth = 100;

  string $refresh =
    `button -height 20 -label "Refresh" ($prefix + "refreshButton")`;

  setParent ..;

  formLayout
    -edit
    -attachForm $refresh "bottom" 4
    -attachPosition $refresh "left" -25 50
    -attachPosition $refresh "right" -25 50
    -attachControl $expand "bottom" 4 $refresh
    -attachPosition $expand "left" (-$expandWidth/2) 50
    -attachPosition $expand "right" (-$expandWidth/2) 50
    -attachForm $list "top" 4
    -attachForm $list "left" 100
    -attachForm $list "right" 100
    -attachControl $list "bottom" 4 $expand
    $form;
}

proc int
selectedLayerLightCategoriesContainsGroupedLights(string $node)
{
  string $groups[] = RS_getLightGroups($node);

  string $lights[];
  for($group in $groups)
  {
    if(DL_isLight($group))
    {
      $lights[size($lights)] = $group;
    }
  }

  string $sets[] = DL_getAllLightSets($lights, {});

  return size($sets) > 0;
}

proc
layerLightsGadgetsReplace(string $prefix, string $node)
{
  if(!`formLayout -q -exists ($prefix + "lightsFormLayout")`)
  {
    return;
  }

  string $list = $prefix + "lightsList";

  DSL_setSelectionCallback($list, ("AE_RS_selectLight " + $node));

  string $expand = $prefix + "expandToggle";

  checkBox -edit -changeCommand "" $expand;
  checkBox
    -edit
    -value (selectedLayerLightCategoriesContainsGroupedLights($node))
    $expand;

  string $update_list_cmd =
    "AE_RS_updateLightsList " + $node + " " + $list + " " + $expand;

  checkBox -edit -changeCommand $update_list_cmd $expand;
  button -edit -command $update_list_cmd ($prefix + "refreshButton");

  /*
    Reconnect the callbacks each time the number of layers changes, because
    there is a separate light groups attribute for each layer.
  */
  scriptJob
    -attributeChange
      ($node + ".layersOrder")
      ("AE_RS_updateLightGroupsCallback " + $node + " " + $list + " " + $expand)
    -disregardIndex
    -compressUndo true
    -replacePrevious
    -parent $list;
  // Connect the callbacks a first time
  AE_RS_updateLightGroupsCallback($node, $list, $expand);

  AE_RS_updateLightsList($node, $list, $expand);
}

global proc
AE_RS_layersNew(string $plug)
{
  global int $gAttributeEditorTemplateSingleWidgetWidth;
  global int $gAttributeEditorTemplateLabelWidth;
  global int $gAttributeEditorTemplateLabelOffset;

  string $prev_parent = `setParent -q`;
  string $basename = AE_getWindowBaseNameFromParent($prev_parent);
  string $ui_prefix = AE_getUIPrefix($plug);
  string $prefix = $basename + $ui_prefix;
  string $col_layout = "_column_layout";

  string $node = plugNode($plug);

  string $parent_layout = $prefix + plugAttr($plug) + $col_layout;
  columnLayout -adjustableColumn true $parent_layout;
  layout -edit -visible false $parent_layout;

  setUITemplate -pushTemplate attributeEditorTemplate;

  text -label "" -height 4;

  string $treeLayout = `formLayout`;

  string $labelFont = "tinyBoldLabelFont";
  if (getApplicationVersionAsFloat() < 2011)
    $labelFont = "smallPlainLabelFont";

  string $outputLabel = `text -label "Out   Multi" -height 22 -font $labelFont`;
  string $layerLabel = `text -label "Image Layer (AOV)" -height 22 -font $labelFont`;

  // The treeView creation is deferred to the replace procedure because it
  // needs to be re-created since the edit operation on pressCommand do not
  // work.
  //
  string $tree = `formLayout ($prefix + "OutputDispatcherTreeForm")`;
  setParent ..;

  string $b1 = `button -height 20 -label "Add..." ($prefix + "selectAOVsButton")`;
  string $b2 = `button -height 20 -enable false -label "Remove" ($prefix + "removeLayersButton")`;
  string $b3 = `button -height 20 -enable false -label "View..." ($prefix + "viewFileButton")`;

  // Add a small gap in between buttons in 2016 and more recent
  int $gap = getApplicationVersionAsFloat() >= 2016 ? 2 : 0;

  formLayout
    -edit
    -attachForm $outputLabel "top" 0
    -attachForm $outputLabel "left" -5
    -attachPosition $outputLabel "right" -85 0
    -attachControl $layerLabel "left" 0 $outputLabel
    -attachForm $layerLabel "top" 0
    -attachForm $layerLabel "right" 0

    -attachControl $tree "top" 0 $outputLabel
    -attachForm $tree "left" 12
    -attachForm $tree "right" 12

    -attachControl $b1 "top" 4 $tree
    -attachControl $b2 "top" 4 $tree
    -attachControl $b3 "top" 4 $tree

    -attachPosition $b1 "left" 20 0
    -attachPosition $b1 "right" -10 33
    -attachPosition $b2 "left" (10 + $gap) 33
    -attachPosition $b2 "right" 0 66
    -attachPosition $b3 "left" (0 + $gap) 66
    -attachPosition $b3 "right" 10 100

    $treeLayout;

  setParent ..; // back to parent_layout

  string $layerAttributesLayout =
    `formLayout -width (AE_getAEWidth()) ($prefix + "layerAttributesLayout")`;

  string $filenameLabel =
    `text
      -align "center"
      -font "smallPlainLabelFont"
      -enable false
      ($prefix + "filenameStaticText")`;

  setParent ..; // back to layerAttributesLayout

  formLayout
    -edit
    -attachForm $filenameLabel "top" 4
    -attachForm $filenameLabel "left" 10
    -attachForm $filenameLabel "right" 10
    $layerAttributesLayout;

  setParent ..; // back to parent_layout

  string $multilightSeparatorLayout = DL_separator("multilightSeparator");

  string $multilightFormLayout = `formLayout`;

  // Lights
  string $lights_frame =
    `frameLayout
      -visible true
      -label "Multi-Light"
      -collapsable true
      ($prefix + "lightsFrameLayout")`;

  setParent ..;  // back to formLayout

  formLayout
    -edit
    -attachForm $lights_frame "top" 0
    -attachForm $lights_frame "left" 10
    -attachForm $lights_frame "right" 10
    -attachForm $lights_frame "bottom" 4
    $multilightFormLayout;

  setParent ..; // back to layerAttributesLayout



  setUITemplate -popTemplate;

  AE_RS_layersReplace($plug);
  setParent $prev_parent;
}

global proc
AE_RS_layersReplace(string $plug)
{
  setUITemplate -pushTemplate "attributeEditorTemplate";

  string $prev_parent = `setParent -q`;
  string $base_name = AE_getWindowBaseNameFromParent($prev_parent);
  string $ui_prefix = AE_getUIPrefix($plug);
  string $prefix = $base_name + $ui_prefix;
  string $col_layout = "_column_layout";

  string $parent_layout = $prefix + plugAttr($plug) + $col_layout;
  setParent $parent_layout;
  layout -edit -visible false $parent_layout;

  string $node = plugNode($plug);

  button -edit
    -command ("AE_RS_selectAOVs " + $node)
    ($prefix + "selectAOVsButton");
  button -edit
    -command ("AE_RS_removeSelectedLayers " + $node)
    ($prefix + "removeLayersButton");
  button -edit
    -command ("AE_RS_viewSelectedLayers " + $node)
    ($prefix + "viewFileButton");

  // Editing treeView press commands do not work; until this is resolved we'll
  // need to destroy and re-create the widget.
  //
  string $tree = $prefix + "OutputDispatcherTree";
  string $tree_form = $tree + "Form";

  if (`treeView -q -exists $tree`)
  {
    deleteUI $tree;
  }

  setParent $tree_form;
  DLT_createLayersTreeView($tree);
  AE_attachControlToAllSidesOfForm($tree, $tree_form);
  DLT_connectLayersTreeView($node, $tree);
  setParent $parent_layout;

  scriptJob
    -attributeChange
      ($node + ".layerSelected")
      ("AE_RS_layerSelectedChanged " + $prefix + " " + $node)
    -disregardIndex
    -compressUndo true
    -parent $tree;
  AE_RS_layerSelectedChanged($prefix, $node);

  string $filenameLabel = ($prefix + "filenameStaticText");

  // Update expanded file name when layer driver or default file name changes
  scriptJob
    -attributeChange
      ($node + ".layerDefaultFilename")
      ("AE_RS_updateFilenameText " + $filenameLabel + " " + $node)
    -compressUndo true
    -replacePrevious
    -parent $filenameLabel;
  scriptJob
    -attributeChange
      ($node + ".layerDefaultDriver")
      ("AE_RS_updateFilenameText " + $filenameLabel + " " + $node)
    -compressUndo true
    -parent $filenameLabel;
  scriptJob
    -attributeChange
      ($node + ".layerFilenames")
      ("AE_RS_updateFilenameText " + $filenameLabel + " " + $node)
    -disregardIndex
    -compressUndo true
    -parent $filenameLabel;
  scriptJob
    -attributeChange
      ($node + ".layerDrivers")
      ("AE_RS_updateFilenameText " + $filenameLabel + " " + $node)
    -disregardIndex
    -compressUndo true
    -parent $filenameLabel;
  scriptJob
    -attributeChange
      ($node + ".layerSelected")
      ("AE_RS_updateFilenameText " + $filenameLabel + " " + $node)
    -disregardIndex
    -compressUndo true
    -parent $filenameLabel;

  AE_RS_updateFilenameText($filenameLabel, $node);

  // Lights
  int $collapse = `getAttr($node + ".layersLightsFrameCollapsed")`;
  string $layout = $prefix + "lightsFrameLayout";
  string $expand_cmd = "AE_RS_layerLightsExpandCmd " +
      $prefix + " " + $node;

  frameLayout
    -edit
    -collapse $collapse
    -collapseCommand ("AE_RS_layerLightsCollapseCmd " +
      $prefix + " " + $node)
    -expandCommand $expand_cmd
    $layout;

  // Create the Lights section gadgets only if the frame layout is already
  // expanded.
  if (!$collapse)
  {
    eval($expand_cmd);
  }

  layout -edit -visible true $parent_layout;

  setParent $prev_parent;

  setUITemplate -popTemplate;
}

global proc
AE_RS_selectAOVs(string $node)
{
  int $indices[] = RS_getLayersOrder($node);

  string $old_aovs[];
  int $old_aovs_indices[];
  int $i;
  for($i in $indices)
  {
    $aov = RS_getLayerOutputVariable($node, $i);

    $old_aovs[size($old_aovs)] = $aov;
    $old_aovs_indices[size($old_aovs_indices)] = $i;
  }

  string $aovs[] = $old_aovs;
  if(DL_aovSelector($aovs))
  {
    int $modified = false;

    string $aov;
    int $i;
    for($i = 0; $i < size($old_aovs); $i++)
    {
      string $aov = $old_aovs[$i];
      if(!stringArrayContains($aov, $aovs))
      {
        RS_removeLayer($node, $old_aovs_indices[$i]);
        $modified = true;
      }
    }

    for($aov in $aovs)
    {
      if(!stringArrayContains($aov, $old_aovs))
      {
        int $index = RS_addLayer($node);

        string $attr_value = $aov;

        string $aov_node = DOV_nodeNameField($aov);
        if($aov_node != "")
        {
          DL_connectNodeToMessagePlug(
            $aov_node,
            ($node + ".layerAOVNodes [" + $index + "]"));

          $attr_value = "<node>";
        }

        setAttr
          ($node + ".layerOutputVariables [" + $index + "]")
          -type "string"
          $attr_value;

        int $shading_aov = DOV_isShadingComponent($aov);
        setAttr(($node + ".layerMultiLight [" + $index + "]"), $shading_aov);

        $modified = true;
      }
    }

    if(size($aovs) == 0)
    {
      RS_addLayer($node);
      warning(
        "3DFM : All image layers were removed. " +
        "A default RGBA layer was added.\n");
      $modified = true;
    }

    if($modified)
    {
      RG_update($node);
    }
  }
}

global proc
AE_RS_removeSelectedLayers(string $node)
{
  int $indices[] = RS_getSelectedLayersIndices($node);
  for($i in $indices)
  {
    RS_removeLayer($node, $i);
  }

  $indices = RS_getLayersOrder($node);
  if(size($indices) == 0)
  {
    RS_addLayer($node);
    warning(
      "3DFM : All image layers were removed. " +
      "A default RGBA layer was added.\n");
  }

  RG_update($node);
}

global proc
AE_RS_viewSelectedLayers(string $node)
{
  string $camera = RS_getCamera($node);

  int $indices[] = RS_getSelectedLayersIndices($node);
  for($i in $indices)
  {
    string $driver = RS_getDriver($node, $i);
    string $name = RS_getLayerFilename($node, $i);
    string $aov = RS_getLayerOutputVariable($node, $i);
    string $aov_token = DOV_fileNameTokenField($aov);

    string $file = DL_expandLayerFilename(
      $name,
      $node,
      $aov_token,
      $driver,
      "",
      $camera,
      true,
      false);

    if(`filetest -r $file`)
    {
      DL_viewFile($file, "image");
      continue;
    }

    string $jpeg = DL_expandLayerFilename(
      $name,
      $node,
      $aov_token,
      "jpeg",
      "",
      $camera,
      true,
      true);

    if(`filetest -r $jpeg`)
    {
      DL_viewFile($jpeg, "image");
    }
  }
}

global proc
AE_RS_layerSelectedChanged(string $prefix, string $node)
{
  int $selectedLayers[] = RS_getSelectedLayersIndices($node);
  int $nbSelectedLayers = size($selectedLayers);

  button -edit -enable ($nbSelectedLayers > 0) ($prefix + "removeLayersButton");
  button -edit -enable ($nbSelectedLayers > 0) ($prefix + "viewFileButton");

  if($nbSelectedLayers == 1)
  {
    layout -edit -visible true ($prefix + "layerAttributesLayout");
    layerFilenameTextReplace($prefix, $node, $selectedLayers[0]);
  }
  else
  {
    layout -edit -visible false ($prefix + "layerAttributesLayout");
  }
}

global proc
AE_RS_updateCustomFilename(
  string $prefix,
  string $node)
{
  int $selectedLayers[] = RS_getSelectedLayersIndices($node);
  if(size($selectedLayers) != 1)
  {
    return;
  }

  string $filename =
    `getAttr ($node + ".layerFilenames[" + $selectedLayers[0] + "]")`;
  if($filename == "<default>")
  {
    optionMenuGrp -edit -value "Default" ($prefix + "filenameOptionMenu");

    string $defaultFilename =
      `getAttr ($node + ".layerDefaultFilename")`;
    textFieldGrp
      -edit
      -enable false
      -text $defaultFilename
      ($prefix + "filenameTextField");
  }
  else
  {
    optionMenuGrp -edit -value "Custom" ($prefix + "filenameOptionMenu");
    textFieldGrp
      -edit
      -enable true
      -text $filename
      ($prefix + "filenameTextField");
  }

}

global proc
AE_RS_updateFilenameText(string $text, string $node)
{
  int $selectedLayers[] = RS_getSelectedLayersIndices($node);
  if(size($selectedLayers) != 1)
  {
    return;
  }

  string $name = RS_getLayerFilename($node, $selectedLayers[0]);
  string $driver = RS_getDriver($node, $selectedLayers[0]);
  string $aov = RS_getLayerOutputVariable($node, $selectedLayers[0]);
  string $aov_token = DOV_fileNameTokenField($aov);
  string $camera = RS_getCamera($node);

  $name = DL_expandLayerFilename(
    $name,
    $node,
    $aov_token,
    $driver,
    "<light>",
    $camera,
    false,
    false);

  text -edit -label $name $text;
}

global proc
AE_RS_layerFilenameTextFieldChanged(string $field, string $node)
{
  int $selectedLayers[] = RS_getSelectedLayersIndices($node);
  if(size($selectedLayers) != 1)
  {
    return;
  }

  string $name = `textFieldGrp -q -text $field`;
  RS_setLayerFilename($node, $selectedLayers[0], $name);
}

global proc
AE_RS_layerDriverMenuChanged(string $menu, string $node)
{
  int $selected[] = RS_getSelectedLayersIndices($node);
  if(size($selected) == 0)
  {
    return;
  }

  string $attribute = $node + ".layerDrivers[" + $selected[0] + "]";
  string $name = `optionMenu -q -value $menu`;
  string $driver;
  if($name == "Default")
  {
    $driver = "";
  }
  else
  {
    int $index = DL_stringArrayFind($name, RS_getAllDriverNames());
    string $drivers[] = RS_getAllDrivers();
    $driver = $drivers[$index];
  }
  setAttr -type "string" $attribute $driver;
}


global proc
AE_RS_layerLightsCollapseCmd(
  string $prefix,
  string $node)
{
  setAttr ($node + ".layersLightsFrameCollapsed") 1;
}

global proc
AE_RS_layerLightsExpandCmd(
  string $prefix,
  string $node)
{
  setAttr ($node + ".layersLightsFrameCollapsed") 0;

  string $frame_name = ($prefix + "lightsFrameLayout");

  if (!`formLayout -q -exists ($prefix + "lightsFormLayout")`)
  {
    frameLayout -edit -visible false $frame_name;

    setUITemplate -pushTemplate "attributeEditorTemplate";

    layerLightsGadgetsCreate($prefix);

    setUITemplate -popTemplate;

    frameLayout -edit -visible true $frame_name;
  }

  layerLightsGadgetsReplace($prefix, $node);
}