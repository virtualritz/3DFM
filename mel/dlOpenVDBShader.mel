/*
  Copyright (c) soho vfx inc.
  Copyright (c) The 3Delight Team.
*/

/*
  Called by the node creation procedure in DL_hypershade.mel
  This is only called for a new node created via the Hypershade or Node editor.
*/
global proc dlOpenVDBShaderInit( string $node )
{
  string $attr = "densityRemapCurve";
  int $interp = 1; // linear
  float $pos = 1.0;

  /*
    Show the float ramps with a linear curve from 0 to 1. The 0 point is
    provided by default by the ramp attribute.
  */
  DL_setOneFloatRampPoint( $node, $attr, 1, $pos, $interp, 1.0 );

  $attr = "emissionIntensityCurve";
  DL_setOneFloatRampPoint( $node, $attr, 1, $pos, $interp, 1.0 );

  $attr = "blackbodyTemperatureCurve";
  DL_setOneFloatRampPoint( $node, $attr, 1, $pos, $interp, 1.0 );

  /*
    Initial color ramp values from the default values in vdbVolume.osl
  */
  $attr = "emissionRampColorCurve";
  $interp = 3; // spline

  /*
    Edit the default initial point interpolation mode
  */
  $pos = 0.0;
  DL_setOneColorRampPoint( $node, $attr, 0, $pos, $interp, 0.0, 0.0, 0.0 );

  $pos = 0.07;
  DL_setOneColorRampPoint( $node, $attr, 1, $pos, $interp, 1.0, 0.0, 0.0 );

  $pos = 0.1;
  DL_setOneColorRampPoint( $node, $attr, 2, $pos, $interp, 1.0, 0.0337, 0.0 );

  $pos = 0.19;
  DL_setOneColorRampPoint( $node, $attr, 3, $pos, $interp, 1.0, 0.2272, 0.0 );

  $pos = 0.65;
  DL_setOneColorRampPoint( $node, $attr, 4, $pos, $interp, 1.0, 0.9445, 0.9853 );

  $pos = 0.69;
  DL_setOneColorRampPoint( $node, $attr, 5, $pos, $interp, 0.9526, 0.9270, 1.0 );

  $pos = 1.0;
  DL_setOneColorRampPoint( $node, $attr, 6, $pos, $interp, 0.6268, 0.7039, 1.0 );
}
