#ifndef __dlCallbacksCmd_h__
#define __dlCallbacksCmd_h__

#include <maya/MPxCommand.h>

#define DLCALLBACKSCMD_STR "dlCallbacks"

class dlCallbacksCmd : public MPxCommand
{
public:
	virtual MStatus doIt( const MArgList& i_argList );

	virtual bool isUndoable() const { return false; }

	static void* creator() { return new dlCallbacksCmd; }

	static MSyntax newSyntax();
};

#endif
