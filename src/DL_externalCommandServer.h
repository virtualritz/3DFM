/*
  Copyright (c) 2014 soho vfx inc.
  Copyright (c) 2014 The 3Delight Team.
*/

#ifndef __DL_EXTERNALCOMMANDSERVER_H__
#define __DL_EXTERNALCOMMANDSERVER_H__

#include <maya/MString.h>

/*
	Handles feedback commands from 3Delight Display.
*/
class DL_externalCommandServer
{
public:
	// Stops the server and destroys its resources
	static void CleanUp();
	// Returns the server's id. Start the server if necessary.
	static MString GetServerID();
};

#endif
