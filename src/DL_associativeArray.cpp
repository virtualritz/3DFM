#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MArgList.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include <maya/MPlug.h>
#include <maya/MFnDependencyNode.h>

#include "DL_stringIntMap.h"

#include "DL_associativeArray.h"

static DL_stringIntMap&
BARFgetMaps()
{
  static DL_stringIntMap maps;

  return maps;
}


MSyntax
DL_associativeArray::newSyntax()
{
  MSyntax syntax;

  syntax.addFlag("-at", "-addTable", MSyntax::kString);
  syntax.addFlag("-dt", "-deleteTable", MSyntax::kString);
  syntax.addFlag("-ct", "-clearTable", MSyntax::kString);

  syntax.addFlag("-svi", "-setValueInt", MSyntax::kString,
                                         MSyntax::kString,
                                         MSyntax::kLong);
  syntax.addFlag("-svs", "-setValueString", MSyntax::kString,
                                            MSyntax::kString,
                                            MSyntax::kString);
  /* syntax.addFlag("-sva", "-setValueStringArray", ...) */
  syntax.addFlag("-rki", "-removeKeyInt", MSyntax::kString, MSyntax::kString);
  syntax.addFlag("-rks", "-removeKeyString", MSyntax::kString, MSyntax::kString);
  /* syntax.addFlag("-rka", "-removeKeyStringArray", ...); */
  syntax.addFlag("-gvi", "-getValueInt", MSyntax::kString, MSyntax::kString);
  syntax.addFlag("-gvs", "-getValueString", MSyntax::kString, MSyntax::kString);
  syntax.addFlag("-gva", "-getValueStringArray",
      MSyntax::kString, MSyntax::kString);

  syntax.addFlag("-imv", "-ignoreMissingValue");

  syntax.addFlag("-te", "-tableExists", MSyntax::kString);
  syntax.addFlag("-ke", "-keyExists", MSyntax::kString, MSyntax::kString);

  syntax.addFlag("-gak", "-getAllKeys", MSyntax::kString);
  syntax.addFlag("-gav", "-getAllValues", MSyntax::kString);

  return syntax;
}

MStatus
DL_associativeArray::doIt(const MArgList& args)
{
  DL_stringIntMap& maps = BARFgetMaps();
  MStatus status;
  MString flag1;
  MString array_name;
  MString key;

  /*
    The -sva / -setValueStringArray flag needs to be handled without the arg
    database as that doesn't support MStringArray arguments (as far as I know).
  */
  if( MS::kSuccess == (status = args.get(0, flag1)) )
  {
    MStringArray value;
    unsigned three = 3;
    if( (flag1 == "-sva" || flag1 == "-setValueStringArray") &&
        MS::kSuccess == (status = args.get(1, array_name)) &&
        MS::kSuccess == (status = args.get(2, key)) &&
        MS::kSuccess == (status = args.get(three, value)))
    {
      if (maps.set(array_name, key, value))
      {
        status = MS::kSuccess;
      }
      else
      {
        MString error = "table: " + array_name + " not found";
        MGlobal::displayError(error);
        status = MS::kFailure;
      }

      return status;
    }
  }

  MArgDatabase argData(syntax(), args, &status);

  if (status == MS::kSuccess)
  {
    MString             error;
    int                 int_value;
    MString             string_value;

    if (argData.isFlagSet("-at"))
    {
      argData.getFlagArgument("-at", 0, array_name);

      if (maps.addMap(array_name))
      {
        status = MS::kSuccess;
      }
      else
      {
        error = "table: " + array_name + " already exists";
        status = MS::kFailure;
      }
    }
    else if (argData.isFlagSet("-dt"))
    {
      argData.getFlagArgument("-dt", 0, array_name);

      if (maps.removeMap(array_name))
        status = MS::kSuccess;
      else
      {
        error = "table: " + array_name + " not found";
        status = MS::kFailure;
      }
    }
    else if (argData.isFlagSet("-ct"))
    {
      argData.getFlagArgument("-ct", 0, array_name);

      if (maps.clearMap(array_name))
        status = MS::kSuccess;
      else
      {
        error = "table: " + array_name + " not found";
        status = MS::kFailure;
      }
    }
    else if (argData.isFlagSet("-svi"))
    {
      argData.getFlagArgument("-svi", 0, array_name);
      argData.getFlagArgument("-svi", 1, key);
      argData.getFlagArgument("-svi", 2, int_value);

      if (maps.set(array_name, key, int_value))
      {
        status = MS::kSuccess;
      }
      else
      {
        error = "table: " + array_name + " not found";
        status = MS::kFailure;
      }
    }
    else if (argData.isFlagSet("-svs"))
    {
      argData.getFlagArgument("-svs", 0, array_name);
      argData.getFlagArgument("-svs", 1, key);
      argData.getFlagArgument("-svs", 2, string_value);

      if (maps.set(array_name, key, string_value))
      {
        status = MS::kSuccess;
      }
      else
      {
        error = "table: " + array_name + " not found";
        status = MS::kFailure;
      }
    }
    else if (argData.isFlagSet("-rki"))
    {
      argData.getFlagArgument("-rki", 0, array_name);
      argData.getFlagArgument("-rki", 1, key);

      if (maps.removeKeyInt(array_name, key))
        setResult(true);
      else
        setResult(false);
    }
    else if (argData.isFlagSet("-rks"))
    {
      argData.getFlagArgument("-rks", 0, array_name);
      argData.getFlagArgument("-rks", 1, key);

      if (maps.removeKeyString(array_name, key))
        setResult(true);
      else
        setResult(false);
    }
    else if (argData.isFlagSet("-gvi"))
    {
      argData.getFlagArgument("-gvi", 0, array_name);
      argData.getFlagArgument("-gvi", 1, key);

      if (maps.get(int_value, array_name, key))
      {
        setResult(int_value);
        status = MS::kSuccess;
      }
      else if (argData.isFlagSet("-imv"))
      {
        setResult(0);
        status = MS::kSuccess;
      }
      else
      {
        error = "could not get value for: " + key + " from table: " + array_name;
        status = MS::kFailure;
      }
    }
    else if (argData.isFlagSet("-gvs"))
    {
      argData.getFlagArgument("-gvs", 0, array_name);
      argData.getFlagArgument("-gvs", 1, key);

      if (maps.get(string_value, array_name, key))
      {
        setResult(string_value);
        status = MS::kSuccess;
      }
      else if (argData.isFlagSet("-imv"))
      {
        setResult("");
        status = MS::kSuccess;
      }
      else
      {
        error = "could not get value for: " + key + " from table: " + array_name;
        status = MS::kFailure;
      }
    }
    else if (argData.isFlagSet("-gva"))
    {
      argData.getFlagArgument("-gva", 0, array_name);
      argData.getFlagArgument("-gva", 1, key);

      MStringArray value;
      if( maps.get(value, array_name, key))
      {
        setResult(value);
        status = MS::kSuccess;
      }
      else if (argData.isFlagSet("-imv"))
      {
        value.clear();
        setResult(value);
        status = MS::kSuccess;
      }
      else
      {
        error = "could not get value for: " + key + " from table: " + array_name;
        status = MS::kFailure;
      }
    }
    else if (argData.isFlagSet("-te"))
    {
      argData.getFlagArgument("-te", 0, array_name);

      if (maps.containsMap(array_name))
        setResult(true);
      else
        setResult(false);
    }
    else if (argData.isFlagSet("-ke"))
    {
      argData.getFlagArgument("-ke", 0, array_name);
      argData.getFlagArgument("-ke", 1, key);

      if (maps.mapContains(array_name, key))
        setResult(true);
      else
        setResult(false);
    }
    else if (argData.isFlagSet("-gak"))
    {
      MStringArray  all_keys;

      argData.getFlagArgument("-gak", 0, array_name);

      if (maps.getAllKeys(all_keys, array_name))
        status = MS::kSuccess;
      else
      {
        error = "table: " + array_name + " not found";
        status = MS::kFailure;
      }

      setResult(all_keys);
    }

    if (!status)
    {
      MGlobal::displayError(error);
    }
  }

  return status;
}

/* BARFgetMaps() has improper naming and should be left like that.
 * It is the back bone of many things in 3Delight For Maya. It's necessary to
 * use a different functoin to get a global accessor.*/
DL_stringIntMap&
DL_associativeArray::getMaps()
{
  return BARFgetMaps();
}
