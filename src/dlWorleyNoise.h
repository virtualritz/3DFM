#ifndef __dlWorleyNoise_H__
#define __dlWorleyNoise_H__

#include <maya/MPxNode.h>

class dlWorleyNoise : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static MObject s_color_in_cell;
	static MObject s_color_across_cell;
	static MObject s_outColor;
};

#endif
