#ifndef __NSIExportMesh_h
#define __NSIExportMesh_h

#include "NSIExportDelegate.h"

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MFloatArray;
class MObject;
class MIntArray;
#endif

/**
	\sa NSIExportDelegate
*/
class NSIExportMesh : public NSIExportDelegate
{
	friend class NSIExportNurbsSurface;

	/**
		\brief Says in which state we where last time the SetAttributes method
		was called

		This depends on the _3delight_poly_as_subd attribute and is needed to
		delete certain attributes during an IPR session.
	*/
	enum LastState
	{
		e_unknown,
		e_polygon,
		e_subdivision
	};

public:
	NSIExportMesh(
		MFnDagNode &i_dag,
		const NSIExportDelegate::Context& i_context );
	virtual void Create();
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual bool IsDeformed( void ) const;

	/*
		\brief Provide a set object which should be inspected for attributes that
		affect how the geometry is produced.

		This is basically to support per-set "polyAsSubd" attribute.
	*/
	virtual void MemberOfSet( MObject& i_set );

protected:
	NSIExportMesh(
		MFnDagNode &,
		MObject &,
		const NSIExportDelegate::Context& i_context );

private:
	void MergeUVs(
		const MFloatArray& i_Us, const MFloatArray& i_Vs, float* o_UVs);

	bool GetUVIndices(
		int* o_uvIndices,
		const MIntArray& i_uvIDs,
		unsigned int i_numExpectedIndices,
		const MIntArray& i_uvCounts,
		const int* const i_nVertices,
		int i_nVerticesLength,
		int i_undefinedUVIndex);

	bool IsSubdivision( void ) const;

private:
	LastState m_last_state;
	MObjectHandle m_set;
	MString m_override_nsi_handle;
};
#endif
