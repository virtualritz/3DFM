/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#include "NSIExportCustom.h"

#include "NSIExportShader.h"

MString NSIUtilsImp::NSIHandle( MObject i_node )
{
	return NSIExportDelegate::NSIHandle( i_node, m_live );
}

MString NSIUtilsImp::NSIAttributesHandle( MObject i_node )
{
	return NSIExportDelegate::NSIAttributesHandle( i_node, m_live );
}

MString NSIUtilsImp::NSIShaderHandle( MObject i_node )
{
	return NSIExportShader::NSIShaderHandle( i_node, m_live );
}

NSIExportCustom::NSIExportCustom(
	const NSICustomExporter* i_exporter,
	MObject& i_object,
	const NSIExportDelegate::Context& i_context,
	void* i_data )
:
	NSIExportDelegate( i_object, i_context ),
	m_exporter( i_exporter ),
	m_delegate( 0x0 ),
	m_utils( i_context.m_live_render )
{
	assert(i_exporter);
	if( i_exporter->m_createDelegate )
	{
		MString handle = m_nsi_handle;

		m_delegate = ( NSICustomDelegate* )(m_exporter->m_createDelegate)(
			i_object,
			m_nsi,
			handle,
			m_utils,
			i_data );
	}
}

NSIExportCustom::~NSIExportCustom()
{
	if( m_delegate )
	{
		delete( m_delegate );
	}
}

void NSIExportCustom::Create()
{
	if( m_delegate )
	{
		m_delegate->Create();
	}
}

void NSIExportCustom::SetAttributes( void )
{
	if( m_delegate )
	{
		m_delegate->SetAttributes();
	}
}

void NSIExportCustom::SetAttributesAtTime(	double i_time, bool i_no_motion )
{
	if( m_delegate )
	{
		m_delegate->SetAttributesAtTime( i_time, i_no_motion );
	}
}

void NSIExportCustom::Connect( const DelegateTable* )
{
	if( m_delegate )
	{
		m_delegate->Connect( );
	}
}

void NSIExportCustom::Finalize()
{
	if( m_delegate )
	{
		m_delegate->Finalize();
	}
}

bool NSIExportCustom::IsDeformed() const
{
	if( m_delegate )
	{
		return m_delegate->IsDeformed();
	}

	return false;
}

bool NSIExportCustom::RegisterCallbacks()
{
	if( m_delegate )
	{
		return m_delegate->RegisterCallbacks();
	}

	return false;
}

void NSIExportCustom::MemberOfSet( MObject& i_set )
{
	if( m_delegate && m_exporter->m_version >= 2)
	{
		m_delegate->OverrideAttributes(i_set);
	}
}
