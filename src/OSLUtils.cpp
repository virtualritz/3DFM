#include "OSLUtils.h"

#include <maya/MString.h>
#include <string.h> // strtok

#include "DL_utils.h"

namespace OSLUtils
{

void tokenize_path( char *osl_path, MStringArray &o_paths )
{
	if( !osl_path )
		return;

#ifdef _WIN32
	const char *delimters = ";";
#else
	const char *delimters = ":";
#endif

	/*
		::strtok will modify the passed string so allocate a
		a copy.
	*/
	osl_path = ::strdup( osl_path );

	char *token = ::strtok( osl_path, delimters );

	do
	{
		o_paths.append( token );
		token = ::strtok(nullptr, delimters);
	} while( token );

	free( osl_path );
}


MStringArray GetBuiltInSearchPaths()
{
	MStringArray shaderPaths;
	char *osl_path = getenv("_3DFM_OSL_PATH");
	tokenize_path( osl_path, shaderPaths );

	shaderPaths.append(getDelightPath() + MString("/maya/osl"));
	shaderPaths.append(getDelightPath() + MString("/osl"));

	return shaderPaths;
}

MStringArray GetUserSearchPaths()
{
	MStringArray paths;
	char* osl_path = getenv("_3DFM_USER_OSL_PATH");
	if( !osl_path )
		osl_path = getenv( "_3DELIGHT_USER_OSO_PATH" );

	tokenize_path( osl_path, paths );

	char* delight_env_var = getenv("DELIGHT");
	if( delight_env_var )
	{
		MString default_path(delight_env_var);
		default_path += MString("/maya/customShadingNodes");
		paths.append( default_path );
	}

	return paths;
}

MString FindBuiltinShader( const MString& i_shaderName )
{
	DlShaderInfo *info;
	MStringArray paths = GetBuiltInSearchPaths();
	MString shader = OpenShader( i_shaderName, paths, info );

	if( shader == "" )
		return "";

	return shader + "/" + i_shaderName;
}

MString OpenShader(
	const MString& i_shaderName,
	const MStringArray& i_paths,
	DlShaderInfo *&o_info)
{
	for( unsigned int i = 0; i < i_paths.length(); i++ )
	{
		MString shaderPath = getFixedPath(i_paths[i]);

		if( shaderPath.length() == 0 )
			continue;

		MString fullPath = shaderPath + "/" + i_shaderName + ".oso";
		o_info = DlGetShaderInfo(fullPath.asChar());

		if( o_info )
			return shaderPath;
	}

	return MString("");
}

MString OpenShader( const MString& i_shaderName, DlShaderInfo *&o_info )
{
	MStringArray searchPaths = GetBuiltInSearchPaths();
	MStringArray userPaths = GetUserSearchPaths();

	for( unsigned i = 0; i < userPaths.length(); i++ )
	{
		searchPaths.append( userPaths[i] );
	}

	return OpenShader( i_shaderName, searchPaths, o_info );
}

MString GetFullpathname(const MString& i_shaderName)
{
	DlShaderInfo *info;
	MString searchPath = OpenShader(i_shaderName, info);
	if( !info )
	{
		return "";
	}
	return searchPath + "/" + info->shadername().c_str() + ".oso";
}

MString GetShaderNiceName( DlShaderInfo *i_shaderInfo )
{
	MString niceName;

	for( const auto &meta : i_shaderInfo->metadata() )
	{
		if( meta.name == "niceName" && meta.sdefault.size() > 0 )
		{
			niceName = meta.sdefault[ 0 ].c_str();
			break;
		}
	}

	return niceName;
}

}
