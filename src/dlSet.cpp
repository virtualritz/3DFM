#include "dlSet.h"

#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>

#include "DL_utils.h"
#include "MU_typeIds.h"


MTypeId dlSet::m_id( MU_typeIds::DL_SET );

MObject dlSet::m_enablePolyAsSubd;
MObject dlSet::m_polyAsSubd;
MObject dlSet::m_enableMatte;
MObject dlSet::m_matte;
MObject dlSet::m_enablePrelit;
MObject dlSet::m_prelit;
MObject dlSet::m_enableCompositingMode;
MObject dlSet::m_compositingMode;
MObject dlSet::m_enableExtraTransformSamples;
MObject dlSet::m_extraTransformSamples;
MObject dlSet::m_enableExtraDeformSamples;
MObject dlSet::m_extraDeformSamples;
MObject dlSet::m_enableSmoothCurves;
MObject dlSet::m_smoothCurves;

MObject dlSet::m_enableCameraVisibility;
MObject dlSet::m_enableDiffuseVisibility;
MObject dlSet::m_enableReflectionVisibility;
MObject dlSet::m_enableRefractionVisibility;
MObject dlSet::m_enableShadowVisibility;

MObject dlSet::m_cameraVisibility;
MObject dlSet::m_diffuseVisibility;
MObject dlSet::m_reflectionVisibility;
MObject dlSet::m_refractionVisibility;
MObject dlSet::m_shadowVisibility;

void
dlSet::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

void*
dlSet::creator()
{
	return new dlSet;
}

MStatus
dlSet::initialize()
{
	MFnNumericAttribute numAttrFn;

	// enablePolyAsSubd attr
	m_enablePolyAsSubd = numAttrFn.create(
		"enablePolyAsSubd", 
		"esubd",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( 
		MString( "Enable Render Mesh as a Subdivision Surface" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enablePolyAsSubd );

	// polyAsSubd attr
	m_polyAsSubd = numAttrFn.create(
		"polyAsSubd", 
		"subd",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( 
		MString( "Render Mesh as a Subdivision Surface" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_polyAsSubd );

	// (obsolete) enableMatte attr
	m_enableMatte = numAttrFn.create(
		"enableMatte", 
		"ematte",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( MString( "Enable Object is Matte" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableMatte );

	// (obsolete) matte attr
	m_matte = numAttrFn.create(
		"matte", 
		"matte",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( MString( "Object is Matte" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_matte );

	// (obsolete) enablePrelit attr
	m_enablePrelit = numAttrFn.create(
		"enablePrelit", 
		"eprelit",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( MString( "Enable Prelit" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enablePrelit );

	// (obsolete) prelit attr
	m_prelit = numAttrFn.create(
		"prelit", 
		"prelit",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( MString( "Prelit" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_prelit );

	// enableCompositingMode attr
	m_enableCompositingMode = numAttrFn.create(
		"enableCompositingMode", 
		"ecomp",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( MString( "Enable Compositing" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableCompositingMode );

	// compositingMode attr
	MFnEnumAttribute enumAttrFn;
	m_compositingMode = enumAttrFn.create(
		"compositingMode", 
		"comp",
		0 );

	enumAttrFn.setNiceNameOverride( MString( "Compositing" ) );

	enumAttrFn.addField( "Regular", 0 );
	enumAttrFn.addField( "Matte", 1 );
	enumAttrFn.addField( "Prelit", 2 );

	Utilities::makeInputOutputAttribute( enumAttrFn );
	addAttribute( m_compositingMode );

	// enableExtraTransformSamples attr
	m_enableExtraTransformSamples = numAttrFn.create(
		"enableExtraTransformSamples", 
		"extrs",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( 
		MString( "Enable Additional Transformation Samples" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableExtraTransformSamples );

	// extraTransformSamples attr
	m_extraTransformSamples = numAttrFn.create(
		"extraTransformSamples", 
		"xtrs",
		MFnNumericData::kLong, 
		0 );

	numAttrFn.setNiceNameOverride( 
		MString( "Additional Transformation Samples" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_extraTransformSamples );

	// enableExtraDeformSamples attr
	m_enableExtraDeformSamples = numAttrFn.create(
		"enableExtraDeformSamples", 
		"exdfs",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( 
		MString( "Enable Additional Deformation Samples" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableExtraDeformSamples );

	// extraDeformSamples attr
	m_extraDeformSamples = numAttrFn.create(
		"extraDeformSamples", 
		"xdfs",
		MFnNumericData::kLong, 
		0 );

	numAttrFn.setNiceNameOverride( 
		MString( "Additional Deformation Samples" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_extraDeformSamples );
	
	// enableSmoothCurves attr
	m_enableSmoothCurves = numAttrFn.create(
		"enableSmoothCurves", 
		"ecurvy",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( MString( "Enable Smooth Curves" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableSmoothCurves );

	// smoothCurves attr
	m_smoothCurves = numAttrFn.create(
		"smoothCurves", 
		"curvy",
		MFnNumericData::kBoolean, 
		false );

	numAttrFn.setNiceNameOverride( MString( "Smooth Curves" ) );

	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_smoothCurves );

	// Visible to diffuse
	m_enableDiffuseVisibility = numAttrFn.create(
		"enableDiffuseVisibility", "ediffuse", MFnNumericData::kBoolean, false);
	numAttrFn.setNiceNameOverride( MString( "Visible to Diffuse" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableDiffuseVisibility );

	m_diffuseVisibility = numAttrFn.create(
		"diffuseVisibility", "vdiffuse", MFnNumericData::kBoolean, true);
	numAttrFn.setNiceNameOverride( MString( "Visible to Diffuse" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_diffuseVisibility );

	// Visible to camera
	m_enableCameraVisibility = numAttrFn.create(
		"enableCameraVisibility", "ecamera", MFnNumericData::kBoolean, false);
	numAttrFn.setNiceNameOverride( MString( "Visible to Camera" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableCameraVisibility );

	m_cameraVisibility = numAttrFn.create(
		"cameraVisibility", "vcamera", MFnNumericData::kBoolean, true);
	numAttrFn.setNiceNameOverride( MString( "Visible to Camera" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_cameraVisibility );

	// Visible to reflection
	m_enableReflectionVisibility = numAttrFn.create(
		"enableReflectionVisibility", "ereflection", MFnNumericData::kBoolean, false);
	numAttrFn.setNiceNameOverride( MString( "Visible to Reflections" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableReflectionVisibility );

	m_reflectionVisibility = numAttrFn.create(
		"reflectionVisibility", "vreflection", MFnNumericData::kBoolean, true);
	numAttrFn.setNiceNameOverride( MString( "Visible to Reflections" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_reflectionVisibility );

	// Visible to refraction
	m_enableRefractionVisibility = numAttrFn.create(
		"enableRefractionVisibility", "erefraction", MFnNumericData::kBoolean, false);
	numAttrFn.setNiceNameOverride( MString( "Visible to Refraction" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableRefractionVisibility );

	m_refractionVisibility = numAttrFn.create(
		"refractionVisibility", "vrefraction", MFnNumericData::kBoolean, true);
	numAttrFn.setNiceNameOverride( MString( "Visible to Refraction" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_refractionVisibility );

	// Visible to shadows
	m_enableShadowVisibility = numAttrFn.create(
		"enableShadowVisibility", "eshadow", MFnNumericData::kBoolean, false);
	numAttrFn.setNiceNameOverride( MString( "Casts Shadows" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_enableShadowVisibility );

	m_shadowVisibility = numAttrFn.create(
		"shadowVisibility", "vshadow", MFnNumericData::kBoolean, true);
	numAttrFn.setNiceNameOverride( MString( "Casts Shadows" ) );
	Utilities::makeInputOutputAttribute( numAttrFn );
	addAttribute( m_shadowVisibility );

	// Ideally we would set MPxObjectSet::renderableOnlySet attribute to true by
	// default here, but I have not been able to achieve this yet.
	//
	return MS::kSuccess;
}

MStatus
dlSet::compute( const MPlug&, MDataBlock& )
{
	return MS::kUnknownParameter;
}
