#include "dlRenderGlobals.h"

#include <maya/MFnMessageAttribute.h>

#include "MU_typeIds.h"

MTypeId dlRenderGlobals::id( MU_typeIds::DL_RENDERGLOBALS );

MObject dlRenderGlobals::m_renderSettings;

void
dlRenderGlobals::postConstructor()
{
  setExistWithoutInConnections(true);
  setExistWithoutOutConnections(true);
}

void*
dlRenderGlobals::creator()
{
  return new dlRenderGlobals();
}

MStatus
dlRenderGlobals::initialize()
{
	MFnMessageAttribute msgAttrFn;
	m_renderSettings = msgAttrFn.create( "renderSettings", "renderSettings" );
	addAttribute( m_renderSettings );

  return MS::kSuccess;
}

MStatus
dlRenderGlobals::compute( const MPlug& /*out_plug*/, MDataBlock& /*data*/)
{
  return MS::kUnknownParameter;
}

