#include "dlSolidRamp.h"

#include <maya/MFloatVector.h>
#include <maya/MObjectArray.h>

#include "DL_autoLoadOSL.h"
#include "DL_utils.h"
#include "OSLUtils.h"

#include <cassert>

MObject dlSolidRamp::s_outColor;

void* dlSolidRamp::creator()
{
	return new dlSolidRamp();
}

MStatus dlSolidRamp::initialize()
{
	MStringArray shaderPaths = OSLUtils::GetBuiltInSearchPaths();
	MString shaderName( "dlSolidRamp" );
	DlShaderInfo *info;

	OSLUtils::OpenShader( shaderName, shaderPaths, info );

	MObjectArray objects;
	MStringArray objectNames;

	DL_OSLShadingNode::CreateAttributesFromShaderParameters(
		info, 0x0, &objects, &objectNames );

	if( !DL_OSLShadingNode::FindAttribute(
			"outColor", s_outColor, objects, objectNames) )
	{
		assert(false);
		// It makes no sense to return success here, but if we don't, Maya crashes.
		return MStatus::kSuccess;
	}

	for( unsigned i = 0; i < objects.length(); i++ )
	{
		addAttribute( objects[ i ] );
	}

	MString name = info->shadername().c_str();
	MString niceName = OSLUtils::GetShaderNiceName( info );
	DL_OSLShadingNode::DefineShaderNiceName( name, niceName );

	return MStatus::kSuccess;
}

void dlSolidRamp::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlSolidRamp::compute(
	const MPlug& i_plug,
	MDataBlock& i_block )
{
	if ((i_plug != s_outColor) && (i_plug.parent() != s_outColor))
	{
		return MS::kUnknownParameter;
	}
	return MS::kSuccess;
}
