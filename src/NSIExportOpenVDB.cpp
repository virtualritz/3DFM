#include "NSIExportOpenVDB.h"

#include <maya/MPlug.h>
#include <maya/MFnDependencyNode.h>

NSIExportOpenVDB::NSIExportOpenVDB(
	MObject i_object,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_object, i_context )
{
}

NSIExportOpenVDB::~NSIExportOpenVDB()
{
}

void NSIExportOpenVDB::Create()
{
	NSI::Context nsi( m_nsi );
	nsi.Create( m_nsi_handle, "volume" );
}

void NSIExportOpenVDB::SetAttributes()
{
	MFnDependencyNode depFn( Object() );

	NSI::Context nsi( m_nsi );
	NSI::ArgumentList argList;

	MPlug plug = depFn.findPlug( "outFilename", true );
	MString stringValue;
	plug.getValue( stringValue );
	argList.Add( new NSI::CStringPArg( "vdbfilename", stringValue.asChar() ) );

	plug = depFn.findPlug( "smokeGrid", true );
	plug.getValue( stringValue );
	argList.Add( new NSI::CStringPArg( "densitygrid", stringValue.asChar() ) );

	plug = depFn.findPlug( "smokeColorGrid", true );
	plug.getValue( stringValue );
	argList.Add( new NSI::CStringPArg( "colorgrid", stringValue.asChar() ) );

	plug = depFn.findPlug( "temperatureGrid", true );
	plug.getValue( stringValue );
	argList.Add(
		new NSI::CStringPArg( "temperaturegrid", stringValue.asChar() ) );

	plug = depFn.findPlug( "emissionIntensityGrid", true );
	plug.getValue( stringValue );
	argList.Add(
		new NSI::CStringPArg( "emissionintensitygrid", stringValue.asChar() ) );

	plug = depFn.findPlug( "emissionTintGrid", true );
	plug.getValue( stringValue );
	argList.Add( new NSI::CStringPArg( "emissiongrid", stringValue.asChar() ) );

	plug = depFn.findPlug( "velocityGrid", true );
	plug.getValue( stringValue );
	argList.Add( new NSI::CStringPArg( "velocitygrid", stringValue.asChar() ) );

	plug = depFn.findPlug( "velocityScale", true );
	double doubleValue = 0.0;
	plug.getValue( doubleValue );
	argList.Add( new NSI::DoubleArg( "velocityscale", doubleValue ) );

	nsi.SetAttribute( m_nsi_handle, argList );
}

void NSIExportOpenVDB::SetAttributesAtTime(	double, bool )
{
}

bool NSIExportOpenVDB::has_vdb_light_layer(MObject i_object)
{
	MFnDependencyNode depFn(i_object);

	MPlug plug;
	MString tempValue;
	MString heatValue;
	MString emissionValue;

	plug = depFn.findPlug("temperatureGrid", true);
	plug.getValue(tempValue);

	plug = depFn.findPlug("emissionIntensityGrid", true);
	plug.getValue(heatValue);

	plug = depFn.findPlug("emissionTintGrid", true);
	plug.getValue(emissionValue);

	if (tempValue == "" && heatValue == "" && emissionValue == "")
	{
		return false;
	}
	return true;
}

