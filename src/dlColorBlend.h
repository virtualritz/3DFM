#ifndef __dlColorBlend_H__
#define __dlColorBlend_H__

#include <maya/MPxNode.h>

class dlColorBlend : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static MObject m_blendMode;
	static MObject m_fg;
	static MObject m_bg;
	static MObject m_blend;

  static MObject m_outColor;
};

#endif
