#include "dlToonGlass.h"

#include <maya/MFloatVector.h>
#include <maya/MObjectArray.h>

#include "DL_autoLoadOSL.h"
#include "DL_utils.h"
#include "OSLUtils.h"

#include <cassert>

void* dlToonGlass::creator()
{
	return new dlToonGlass();
}

MStatus dlToonGlass::initialize()
{
	MStringArray shaderPaths = OSLUtils::GetBuiltInSearchPaths();
	MString shaderName( "dlToonGlass" );
	DlShaderInfo *info;

	OSLUtils::OpenShader( shaderName, shaderPaths, info );

	MObjectArray objects;
	MStringArray objectNames;

	DL_OSLShadingNode::CreateAttributesFromShaderParameters(
		info, 0x0, &objects, &objectNames );

	for( unsigned i = 0; i < objects.length(); i++ )
	{
		addAttribute( objects[ i ] ); 
	}

	MString name = info->shadername().c_str();
	MString niceName = OSLUtils::GetShaderNiceName( info );
	DL_OSLShadingNode::DefineShaderNiceName( name, niceName );

	return MStatus::kSuccess;
}

void dlToonGlass::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}
