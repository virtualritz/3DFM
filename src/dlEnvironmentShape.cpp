/*
	Copyright (c) soho vfx inc.
	Copyright (c) The 3Delight Team.
*/

/*
	Quiet warnings about the old viewport code in here. To be updated
	eventually but not today.
*/
#define _OPENMAYA_DEPRECATION_DISABLE_WARNING

#include "dlEnvironmentShape.h"

#include "DL_utils.h"

#include "MU_typeIds.h"

#include <maya/MColor.h>
#include <maya/MDagPath.h>
#include <maya/MDrawContext.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFrameContext.h>
#include <maya/MPlugArray.h>
#include <maya/MPointArray.h>
#include <maya/MUintArray.h>

#include <cassert>
#include <cmath>

MTypeId dlEnvironmentShape::id( MU_typeIds::DL_ENVIRONMENTNODE );

MString dlEnvironmentShape::drawDbClassification("drawdb/geometry/dlEnvironmentShape");
MString dlEnvironmentShape::drawRegistrantId("dlEnvironmentShapePlugin");

MObject dlEnvironmentShape::m_mappingObj;
MObject dlEnvironmentShape::m_textureObj;
MObject dlEnvironmentShape::m_radiusObj;
MObject dlEnvironmentShape::m_intensityObj;
MObject dlEnvironmentShape::m_exposureObj;
MObject dlEnvironmentShape::m_tintObj;
MObject dlEnvironmentShape::m_useBackgroundTextureObj;
MObject dlEnvironmentShape::m_backgroundTextureObj;
MObject dlEnvironmentShape::m_visibleToCameraObj;
MObject dlEnvironmentShape::m_prelitObj;
MObject dlEnvironmentShape::m_diffuseContrObj;
MObject dlEnvironmentShape::m_specularContrObj;
MObject dlEnvironmentShape::m_hairContrObj;
MObject dlEnvironmentShape::m_volumeContrObj;

// SHAPE NODE IMPLEMENTATION

dlEnvironmentShape::dlEnvironmentShape() :
  m_textureVersion(1),
  m_radius( 1000.0 )
{
}

dlEnvironmentShape::~dlEnvironmentShape()
{
}

void dlEnvironmentShape::postConstructor()
{
	MFnDependencyNode depFn(thisMObject());
	depFn.setName("dlEnvironmentShape#");
}

MStatus dlEnvironmentShape::compute(
	const MPlug& /*plug*/,
	MDataBlock& /*datablock*/ )
{
	return MS::kUnknownParameter;
}

bool dlEnvironmentShape::setInternalValue(
	const MPlug& plug,
	const MDataHandle& datahandle )
{
	if( plug == m_textureObj )
	{
		m_textureVersion++;
	}
	else if( plug == m_radiusObj )
	{
		m_radius = datahandle.asFloat();
	}

	/* Use default storage too as we don't implement getInternalValue(). */
	return MPxSurfaceShape::setInternalValue( plug, datahandle );
}

bool dlEnvironmentShape::isBounded() const
{
	return true;
}

MBoundingBox dlEnvironmentShape::boundingBox() const
{
	MBoundingBox result;

	result.expand( MPoint( m_radius, m_radius, m_radius) );
	result.expand( MPoint(-m_radius,-m_radius,-m_radius) );

	return result;
}

void* dlEnvironmentShape::creator()
{
	return new dlEnvironmentShape();
}

MStatus dlEnvironmentShape::initialize()
{
	MStatus stat;
	MFnEnumAttribute enumAttr;
	MFnNumericAttribute numericAttr;

	m_mappingObj = enumAttr.create( "mapping", "st", 0, &stat );
	if ( stat != MS::kSuccess )
	{
		stat.perror(
			"dlEnvironmentShape::initialize: create shapeType attribute");
		return stat;
	}
	enumAttr.addField( "Spherical", 0 );
	enumAttr.addField( "Angular", 1 );
	enumAttr.setHidden( false );
	enumAttr.setKeyable( true );
	enumAttr.setInternal( true );
	stat = addAttribute( m_mappingObj );
	if ( stat != MS::kSuccess )
	{
		stat.perror(
			"dlEnvironmentShape::initialize: adding shapeType attribute");
		return stat;
	}

	m_textureObj = numericAttr.createColor( "texture", "ip");
	numericAttr.setHidden( false );
	numericAttr.setKeyable(true);
	numericAttr.setInternal( true );
	numericAttr.setDefault(0.5f, 0.5f, 0.5f);
	stat = addAttribute( m_textureObj );
	if ( stat != MS::kSuccess )
	{
		stat.perror(
			"dlEnvironmentShape::initialize: adding texture attribute");
		return stat;
	}

	m_radiusObj =
		numericAttr.create( "radius", "r", MFnNumericData::kFloat, 1000.0, &stat );

	if ( stat != MS::kSuccess )
	{
		stat.perror(
			"dlEnvironmentShape::initialize: create radius attribute");
		return stat;
	}

	numericAttr.setHidden( false );
	numericAttr.setKeyable( true );
	numericAttr.setInternal( true );
	numericAttr.setDefault( 1000.0 );
	numericAttr.setMin( 0.0 );
	numericAttr.setSoftMin( 0.0 );
	numericAttr.setSoftMax( 10000.0 );
	stat = addAttribute( m_radiusObj );
	if ( stat != MS::kSuccess )
	{
		stat.perror(
				"dlEnvironmentShape::initialize: adding radius attribute");
		return stat;
	}

	m_intensityObj = numericAttr.create(
			"intensity", "int", MFnNumericData::kFloat, 1.0);
	numericAttr.setHidden( false );
	numericAttr.setKeyable( true );
	numericAttr.setInternal( true );
	numericAttr.setMin( 0.0 );
	numericAttr.setSoftMax( 1.0 );
	stat = addAttribute( m_intensityObj );
	if ( stat != MS::kSuccess )
	{
		stat.perror(
				"dlEnvironmentShape::initialize: adding intensity attribute");
		return stat;
	}

	m_exposureObj = numericAttr.create(
		"_3delight_light_exposure", "e", MFnNumericData::kFloat, 0.0);
	numericAttr.setNiceNameOverride("Exposure");
	numericAttr.setHidden( false );
	numericAttr.setKeyable( true );
	numericAttr.setInternal( true );
	numericAttr.setSoftMin( -5.f );
	numericAttr.setSoftMax( 10.0f );
	stat = addAttribute( m_exposureObj );
	if ( stat != MS::kSuccess )
	{
		stat.perror(
				"dlEnvironmentShape::initialize: adding exposure attribute");
		return stat;
	}

	m_tintObj = numericAttr.createColor( "tint", "tint");
	numericAttr.setHidden( false );
	numericAttr.setKeyable( true );
	numericAttr.setInternal( true );
	numericAttr.setDefault( 1.0f, 1.0f, 1.0f );
	stat = addAttribute( m_tintObj );
	if ( stat != MS::kSuccess )
	{
		stat.perror(
				"dlEnvironmentShape::initialize: adding tint attribute");
		return stat;
	}

	m_useBackgroundTextureObj = numericAttr.create(
		"use_background_texture", "use_background_texture",
		MFnNumericData::kBoolean, 0 );
	numericAttr.setNiceNameOverride("Use Background Texture");
	numericAttr.setDefault( false );
	stat = addAttribute( m_useBackgroundTextureObj );

	if ( stat != MS::kSuccess )
	{
		stat.perror(
			"dlEnvironmentShape::initialize: adding use bg texture attribute");
		return stat;
	}

	m_backgroundTextureObj = numericAttr.createColor(
		"background_texture", "background_texture");
	numericAttr.setNiceNameOverride("Background Texture");
	numericAttr.setKeyable( true );
	numericAttr.setDefault(0.5f, 0.5f, 0.5f);
	stat = addAttribute( m_backgroundTextureObj );
	if ( stat != MS::kSuccess )
	{
		stat.perror(
			"dlEnvironmentShape::initialize: adding bg texture attribute");
		return stat;
	}

	/* Visible to Camera */
	{
		MString attrName = MString("_3delight_arealight_visibility");
		m_visibleToCameraObj = numericAttr.create(
			attrName, attrName, MFnNumericData::kBoolean, 0 );

		numericAttr.setNiceNameOverride("Visible to Camera");
		numericAttr.setDefault(false);
		numericAttr.setHidden( false );
		numericAttr.setKeyable( true );
		numericAttr.setInternal( true );
		stat = addAttribute( m_visibleToCameraObj );

		if ( stat != MS::kSuccess )
		{
			stat.perror(
				"dlEnvironmentShape::initialize: adding visibility attribute");
			return stat;
		}
	}

	// Prelit
	{
		MString attrName = MString("_3delight_prelit");
		m_prelitObj = numericAttr.create(
			attrName, attrName, MFnNumericData::kBoolean, 0 );

		numericAttr.setNiceNameOverride("Prelit");
		numericAttr.setDefault( false );
		numericAttr.setHidden( false );
		numericAttr.setKeyable( false );
		numericAttr.setInternal( true );
		stat = addAttribute( m_prelitObj );

		if ( stat != MS::kSuccess )
		{
			stat.perror(
				"dlEnvironmentShape::initialize: adding prelit attribute");
			return stat;
		}
	}

	/*Contributions */

	//Diffuse
	{
		MString attrName = MString("diffuse_contribution");
		m_diffuseContrObj = numericAttr.create(
			attrName, attrName, MFnNumericData::kFloat, 1.0);
		numericAttr.setNiceNameOverride("Diffuse");
		numericAttr.setHidden(false);
		numericAttr.setKeyable(true);
		numericAttr.setInternal(true);
		numericAttr.setMin(0.0);
		numericAttr.setSoftMax(10.0);
		stat = addAttribute(m_diffuseContrObj);
		if (stat != MS::kSuccess)
		{
			stat.perror(
				"dlEnvironmentShape::initialize: adding diffuse contribution attribute");
			return stat;
		}
	}

	//Reflection
	{
		MString attrName = MString("specular_contribution");
		m_specularContrObj = numericAttr.create(
			attrName, attrName, MFnNumericData::kFloat, 1.0);
		numericAttr.setNiceNameOverride("Specular");
		numericAttr.setHidden(false);
		numericAttr.setKeyable(true);
		numericAttr.setInternal(true);
		numericAttr.setMin(0.0);
		numericAttr.setSoftMax(10.0);
		stat = addAttribute(m_specularContrObj);
		if (stat != MS::kSuccess)
		{
			stat.perror(
				"dlEnvironmentShape::initialize: adding reflection contribution attribute");
			return stat;
		}
	}

	//Hair
	{
		MString attrName = MString("hair_contribution");
		m_hairContrObj = numericAttr.create(
			attrName, attrName, MFnNumericData::kFloat, 1.0);
		numericAttr.setNiceNameOverride("Hair");
		numericAttr.setHidden(false);
		numericAttr.setKeyable(true);
		numericAttr.setInternal(true);
		numericAttr.setMin(0.0);
		numericAttr.setSoftMax(10.0);
		stat = addAttribute(m_hairContrObj);
		if (stat != MS::kSuccess)
		{
			stat.perror(
				"dlEnvironmentShape::initialize: adding hair contribution attribute");
			return stat;
		}
	}

	//Volume
	{
		MString attrName = MString("volume_contribution");
		m_volumeContrObj = numericAttr.create(
			attrName, attrName, MFnNumericData::kFloat, 1.0);
		numericAttr.setNiceNameOverride("Volume");
		numericAttr.setHidden(false);
		numericAttr.setKeyable(true);
		numericAttr.setInternal(true);
		numericAttr.setMin(0.0);
		numericAttr.setSoftMax(10.0);
		stat = addAttribute(m_volumeContrObj);
		if (stat != MS::kSuccess)
		{
			stat.perror(
				"dlEnvironmentShape::initialize: adding volume contribution attribute");
			return stat;
		}
	}


	return MS::kSuccess;
}


class dlEnvironmentShapeDrawOverrideUserData : public MUserData
{
public:
    dlEnvironmentShapeDrawOverrideUserData(unsigned* i_shape_texture_version) :
      MUserData(false),
      m_shape_texture_version(i_shape_texture_version),
      m_scale(1.0f, 1.0f, 1.0f, 1.0f),
      m_texture_version(0),
      m_radius(1.0f),
      m_mapping(0)
    {
    }

    virtual ~dlEnvironmentShapeDrawOverrideUserData()
    {
		MRenderer *renderer = MRenderer::theRenderer();
		MTextureManager *textureManager = renderer
			? renderer->getTextureManager() : nullptr;
		if( textureManager && m_texture )
		{
			textureManager->releaseTexture(m_texture);
			m_texture = nullptr;
		}
    }

    unsigned* m_shape_texture_version;

    MColor m_scale;
    unsigned m_texture_version;
	MTexture *m_texture{nullptr};
	bool m_use_texture{false};

    float m_radius;

    int m_mapping;
};

MHWRender::MPxDrawOverride* dlEnvironmentShapeDrawOverride::creator(
	const MObject& i_obj)
{
	return new dlEnvironmentShapeDrawOverride(i_obj);
}

dlEnvironmentShapeDrawOverride::~dlEnvironmentShapeDrawOverride()
{
}

MHWRender::DrawAPI dlEnvironmentShapeDrawOverride::supportedDrawAPIs() const
{
	return MHWRender::kAllDevices;
}

MMatrix dlEnvironmentShapeDrawOverride::transform(const MDagPath &i_objPath,
  const MDagPath &i_cameraPath) const
{
	MMatrix inclusiveMatrix(i_objPath.inclusiveMatrix());
	return inclusiveMatrix;
}

MBoundingBox dlEnvironmentShapeDrawOverride::boundingBox(
	const MDagPath &i_objPath,
	const MDagPath &i_cameraPath ) const
{
	MFnDagNode fnDag(i_objPath);
	MPlug radiusPlug = fnDag.findPlug("radius", true);
	const float radius = radiusPlug.asFloat();

	MBoundingBox result;
	result.expand( MPoint( radius, radius, radius) );
	result.expand( MPoint(-radius,-radius,-radius) );
	return result;
}

bool dlEnvironmentShapeDrawOverride::isBounded(const MDagPath &i_objPath,
  const MDagPath &i_cameraPath) const
{
	return true;
}

MUserData* dlEnvironmentShapeDrawOverride::prepareForDraw(
	const MDagPath &i_objPath,
	const MDagPath &i_cameraPath,
	const MHWRender::MFrameContext &i_frameContext,
	MUserData *i_oldData )
{
	MFnDagNode fnDag(i_objPath);

	dlEnvironmentShape *shape =
		dynamic_cast<dlEnvironmentShape *>(fnDag.userNode());

	dlEnvironmentShapeDrawOverrideUserData *user_data =
		static_cast<dlEnvironmentShapeDrawOverrideUserData *>(i_oldData);
	assert(
		!user_data ||
		user_data->m_shape_texture_version == &shape->m_textureVersion);
	if (! user_data)
	{
		user_data =
			new dlEnvironmentShapeDrawOverrideUserData(&shape->m_textureVersion);
	}

	/* Generate texture data */
	MPlug texturePlug = fnDag.findPlug("texture", true);
	MPlugArray texturePlugs;
	texturePlug.connectedTo(texturePlugs, true, false);
	user_data->m_use_texture = texturePlugs.length() != 0;
	if(user_data->m_use_texture)
	{
		if(user_data->m_texture_version != *user_data->m_shape_texture_version)
		{
			MRenderer *renderer = MRenderer::theRenderer();
			MTextureManager *textureManager = renderer
				? renderer->getTextureManager() : nullptr;
			if( textureManager )
			{
				if( user_data->m_texture )
				{
					textureManager->releaseTexture(user_data->m_texture);
				}
				user_data->m_texture = textureManager->acquireTexture(
					"", texturePlugs[0], 512, 512, true);
			}

			user_data->m_texture_version = *user_data->m_shape_texture_version;
		}
		user_data->m_scale = MColor(1.0f, 1.0f, 1.0f, 1.0f);
	}
	else
	{
		user_data->m_scale =
			MColor(
				texturePlug.child(0).asFloat(),
				texturePlug.child(1).asFloat(),
				texturePlug.child(2).asFloat(),
				1.0f);
	}

	/* "tint" */
	MPlug tintPlug = fnDag.findPlug("tint", true);
	if(!tintPlug.isNull())
	{
		user_data->m_scale *=
			MColor(
				tintPlug.child(0).asFloat(),
				tintPlug.child(1).asFloat(),
				tintPlug.child(2).asFloat(),
				1.0f);
	}

	/* "intensity" */
	MPlug intensityPlug = fnDag.findPlug("intensity", true);
	if(!intensityPlug.isNull())
	{
		user_data->m_scale *= intensityPlug.asFloat();
	}

	/* "exposure" */
	MPlug exposurePlug = fnDag.findPlug("_3delight_light_exposure", true);
	if(!exposurePlug.isNull())
	{
		user_data->m_scale *= ::exp2(exposurePlug.asFloat());
	}

	/* "radius" */
	MPlug radiusPlug = fnDag.findPlug("radius", true);
	user_data->m_radius = radiusPlug.asFloat();

	/* "mapping" */
	MPlug mappingPlug = fnDag.findPlug("mapping", true);
	user_data->m_mapping = mappingPlug.asInt();

	return user_data;
}

bool dlEnvironmentShapeDrawOverride::hasUIDrawables() const
{
	return true;
}

void dlEnvironmentShapeDrawOverride::addUIDrawables (
	const MDagPath &objPath,
	MUIDrawManager &drawManager,
	const MFrameContext &frameContext,
	const MUserData *data)
{
	auto &user_data =
		*static_cast<const dlEnvironmentShapeDrawOverrideUserData*>(data);

	drawManager.beginDrawable();

	drawManager.setPaintStyle(MUIDrawManager::kStippled);
	drawManager.setDepthPriority(
		MHWRender::MRenderItem::sDormantFilledDepthPriority);
	drawManager.setColor(user_data.m_scale);
	if( user_data.m_texture && user_data.m_use_texture )
	{
		drawManager.setTexture(user_data.m_texture);
		drawManager.setTextureSampler(
			MHWRender::MSamplerState::kMinMagMipLinear,
			MHWRender::MSamplerState::kTexWrap);
		drawManager.setTextureMask(MHWRender::MBlendState::kRGBChannels);
	}

	addSphere(
		drawManager,
		user_data.m_radius,
		36,
		user_data.m_mapping != 0);

	drawManager.endDrawable();
}

void dlEnvironmentShapeDrawOverride::addSphere(
	MUIDrawManager &drawManager,
	float i_radius,
	int i_numSubdiv,
	bool i_isLightProbe)
{
	const float PI = std::acos(-1.0f);
	MFloatPointArray positions;
	MFloatPointArray texcoords;
	MUintArray indices;

	for( int j = 0; j <= i_numSubdiv; ++j )
	{
		float v = float(j) / float(i_numSubdiv);
		float phi = (v - 0.5f) * PI;
		float sin_phi = std::sin(phi);
		float cos_phi = std::cos(phi);

		for( int i = 0; i <= i_numSubdiv; ++i )
		{
			float u = float(i) / float(i_numSubdiv);
			float theta = u * 2.0f * PI;
			float nx = cos_phi * std::sin(theta);
			float ny = sin_phi;
			float nz = -cos_phi * std::cos(theta);

			positions.append(i_radius * nx, i_radius * ny, i_radius * nz);
			if( i_isLightProbe )
			{
				float scale = 1.0f / std::sqrt(2.0f * (nz + 1.0f));
				texcoords.append(
					(nx * scale + 1.0f) * 0.5f,
					(ny * scale + 1.0f) * 0.5f);
			}
			else
			{
				texcoords.append(u, v);
			}
		}
	}

	for( int j = 0; j < i_numSubdiv; ++j )
	{
		for( int i = 0; i < i_numSubdiv; ++i )
		{
			int base = j * (i_numSubdiv + 1) + i;
			if( j != 0 )
			{
				indices.append(base);
				indices.append(base + 1);
				indices.append(base + (i_numSubdiv + 1) + 1);
			}
			if( j + 1 != i_numSubdiv )
			{
				indices.append(base + (i_numSubdiv + 1) + 1);
				indices.append(base + (i_numSubdiv + 1));
				indices.append(base);
			}
		}
	}

	drawManager.mesh(
		MUIDrawManager::kTriangles,
		positions, nullptr, nullptr, &indices, &texcoords);
}

dlEnvironmentShapeDrawOverride::dlEnvironmentShapeDrawOverride(
	const MObject &i_obj) :
	MHWRender::MPxDrawOverride(i_obj, nullptr)
{
}
