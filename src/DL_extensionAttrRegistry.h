/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __DL_EXTENSIONATTRREGISTRY_H__
#define __DL_EXTENSIONATTRREGISTRY_H__

#include <maya/MObjectHandle.h>
#include <maya/MGlobal.h>

#include <map>
#include <vector>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MFnAttribute;
class MDGModifier;
class MNodeClass;
#endif

// A list of Maya object handles
typedef std::vector<MObjectHandle> HandleList;

// A group of extension attributes, made of a group name and an attribute handle
// list.
typedef std::pair<MString, HandleList> AttrGroup;

// A list of extension attribute groups
typedef std::vector<AttrGroup> AttrGroupList;

// A map of a node type id and a list of extension attribute groups.
typedef std::map<unsigned int, AttrGroupList> AttrRegistry;

//
// DL_extensionAttrRegistry
//
// Handles all operations pertaining to extension attributes. Extension
// attributes are added in groups, on a given node type.
//
//
class DL_extensionAttrRegistry
{
public:
	~DL_extensionAttrRegistry();

	// Creates and return the sole registry instance.
	static DL_extensionAttrRegistry& getInstance();

	// Empties the registry and delete the instance.
	static void deleteInstance();

	// Defines the plugin MObject that will be linked to extension attributes.
	// This should be called in the plugin's initialize() function. To be
	// effective, it should be called before any extension attributes are added.
	//
	void setPlugin(const MObject& i_plugin);

	// Registers all extension attributes on Maya built-in node types.
	MStatus registerAll();

	// Unregisters all extension attributes, from Maya built-in typea and from
	// user-defined node types.
	//
	MStatus unregisterAll();

	// Returns the names of the available extension attribute groups.
	MStringArray getAttributeGroupNames();

	// Returns the names of the extension attribute groups added to the speicifed
	// node type.
	//
	MStringArray getAttributeGroupNames(const MString& i_nodeType);

	// Adds a given group of extension attributes to the specified node type.
	//
	// Return values:
	//   MStatus::kInvalidParameter : the node type is invalid
	//   MStatus::kUnknownParameter: the attribute group is invalid
	//   MStatus::kFailure : the attribute group already exists on the node
	//   MStatus::kSuccess : no error.
	//
	MStatus addAttributeGroup(
		const MString& i_nodeType,
		const MString& i_groupName);

	// Removes a given group of extension attributes from the specified node type.
	//
	// Return values:
	//   MStatus::kInvalidParameter: the node type is invalid
	//   MStatus::kFailure: the attribute group does not exist for that node type.
	//   MStatus::kSuccess: no error.
	//
	MStatus removeAttributeGroup(
		const MString& i_nodeType,
		const MString& i_groupName);

	// Add Displacement Shader attributes
	MStatus addDisplacementShaderAttrGroup(const MString& i_nodeType);

	// Add Bump2d extension attributes
	MStatus addBump2dAttrGroup(const MString& i_nodeType);

	// Add Mesh extension attributes
	MStatus addMeshAttrGroup(const MString& i_nodeType);

	// Add Shading Engine attributes
	MStatus addShadingEngineAttrGroup(const MString& i_nodeType);

	// Add Camera extension attributes
	MStatus addCameraAttrGroup(const MString& i_nodeType);

	// Add Light extension attributes
	MStatus addLightAttrGroup(const MString& i_nodeType);

	// Add Area light-specific extension attributes
	MStatus addAreaLightAttrGroup(const MString& i_nodeType);

	/// Add directional light-specific extension attributes
	MStatus addDirectionalAttributesGroup( const MString &);

	/// Add spotlight & pointlight specific extension attributes
	MStatus addSpotAndPointLightsAttributesGroup( const MString& i_nodeType );

	// Add illumination from extension attribute
	MStatus addIlluminationFromAttrGroup(const MString& i_nodeType);

	// Add NURBS curve extension attributes
	MStatus addNURBScurveAttrGroup(const MString& i_nodeType);

	// Add visibility extension attributes
	MStatus addVisibilityAttrGroup(const MString& i_nodeType);

	// Add per object motion blur extension attributes
	MStatus addMotionBlurGroup(const MString& i_nodeType);

	// Add per object curves extension attributes
	MStatus addCurvesGroup(const MString& i_nodeType);

	// Add attribute override volume attributes
	MStatus addAttributeOverrideVolumeAttrGroup(const MString& i_nodeType);

private:
	DL_extensionAttrRegistry();

	// Returns the name of the various extension attribute groups.
	MString getDisplacementShaderGroupName();
	MString getBump2dGroupName();
	MString getMeshGroupName();
	MString getCameraGroupName();
	MString getLightGroupName();
	MString getAreaLightGroupName();
	MString getDirectionalLightGroupName();
	MString getSpotAndPointLightsGroupName();
	MString getVisibilityGroupName();
	MString getMotionBlurGroupName();
	MString getCurvesGroupName();
	MString getAttributeOverrideVolumeGroupName();

	// Returns true after setPlugin has been called with a usable MFnPlugin object.
	bool isPluginValid();

	// Utility procedure that adds a new attribute group for the specified node
	// type. A registry entry is created if needed, and a new attribute group is
	// added to the registry item's attribute group list.
	//
	// Parameters
	//   i_nodeClass: the node type class object
	//   i_groupName: the attribute group name to add
	//   o_attrHandles: pointer to the newly added attribute handle list
	//
	// Return values:
	//   MStatus::kInvalidParameter : the node type class obj is invalid
	//   MStatus::kFailure : the attribute group already exists on the node
	//   MStatus::kSuccess : no error.
	//
	MStatus addAttrGroupForNodeType(
		MNodeClass& i_nodeClass,
		const MString& i_groupName,
		HandleList*& o_attrHandles);

	// Utility procedure to add an attribute as an extension attribute; adds it
	// to the attribute handles vector and return the addition status.
	//
	MStatus addExtensionAttribute(
		MObject& i_attr,
		HandleList& o_attrHandles,
		MDGModifier& i_dgMod,
		MNodeClass& i_nodeClass);


	// Utility procedure to remove an extension attribute. The related object
	// handles becomes invalid after the removal.
	//
	MStatus removeExtensionAttribute(
		MObject i_attr,
		MDGModifier& i_dgMod,
		MNodeClass& i_nodeClass);

	// Returns true if an attribute of the given name is found in the handle list.
	//
	bool attributeExists(const MString& i_attrName, HandleList& i_attrHandles);

	// Returns true if one of the groups in the list is named i_groupName.
	bool hasAttributeGroup(AttrGroupList& i_groupList, const MString& i_groupName);

private:
	static DL_extensionAttrRegistry* m_instance;

	MObjectHandle m_plugin;
	AttrRegistry m_registry;
};

#endif
