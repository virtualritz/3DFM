#ifndef __NSIExportGoboFilter_h
#define __NSIExportGoboFilter_h

#include "NSIExportShader.h"

/**
	\brief Export delegate for shader gobo filter.

	This is needed to set unshown attribute "filterCoordinateSystem".
*/
class NSIExportGoboFilter: public NSIExportShader
{
public:
	NSIExportGoboFilter(
		MObject i_object,
		const NSIExportDelegate::Context& i_context );

	virtual void SetAttributes();
};

#endif
