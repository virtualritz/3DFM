
#include <nsi.hpp>

#include "DL_utils.h"
#include "NSIDelegatesContainer.h"
#include "NSIExportUtilities.h"
#include "NSIExportCamera.h"
#include "NSIExportDelegate.h"
#include "RenderPassInterface.h"
#include "ShaderDataCache.h"

#include <maya/MAngle.h>
#include <maya/MDagPath.h>
#include <maya/MFnCamera.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnMatrixData.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnSet.h>
#include <maya/MGlobal.h>
#include <maya/MIteratorType.h>
#include <maya/MItSelectionList.h>
#include <maya/MItDag.h>
#include <maya/MIntArray.h>
#include <maya/MMatrix.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MString.h>
#include <maya/MStringArray.h>

namespace
{
using namespace NSIExportUtilities;
/**
	\brief Set type & array size of NSI argument according to the TypeDesc.

	\param i_typeDesc
		The OSL typedesc of the OSL parameter.

	\param i_numValuesInAttr
		The number of single values in the argument.

	\param o_arg
		The NSI::Argument to adjust.
*/
NSIType_t SetNSIArgumentTypeForOSLParam(
	const DlShaderInfo::TypeDesc& i_typeDesc,
	int i_numValuesInAttr,
	NSI::Argument& o_arg)
{
	// Figure the proper NSI argument based on the OSL shader param type
	bool isArray = i_typeDesc.is_array();

	/*
		We don't care about the shader array size expectation. We just output
		the data we have.
	*/
	int numElements = i_numValuesInAttr;

	NSIType_t nsiType = static_cast<NSIType_t>(i_typeDesc.elementtype);

	if( nsiType == NSITypeColor || nsiType == NSITypePoint ||
	    nsiType == NSITypeVector || nsiType == NSITypeNormal )
	{
		numElements /= 3;
	}

	if( isArray )
	{
		o_arg.SetArrayType( nsiType, numElements );
	}
	else
	{
		o_arg.SetType( nsiType );
	}

	return nsiType;
}

/**
	\brief
	Retrieve numeric values from Maya attribute and copy them to a NSI data
	buffer.

	Handles any numeric attributes containing one value, and numeric attributes
	compatible with MFnNumericAttribute containing 2 or 3 values.

	\param i_plug
		The plug designating the Maya attribute to output.

	\param i_buffer
		Buffer to store data retrieved from the Maya attrribute in its native
		format.

	\param i_nsiType
		The NSI type of the parametr that will receive the o_nsiData.
		We basically need to know if this will work with ints or floats.

	\param o_nsiData
		The NSI data buffer that holds a copy of the values from i_buffer, in
		integer or float.

	\param i_numValues
		The number of values to retreive in the Maya Attribute.
*/
template<typename Maya_T> void GetNumericAttrData(
	const MPlug& i_plug,
	Maya_T i_buffer[],
	NSIType_t i_nsiType,
	void* o_nsiData,
	int i_numValues );

template<> void GetNumericAttrData<short>(
	const MPlug& i_plug,
	short i_buffer[],
	NSIType_t i_nsiType,
	void* o_nsiData,
	int i_numValues )
{
	assert( i_numValues > 0 && i_numValues < 4 );
	assert( o_nsiData != 0x0 );

	if(i_numValues == 1)
	{
		if( i_nsiType == NSITypeInteger )
		{
			int* nsiData = (int*)o_nsiData;
			i_plug.getValue( *nsiData );
		}
		else
		{
			float* nsiData = (float*)o_nsiData;
			i_plug.getValue( *nsiData );
		}
		return;
	}

	MStatus status;
	MFnNumericData numDataFn( i_plug.asMObject(), &status );
	if( status != MStatus::kSuccess )
		return;

	assert( i_buffer != 0x0 );
	if( i_numValues == 2 )
	{
		numDataFn.getData2Short( i_buffer[ 0 ], i_buffer[ 1 ] );
	}
	else
	{
		numDataFn.getData3Short( i_buffer[0], i_buffer[1], i_buffer[2] );
	}

	if( i_nsiType == NSITypeInteger )
	{
		int* nsiData = (int*)o_nsiData;
		for( int i = 0; i < i_numValues; i++ )
		{
			*( nsiData + i ) = i_buffer[ i ];
		}
	}
	else
	{
		float* nsiData = (float*)o_nsiData;
		for( int i = 0; i < i_numValues; i++ )
		{
			*( nsiData + i ) = i_buffer[ i ];
		}
	}
}

template<> void GetNumericAttrData<int>(
	const MPlug& i_plug,
	int i_buffer[],
	NSIType_t i_nsiType,
	void* o_nsiData,
	int i_numValues )
{
	assert( i_numValues > 0 && i_numValues < 4 );
	assert( o_nsiData != 0x0 );

	if(i_numValues == 1)
	{
		if( i_nsiType == NSITypeInteger )
		{
			int* nsiData = (int*)o_nsiData;
			i_plug.getValue( *nsiData );
		}
		else
		{
			float* nsiData = (float*)o_nsiData;
			i_plug.getValue( *nsiData );
		}
		return;
	}

	MStatus status;
	MFnNumericData numDataFn( i_plug.asMObject(), &status );
	if( status != MStatus::kSuccess )
		return;

	assert( i_buffer != 0x0 );
	if( i_numValues == 2 )
	{
		numDataFn.getData2Int( i_buffer[ 0 ], i_buffer[ 1 ] );
	}
	else
	{
		numDataFn.getData3Int( i_buffer[0], i_buffer[1], i_buffer[2] );
	}

	if( i_nsiType == NSITypeInteger )
	{
		int* nsiData = (int*)o_nsiData;
		for( int i = 0; i < i_numValues; i++ )
		{
			*( nsiData + i ) = i_buffer[ i ];
		}
	}
	else
	{
		float* nsiData = (float*)o_nsiData;
		for( int i = 0; i < i_numValues; i++ )
		{
			*( nsiData + i ) = i_buffer[ i ];
		}
	}
}

template<> void GetNumericAttrData<float>(
	const MPlug& i_plug,
	float i_buffer[],
	NSIType_t i_nsiType,
	void* o_nsiData,
	int i_numValues )
{
	assert( i_numValues > 0 && i_numValues < 4 );
	assert( o_nsiData != 0x0 );

	if(i_numValues == 1)
	{
		if( i_nsiType == NSITypeInteger )
		{
			int* nsiData = (int*)o_nsiData;
			i_plug.getValue( *nsiData );
		}
		else
		{
			float* nsiData = (float*)o_nsiData;
			i_plug.getValue( *nsiData );
		}
		return;
	}

	MStatus status;
	MFnNumericData numDataFn( i_plug.asMObject(), &status );
	if( status != MStatus::kSuccess )
		return;

	assert( i_buffer != 0x0 );
	if( i_numValues == 2 )
	{
		numDataFn.getData2Float( i_buffer[ 0 ], i_buffer[ 1 ] );
	}
	else
	{
		numDataFn.getData3Float( i_buffer[0], i_buffer[1], i_buffer[2] );
	}

	if( i_nsiType == NSITypeInteger )
	{
		int* nsiData = (int*)o_nsiData;
		for( int i = 0; i < i_numValues; i++ )
		{
			*( nsiData + i ) = i_buffer[ i ];
		}
	}
	else
	{
		float* nsiData = (float*)o_nsiData;
		for( int i = 0; i < i_numValues; i++ )
		{
			*( nsiData + i ) = i_buffer[ i ];
		}
	}
}

template<> void GetNumericAttrData<double>(
	const MPlug& i_plug,
	double i_buffer[],
	NSIType_t i_nsiType,
	void* o_nsiData,
	int i_numValues )
{
	assert( i_numValues > 0 && i_numValues < 4 );
	assert( o_nsiData != 0x0 );

	if(i_numValues == 1)
	{
		if( i_nsiType == NSITypeInteger )
		{
			int* nsiData = (int*)o_nsiData;
			i_plug.getValue( *nsiData );
		}
		else
		{
			float* nsiData = (float*)o_nsiData;
			i_plug.getValue( *nsiData );
		}
		return;
	}

	MStatus status;
	MFnNumericData numDataFn( i_plug.asMObject(), &status );
	if( status != MStatus::kSuccess )
		return;

	assert( i_buffer != 0x0 );
	if( i_numValues == 2 )
	{
		numDataFn.getData2Double( i_buffer[ 0 ], i_buffer[ 1 ] );
	}
	else
	{
		numDataFn.getData3Double( i_buffer[0], i_buffer[1], i_buffer[2] );
	}

	if( i_nsiType == NSITypeInteger )
	{
		int* nsiData = (int*)o_nsiData;
		for( int i = 0; i < i_numValues; i++ )
		{
			*( nsiData + i ) = i_buffer[ i ];
		}
	}
	else
	{
		float* nsiData = (float*)o_nsiData;
		for( int i = 0; i < i_numValues; i++ )
		{
			*( nsiData + i ) = i_buffer[ i ];
		}
	}
}

/**
	\brief Retrieve numeric values contained in i_plug and set them in a NSI attribute.

	Works withs single values of any Maya attribute that are numbers, and with
	multiple numeric values of Maya attributes compatible with the
	MFnNumericData function set.

	i_plug must be a single attribute - not an array, nor a compound attribute.

	\param i_plug
		The plug designating the Maya attribute to output.

	\param i_shaderParamData The shader parameter data, from the ShaderDataCache.

	\param i_numValuesInAttr
		The number of distinct numeric values contained in one element of i_plug
		(for array attributes) or i_plug itself (for single attributes). This is
		usually 1, 2 (e.g. uvs) or 3 (e.g. vectors).

	\param o_argList The NSI argument list that will be appended with the result.
*/
template<typename Maya_T> void SingleNumAttrToNSIAttr(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	int i_numValuesInAttr,
	NSI::ArgumentList& o_argList )
{
	assert( i_shaderParamData != 0x0 );
	assert( !IsChildOfArrayOfCompoundAttr( i_plug ) );
	assert( !i_plug.isArray() );

	NSI::Argument* nsiArg = new NSI::Argument( i_shaderParamData->m_name );

	NSIType_t nsiType = SetNSIArgumentTypeForOSLParam(
		i_shaderParamData->m_type, i_numValuesInAttr, *nsiArg );

	/*
		We need to ask Maya for i_numValuesInAttr values of Maya_T for the
		request to succeed. These values are then copied to an NSI_T array.
	*/

	// Buffer to store the attribute value from Maya
	Maya_T* buffer = new Maya_T[ i_numValuesInAttr ];
	void* values;

	if( nsiType == NSITypeInteger )
	{
		values = nsiArg->AllocValue( sizeof( int ) * i_numValuesInAttr );
	}
	else
	{
		values = nsiArg->AllocValue( sizeof( float ) * i_numValuesInAttr );
	}

	GetNumericAttrData<Maya_T>(
		i_plug, buffer, nsiType, values, i_numValuesInAttr );

	o_argList.Add( nsiArg );

	delete[] buffer;
	buffer = 0x0;
}

/**
	\brief Retrieve numeric values contained in i_plug and set them in a NSI attribute.

	Works withs single values of any Maya attribute that are numbers, and with
	multiple numeric values of Maya attributes compatible with the
	MFnNumericData function set.

	i_plug must be an array attribute.

	\param i_plug
		The plug designating the Maya attribute to output.

	\param i_shaderParamData The shader parameter data, from the ShaderDataCache.

	\param i_numValuesInAttr
		The number of distinct numeric values contained in one element of i_plug
		(for array attributes) or i_plug itself (for single attributes). This is
		usually 1, 2 (e.g. uvs) or 3 (e.g. vectors).

	\param o_argList The NSI argument list that will be appended with the result.
*/
template<typename Maya_T> void NumArrayAttrToNSIAttr(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	int i_numValuesInAttr,
	NSI::ArgumentList& o_argList )
{
	assert( i_shaderParamData != 0x0 );
	assert( !IsChildOfArrayOfCompoundAttr( i_plug ) );
	assert( i_plug.isArray() );

	int numValues = i_numValuesInAttr;
	int numElements = i_plug.numElements();
	numValues *= numElements;

	NSI::Argument* nsiArg = new NSI::Argument( i_shaderParamData->m_name );

	NSIType_t nsiType = SetNSIArgumentTypeForOSLParam(
		i_shaderParamData->m_type, numValues, *nsiArg );

	/*
		We need to ask Maya for i_numValuesInAttr values of Maya_T for the
		request to succeed. These values are then copied to an NSI_T array.
	*/

	// Buffer to store the attribute value from Maya
	Maya_T* buffer = new Maya_T[ numValues ];
	void* values;

	if( nsiType == NSITypeInteger )
	{
		values = nsiArg->AllocValue( sizeof( int ) * numValues );
	}
	else
	{
		values = nsiArg->AllocValue( sizeof( float ) * numValues );
	}

	void* curr_nsi_data = values;

	// Iterate on the array elements
	for(unsigned int i = 0; i < numElements; i++ )
	{
		// Get the correct compound child of this array elements
		MPlug curr_plug = i_plug.elementByPhysicalIndex( i );

		// Get the data of this child with the type Maya requires
		GetNumericAttrData<Maya_T>(
			curr_plug, buffer, nsiType, curr_nsi_data, i_numValuesInAttr );

		if( nsiType == NSITypeInteger )
		{
			curr_nsi_data = (int*)(curr_nsi_data) + i_numValuesInAttr;
		}
		else
		{
			curr_nsi_data = (float*)(curr_nsi_data) + i_numValuesInAttr;
		}
	}

	o_argList.Add( nsiArg );

	delete[] buffer;
	buffer = 0x0;
}

/**
	\brief
	Produce a NSI attribute that contains all the values of a specific child
	of a array of compound attributes.

	i.e. ramp1.colorEntryList[*].color -> array of color values.

	i_plug must be the compound child to export. It can come from any array
	element. The function will find which child this is and export that child
	from all array elements.

	This Maya contraption is used for attributes controled by ramp widgets, which
	are constructed as:

	colorEntryList <------ array attr, one entry per ramp element
	  colorEntryList[x] <- array element, which is a compound attr
	    color <----------- compound child
	    position <-------- compound child
	    interp <---------- compound child

	So pass i_plug = ramp1.colorEntryList[2].color and you'll get a NSI attribute
	that is an array of colors from all elements of colorEntryList[*].color .

	\param i_plug
		One of the compound child attribute that will be an array element
		i.e. ramp1.colorEntryList[3].color

	\param i_shaderParamData The shader parameter data, from the ShaderDataCache.

	\param i_numValuesInAttr
		The number of values stored in i_plug. 1, 2 or 3.

	\param o_argList The NSI argument list that will be appended with the result.
*/
template<typename Maya_T>
void ArrayOfCompoundNumValuesAsNSIAttr(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	int i_numValuesInAttr,
	NSI::ArgumentList& o_argList )
{
	assert( i_shaderParamData != 0x0 );
	assert( IsChildOfArrayOfCompoundAttr( i_plug ) );

	/*
		This is specifically written to handle array of compound attributes such
		as the ones found in where ramp widgets are used.
	*/
	if( i_shaderParamData == 0x0 || !IsChildOfArrayOfCompoundAttr( i_plug ) )
		return;

	int num_values = i_numValuesInAttr;

	// Figure the amount of data to gather
	MPlug array = i_plug.parent().array();

	int num_elements = array.evaluateNumElements();
	if( num_elements == 0 )
		return;

	num_values *= num_elements;

	// Figure if we are dealing with colors
	bool asColor = IsColorAttr( i_plug );

	// Figure the child index of i_plug
	int child_index = CompoundAttrChildIndex( i_plug );
	assert( child_index >= 0 );
	if( child_index < 0 )
		return;

	NSI::Argument* nsiArg = new NSI::Argument( i_shaderParamData->m_name );
	NSIType_t nsiType = SetNSIArgumentTypeForOSLParam(
		i_shaderParamData->m_type, num_values, *nsiArg );

	// Buffer to store the attribute value from Maya
	Maya_T* buffer = new Maya_T[ num_values ];

	// NSI data buffer
	void* values = 0x0;
	if( nsiType == NSITypeInteger )
	{
		values = nsiArg->AllocValue( sizeof( int ) * num_values );
	}
	else
	{
		values = nsiArg->AllocValue( sizeof( float ) * num_values );
	}

	void* curr_nsi_data = values;

	// Iterate on the array elements
	for(unsigned int i = 0; i < num_elements; i++ )
	{
		// Get the correct compound child of this array elements
		MPlug curr_plug = array.elementByPhysicalIndex( i ).child( child_index );

		// Get the data of this child with the type Maya requires
		GetNumericAttrData<Maya_T>(
			curr_plug, buffer, nsiType, curr_nsi_data, i_numValuesInAttr );

		if( nsiType == NSITypeInteger )
		{
			curr_nsi_data = (int*)(curr_nsi_data) + i_numValuesInAttr;
		}
		else
		{
			curr_nsi_data = (float*)(curr_nsi_data) + i_numValuesInAttr;
		}
	}

	o_argList.Add( nsiArg );

	delete[] buffer;
	buffer = 0x0;
}

/**
	\brief Retrieve numeric values contained in i_plug and set them in a NSI attribute.

	Works withs single values of any Maya attribute that are numbers, and with
	multiple numeric values of Maya attributes compatible with the
	MFnNumericData function set.

	Works with single attributes, array attributes and array of compound
	attributes.

	\param i_plug
		The plug designating the Maya attribute to output.

	\param i_shaderParamData The shader parameter data, from the ShaderDataCache.

	\param i_numValuesInAttr
		The number of distinct numeric values contained in one element of i_plug
		(for array attributes) or i_plug itself (for single attributes). This is
		usually 1, 2 (e.g. uvs) or 3 (e.g. vectors).

	\param o_argList The NSI argument list that will be appended with the result.
*/
template<typename Maya_T> void NumAttrToNSIAttr(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	int i_numValuesInAttr,
	NSI::ArgumentList& o_argList )
{
	if( i_shaderParamData == 0x0 )
		return;

	bool isArrayOfCompound = IsChildOfArrayOfCompoundAttr( i_plug );
	if( isArrayOfCompound )
	{
		ArrayOfCompoundNumValuesAsNSIAttr<Maya_T>(
			i_plug, i_shaderParamData, i_numValuesInAttr, o_argList);
	}
	else if( i_plug.isArray() )
	{
		NumArrayAttrToNSIAttr<Maya_T>(
			i_plug, i_shaderParamData, i_numValuesInAttr, o_argList );
	}
	else
	{
		SingleNumAttrToNSIAttr<Maya_T>(
			i_plug, i_shaderParamData, i_numValuesInAttr, o_argList );
	}
}


void ProcessSingleMatrixPlug(	const MPlug& i_plug, float* o_matrix )
{
	MFnMatrixData matrixData( i_plug.asMObject() );
	MMatrix matrix = matrixData.matrix();
	for( int i = 0; i < 4; i++ )
	{
		for( int j = 0; j < 4; j++ )
		{
			*o_matrix++ = matrix[ i ][ j ];
		}
	}
}

void MatrixAttributeToNSIAttribute(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	NSI::ArgumentList& o_argList )
{
	bool isArray = i_plug.isArray();

	NSI::Argument* nsiArg = new NSI::Argument( i_shaderParamData->m_name );

	int num_matrices = 1;
	if( isArray )
	{
		num_matrices = i_plug.numElements();
	}

	void* value = nsiArg->AllocValue( num_matrices * 16 * sizeof( float ) );
	float* nsiMatrix = (float*)value;

	if( isArray )
	{
		nsiArg->SetArrayType( NSITypeMatrix, num_matrices );

		for( unsigned int i = 0; i < num_matrices; i++ )
		{
			MPlug currElement = i_plug.elementByPhysicalIndex( i );
			ProcessSingleMatrixPlug( currElement, nsiMatrix );
			nsiMatrix += 16;
		}
	}
	else
	{
		nsiArg->SetType( NSITypeMatrix );
		ProcessSingleMatrixPlug( i_plug, nsiMatrix );
	}

	o_argList.Add( nsiArg );
}

/**
	\brief Output a numeric shader maya attribute as a NSI argument.

	This handles Maya attributes that are compatible with the
	MFnNumericAttribute function set.

	\param i_plug The Maya node & attribute to output

	\param i_shaderParamData The shader parameter data, from the ShaderDataCache.

	\param o_argList The NSI argument list that will be appended with the result.
*/
void ShaderNumericAttributeToNSIAttribute(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	NSI::ArgumentList& o_argList )
{
	MStatus status;

	MObject attr = i_plug.attribute();
	MFnNumericAttribute numAttrFn( attr, &status );
	if( status != MStatus::kSuccess  || i_shaderParamData == 0x0 )
		return;

	switch( numAttrFn.unitType() )
	{
		case MFnNumericData::kBoolean:
		case MFnNumericData::kByte:
		case MFnNumericData::kChar:
		case MFnNumericData::kShort:
		case MFnNumericData::kLong:
			NumAttrToNSIAttr<int>( i_plug, i_shaderParamData, 1, o_argList );
			break;

		case MFnNumericData::k2Short:
			NumAttrToNSIAttr<short>( i_plug, i_shaderParamData, 2, o_argList );
			break;

		case MFnNumericData::k2Long:
			NumAttrToNSIAttr<int>( i_plug, i_shaderParamData, 2, o_argList );
			break;

		case MFnNumericData::k3Short:
			NumAttrToNSIAttr<short>( i_plug, i_shaderParamData, 3, o_argList );
			break;

		case MFnNumericData::k3Long:
			NumAttrToNSIAttr<int>( i_plug, i_shaderParamData, 3, o_argList );
			break;

		case MFnNumericData::kFloat:
		case MFnNumericData::kDouble:
			NumAttrToNSIAttr<float>( i_plug, i_shaderParamData, 1, o_argList );
			break;

		case MFnNumericData::k2Float:
			NumAttrToNSIAttr<float>( i_plug, i_shaderParamData, 2, o_argList );
			break;

		case MFnNumericData::k2Double:
			NumAttrToNSIAttr<double>( i_plug, i_shaderParamData, 2, o_argList );
			break;

		case MFnNumericData::k3Float:
			NumAttrToNSIAttr<float>( i_plug, i_shaderParamData, 3, o_argList );
			break;

		case MFnNumericData::k3Double:
			NumAttrToNSIAttr<double>( i_plug, i_shaderParamData, 3, o_argList );
			break;

		default:
			break;
	}
}

/**
	\brief Produce a numeric shader maya attribute as a NSI argument

	This handles Maya attributes that are compatible with the
	MFnNumericAttribute function set. The resultin NSI argument is appended to the
	provided argument list.

	\param i_plug The Maya node & attribute to output

	\param i_shaderParamData The shader parameter data, from the ShaderDataCache.

	\param o_argList The NSI Argument list to which the new argument will be
		appended.
*/
void ShaderUnitAttributeToNSIAttribute(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	NSI::ArgumentList& o_argList )
{
	MStatus status;

	MObject attr = i_plug.attribute();
	MFnUnitAttribute unitAttrFn( attr, &status );
	if( status != MStatus::kSuccess || i_shaderParamData == 0x0 )
		return;

	float value = 0.0f;

	if( unitAttrFn.unitType() == MFnUnitAttribute::kAngle )
	{
		/*
			FIXME: this assumes that all angle attributes must be output in degrees.
			This is the needed behaviour for cone angle light shader attributes, so
			that's why it is done here. Some shader metadata would be needed to
			figure if the angle attribute should be treated differently.
		*/
		MAngle angle;
		i_plug.getValue( angle );
		value = angle.asDegrees();
	}
	else
	{
		/*
			FIXME: probably need to handle time and distance values in a more
			specific way.
		*/
		i_plug.getValue( value );
	}

	o_argList.Add( new NSI::FloatArg( i_shaderParamData->m_name, value ) );
}

void ShaderTypedAttributeToNSIAttribute(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	NSI::ArgumentList& o_argList )
{
	MStatus status;

	MObject attr = i_plug.attribute();
	MFnTypedAttribute typedAttrFn( attr, &status );
	if( status != MStatus::kSuccess || i_shaderParamData == 0x0 )
		return;

	if( typedAttrFn.attrType() == MFnData::kString )
	{
		if( IsChildOfArrayOfCompoundAttr( i_plug ) )
		{
			// Figure the child index of i_plug
			int child_index = CompoundAttrChildIndex( i_plug );
			assert( child_index >= 0 );
			if( child_index < 0 )
				return;

			// Figure the amount of data to gather
			MPlug arrayPlug = i_plug.parent().array();

			int num_elements = arrayPlug.evaluateNumElements();

			MStringArray array;
			array.setLength( num_elements );

			// Iterate on the array elements
			for(unsigned int i = 0; i < num_elements; i++ )
			{
				// Get the correct compound child of this array elements
				MPlug curr_plug =
					arrayPlug.elementByPhysicalIndex( i ).child( child_index );

				array.set( curr_plug.asString(), i );
			}

			o_argList.Add(
				new MStringArrayNSIArg( i_shaderParamData->m_name, array ) );
		}
		else if( i_plug.isArray() )
		{
			MStringArray array;
			int num_elements = i_plug.numElements();
			array.setLength( num_elements );

			for(unsigned int i = 0; i < num_elements; i++ )
			{
				MPlug curr_plug = i_plug.elementByPhysicalIndex( i );
				array.set( curr_plug.asString(), i );
			}

			o_argList.Add(
				new MStringArrayNSIArg( i_shaderParamData->m_name, array ) );
		}
		else
		{
			MString value;
			i_plug.getValue( value );

			if( i_shaderParamData->m_flags & ParamData::Flag_TextureFile )
			{
				MObject node = i_plug.node();

				/*
					Attempt to use Maya node-wide "colorSpace" attr. Supports,
					for example, the file node.
				*/
				if( isAttrString( node, "colorSpace" ) )
				{
					std::string cs = ReadColorSpace( node, "colorSpace" );
					std::string argname( i_shaderParamData->m_name );
					argname += ".meta.colorspace";
					o_argList.Add( new NSI::StringArg( argname, cs ) );
				}

				/* For the file node, work with maya's generated pattern. */
				bool pattern = false;
				if( typedAttrFn.name() == "fileTextureName" )
				{
					value = getAttrString(
						node, "computedFileTextureNamePattern", value, pattern );
				}
				/* Support for relative paths for dlTexture */
				else if( typedAttrFn.name() == "textureFile" )
				{
					value = getFixedPath(value);
					if( value.length() != 0 )
					{
						value = MGlobal::executeCommandStringResult(
							"DL_getAbsolutePath(\"" + value + "\")");
					}
				}

				int frame = 0;
				bool do_img_seq = false;

				/* Do "Use Image Sequence" of the file node, if enabled. 
				For Maya File Node, uses useFrameExtension attr */
				if (getAttrBool( node, "useFrameExtension", false ))
				{
					frame = getAttrInt( node, "frameExtension", 1 ) +
							getAttrInt( node, "frameOffset", 0 );
					value = Utilities::expandFToken( value, float(frame) );
					do_img_seq = true;
				}
				/* Do "Use Image Sequence" of dlTexture, if enabled. */
				else if( getAttrBool( node, "useImageSequence", false ) )
				{
					frame = getAttrInt( node, "frame", 1 ) +
							getAttrInt( node, "frameOffset", 0 );
					value = Utilities::expandFToken( value, float(frame) );
					do_img_seq = true;
				}

				/* Convert <UDIM> pattern to what 3Delight understands. */
				if( getAttrInt( node, "uvTilingMode", 0 ) == 3 ||
					typedAttrFn.name() == "textureFile")
				{
					value.substitute( "<UDIM>", "UDIM" );
				}

				/* Expand Env Vars and frame number when using "#"" token.
				Related frame number token "<f>" is handled by expandFToken */
				unsigned buffer_size = value.length() + 1024;
				char *buffer = new char[buffer_size];
				expandEnvVars( 	buffer, buffer_size,
								value.asChar(), do_img_seq, float(frame) );
				value = buffer;
				delete[] buffer;
				
			}

			o_argList.Add(
				new NSI::StringArg( i_shaderParamData->m_name, value.asChar() ) );
		}
	}
	if( typedAttrFn.attrType() == MFnData::kMatrix )
	{
		MatrixAttributeToNSIAttribute( i_plug, i_shaderParamData, o_argList );
	}

	/**
		MFnData lists many other potentially useful cases:
		kStringArray,
		kDoubleArray,
		kFloatArray,
		kIntArray,
		kPointArray,
		kVectorArray,
		kMatrixArray,

		They remain TODO until I encounter a use case for one of them.
	*/
}

MString MessageAttributeStringValue( const MPlug& i_plug, bool i_live )
{
	// NSI handle of the node connected to i_plug
	MString handle;

	// Find that connected node
	MObject obj;
	MPlugArray incoming;
	i_plug.connectedTo( incoming, true, false );

	// Maya API states that there will be 0 or 1 element in incoming.
	if( incoming.length() >= 1 )
	{
		MObject connectedNode = incoming[0].node();

		/*
			AOV nodes are passed as node names. Other nodes are passed as their
			NSI handle.

			TODO: This could be dependent on some shader param metadata instead of
			being based on classification strings.
		*/
		MFnDependencyNode depFn( connectedNode );
		MString typeName = depFn.typeName();
		MStringArray classificationStrings;

		Utilities::GetTypeClassificationStrings( typeName, classificationStrings );

		MString aovClass = "rendernode/dl/aov";
		if( Utilities::HasClass( aovClass, classificationStrings ) )
		{
			handle = depFn.name();
		}
		else
		{
			handle = NSIExportDelegate::NSIHandle( connectedNode, i_live );
		}
	}

	return handle;
}

void ShaderMessageAttributeToNSIAttribute(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	bool i_live,
	NSI::ArgumentList& o_argList )
{
	MStatus status;
	MObject attr = i_plug.attribute();
	MFnMessageAttribute msgAttrFn( attr, &status );

	if( i_shaderParamData->m_type.arraylen != 0 )
	{
		MStringArray array;

		if( IsChildOfArrayOfCompoundAttr( i_plug ) )
		{
			// Element of array of compound attribute to string array
			// Figure the child index of i_plug
			int child_index = CompoundAttrChildIndex( i_plug );

			assert( child_index >= 0 );
			if( child_index < 0 )
				return;

			// Figure the amount of data to gather
			MPlug arrayPlug = i_plug.parent().array();
			int num_elements = arrayPlug.evaluateNumElements();
			array.setLength( num_elements );

			// Iterate on the array elements
			for(unsigned int i = 0; i < num_elements; i++ )
			{
 				// Get the correct compound child of this array elements
				MPlug curr_plug =
					arrayPlug.elementByPhysicalIndex( i ).child( child_index );

				array.set( MessageAttributeStringValue(curr_plug, i_live), i );
			}
		}
		else if( i_plug.isArray() )
		{
			// Array of message attributes to string array
			// TODO: not tested
			int num_elements = i_plug.numElements();
			array.setLength( num_elements );

			// Iterate on the array elements
			for(unsigned int i = 0; i < num_elements; i++ )
			{
				MPlug curr_plug =	i_plug.elementByPhysicalIndex( i );
				array.set( MessageAttributeStringValue(curr_plug, i_live), i );
			}
		}
		else
		{
			// Message attribute to string array
			array.setLength( 1 );
			array.set( MessageAttributeStringValue( i_plug, i_live ), 0 );
		}

		if( array.length() > 0 )
		{
			o_argList.Add(
				new MStringArrayNSIArg( i_shaderParamData->m_name, array ) );
		}
	}
	else
	{
		// Message attribute to string parameter.
		if( !IsChildOfArrayOfCompoundAttr( i_plug ) && !i_plug.isArray() )
		{
			MString value = MessageAttributeStringValue( i_plug, i_live );
			o_argList.Add(
				new NSI::StringArg( i_shaderParamData->m_name, value.asChar() ) );
		}
	}
}

void DisambiguateFilmFit(
	const MFnCamera &cam,
	double aperture_ratio,
	double resolution_ratio,
	MFnCamera::FilmFit& o_film_fit,
	float& o_film_fit_offset)
{
	o_film_fit = cam.filmFit();
	o_film_fit_offset = cam.filmFitOffset();

	if( o_film_fit == MFnCamera::kFillFilmFit )
	{
		o_film_fit_offset = 0.0f;
		o_film_fit = aperture_ratio < resolution_ratio ?
			MFnCamera::kHorizontalFilmFit : MFnCamera::kVerticalFilmFit;
	}
	else if( o_film_fit == MFnCamera::kOverscanFilmFit )
	{
		o_film_fit_offset = 0.0f;
		o_film_fit = aperture_ratio > resolution_ratio ?
			MFnCamera::kHorizontalFilmFit : MFnCamera::kVerticalFilmFit;
	}
}

} // anonymous namespace


NSIExportUtilities::MStringArrayNSIArg::MStringArrayNSIArg(
	const std::string &i_name,
	const MStringArray& i_array )
:
	ArgBase( i_name ),
	m_array( 0x0 ),
	m_arraySize( 0 )
{
	m_arraySize = i_array.length();
	m_array = (char**) malloc( m_arraySize * sizeof( char* ) );

	for( int i = 0; i < m_arraySize; i++ )
	{
		m_array[i] = strdup( i_array[i].asChar() );
	}
}

NSIExportUtilities::MStringArrayNSIArg::~MStringArrayNSIArg()
{
	for( int i = 0; i < m_arraySize; i++ )
	{
		free( m_array[ i ] );
	}

	free( m_array );
}

void NSIExportUtilities::MStringArrayNSIArg::FillNSIParam( NSIParam_t& p ) const
{
	p.name = m_name;
	p.data = m_array;
	p.type = NSITypeString;
	p.arraylength = m_arraySize;
	p.count = 1;
	p.flags = NSIParamIsArray;
}

void NSIExportUtilities::GetLightsFromLightGroup(
	MObject i_lightGroup,
	bool i_flatten,
	MObjectArray& o_items )
{
	// Make sure we are dealing with a lightGroup node.
	MFnDependencyNode depFn( i_lightGroup );
	if( depFn.typeName() != MString( "lightGroup" ) )
	{
		return;
	}

	/*
		MPlug.source & .destinations are not defined in Maya 2016
		Anyway there is no light group node type before Maya 2018, so no light
		groups to export for Maya versions below 2017.
	*/
#if MAYA_API_VERSION > 201600

	// the lightGroup's list elements are connected to its "listItems" plug.
	MPlug listItemsPlug = depFn.findPlug( "listItems", true );
	MPlugArray groupItemPlugs;

	listItemsPlug.destinations( groupItemPlugs );

	for( unsigned i = 0; i < groupItemPlugs.length(); i++ )
	{
		MString groupMemberHandle;

		MFnDependencyNode itemDepFn( groupItemPlugs[i].node() );

		/*
			We support two types of nodes as light group elements:
			- lightItem nodes, which are connected to actual light sources
			- lightGroup nodes, which are other light groups.

			There are no MFn API type for these (they are generic kPluginDependNode),
			so we identify them by type name. We could also figure the numeric value
			of their type ID and test against that.
		*/
		if( itemDepFn.typeName() == MString( "lightItem" ) )
		{
			/*
				Figure the light handle of the light shape connected to the light
				item's "light" attribute.
			*/
			MPlug itemLightPlug = itemDepFn.findPlug( "light", true );
			MPlug sourcePlug = itemLightPlug.source();
			if( !sourcePlug.isNull() )
			{
				o_items.append( sourcePlug.node() );
			}
		}
		else if( i_flatten && itemDepFn.typeName() == MString( "lightGroup" ) )
		{
			GetLightsFromLightGroup( itemDepFn.object(), i_flatten, o_items );
		}
	}
#endif
}

void NSIExportUtilities::FlattenMayaSet(
	const MFnSet &i_set,
	const MIntArray &i_types,
	bool i_live,
	std::unordered_set<std::string> &o_paths )
{
	MSelectionList list;
	MStatus status = i_set.getMembers( list, true );

	if( status != MStatus::kSuccess )
		return;

	/* We absolutely needs a transform to travers the DAG */
	MIntArray types = i_types;
	types.append( MFn::kTransform );

	MItSelectionList iter( list );
	for ( ; !iter.isDone(); iter.next() )
	{
		MDagPath dag_path;
		MObject component;
		iter.getDagPath( dag_path, component );

		MFnDagNode dag_node( dag_path, &status );

		if( status != MStatus::kSuccess )
			continue;

		if( dag_node.object().hasFn(MFn::kTransform) )
		{
			std::vector<MObject> unused;
			FlattenMayaTree( dag_node, types, i_live, o_paths, unused );
			continue;
		}

		MString nsi_handle = NSIExportDelegate::NSIHandle(dag_node, i_live);
		o_paths.emplace(nsi_handle.asChar());
	}
}

void NSIExportUtilities::FlattenMayaLightGroup(
	MObject i_lightGroup,
	bool i_live,
	std::unordered_set<std::string>& o_nsi_handles )
{
	MObjectArray items;
	GetLightsFromLightGroup( i_lightGroup, true, items );

	for( unsigned int i = 0; i < items.length(); i++ )
	{
		MString nsi_handle = NSIExportDelegate::NSIHandle( items[i], i_live );
		o_nsi_handles.emplace(nsi_handle.asChar());
	}
}

/**
	FIXME: Note that we don't use "prune" here and instead just go through the
	tree collecting objects since they are anyway de-deduplicated by the
	string hash map.
*/
void NSIExportUtilities::FlattenMayaTree(
	const MFnDagNode &i_root,
	MIntArray &i_types,
	bool i_live,
	std::unordered_set<std::string> &o_nsi_handles,
	std::vector<MObject> &o_objects )
{
	MIteratorType filter;
	// This is why i_types is not a const reference
	filter.setFilterList( i_types );

	MItDag iterator( filter, MItDag::kDepthFirst );

	if( !i_root.object().isNull() )
	{
		MObject object = i_root.object();
		iterator.reset( filter, &object, 0x0, MItDag::kDepthFirst );
	}

	MStatus status;
	for( ; !iterator.isDone(); iterator.next() )
	{
		MDagPath dag_path;
		iterator.getPath( dag_path );

		MFnDagNode dag_node(dag_path, &status);

		if( status != MStatus::kSuccess )
			continue;

		if( dag_node.object().hasFn(MFn::kTransform) )
		{
			/* We could check if already went here and do a prune() */
			continue;
		}

		MString nsi_handle = NSIExportDelegate::NSIHandle(dag_node, i_live);
		if( o_nsi_handles.emplace(nsi_handle.asChar()).second )
		{
			o_objects.push_back( dag_node.object() );
		}
	}
}

void NSIExportUtilities::FlattenMayaTree(
	const MFnDagNode &i_root,
	MIntArray &i_types,
	std::vector<MObject> &o_objects )
{
	/*
		Handles are unused, so we don't care whether they're generated for a
		live render or not.
	*/
	bool live = false;
	std::unordered_set<std::string> unused_handles;
	FlattenMayaTree(i_root, i_types, live, unused_handles, o_objects);
}

bool NSIExportUtilities::IsShadingNode( MObject i_node )
{
	/*
		Look for a shader classification string on the dependency node.
	*/
	MFnDependencyNode depFn( i_node );
	MStringArray tokens;
	Utilities::GetTypeClassificationStrings( depFn.typeName(), tokens );

	if(( i_node.hasFn( MFn::kPluginDependNode ) ||
		i_node.hasFn( MFn::kPluginShape ) ||
		i_node.hasFn( MFn::kPluginLocatorNode ) ) )
	{
		/*
			Possible plug-in shader. Look for a classification string
			that begins with "rendernode/dl/shader".
		*/
		for(unsigned i = 0; i < tokens.length(); i++ )
		{
			if( !strncmp(
					tokens[i].asChar(), "rendernode/dl/shader/", 21 ) )
			{
				return true;
			}
		}
	}
	else
	{
		/*
			Possible Maya built-in shading node. Look for possible
			classification prefixes at the beginning of a classification
			string.
		*/
		for(unsigned i = 0; i < tokens.length(); i++ )
		{
			if( !strncmp( tokens[i].asChar(), "shader/", 7 ) ||
				!strncmp( tokens[i].asChar(), "texture/", 8 ) ||
				!strncmp( tokens[i].asChar(), "utility/", 8 ) )
			{
				return true;
			}
		}
	}

	return false;
}

bool NSIExportUtilities::IsChildOfArrayOfCompoundAttr( const MPlug& i_plug )
{
	return( i_plug.isChild() && i_plug.parent().array().isArray() );
}

int NSIExportUtilities::CompoundAttrChildIndex(const MPlug& i_plug )
{
	int index = -1;

	MPlug parent_plug = i_plug.parent();

	for(unsigned i = 0; i < parent_plug.numChildren(); i++ )
	{
		if( parent_plug.child(i) == i_plug )
		{
			index = i;
		}
	}

	return index;
}

bool NSIExportUtilities::IsColorAttr( const MPlug& i_plug )
{
	MStatus status;
	MFnNumericAttribute numAttrFn( i_plug.attribute(), &status );

	if( status == MStatus::kSuccess )
	{
		return (
			numAttrFn.unitType() == MFnNumericData::k3Float &&
			numAttrFn.isUsedAsColor() );
	}

	return false;
}

const ShaderData* NSIExportUtilities::GetShaderData( const MObject& i_object )
{
	// Lookup cached shader data matching i_object's node nodetype.
	const ShaderData* shaderData =
		ShaderDataCache::Instance()->Find( i_object );

	if( !shaderData )
	{
		// No cache hit, so attempt to cache that shader's data.
		shaderData = ShaderDataCache::Instance()->Add( i_object );
	}

	return shaderData;
}

const ParamData*
NSIExportUtilities::GetShaderParameterData( const MPlug& i_plug, IoType i_type )
{
	// Lookup cached shader data matching i_plug's node nodetype.
	const ShaderData* shaderData = GetShaderData( i_plug.node() );
	if( shaderData )
	{
		return shaderData->GetParameterData( i_plug, i_type );
	}

	return 0x0;
}

MString
NSIExportUtilities::GetShaderParameterName( const MPlug& i_plug, IoType i_type )
{
	MString name;

	const ParamData* paramData = GetShaderParameterData( i_plug, i_type );
	if( paramData != 0x0 )
	{
		name = MString( paramData->m_name.c_str() );
	}

	return name;
}

static void ShaderAttributeToNSIAttribute(
	const MPlug& i_plug,
	const ParamData* i_shaderParamData,
	bool i_live,
	NSI::ArgumentList& o_argList )
{
	// Figure the data type of the attr we are dealing with
	MObject attr = i_plug.attribute();

	if( attr.hasFn( MFn::kNumericAttribute ) )
	{
		ShaderNumericAttributeToNSIAttribute(
			i_plug, i_shaderParamData, o_argList );
	}
	else if( attr.hasFn( MFn::kEnumAttribute ) )
	{
		NumAttrToNSIAttr<int>( i_plug, i_shaderParamData, 1, o_argList );
	}
	else if( attr.hasFn( MFn::kTypedAttribute ) )
	{
		ShaderTypedAttributeToNSIAttribute(
			i_plug, i_shaderParamData, o_argList );
	}
	else if( attr.hasFn( MFn::kUnitAttribute ) )
	{
		ShaderUnitAttributeToNSIAttribute(
			i_plug, i_shaderParamData, o_argList );
	}
	else if( attr.hasFn( MFn::kMessageAttribute ) )
	{
		ShaderMessageAttributeToNSIAttribute(
			i_plug, i_shaderParamData, i_live, o_argList );
	}
	else if( attr.hasFn( MFn::kMatrixAttribute ) )
	{
		MatrixAttributeToNSIAttribute( i_plug, i_shaderParamData, o_argList );
	}
}

const ParamData*
NSIExportUtilities::MayaShaderAttributeToNSIAttribute(
	const MPlug& i_plug,
	IoType i_type,
	const ShaderData* i_shaderData,
	bool i_live,
	NSI::ArgumentList& o_argList )
{
	/*
		First, attempt to find some shader data for a parameter that corresponds
		to i_plug
	*/
	if( i_shaderData == 0x0 )
		return 0x0;

	const ParamData* paramData = i_shaderData->GetParameterData( i_plug, i_type );

	if( paramData && !(paramData->m_flags & ParamData::Flag_SkipInit) )
	{
		ShaderAttributeToNSIAttribute( i_plug, paramData, i_live, o_argList );
	}

	return paramData;
}

float NSIExportUtilities::ComputeFieldOfView(const MFnCamera &cam)
{
	return cam.horizontalFieldOfView();
}

void NSIExportUtilities::ComputeScreenSizeAndCenter(
	const MFnCamera &cam,
	const dl::V2i &res,
	dl::V2f &size,
	dl::V2f &center )
{
	double resolution[2] =
		{
			(double)res[0],
			(double)res[1]
		};

	dl::V2f aperture( cam.horizontalFilmAperture(), cam.verticalFilmAperture());
	dl::V2f offset( cam.horizontalFilmOffset(), cam.verticalFilmOffset() );

	double aperture_ratio = aperture[0] / aperture[1];
	double resolution_ratio = resolution[0] / resolution[1];

	if( cam.isOrtho() )
	{
	    float ortho_width = cam.orthoWidth();
		size = dl::V2f( ortho_width, ortho_width / resolution_ratio );
		return;
	}

	MFnCamera::FilmFit filmFit;
	float film_fit_offset;
	DisambiguateFilmFit(
		cam,
		aperture_ratio,
		resolution_ratio,
		filmFit,
		film_fit_offset);

	double lens_squeeze_ratio = 1.0f;
	float ratio_diff = 0.0f;
	float shake_overscan =
		cam.shakeOverscanEnabled() ? cam.shakeOverscan() : 1.0f;

	assert( filmFit == MFnCamera::kHorizontalFilmFit ||
		filmFit == MFnCamera::kVerticalFilmFit );

	if( filmFit == MFnCamera::kHorizontalFilmFit )
	{
		size = dl::V2f(2.0f, 2.0f / resolution_ratio );
		lens_squeeze_ratio =  cam.lensSqueezeRatio();

		ratio_diff = (1.0f/resolution_ratio) - (1.0/aperture_ratio);
		if( ratio_diff < 0.0f )
			ratio_diff = 0.0f;

		offset *= 2.0f / aperture[0];
		offset[1] -= ratio_diff * film_fit_offset * shake_overscan;
	}
	else
	{
		/*
			Since the screen resolution is unknown at the time we retrieve the
			camera's field of view, we always set the NSI camera's FOV to Maya's
			camera's horizontal FOV. Since actually need it to be set to the
			vertical FOV, we scale the screen window to compensate.
		*/
		float fov_ratio =
			cam.verticalFieldOfView() / cam.horizontalFieldOfView();

		size = dl::V2f(2.0f * resolution_ratio, 2.0f ) * fov_ratio;

		ratio_diff = resolution_ratio - aperture_ratio;
		if( ratio_diff < 0.0f )
			ratio_diff = 0.0f;

		offset *= 2.0f / aperture[1];
		offset[0] -= ratio_diff * film_fit_offset * shake_overscan;
	}

	float camera_scale = cam.cameraScale() * lens_squeeze_ratio;
	center = offset * camera_scale;
	size *= camera_scale;

	/*
	   Apply the shake overscan on the screen window size before the translate
	   and options, since they depend on that size.
	*/
	size *= shake_overscan;
}

void NSIExportUtilities::ComputeRollAndTranslate(
	const MFnCamera &cam, dl::V2f &size, dl::V2f &center )
{
	// Deal with the Pre/Post Scale, Film Translate and Film Roll attributes.
	//
	dl::V2f film_translate( cam.filmTranslateH(), cam.filmTranslateV() );

	dl::V2f pivot( cam.horizontalRollPivot(), cam.verticalRollPivot() );

	bool rotate_first = cam.filmRollOrder() == MFnCamera::kRotateTranslate;

	/*
		The Film translate and Film Roll pivots are expressed as normalized
		values with respect to the screen window.
	*/
	float normalization_factor = size[0] / 2.0;
	float pre_scale_factor = 1.0 / cam.preScale();
	float scale = pre_scale_factor * normalization_factor;

	/* Not sure why this negation is required. */
	pivot *= -1 * scale;

	if( !rotate_first )
		center += film_translate * scale;

	center -= pivot;
	float angle = -cam.filmRollValue();
	center = dl::V2f(
		center[0]*cosf(angle) - center[1]*sinf(angle),
		center[1]*cosf(angle) + center[0]*sinf(angle) );
	center += pivot;

	if( rotate_first )
		center += film_translate * scale;

	// Apply Pre / Post scale to the screen window size too.
	size *= 1.0 / (cam.preScale() * cam.postScale() );
}

/*
	FIXME: This will work only if there is no offet applied to camera
	in ComputeRollAndTranslate above. These two functions whould be
	put into one.
*/
void NSIExportUtilities::ComputePanAndZoom(
	const MFnCamera &cam,
	const dl::V2i &res,
	dl::V2f &size,
	dl::V2f &center )
{
	if ( !cam.renderPanZoom() || !cam.panZoomEnabled() )
	{
		return;
	}

	size *= cam.zoom();
	center *= cam.zoom();

	double horizontal_aperture = cam.horizontalFilmAperture();
	double vertical_aperture = cam.verticalFilmAperture();
	double film_aspect = horizontal_aperture / vertical_aperture;
	double device_aspect_ratio = double(res[0])/double(res[1]);

	MFnCamera::FilmFit film_fit = cam.filmFit();

	if( film_fit == MFnCamera::kFillFilmFit )
	{
		film_fit = film_aspect<device_aspect_ratio ?
			MFnCamera::kHorizontalFilmFit : MFnCamera::kVerticalFilmFit;
	}
	else if( film_fit == MFnCamera::kOverscanFilmFit )
	{
		film_fit = film_aspect>device_aspect_ratio ?
			MFnCamera::kHorizontalFilmFit : MFnCamera::kVerticalFilmFit;
	}

	double reference_aperture = film_fit == MFnCamera::kVerticalFilmFit ?
		vertical_aperture : horizontal_aperture;

	double pan_x = cam.horizontalPan() / reference_aperture * 2;
	double pan_y = cam.verticalPan() / reference_aperture * 2 ;

	center[0] += pan_x;
	center[1] += pan_y;
}


std::string
NSIExportUtilities::ReadColorSpace(
	const MObject &i_node,
	const MString &i_attribute_name )
{
	bool found = false;
	MString maya_cs = getAttrString( i_node, i_attribute_name, "", found );
	if( !found )
		return "auto";

	if( maya_cs == "sRGB" )
		return "sRGB";
	if( maya_cs == "Raw" || maya_cs == "linear" )
		return "linear";
	if( ::strstr( maya_cs.asChar(), "scene-linear" ) )
		return "linear";
	if( ::strstr( maya_cs.asChar(), "Rec 709" ) || maya_cs == "rec.709" )
		return "rec.709";

	/* TODO: 'gamma x.y Rec 709' which needs 3Delight support too. */

	bool maya_file_found = false;
	getAttrString(i_node, "fileTextureName", "", maya_file_found );

	if ( !maya_file_found )
		return "auto";
	else
		return maya_cs.asChar();
}

MString NSIExportUtilities::AttributeName( const MPlug &i_plug )
{
	return i_plug.partialName(
		false, /* node name  */
		false, /* non-mandatory indices */
		false, /* instanced indices */
		false, /* use Alias */
		false, /* full attribute path */
		true /* use long names */);
}

void
NSIExportUtilities::ExportScreen(
	NSIContext_t i_context,
	const RenderPassInterfaceForScreen& i_render_pass,
	const MFnCamera& i_camera,
	const MString& i_screen_handle,
	bool i_apply_display_options,
	bool i_live_render )
{
	MString camera_handle =
		NSIExportDelegate::NSIHandle( i_camera, i_live_render );

	NSI::Context nsi( i_context );
	nsi.Create( i_screen_handle.asChar(), "screen" );
	nsi.Connect( i_screen_handle.asChar(), "", camera_handle.asChar(), "screens" );

	NSI::ArgumentList screen_args;

	// resolution
	dl::V2i res( i_render_pass.ResolutionX(), i_render_pass.ResolutionY() );
	screen_args.Add(
		NSI::Argument::New( "resolution" )
		->SetArrayType( NSITypeInteger, 2 )
		->CopyValue( &res[0], sizeof(res) ) );

	float crop[2][2];
	i_render_pass.Crop( crop[0], crop[1] );

	if ( i_live_render )
	{
		/* left, top, right, bottom. */
		int pw[4] =
		{
			static_cast<int>(crop[0][0] * res[0] + 0.5f),
			static_cast<int>(crop[0][1] * res[1] + 0.5f),
			static_cast<int>(crop[1][0] * res[0] + 0.5f),
			static_cast<int>(crop[1][1] * res[1] + 0.5f)
		};

		// prioritywindow
		screen_args.Add(
			NSI::Argument::New( "prioritywindow" )
			->SetArrayType( NSITypeInteger, 2 )
			->SetCount( 2 )
			->CopyValue( &pw[0], sizeof(pw) ) );
	}
	// send crop also in live render, idisplay driver needs this info
	{
		// crop
		screen_args.Add(
			NSI::Argument::New( "crop" )
			->SetArrayType( NSITypeFloat, 2 )
			->SetCount( 2 )
			->SetValuePointer(crop) );
	}

	// oversampling & pixelaspectratio
	screen_args.Add(
		new NSI::IntegerArg("oversampling", i_render_pass.PixelSamples()) );
	screen_args.Add(
		new NSI::FloatArg("pixelaspectratio", i_render_pass.PixelAspectRatio()));

	// screenwindow

	dl::V2f size( 2.0f, 2.0f );
	dl::V2f center( 0.0f, 0.0f );
	{
		ComputeScreenSizeAndCenter( i_camera, res, size, center );
		ComputeRollAndTranslate( i_camera, size, center );
		ComputePanAndZoom( i_camera, res, size, center );
	}
	NSIExportCamera::OverrideScreen(i_camera, size, center);

	double overscan = i_apply_display_options ? i_camera.overscan() : 1.0;
	double scale = overscan/2.0;
	double sw[2][2] =
		{
			{-size[0]*scale + center[0], -size[1]*scale + center[1]},
			{+size[0]*scale + center[0], +size[1]*scale + center[1]},
		};

	screen_args.Add(
		NSI::Argument::New( "screenwindow" )
		->SetArrayType( NSITypeDouble, 2 )
		->SetCount( 2 )
		->CopyValue(sw, sizeof(sw)) );

	int pixel_filter_index = i_render_pass.PixelFilterIndex();

	//If blackman-harris-importance-filter is selected set the parameter to 1.
	if (pixel_filter_index == 1)
	{
		screen_args.Add(new NSI::IntegerArg("importancesamplefilter", 1));
	}
	nsi.SetAttribute( i_screen_handle.asChar(), screen_args );
}
