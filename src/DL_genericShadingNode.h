/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2013                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __DL_DUMMYSHADINGNODE_H__
#define __DL_DUMMYSHADINGNODE_H__

#include <maya/MPxNode.h>
#include <maya/MPxCommand.h>

class DL_genericShadingNode : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug&, MDataBlock& );

private:
  /* Called with callback when node name is first time changed and then this
   * function removes the callback. So the function is called when the node
   * is completely initialized. */
  static void nameChanged(MObject&, const MString&, void*);

public:
  /* Output attributes */
  static MObject s_color;
  static MObject s_outColor;
  static MObject s_outTransparency;
};

#endif

