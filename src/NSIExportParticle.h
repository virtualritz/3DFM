#ifndef __NSIExportParticle_h
#define __NSIExportParticle_h

#include "NSIExportDelegate.h"

/**
	\sa NSIExportDelegate
*/
class NSIExportParticle : public NSIExportDelegate
{
public:
	NSIExportParticle(
		MFnDagNode &i_dag,
		const NSIExportDelegate::Context& i_context )
	:
		NSIExportDelegate( i_dag, i_context )
	{
	}

	virtual void Create();
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual bool IsDeformed( void ) const;
	virtual void Connect( const DelegateTable * ) {};


private:
};
#endif
