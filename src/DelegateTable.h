#ifndef DelegateTable_h
#define DelegateTable_h

#include <maya/MString.h>

#include <memory>
#include <string>
#include <unordered_map>

class NSIExportDelegate;

class DelegateTable :
	public std::unordered_map<std::string, std::shared_ptr<NSIExportDelegate>>
{
public:
	/* Add this ourselves, until we switch to c++20. */
	bool contains(const key_type &key) const
	{
		return find(key) != end();
	}

	/* We often use it with MString so it's convenient to have this version. */
	bool contains(const MString &key) const
	{
		return contains(key.asChar());
	}

	/* Fix ambiguity between the above two. */
	bool contains(const char *key) const
	{
		return contains(key_type(key));
	}
};

#endif
