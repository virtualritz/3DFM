#include "NSIExportYeti.h"

#include <maya/MAnimControl.h>
#include <maya/MGlobal.h>

#include "nsi.hpp"
#include "ri.h"
#include "rif.h"

NSIExportYeti::NSIExportYeti(
	MFnDagNode &i_dagNode,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_dagNode, i_context ),
	m_dagPath( i_dagNode.fullPathName() )
{
}


void NSIExportYeti::Create()
{
	/* Just build a transform node that the procedural will connect to. */
	NSICreate( m_nsi, m_nsi_handle, "transform", 0, 0x0 );
}


void NSIExportYeti::SetAttributes()
{
}


void NSIExportYeti::SetAttributesAtTime(
	double i_time,
	bool i_no_motion )
{
	/*
		Yeti requires global time values for motion blur to work. It will end
		up producing motion samples relative to the (rounded) frame however.
	*/
	double global_time = MAnimControl::currentTime().as(MTime::uiUnit());
	double frame = global_time - i_time;

	MString command = "pgYetiRenderCommand -a -sampleTime ";
	command += global_time;
	command += " -frame ";
	command += frame;
	command += " ";
	command += m_dagPath;
	MGlobal::executeCommand( command );
}


/**
	Always consider Yeti node to be deformed. Otherwise, fur will not track a
	moving object.
*/
bool NSIExportYeti::IsDeformed() const
{
	return true;
}


class YetiRif : public RifPlugin
{
public:
	YetiRif(
		NSIContext_t i_ctx,
		const char *i_transform )
	:
		m_ctx( i_ctx ),
		m_transform( i_transform )
	{
		/* Stop all the Ri calls from going through. */
		m_rif.Filtering = RifFilter::k_Terminate;

		/* Grab RiProcedural. That seems enough. */
		m_rif.ProceduralV = MyProceduralV;
	}

	virtual RifFilter& GetFilter() { return m_rif; }

	static RtVoid MyProceduralV(
		RtPointer i_data, RtBound i_bound,
		RtVoid (*i_Subdivfunc)(RtPointer, RtFloat),
		RtVoid (*i_Freefunc)(RtPointer),
		RtInt n, RtToken tokens[], RtPointer parms[] );

private:
	RifFilter m_rif;
	/* NSI context we're exporting to. */
	NSIContext_t m_ctx;
	/* The NSI transform node we want to attach the procedural to. */
	const char *m_transform;
};


RtVoid YetiRif::MyProceduralV(
	RtPointer i_data, RtBound i_bound,
	RtVoid (*i_Subdivfunc)(RtPointer, RtFloat),
	RtVoid (*i_Freefunc)(RtPointer),
	RtInt n, RtToken tokens[], RtPointer parms[] )
{
	YetiRif *rif = static_cast<YetiRif*>( RifGetCurrentPlugin() );

	if( i_Subdivfunc == RiProcDynamicLoad )
	{
		const char *const *args = (const char*const*)i_data;
		NSI::Context ctx( rif->m_ctx );
		ctx.Evaluate( (
			NSI::StringArg( "type", "RiProcDynamicLoad" ),
			NSI::StringArg( "filename", args[0] ),
			NSI::StringArg( "parameters", args[1] ),
			NSI::StringArg( "transform", rif->m_transform )
		) );
	}

	if( i_Freefunc )
	{
		i_Freefunc( i_data );
	}
}


void NSIExportYeti::Finalize()
{
	/* Build dummy Ri interface to grab output with a filter. */
	RtContextHandle prev_ctx = RiGetContext();
	RiBegin( RI_NULL );
	YetiRif filter( m_nsi, m_nsi_handle );
	RifAddPlugin( &filter );

	MString command = "pgYetiRenderCommand -emit ";
	command += m_dagPath;
	MGlobal::executeCommand( command );

	RifRemovePlugin( &filter );
	RiEnd();
	RiContext( prev_ctx );

	/* Flush item from the command's cache. */
	command = "pgYetiRenderCommand -remove ";
	command += m_dagPath;
	MGlobal::executeCommand( command );
}
