#include "DL_packageSpecific.h"

const MString
DL_packageSpecific::getPackageDescriptionString()
{
	return MString(_3DFM_PACKAGE_NAME);
}

void
DL_packageSpecific::set3dfmRibOption()
{
}

MStatus
DL_packageSpecific::registerNSIArchiveTranslator(MFnPlugin& i_plugin)
{
	// Nothing to do for this package
	return MStatus::kSuccess;
}

MStatus
DL_packageSpecific::deregisterNSIArchiveTranslator(MFnPlugin& i_plugin)
{
	// Nothing to do for this package
	return MStatus::kSuccess;
}
