
#include <nsi.hpp>

#include <maya/MColorArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnMesh.h>
#include <maya/MGlobal.h>
#include <maya/MIntArray.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MUintArray.h>

#include <algorithm>
#include <assert.h>
#include <memory>
#include <vector>

#include "NSIExportMesh.h"
#include "dlVec.h"
#include "DL_utils.h"

NSIExportMesh::NSIExportMesh(
	MFnDagNode &i_dag,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_dag, i_context ),
	m_last_state(e_unknown)
{
	m_override_nsi_handle = NSIAttributeOverridesHandle( i_dag, m_live );
}

NSIExportMesh::NSIExportMesh(
	MFnDagNode &i_dag,
	MObject &i_mesh,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate(i_dag, i_mesh, i_context),
	m_last_state(e_unknown)
{
	m_override_nsi_handle = NSIAttributeOverridesHandle( i_dag, m_live );
}


void NSIExportMesh::Create()
{
	NSICreate( m_nsi, m_nsi_handle, "mesh", 0, 0x0 );
	NSICreate( m_nsi, m_override_nsi_handle.asChar(), "attributes", 0, 0x0 );
}


void NSIExportMesh::SetAttributes( void )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kMesh) );

	if( !IsValid() )
	{
		assert( false );
		return;
	}

	NSI::Context nsi( m_nsi );

	if( m_last_state != e_unknown )
	{
		nsi.DeleteAttribute( m_nsi_handle, "N" );
		nsi.DeleteAttribute( m_nsi_handle, "N.indices" );
		nsi.DeleteAttribute( m_nsi_handle, "subdivision.scheme" );
	}

	MFnMesh meshFn( Object() );

	MStringArray uvSetNames;
	meshFn.getUVSetNames(uvSetNames);
	unsigned int numUVSets = uvSetNames.length();

	// Retrieve vertex data
	MIntArray vertexCountPP;
	MIntArray vertexList;
	meshFn.getVertices( vertexCountPP, vertexList );

	MIntArray normalIndicesArray;
	MIntArray normalCountsArray_unused;
	meshFn.getNormalIds(normalCountsArray_unused, normalIndicesArray);

	int* nVertices = new int[vertexCountPP.length()];
	int* vertices = new int[vertexList.length()];
	int* normalIndices = new int[normalIndicesArray.length()];

	vertexCountPP.get(nVertices);
	normalIndicesArray.get(normalIndices);
	vertexList.get(vertices);

	/*
		Maya's hole API is quite complex and (too) flexible. It could
		potentially require that we reorder the vertices in vertexList.
		Fortunately, the data I've seen so far does not require it so I'm
		hoping maya will always have a nice structure with the hole vertices
		being packed together and coming after the main face vertices. The
		alternative is just too ugly to think about.

		This means that this code makes some unchecked assumptions. The code to
		check them would be messy and complex by itself which is why I have not
		written it.
	*/
	std::vector<int> nholes, alt_nvertices;
	MIntArray holeInfo, holeVertex;
	meshFn.getHoles( holeInfo, holeVertex );
	if( holeInfo.length() != 0 )
	{
		/* Count the holes in each face. */
		nholes.resize( vertexCountPP.length(), 0 );
		for( unsigned i = 0; i < holeInfo.length(); i += 3 )
		{
			++nholes[holeInfo[i]];
		}

		/* Direct index to the first loop of each face. */
		std::vector<int> first_loop;
		/*
			For each loop in the mesh, index of the first vertex in vertexList.
			Initially, only the main loop of each face is set and the holes are
			set to -1. They are filled in below. The whole point of this mess
			is to then use this to compute a new 'nvertices' array with values
			for each loop.
		*/
		std::vector<int> loop_first_vertex;
		for( unsigned i = 0, l = 0, v = 0; i < nholes.size(); ++i )
		{
			first_loop.push_back( l );
			l += 1u + nholes[i];

			loop_first_vertex.push_back( v );
			for( int j = 0; j < nholes[i]; ++j )
				loop_first_vertex.push_back( -1 );
			v += vertexCountPP[i];
		}
		/* Add an extra end value. Makes code simpler below. */
		first_loop.push_back( loop_first_vertex.size() );
		loop_first_vertex.push_back( vertexList.length() );

		int *holeVertexData = new int[holeVertex.length()];
		holeVertex.get( holeVertexData );

		for( unsigned i = 0; i < holeInfo.length(); i += 3 )
		{
			int f = holeInfo[i];
			int nv = holeInfo[i+1];
			int fv = holeInfo[i+2];

			assert( f + 1 < first_loop.size() );
			int face_begin = loop_first_vertex[first_loop[f]];
			int face_end = loop_first_vertex[first_loop[f+1]];
			const int *hole_it = std::search(
				vertices + face_begin, vertices + face_end,
				holeVertexData + fv, holeVertexData + fv + nv );
			if( hole_it == vertices + face_end )
			{
				/* Hole data is not nice. You're in trouble. */
				assert( false );
				delete[] holeVertexData;
				return;
			}
			int hole_first_vertex = hole_it - vertices;
			for( int j = first_loop[f] + 1; true; ++j )
			{
				if( j == first_loop[f+1] )
				{
					assert( false ); /* algorithm logic bug */
					break;
				}
				if( loop_first_vertex[j] == -1 )
				{
					loop_first_vertex[j] = hole_first_vertex;
					break;
				}
			}
		}

		delete[] holeVertexData;

		std::sort( loop_first_vertex.begin(), loop_first_vertex.end() );

		for( unsigned i = 0; i < loop_first_vertex.size() - 1u; ++i )
		{
			alt_nvertices.push_back(
				loop_first_vertex[i + 1] - loop_first_vertex[i] );
		}
	}

	NSI::ArgumentList mesh_args;

	if( !nholes.empty() )
	{
		mesh_args.Add( NSI::Argument::New( "nholes" )
			->SetType( NSITypeInteger )
			->SetCount( nholes.size() )
			->SetValuePointer( &nholes[0] ) );

		mesh_args.Add( NSI::Argument::New( "nvertices" )
			->SetType( NSITypeInteger )
			->SetCount( alt_nvertices.size() )
			->SetValuePointer( &alt_nvertices[0] ) );
	}
	else
	{
		mesh_args.Add( NSI::Argument::New( "nvertices" )
			->SetType( NSITypeInteger )
			->SetCount( vertexCountPP.length() )
			->SetValuePointer( nVertices ) );
	}

	mesh_args.Add( NSI::Argument::New( "P.indices" )
		->SetType( NSITypeInteger )
		->SetCount( vertexList.length() )
		->SetValuePointer( vertices ) );

	MFnDependencyNode dep( Object() );
	MObject reference_object;
	if( getReferenceObject(dep, reference_object) )
	{
		/** Reference object has same indices as P */
		mesh_args.Add( NSI::Argument::New( "Pref.indices" )
			->SetType( NSITypeInteger )
			->SetCount( vertexList.length() )
			->SetValuePointer( vertices ) );

		/** Reference object has same indices as N */
		mesh_args.Add( NSI::Argument::New( "Nref.indices" )
			->SetType( NSITypeInteger )
			->SetCount( normalIndicesArray.length() )
			->SetValuePointer( normalIndices ) );
	}

	bool is_subdivision = IsSubdivision();
	m_last_state = is_subdivision ? e_subdivision : e_polygon;

	MUintArray cornerVertices;
	MFloatArray cornerSharpness;
	MUintArray creaseVertices;
	MFloatArray creaseSharpness;

	if( !is_subdivision )
	{
		/* Don't export normals for subdivisions as it makes a poor shading. */
		mesh_args.Add( NSI::Argument::New( "N.indices" )
			->SetType( NSITypeInteger )
			->SetCount( normalIndicesArray.length() )
			->SetValuePointer( normalIndices ) );
	}
	else
	{
		/*
			This is a subdivision surface, so export creases and corners.
		*/

		MDoubleArray creaseData;
		meshFn.getCreaseVertices( cornerVertices, creaseData );

		/*
			From DL_geoConverter.cpp (RenderMan export code) :

			This will be false if you try to add creases to a poly mesh created
			from a subdiv proxy and a low-res mesh. For some reason, the edgeIds
			array contains the indices of all creases (those inherited through
			the proxy and those added) but the creaseData array only contains
			data for the newly added creases. So there's no way to export any
			crease at all.
		*/
		if( cornerVertices.length()!=0 && cornerVertices.length() == creaseData.length() )
		{
			cornerSharpness.setLength( creaseData.length() );
			for( unsigned i=0; i<creaseData.length(); i++ )
				cornerSharpness[i] = creaseData[i];

			mesh_args.Add( NSI::Argument::New( "subdivision.cornervertices" )
				->SetType( NSITypeInteger )
				->SetCount( cornerVertices.length() )
				->SetValuePointer( &cornerVertices[0] ) );

			mesh_args.Add( NSI::Argument::New( "subdivision.cornersharpness" )
				->SetType( NSITypeFloat )
				->SetCount( cornerSharpness.length() )
				->SetValuePointer( &cornerSharpness[0] ) );
		}

		MUintArray edgeIds;
		meshFn.getCreaseEdges( edgeIds, creaseData );

		/* See comment above regarding this condition */
		if( edgeIds.length()!=0 && edgeIds.length()==creaseData.length() )
		{
			creaseVertices.setLength( creaseData.length()*2 );
			creaseSharpness.setLength( creaseData.length() );
			for( unsigned i=0; i<creaseData.length(); i++ )
			{
				int vertices[2];
				meshFn.getEdgeVertices( edgeIds[i], vertices );

				creaseVertices[i*2 + 0] = vertices[0];
				creaseVertices[i*2 + 1] = vertices[1];

				creaseSharpness[i] = creaseData[i];
			}

			mesh_args.Add( NSI::Argument::New( "subdivision.creasevertices" )
				->SetType( NSITypeInteger )
				->SetCount( creaseVertices.length() )
				->SetValuePointer( &creaseVertices[0] ) );

			mesh_args.Add( NSI::Argument::New( "subdivision.creasesharpness" )
				->SetType( NSITypeFloat )
				->SetCount( creaseSharpness.length() )
				->SetValuePointer( &creaseSharpness[0] ) );
		}
	}

	// UVs
	std::vector<std::unique_ptr<float[]>> allUVs;
	std::vector<std::unique_ptr<int[]>> allUVIndices;

	if( numUVSets > 0 )
	{
		allUVs.resize(numUVSets);
		allUVIndices.resize(numUVSets);

		for(int i = 0; i < numUVSets; i++)
		{
			MFloatArray us, vs;
			meshFn.getUVs(us, vs, &uvSetNames[i]);

			if(us.length() == 0 || vs.length() == 0)
			{
				// Wrong or emtpy uv set, skip it.
				continue;
			}

			// Add an extra entry for the possible undefined UV
			int numUVs = us.length() + 1;
			int undefinedUVIndex = numUVs - 1;

			allUVs[i].reset(new float[numUVs * 2]);
			MergeUVs(us, vs, allUVs[i].get());

			// Define whatever value for an undefined UV
			allUVs[i][2 * undefinedUVIndex] = 1e10;
			allUVs[i][2 * undefinedUVIndex + 1] = 1e10;

			std::string uvName = uvSetNames[i].asChar();

			// Rename first UV set as "st", maya default uv set is called "map1"
			if (i == 0)
				uvName = "st";

			mesh_args.Add( NSI::Argument::New( uvName )
				->SetArrayType( NSITypeFloat, 2 )
				->SetCount( numUVs )
				->SetValuePointer( allUVs[i].get() )
				->SetFlags(NSIParamIsArray) );

			// Retrieve the UV indices
			MIntArray uvCounts, uvIDs;
			meshFn.getAssignedUVs(uvCounts, uvIDs, &uvSetNames[i]);

			allUVIndices[i].reset(new int[vertexList.length()]);
			bool valid = GetUVIndices(
				allUVIndices[i].get(),
				uvIDs,
				vertexList.length(),
				uvCounts,
				nVertices,
				vertexCountPP.length(),
				undefinedUVIndex);

			if( valid )
			{
				// Add NSI param for the UV indices
				std::string uvIndicesName = uvName;
				uvIndicesName += ".indices";
				mesh_args.Add( NSI::Argument::New( uvIndicesName )
					->SetType( NSITypeInteger )
					->SetCount( vertexList.length() )
					->SetValuePointer( allUVIndices[i].get() ) );
			}
		}
	}

	/* Vertex colors */
	MStringArray color_sets_names;
	meshFn.getColorSetNames(color_sets_names);
	MColor default_unset_color(0.0f, 0.0f, 0.0f);
	std::string indicesName;

	for( int i=0; i<color_sets_names.length(); i++ )
	{
		if( color_sets_names[i].length()==0 )
			continue;

		MColorArray vertex_colors;
		std::vector<dl::V3f> nsicolors;
		meshFn.getVertexColors(
			vertex_colors, &color_sets_names[i], &default_unset_color );
		assert( vertex_colors.length() == meshFn.numVertices() );

		nsicolors.reserve( vertex_colors.length() );

		for( int j=0; j<vertex_colors.length(); j++ )
		{
			nsicolors.push_back(
				dl::V3f(
					vertex_colors[j][0],
					vertex_colors[j][1],
					vertex_colors[j][2]) );
		}

		mesh_args.Add(
			NSI::Argument::New( color_sets_names[i].asChar() )
			->SetType( NSITypeColor )
			->SetCount( meshFn.numVertices() )
			->CopyValue( (float*)(&nsicolors[0]),
				vertex_colors.length()*sizeof(float)*3) );

		indicesName = color_sets_names[i].asChar();
        indicesName += ".indices";
		mesh_args.Add( NSI::Argument::New(
			indicesName )
			->SetType( NSITypeInteger )
			->SetCount( vertexList.length() )
			->SetValuePointer( vertices ) );
	}

	if( is_subdivision )
	{
		mesh_args.Add(
			new NSI::CStringPArg("subdivision.scheme", "catmull-clark") );
	}

	nsi.SetAttribute( m_nsi_handle, mesh_args );

 	delete[] nVertices;
 	delete[] vertices;
	delete[] normalIndices;

	MFnDependencyNode depFn( Object() );

	MStatus status;
	int priority = 2;

	NSI::ArgumentList arg_list;

	// Visible to camera override
	bool enable_vis_camera_over = false;
	MPlug enable_vis_camera_plug =
		depFn.findPlug( "_3delight_enableVisibilityCameraOverride", true );
	enable_vis_camera_plug.getValue( enable_vis_camera_over );

	if( enable_vis_camera_over )
	{
		bool vis_camera_over = false;
		MPlug vis_camera_plug =
			depFn.findPlug( "_3delight_visibilityCameraOverride", true );
		vis_camera_plug.getValue( vis_camera_over );

		arg_list.Add(new NSI::IntegerArg("visibility.camera", vis_camera_over));
		arg_list.Add(new NSI::IntegerArg("visibility.camera.priority", 10));
	}
	else
	{
		nsi.DeleteAttribute( m_override_nsi_handle.asChar(), "visibility.camera" );
		nsi.DeleteAttribute(
			m_override_nsi_handle.asChar(), "visibility.camera.priority" );
	}

	// Visible in Diffuse override
	bool enable_vis_diffuse_over = false;
	MPlug enable_vis_diffuse_plug =
		depFn.findPlug( "_3delight_enableVisibilityDiffuseOverride", true );
	enable_vis_diffuse_plug.getValue( enable_vis_diffuse_over );

	if( enable_vis_diffuse_over )
	{
		bool vis_diffuse_over = false;
		MPlug vis_diffuse_plug =
			depFn.findPlug( "_3delight_visibilityDiffuseOverride", true );
		vis_diffuse_plug.getValue( vis_diffuse_over );

		arg_list.Add(new NSI::IntegerArg("visibility.diffuse", vis_diffuse_over));
		arg_list.Add(new NSI::IntegerArg("visibility.diffuse.priority", 10));
	}
	else
	{
		nsi.DeleteAttribute( m_override_nsi_handle.asChar(), "visibility.diffuse" );
		nsi.DeleteAttribute(
			m_override_nsi_handle.asChar(), "visibility.diffuse.priority" );
	}

	// Visible in Reflections override
	bool enable_vis_reflections_over = false;
	MPlug enable_vis_reflections_plug =
		depFn.findPlug( "_3delight_enableVisibilityReflectionsOverride", true );
	enable_vis_reflections_plug.getValue( enable_vis_reflections_over );

	if( enable_vis_reflections_over )
	{
		bool vis_reflections_over = false;
		MPlug vis_reflections_plug =
			depFn.findPlug( "_3delight_visibilityReflectionsOverride", true );
		vis_reflections_plug.getValue( vis_reflections_over );

		arg_list.Add(new NSI::IntegerArg("visibility.reflection", vis_reflections_over));
		arg_list.Add(new NSI::IntegerArg("visibility.reflection.priority", 10));
	}
	else
	{
		nsi.DeleteAttribute( m_override_nsi_handle.asChar(), "visibility.reflection" );
		nsi.DeleteAttribute(
			m_override_nsi_handle.asChar(), "visibility.reflection.priority" );
	}

	// Visible in Refractions override
	bool enable_vis_refractions_over = false;
	MPlug enable_vis_refractions_plug =
		depFn.findPlug( "_3delight_enableVisibilityRefractionsOverride", true );
	enable_vis_refractions_plug.getValue( enable_vis_refractions_over );

	if( enable_vis_refractions_over )
	{
		bool vis_refractions_over = false;
		MPlug vis_refractions_plug =
			depFn.findPlug( "_3delight_visibilityRefractionsOverride", true );
		vis_refractions_plug.getValue( vis_refractions_over );

		arg_list.Add(new NSI::IntegerArg("visibility.refraction", vis_refractions_over));
		arg_list.Add(new NSI::IntegerArg("visibility.refraction.priority", 10));
	}
	else
	{
		nsi.DeleteAttribute( m_override_nsi_handle.asChar(), "visibility.refraction" );
		nsi.DeleteAttribute(
			m_override_nsi_handle.asChar(), "visibility.refraction.priority" );
	}


	// Compositing mode override
	bool enable_comp_over = false;
	MPlug enable_comp_plug =
		depFn.findPlug( "_3delight_enableCompositingModeOverride", true );
	enable_comp_plug.getValue( enable_comp_over );

	if( enable_comp_over )
	{
		int comp_mode_over = 0;
		MPlug comp_over_plug =
			depFn.findPlug( "_3delight_compositingModeOverride", true );
		comp_over_plug.getValue( comp_mode_over );

		bool matte = comp_mode_over == 1;
		bool prelit = comp_mode_over == 2;

		arg_list.Add(new NSI::IntegerArg("matte", matte));
		arg_list.Add(new NSI::IntegerArg("matte.priority", 10));
		arg_list.Add(new NSI::IntegerArg("prelit", prelit));
		arg_list.Add(new NSI::IntegerArg("prelit.priority", 10));
	}
	else
	{
		nsi.DeleteAttribute( m_override_nsi_handle.asChar(), "matte" );
		nsi.DeleteAttribute(
			m_override_nsi_handle.asChar(), "matte.priority" );
		nsi.DeleteAttribute( m_override_nsi_handle.asChar(), "prelit" );
		nsi.DeleteAttribute(
			m_override_nsi_handle.asChar(), "prelit.priority" );
	}

	// Set all override attributes defined above
	if( arg_list.size() > 0 )
	{
		nsi.SetAttribute( m_override_nsi_handle.asChar(), arg_list );
	}

	/**
		Adjust connections to make the override effective or not according to its
		main override toggle.
	*/
	bool enable_override = false;
	MPlug override_plug =	depFn.findPlug( "_3delight_isOverrideVolume", true );
	override_plug.getValue( enable_override );

	if( enable_override )
	{
		nsi.Connect( m_nsi_handle, "",
			m_override_nsi_handle.asChar(), "bounds" );
		nsi.Connect( m_override_nsi_handle.asChar(), "",
			NSI_SCENE_ROOT, "geometryattributes" );
	}
	else
	{
		nsi.Disconnect( m_nsi_handle, "",
			m_override_nsi_handle.asChar(), "bounds" );
		nsi.Disconnect( m_override_nsi_handle.asChar(), "",
			NSI_SCENE_ROOT, "geometryattributes" );
	}
}

/**
	Only output P and N data at the given time, all other attributes are not
	time dependent. Note that P.indices and N.indices are aso declared only
	once per frame.
*/
void NSIExportMesh::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kMesh) );

	MFnMesh meshFn( Object() );

	MStatus status;
	const float* points = meshFn.getRawPoints(&status);
	const float* normals = meshFn.getRawNormals(&status);

	if( status != MStatus::kSuccess )
	{
		/* This could happen if object not completely created yet. */
		return;
	}

	NSI::Context nsi( m_nsi );

	NSI::ArgumentList mesh_args;
	mesh_args.Add(
		NSI::Argument::New( "P" )
		->SetType( NSITypePoint )
		->SetCount( meshFn.numVertices() )
		->SetValuePointer( const_cast<float*>(points) ) );

	if( !IsSubdivision() )
	{
		mesh_args.Add( NSI::Argument::New( "N" )
			->SetType( NSITypeNormal )
			->SetCount( meshFn.numNormals() )
			->SetValuePointer( const_cast<float*>(normals) ) );
	}

	MObject reference_object;

	MFnDependencyNode dep( Object() );
	if( getReferenceObject(dep, reference_object) )
	{
		MFnMesh reference_mesh_fn;
		MStatus status = reference_mesh_fn.setObject( reference_object );
		if( status == MStatus::kSuccess )
		{
			const float* Pref = reference_mesh_fn.getRawPoints(&status);
			mesh_args.Add(
				NSI::Argument::New( "Pref" )
				->SetType( NSITypePoint )
				->SetCount( meshFn.numVertices() )
				->SetValuePointer( const_cast<float*>(Pref) ) );

			const float* Nref = reference_mesh_fn.getRawNormals(&status);
			mesh_args.Add(
				NSI::Argument::New( "Nref" )
				->SetType( NSITypeNormal )
				->SetCount( meshFn.numVertices() )
				->SetValuePointer( const_cast<float*>(Nref) ) );
		}

		if( status != MStatus::kSuccess )
		{
			MFnDagNode dag( Object() );
			MGlobal::displayWarning(
				"3Delight: couldn't process texture reference for " +
				dag.fullPathName() );
		}
	}

	if( i_no_motion )
		nsi.SetAttribute( m_nsi_handle, mesh_args );
	else
		nsi.SetAttributeAtTime( m_nsi_handle, i_time, mesh_args );
}

/**
	\brief Merges two floats representing UVs into one array.
*/
void NSIExportMesh::MergeUVs(
	const MFloatArray& i_Us, const MFloatArray& i_Vs, float* o_UVs )
{
	if( i_Us.length() != i_Vs.length() )
	{
		assert( false );
		return;
	}

	/*
		Maxime said:

		An alternative would be:

		- Allocate a float* us, float* vs
	    - Use MFloatArray::get(us), (vs)
	    - loop over these to copy each u, v to o_UVs.

		It appeared to be much slower than this simple loop.
	*/
	for(unsigned i = 0; i < i_Us.length(); i++)
	{
		o_UVs[2 * i] = i_Us[i];
		o_UVs[2 * i + 1] = i_Vs[i];
	}
}

bool NSIExportMesh::GetUVIndices(
	int* o_uvIndices,
	const MIntArray& i_uvIDs,
	unsigned int i_numExpectedIndices,
	const MIntArray& i_uvCounts,
	const int* const i_nVertices,
	int i_nVerticesLength,
	int i_undefinedUVIndex)
{
	// Fill o_uvIndices with i_numExpectedIndices, which should be 1 uv index per
	// face vertex.
	//
	// uvIDs is not guaranteed to contain that many indices. If it does not, we
	// copy the defined uvs (as indicated by i_uvCount) and fill the holes with
	// i_undefinedUVIndex.
	//
	if(i_uvIDs.length() == i_numExpectedIndices)
	{
		// This is the most common case - each face vertex has 1 UV index
		i_uvIDs.get(o_uvIndices);
	}
	else
	{
		/*
			Fill in missing UV indices with an index to the undefined UV that
			has been added to the UV set above.

			The assert below can happen in a live session when deleting an
			object.
		*/
		assert(i_uvCounts.length() == i_nVerticesLength);

		if( i_uvCounts.length() != i_nVerticesLength )
			return false;

		// Iterate on each polygon
		//
		for(unsigned j = 0, srcIndex = 0, dstIndex = 0;
		 j < i_nVerticesLength;
		 j++)
		{
			// Copy the defined uv indices
			unsigned k;
			for(k = 0; k < i_uvCounts[j]; k++)
			{
				assert(dstIndex < i_numExpectedIndices);
				assert(srcIndex < i_uvIDs.length());

				o_uvIndices[dstIndex++] = i_uvIDs[srcIndex++];
			}

			// For the remaining face vertices, use i_undefinedUVIndex
			for(; k < i_nVertices[j]; k++)
			{
				assert(dstIndex < i_numExpectedIndices);
				o_uvIndices[dstIndex++] = i_undefinedUVIndex;
			}
		}
	}

	return true;
}

/**
	\brief Returns true if this mesh is a subdivision surface.
*/
bool NSIExportMesh::IsSubdivision( void ) const
{
	bool is_subdivision =
		getAttrInt( Object(), "displaySmoothMesh", 0 ) ;

	if( !is_subdivision )
	{
		if( m_set.objectRef() != MObject::kNullObj )
		{
			bool found;
			bool enabled =
				getAttrBool( m_set.objectRef(), "enablePolyAsSubd", false, found );

			if( enabled )
			{
				is_subdivision =
					getAttrBool( m_set.objectRef(), "polyAsSubd", false, found );

				assert( found );
				if( found )
					return is_subdivision;
			}
		}

		is_subdivision =
			getAttrInt( Object(), "_3delight_poly_as_subd", 0) > 0;
	}

	return is_subdivision;
}

/**
	\brief We consider the presence of a deformer as a good indication
	of deformation.

	For a more thorough check we should check if any of the deformer's
	parameters are changed in time, but that might be overkill.

	FIXME: is "inMesh" the only way to connect a deformer ?
*/
bool NSIExportMesh::IsDeformed( void ) const
{
	MStatus status;

	MFnDependencyNode dep( Object() );
	MPlug plug = dep.findPlug( "inMesh", status );

	if( status != MStatus::kSuccess )
		return false;

	MPlugArray upstream;
	plug.connectedTo( upstream, true, false );

	for( int i=0; i<upstream.length(); i++ )
	{
		const MObject &node = upstream[i].node();

		if( node.hasFn(MFn::kPluginDeformerNode) )
			return true;

		if( node.hasFn(MFn::kDeformFunc) )
			return true;

		if( node.hasFn(MFn::kNonLinear) )
			return true;
	}

	return false;
}

void NSIExportMesh::MemberOfSet( MObject& i_set )
{
	m_set = MObjectHandle( i_set );
}
