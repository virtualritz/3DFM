#include "NSIExportCamera.h"
#include "NSIExportUtilities.h"
#include "RenderPassInterface.h"
#include "DL_utils.h"
#include "OSLUtils.h"
#include "dlVec.h"

#include "dlViewport.h"

#include <assert.h>
#include <maya/MFnCamera.h>
#include <maya/MDistance.h>
#include <maya/MMatrix.h>
#include <maya/MIntArray.h>
#include <maya/MUuid.h>
#include <math.h>

#include <nsi.hpp>

NSIExportCamera::NSIExportCamera(
	const RenderPassInterfaceForCamera &i_render_pass,
	MFnDagNode &i_dag,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_dag, i_context ),
	m_render_pass(i_render_pass)
{
}

NSIExportCamera::NSIExportCamera(
	const RenderPassInterfaceForCamera &i_render_pass,
	MFnDagNode &i_dag,
	const NSIExportDelegate::Context& i_context,
	const MString & i_handle )
:
	NSIExportDelegate( i_dag, i_context, i_handle ),
	m_render_pass( i_render_pass )
{
}

void NSIExportCamera::Create()
{
	NSICreate( m_nsi, m_nsi_handle, CameraNodeType(), 0, 0x0 );

	NSI::Context ctx( m_nsi );

	m_lens_shader_handle = MString(Handle()) + "_lens_shader";
	ctx.Create( m_lens_shader_handle.asChar(), "shader" );
}

void NSIExportCamera::SetAttributesAtTime( double i_time, bool i_no_motion )
{
	return;
}

/**
	Some notes:

	- We must be careful not to enable depth of field if we have and
	override in the render pass affecting it.
	- Focal length is in millimeters
	- Focal distance is in scene units.
	- focusRegionScale seems to have no accessor in MFnCamera ?
*/
void NSIExportCamera::SetDepthOfField(
	const MFnCamera &i_camera, NSI::ArgumentList &o_arguments ) const
{
	if( i_camera.isOrtho() )
	{
		return;
	}

	double focus_region_scale = getAttrDouble(Object(),"focusRegionScale",1.0);
	double focal_length = i_camera.focalLength();
	double lens_squeeze = i_camera.lensSqueezeRatio();
	double focal_distance = i_camera.focusDistance();
	double fstop = i_camera.fStop() * focus_region_scale;
	MDistance fd( focal_distance, MDistance::uiUnit() );

	int enable =
		!m_render_pass.DisableDepthOfField() && i_camera.isDepthOfField();

	o_arguments.Add( new NSI::IntegerArg("depthoffield.enable", enable) );
	o_arguments.Add( new NSI::DoubleArg("depthoffield.fstop", fstop) );
	o_arguments.Add(
		new NSI::DoubleArg("depthoffield.focallength", focal_length/10.0) );
	o_arguments.Add(
		new NSI::DoubleArg("depthoffield.focallengthratio", lens_squeeze) );
	o_arguments.Add(
		new NSI::DoubleArg("depthoffield.focaldistance", fd.asCentimeters()) );

	int use_aperture = getAttrInt(Object(), "_3delight_use_aperture", 0);
	int number_blades = getAttrInt(Object(), "_3delight_number_of_blades", 5);
	double rotation = getAttrDouble(Object(), "_3delight_aperture_rotation", 0);

	o_arguments.Add(
		new NSI::IntegerArg( "depthoffield.aperture.enable", use_aperture) );
	o_arguments.Add(
		new NSI::IntegerArg( "depthoffield.aperture.sides", number_blades ));
	o_arguments.Add(
		new NSI::DoubleArg( "depthoffield.aperture.angle", rotation) );
}

void NSIExportCamera::SetAttributes( void )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kCamera) );

	NSI::Context ctx( m_nsi );
	MFnCamera camera( Object() );

	NSI::ArgumentList camera_args;

	SetDepthOfField( camera, camera_args );

	/* Setup clipping planes */
	double clipping[2] = {camera.nearClippingPlane(), camera.farClippingPlane()};
	camera_args.Add(
		NSI::Argument::New( "clippingrange" )
		->SetType( NSITypeDouble )
		->SetCount( 2 )
		->CopyValue( clipping, sizeof(clipping) ) );

	/* Setup shutter angle */
	double shutter_angle =
		getAttrDouble( Object(), "shutterAngle", 360.0 );
	double shutter_interval =
		m_render_pass.DisableMotionBlur() ? 0 : shutter_angle / (2.0 * M_PI);
	double shutter_open_offset = -shutter_interval / 2.0;
	double camera_shutter[2] =
	{
		-shutter_interval / 2.0,
		shutter_interval / 2.0
	};
	camera_args.Add(
		NSI::Argument::New( "shutterrange" )
		->SetType( NSITypeDouble )
		->SetCount( 2 )
		->CopyValue( camera_shutter, sizeof(camera_shutter) ) );

	double shutter_efficiency[2] =
	{
		0.5 * (1 - getAttrDouble( Object(), "_3delight_shutter_open_efficiency", 1.0/3.0)),
		1 - 0.5 * (1 - getAttrDouble(Object(), "_3delight_shutter_close_efficiency", 2.0/3.0))
	};
	camera_args.Add(
		NSI::Argument::New("shutteropening")
		->SetType(NSITypeDouble)
		->SetCount(2)
		->CopyValue(shutter_efficiency, sizeof(shutter_efficiency)));

	if(!camera.isOrtho())
	{
		/* Deal with projection type attributes */
		float fov = NSIExportUtilities::ComputeFieldOfView( camera );
		double alt_fov = getAttrDouble( Object(), "_3delight_fov", 180.0 );
		int projection_type = getAttrInt( Object(), "_3delight_projectionType", 0 );

		switch( projection_type )
		{
			case 1: /* cylindrical has both a vertical and a horizontal fov */
				/* alt_fov is already in degrees, so there is no need to convert.*/
				camera_args.Add(
					new NSI::FloatArg( "horizontalfov", alt_fov) );
				/* Fall through */
			case 0:
			{
				camera_args.Add( new NSI::FloatArg( "fov", fov*360.0f/(2*M_PI)) );

				int distortion_type =
					getAttrInt( Object(), "_3delight_distortion_type", 0 );

				if(distortion_type > 0)
				{
					double distortion_intensity =
						getAttrDouble( Object(), "_3delight_distortion_intensity", 0.0 );

					if (distortion_type == 2) // Pincushion Distortion, invert k2
						distortion_intensity *= -1;

					// Milder distortion so 0-1 range is usable
					distortion_intensity *= 0.2;

					MString lens_shader_fullPath = OSLUtils::GetFullpathname("dlLensDistortion");
					ctx.SetAttribute( m_lens_shader_handle.asChar(),
						(
						NSI::StringArg("shaderfilename", lens_shader_fullPath.asChar() ),
						NSI::FloatArg("k1", 0),
						NSI::FloatArg("k2", distortion_intensity),
						NSI::FloatArg("k3", 0)
						));

					ctx.Connect( m_lens_shader_handle.asChar(), "", Handle(), "lensshader");
				}
			}
				break;
			case 2: case 3: case 4: case 5:
			{
				const char *mappings[] =
					{"stereographic", "equidistant", "equisolidangle", "orthographic"};
				camera_args.Add( new NSI::FloatArg( "fov", alt_fov) );
				camera_args.Add(
					new NSI::CStringPArg( "mapping", mappings[projection_type-2]) );
			}
				break;
		}
	}

	/* Export camera AR. This is used by the projection shader. */
	camera_args.Add(
		new NSI::FloatArg( "maya_aspectRatio", camera.aspectRatio() ) );

	ctx.SetAttribute( m_nsi_handle, camera_args );
}

/*
	This allows some cameras to override the screen window calculation.
*/
void NSIExportCamera::OverrideScreen(
	const MFnCamera &i_camera,
	dl::V2f &io_screen_size,
	dl::V2f &io_screen_center)
{
	int projection_type = getAttrInt(
		i_camera.object(), "_3delight_projectionType", 0 );
	/* Spherical camera needs a fixed screen window to get the whole sphere. */
	if( projection_type == 6 )
	{
		io_screen_size = dl::V2f{2.0f, 2.0f};
		io_screen_center = dl::V2f{0.0f, 0.0f};
	}
}

const char *NSIExportCamera::CameraNodeType() const
{
	MFnCamera cam( Object() );
	if( cam.isOrtho() )
	{
		return "orthographiccamera";
	}

	int projection_type = getAttrInt( Object(), "_3delight_projectionType", 0 );
	switch( projection_type )
	{
		case 0: return "perspectivecamera";
		case 1: return "cylindricalcamera";
		case 2: return "fisheyecamera";
		case 3: return "fisheyecamera";
		case 4: return "fisheyecamera";
		case 5: return "fisheyecamera";
		case 6: return "sphericalcamera";
	}

	return "perspectivecamera";
}

