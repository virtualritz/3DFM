#ifndef __DL_nsiArchiveTranslator_h
#define __DL_nsiArchiveTranslator_h

#include <maya/MPxFileTranslator.h>

/// Allows a scene to be exported to an NSI archive from Maya's File menu.
class DL_nsiArchiveTranslator : public MPxFileTranslator
{
public:
	DL_nsiArchiveTranslator();
	virtual ~DL_nsiArchiveTranslator();

	static void* creator();

	virtual MStatus writer(
		const MFileObject& i_file,
		const MString& i_options,
		FileAccessMode i_mode );
	
	virtual bool haveReadMethod() const;
	virtual bool haveWriteMethod() const;
	virtual MString defaultExtension() const;
};

#endif
