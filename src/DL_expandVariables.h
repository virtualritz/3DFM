#ifndef __DL_EXPANDVARIABLES_H__
#define __DL_EXPANDVARIABLES_H__

#include <stdio.h>
#include <maya/MPxCommand.h>
#include <maya/MString.h>

class DL_expandVariables : public MPxCommand
{
public:
  virtual MStatus       doIt(const MArgList& args);

  virtual bool          isUndoable() const
                        { return false; }

  static void*          creator()
                        { return new DL_expandVariables(); }

  static MSyntax        newSyntax();
private:
};

#endif
