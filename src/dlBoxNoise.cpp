#include "dlBoxNoise.h"

#include <maya/MFloatVector.h>
#include <maya/MObjectArray.h>

#include "DL_autoLoadOSL.h"
#include "DL_utils.h"
#include "OSLUtils.h"

#include <cassert>

MObject dlBoxNoise::s_color;
MObject dlBoxNoise::s_outColor;

#define NUM_ATTRIBUTES 17

void* dlBoxNoise::creator()
{
	return new dlBoxNoise();
}

MStatus dlBoxNoise::initialize()
{
	MStringArray shaderPaths = OSLUtils::GetBuiltInSearchPaths();
	MString shaderName( "dlBoxNoise" );
	DlShaderInfo *info;

	OSLUtils::OpenShader( shaderName, shaderPaths, info );

	MObjectArray objects;
	MStringArray objectNames;

	DL_OSLShadingNode::CreateAttributesFromShaderParameters(
		info, 0x0, &objects, &objectNames );

	if(
		!DL_OSLShadingNode::FindAttribute(
			"color.color_Color", s_color, objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"outColor", s_outColor, objects, objectNames) )
	{
		assert(false);
		// It makes no sense to return success here, but if we don't, Maya crashes.
		return MStatus::kSuccess;
	}

	for( unsigned i = 0; i < objects.length(); i++ )
	{
		addAttribute( objects[ i ] );
	}

	attributeAffects( s_color, s_outColor );

	MString name = info->shadername().c_str();
	MString niceName = OSLUtils::GetShaderNiceName( info );
	DL_OSLShadingNode::DefineShaderNiceName( name, niceName );

	return MStatus::kSuccess;
}

void dlBoxNoise::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlBoxNoise::compute(
	const MPlug& i_plug,
	MDataBlock& i_block )
{
	if ((i_plug != s_outColor) && (i_plug.parent() != s_outColor))
	{
		return MS::kUnknownParameter;
	}
  /*
    FIXME: set s_outColor to average(colors in s_color)
    note that the Maya Ramp node seem to merely pass the first ramp color.
  */
	return MS::kSuccess;
}
