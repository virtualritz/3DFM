#include "NSIExportSet.h"

#include "DelegateTable.h"
#include "DL_utils.h"
#include "MU_typeIds.h"
#include "NSIExportPriorities.h"
#include "NSIExportUtilities.h"

#include <maya/MDagPath.h>
#include <maya/MFnSet.h>
#include <maya/MItSelectionList.h>
#include <maya/MPlug.h>
#include <maya/MSelectionList.h>

#include <nsi.hpp>

static const char *k_bool_attributes[] =
	{
		"enableCompositingMode",
		"enableDiffuseVisibility",
		"enableRefractionVisibility",
		"enableReflectionVisibility",
		"enableShadowVisibility",
		"enableCameraVisibility"
	};

#include <assert.h>
NSIExportSet::NSIExportSet(
	MFnDependencyNode &i_dag,
	MObject &i_object,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate(i_dag, i_object, i_context)
{
}


/**
	\brief Iterate through all the sets and connect the members.
*/
void NSIExportSet::Connect( const DelegateTable *i_dag_hash )
{
	assert( Object().hasFn(MFn::kSet) );

	MFnSet set( Object() );

	MSelectionList list;
	MStatus status = set.getMembers( list, false /* don't flatten*/ );

	if( status != MStatus::kSuccess )
		return;

	NSI::Context nsi( m_nsi );
	MItSelectionList iter( list );
	for ( ; !iter.isDone(); iter.next() )
	{
		MDagPath item;
		MObject component;
		iter.getDagPath( item, component );

		MFnDagNode node( item, &status );

		if( status != MStatus::kSuccess )
			continue;

		MString source_node = NSIHandle( node, m_live );

		if( i_dag_hash && !i_dag_hash->contains(source_node) )
		{
			continue;
		}

		nsi.Connect(
			source_node.asChar(), "",
			m_nsi_handle, "objects" );
	}

	nsi.Connect(
		NSIAttributesHandle( Object(), m_live ).asChar(), "",
		NSIHandle( Object(), m_live ).asChar(), "geometryattributes" );

	for( auto A : k_bool_attributes )
	{
		AdjustAttributeConnection( A );
	}
}

void NSIExportSet::Create()
{
	NSICreate( m_nsi, m_nsi_handle, "set", 0, 0x0 );
	NSICreate(
		m_nsi,
		NSIAttributesHandle( Object(), m_live ).asChar(),
		"attributes",
		0, 0x0 );

	for( auto A : k_bool_attributes )
	{
		NSICreate(
			m_nsi,
			NSIDlSetAttributesHandle( Object(), A ).asChar() ,
			"attributes", 0, 0x0 );
	}
}

void NSIExportSet::SetAttributes( void )
{
	MFnSet setFn( Object() );
	if( setFn.typeId() != MU_typeIds::DL_SET )
		return;

	bool found;
	int compositingMode = getAttrInt( Object(), "compositingMode", 0, found);

	int camera = getAttrInt( Object(), "cameraVisibility", 1, found);
	int diffuse = getAttrInt( Object(), "diffuseVisibility", 1, found);
	int reflection = getAttrInt( Object(), "reflectionVisibility", 1, found);
	int refraction = getAttrInt( Object(), "refractionVisibility", 1, found);
	int shadow = getAttrInt( Object(), "shadowVisibility", 1, found);

	bool matte = compositingMode == 1;
	bool prelit = compositingMode == 2;

	NSI::Context nsi( m_nsi );
	nsi.SetAttribute(
		NSIDlSetAttributesHandle( Object(), "enableCompositingMode" ).asChar(),
		(
			NSI::IntegerArg( "matte", matte ),
			NSI::IntegerArg( "matte.priority", NSI_MATTE_SET_PRIORITY ),
			NSI::IntegerArg( "prelit", prelit ),
			NSI::IntegerArg( "prelit.priority", NSI_PRELIT_SET_PRIORITY )
		) );

	nsi.SetAttribute(
		NSIDlSetAttributesHandle( Object(), "enableCameraVisibility" ).asChar(),
		(
			NSI::IntegerArg( "visibility.camera", camera ),
			NSI::IntegerArg( "visibllity.camera.priority",
				NSI_VISIBILITY_SET_PRIORITY )
		) );

	nsi.SetAttribute(
		NSIDlSetAttributesHandle( Object(), "enableDiffuseVisibility" ).asChar(),
		(
			NSI::IntegerArg( "visibility.diffuse", diffuse ),
			NSI::IntegerArg( "visibllity.diffuse.priority",
				NSI_VISIBILITY_SET_PRIORITY )
		) );

	nsi.SetAttribute(
		NSIDlSetAttributesHandle( Object(), "enableReflectionVisibility" ).asChar(),
		(
			NSI::IntegerArg( "visibility.reflection", reflection ),
			NSI::IntegerArg( "visibllity.reflection.priority",
				NSI_VISIBILITY_SET_PRIORITY )
		) );

	nsi.SetAttribute(
		NSIDlSetAttributesHandle( Object(), "enableRefractionVisibility" ).asChar(),
		(
			NSI::IntegerArg( "visibility.refraction", refraction ),
			NSI::IntegerArg( "visibllity.refraction.priority",
				NSI_VISIBILITY_SET_PRIORITY )
		) );

	nsi.SetAttribute(
		NSIDlSetAttributesHandle( Object(), "enableShadowVisibility" ).asChar(),
		(
			NSI::IntegerArg( "visibility.shadow", shadow ),
			NSI::IntegerArg( "visibllity.shadow.priority",
				NSI_VISIBILITY_SET_PRIORITY )
		) );

}

void NSIExportSet::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
}

void NSIExportSet::AdjustAttributeConnection(
	const char* i_enable_attr )
{
	MFnSet setFn( Object() );
	if( setFn.typeId() != MU_typeIds::DL_SET )
		return;

	bool found;
	bool enable = getAttrBool( Object(), i_enable_attr, false, found );
	NSI::Context nsi( m_nsi );

	if( enable )
	{
		nsi.Connect(
			NSIDlSetAttributesHandle(Object(), i_enable_attr).asChar(), "",
			NSIHandle( Object(), m_live ).asChar(), "geometryattributes" );
	}
	else
	{
		nsi.Disconnect(
			NSIDlSetAttributesHandle(Object(), i_enable_attr).asChar(), "",
			NSIHandle( Object(), m_live ).asChar(), "geometryattributes" );
	}
}

void NSIExportSet::AttributeChangeCallback(
	MNodeMessage::AttributeMessage i_msg,
	MPlug& i_plug,
	MPlug& i_other_plug,
	void* i_data)
{
	bool need_sync = false;
	NSIExportSet* delegate = (NSIExportSet*) i_data;

	// 4d8a41fba83eab72afb6837c0a603d0c8f8fad1c
	i_plug.asString();

	if( i_msg & MNodeMessage::kAttributeSet )
	{
		MString attr_name = NSIExportUtilities::AttributeName( i_plug );

		const char *enable = attr_name.asChar();
		bool is_enable = strstr(enable, "enable") == enable;
		if( is_enable )
		{
			delegate->AdjustAttributeConnection(enable);
		}
		else
		{
			delegate->SetAttributes();
		}
	}

	NSI::Context nsi( delegate->m_nsi );
	nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
}

MString NSIExportSet::NSIDlSetAttributesHandle(
	const MObject& i_obj, const char *i_enable_attr )
{
	return NSIHandle( i_obj, m_live ) + MString("|DlSetAttributes|") +
		i_enable_attr;
}
