#include "MU_typeIds.h"
#include "NSIExport.h"
#include "NSIExportCamera.h"
#include "NSIExportUtilities.h"
#include "dlViewport.h"

#include <assert.h>
#include <list>
#include <ndspy.h>
#include <nsi.hpp>

#include <maya/MFnAttribute.h>
#include <maya/MFnCamera.h>
#include <maya/MGlobal.h>
#include <maya/MRenderView.h>
#include <maya/MSelectionList.h>
#include <maya/MSpinLock.h>


// #define DEBUG_OUTPUT

//
struct ViewportImageHandle
{
	ViewportImageHandle() :
		m_original_size_x(0),
		m_original_size_y(1),
		m_origin_x(0),
		m_origin_y(0),
		m_num_formats(0),
		m_with_alpha(0)
	{
	}

	~ViewportImageHandle()
	{
	}

	//
	MString m_render_panel;

	//
	int m_original_size_x;
	int m_original_size_y;

	//
	int m_origin_x;
	int m_origin_y;

	//
	int m_num_formats;

	//
	int m_with_alpha;

	//
	MString m_variable_name;
};

struct ViewportUpdateJob
{
	ViewportUpdateJob() :
		m_original_size_x(0),
		m_original_size_y(0),
		m_origin_x(0),
		m_origin_y(0),
		m_xmin(0),
		m_xmax_plusone(0),
		m_ymin(0),
		m_ymax_plusone(0),
		m_pixels(NULL)
	{
	}

	~ViewportUpdateJob()
	{
	}

	//
	MString m_render_panel;

	//
	int m_original_size_x;
	int m_original_size_y;

	//
	int m_origin_x;
	int m_origin_y;

	//
	int m_xmin;
	int m_xmax_plusone;

	//
	int m_ymin;
	int m_ymax_plusone;

	//
	RV_PIXEL *m_pixels;

	static MSpinLock s_lock;
	static std::list<ViewportUpdateJob *> s_jobs;
};

MSpinLock ViewportUpdateJob::s_lock;
std::list<ViewportUpdateJob *> ViewportUpdateJob::s_jobs;

static const bool k_live_render = true;

static void NSIRenderControlStart(NSI::Context& i_context)
{
	assert(k_live_render);
	i_context.RenderControl(
		(
			NSI::CStringPArg("action", "start"),
			NSI::IntegerArg("interactive", 1),
			NSI::IntegerArg("progressive", 1)
		));
}

static PtDspyError ViewportDspyImageOpen(PtDspyImageHandle *i_phImage,
	const char *i_drivername,
	const char *i_filename,
	int i_width, int i_height,
	int i_paramCount,
	const UserParameter *i_parameters,
	int i_numFormats,
	PtDspyDevFormat i_formats[],
	PtFlagStuff *flagstuff)
{
	if(! i_phImage)
	{
		return PkDspyErrorBadParams;
	}

	//
	for(int i = 0; i < i_numFormats; i ++)
	{
		i_formats[i].type = PkDspyFloat32;
	}

	//
	ViewportImageHandle *image_handle = new ViewportImageHandle;

	for(int i = 0;i < i_paramCount; ++ i)
	{
		const UserParameter *curr_param = i_parameters + i;
		const MString param_name(curr_param->name);
		if (param_name == "OriginalSize")
		{
			const int *original_size =
				static_cast<const int *>(curr_param->value);
			image_handle->m_original_size_x = original_size[0];
			image_handle->m_original_size_y = original_size[1];
		}
		else if (param_name == "origin")
		{
			const int *origin =
				static_cast<const int *>(curr_param->value);
			image_handle->m_origin_x = origin[0];
			image_handle->m_origin_y = origin[1];
		}
		else if (param_name == "withalpha")
		{
			const int *with_alpha =
				static_cast<const int *>(curr_param->value);
			image_handle->m_with_alpha = *with_alpha;
		}
		else if (param_name == "variablename")
		{
			assert(curr_param->valueCount == 1);
			const char **variablename = (const char **)(curr_param->value);
			image_handle->m_variable_name = variablename[0];
		}
		else if (param_name == "renderpanel")
		{
			assert(curr_param->valueCount == 1);
			const char **renderpanel = (const char **)(curr_param->value);
			image_handle->m_render_panel = renderpanel[0];
		}
	}

	assert(i_numFormats > 0 && i_numFormats <= 4);
	image_handle->m_num_formats = i_numFormats;

	*i_phImage = reinterpret_cast<PtDspyImageHandle>(image_handle);

#ifdef DEBUG_OUTPUT
	uint64_t tid = 0;
	pthread_threadid_np(NULL, &tid);
	printf("INFO - ViewportDspyImageOpen %u\n", (uint32_t)tid);
#endif

	return PkDspyErrorNone;
}

static PtDspyError ViewportDspyImageQuery(
	PtDspyImageHandle i_hImage,
	PtDspyQueryType i_type,
	int i_datalen,
	void *i_data)
{
	if(! i_data && i_type != PkStopQuery)
	{
		return PkDspyErrorBadParams;
	}

	ViewportImageHandle *image =
		reinterpret_cast<ViewportImageHandle *>(i_hImage);

#ifdef DEBUG_OUTPUT
	uint64_t tid = 0;
	pthread_threadid_np(NULL, &tid);
	printf("INFO - ViewportDspyImageQuery %u (%d)\n",
		(uint32_t)tid,
		(int)i_type);
#endif

	switch(i_type)
	{
		case PkSizeQuery:
		{
			PtDspySizeInfo size_info;
			size_info.width = 256;
			size_info.height = 256;
			size_info.aspectRatio = 1;

			break;
		}
		case PkOverwriteQuery:
		{
			PtDspyOverwriteInfo overwrite_info;
			overwrite_info.overwrite = 1;
			memcpy(i_data, &overwrite_info,
				i_datalen > int(sizeof(overwrite_info))
					? sizeof(overwrite_info) : i_datalen);

			break;
		}
		case PkProgressiveQuery:
		{
			if(i_datalen < int(sizeof(PtDspyProgressiveInfo)))
			{
				return PkDspyErrorBadParams;
			}
			((PtDspyProgressiveInfo*)i_data)->acceptProgressive = 1;

			break;
		}
		case PkCookedQuery:
		{
			PtDspyCookedInfo cooked_info;
			cooked_info.cooked = 1;

			memcpy(i_data, &cooked_info,
				i_datalen > int(sizeof(cooked_info))
					? sizeof(cooked_info) : i_datalen);

			break;
		}
		case PkStopQuery:
		{
			return PkDspyErrorNone;

			break;
		}
		case PkThreadQuery:
		{
			PtDspyThreadInfo info;
			info.multithread = 1;
			assert( i_datalen >= sizeof(info) );
			memcpy(i_data, &info, sizeof(info) );
			break;
		}

		default:
		{
			return PkDspyErrorUnsupported;
		}
	}

	return PkDspyErrorNone;
}

static PtDspyError ViewportDspyImageData(
	PtDspyImageHandle i_hImage,
	int i_xmin, int i_xmax_plusone,
	int i_ymin, int i_ymax_plusone,
	int i_entrySize,
	const unsigned char* i_cdata)
{
	// TODO: Accepts new pixels and create the update job.
	//
	ViewportImageHandle *image =
		reinterpret_cast<ViewportImageHandle *>(i_hImage);

#ifdef DEBUG_OUTPUT
	uint64_t tid = 0;
	pthread_threadid_np(NULL, &tid);
	printf("INFO - ViewportDspyImageData - %u - %d %d %d %d %d %p\n",
		(uint32_t)tid,
		i_xmin, i_xmax_plusone,
		i_ymin, i_ymax_plusone,
		i_entrySize,
		i_cdata);
#endif

	//
	if (! i_entrySize || ! i_cdata)
	{
		return PkDspyErrorStop;
	}

	//
	ViewportUpdateJob *job = new ViewportUpdateJob;
	job->m_render_panel = image->m_render_panel;
	job->m_original_size_x = image->m_original_size_x;
	job->m_original_size_y = image->m_original_size_y;
	job->m_origin_x = image->m_origin_x;
	job->m_origin_y = image->m_origin_y;
	job->m_xmin = i_xmin;
	job->m_xmax_plusone = i_xmax_plusone;
	job->m_ymin = i_ymin;
	job->m_ymax_plusone = i_ymax_plusone;

	const int bucket_w = i_xmax_plusone - i_xmin;
	const int bucket_h = i_ymax_plusone - i_ymin;

	job->m_pixels = new RV_PIXEL[bucket_w * bucket_h];
	switch (image->m_num_formats)
	{
		case 1:
		{
			/* RRR, A = 1 */
			const float *r_data = reinterpret_cast<const float *>(i_cdata);
			for (int i = 0; i < bucket_w * bucket_h; ++ i)
			{
				RV_PIXEL *pixel = job->m_pixels + i;
				pixel->r = pixel->g = pixel->b = r_data[i];
				pixel->a = 1.0f;
			}

			break;
		}
		case 2:
		{
			/* RG, B = 0, A = 1 */
			const float2 *rg_data = reinterpret_cast<const float2 *>(i_cdata);
			for (int i = 0; i < bucket_w * bucket_h; ++ i)
			{
				RV_PIXEL *pixel = job->m_pixels + i;
				pixel->r = rg_data[i][0];
				pixel->g = rg_data[i][1];
				pixel->b = 0.0f;
				pixel->a = 1.0f;
			}

			break;
		}
		case 3:
		{
			/* RGB, A = 1 */
			const float3 *rgb_data = reinterpret_cast<const float3 *>(i_cdata);
			for (int i = 0; i < bucket_w * bucket_h; ++ i)
			{
				RV_PIXEL *pixel = job->m_pixels + i;
				pixel->r = rgb_data[i][0];
				pixel->g = rgb_data[i][1];
				pixel->b = rgb_data[i][2];
				pixel->a = 1.0f;
			}

			break;
		}
		case 4:
		{
			/* RGBA */
			memcpy(job->m_pixels,
				i_cdata,
				bucket_w * bucket_h * sizeof(RV_PIXEL));

			break;
		}
		default:
		{
			/* Blank */
			for (int i = 0; i < bucket_w * bucket_h; ++ i)
			{
				RV_PIXEL *pixel = job->m_pixels + i;
				pixel->r = pixel->g = pixel->b = 0.0f, pixel->a = 1.0f;
			}
		}
	}

	ViewportUpdateJob::s_lock.lock();
	{
		ViewportUpdateJob::s_jobs.push_back(job);
	}
	ViewportUpdateJob::s_lock.unlock();

	// Tell Maya to update at next idle event.
	M3dView::scheduleRefreshAllViews();

	return PkDspyErrorNone;
}

static PtDspyError ViewportDspyImageClose(PtDspyImageHandle i_hImage)
{
	ViewportImageHandle *image =
		reinterpret_cast<ViewportImageHandle *>(i_hImage);

#ifdef DEBUG_OUTPUT
	uint64_t tid = 0;
	pthread_threadid_np(NULL, &tid);
	printf("DEBUG - ViewportDspyImageClose - %u\n", (uint32_t)tid);
#endif

	delete image;

	ViewportUpdateJob::s_lock.lock();

	/* Clean all old buffers */
	std::list<ViewportUpdateJob *>::iterator it =
		ViewportUpdateJob::s_jobs.begin();
	for (; it != ViewportUpdateJob::s_jobs.end(); ++ it)
	{
		ViewportUpdateJob *job = (*it);
		delete [] job->m_pixels;
		delete job;
	}
	ViewportUpdateJob::s_jobs.clear();

	/* Update Maya viewport */
	M3dView::scheduleRefreshAllViews();

	ViewportUpdateJob::s_lock.unlock();

	return PkDspyErrorNone;
}

static MDagPath GetCameraPath(const MString& i_panel)
{
	M3dView view;
	CHECK_MSTATUS(M3dView::getM3dViewFromModelPanel(i_panel, view));

	MDagPath camera_path;
	CHECK_MSTATUS(view.getCamera(camera_path));

	return camera_path;
}

void dlViewportNsiSceneRenderer::RegisterDisplayDriver()
{
	PtDspyDriverFunctionTable table;
	memset(&table, 0, sizeof(table));

	table.Version = k_PtDriverCurrentVersion;
	table.pOpen = &ViewportDspyImageOpen;
	table.pQuery = &ViewportDspyImageQuery;
	table.pWrite = &ViewportDspyImageData;
	table.pClose = &ViewportDspyImageClose;

	DspyRegisterDriverTable("nsi_viewport", &table);
}

dlViewportNsiSceneRenderer::dlViewportNsiSceneRenderer(const MString &name)
	:
	MHWRender::MUserRenderOperation(name)
{
}

dlViewportNsiSceneRenderer::~dlViewportNsiSceneRenderer()
{
}

MStatus dlViewportNsiSceneRenderer::execute(
	const MHWRender::MDrawContext &draw_context)
{
	/*
		Hold a mutex during the list copy only. Holding a mutex around the
		entire update loop would be a major contention point.
	*/
	std::list<ViewportUpdateJob *> local;
	ViewportUpdateJob::s_lock.lock();
	{
		local.swap( ViewportUpdateJob::s_jobs );
	}
	ViewportUpdateJob::s_lock.unlock();

	std::list<ViewportUpdateJob *>::iterator it = local.begin();
	for (;it != local.end(); it++)
	{
		ViewportUpdateJob *job = *it;

		/* Get the session. */
		tStringSessionMap::iterator session_it =
			m_session_map.find(job->m_render_panel);

		if (session_it != m_session_map.end())
		{
			Session *session = session_it->second;

			MHWRender::MTextureDescription desc;
			session->m_color_texture->textureDescription(desc);

			/* Update the bucket if the viewport size is matched. */
			if (desc.fWidth == job->m_original_size_x &&
				desc.fHeight == job->m_original_size_y)
			{
				MHWRender::MTextureUpdateRegion update_region;
				update_region.fXRangeMin = job->m_origin_x + job->m_xmin;
				update_region.fXRangeMax = job->m_origin_x + job->m_xmax_plusone;
				update_region.fYRangeMin = job->m_origin_y + job->m_ymin;
				update_region.fYRangeMax = job->m_origin_y + job->m_ymax_plusone;

				unsigned int row_pitch =
					(job->m_xmax_plusone - job->m_xmin) * sizeof(RV_PIXEL);
				CHECK_MSTATUS(session->m_color_texture->update(
					job->m_pixels,
					false /* no mip maps */,
					row_pitch,
					&update_region));
				delete [] job->m_pixels, delete job;
			}
		}
	}
	return MS::kSuccess;
}

void dlViewportNsiSceneRenderer::CreateSession(
	const MString &panel,
	NSIViewportExport *exporter)
{
	tStringSessionMap::iterator it = m_session_map.find(panel);
	if(it != m_session_map.end())
	{
		assert( false );
		return;
	}

	Session *session = new Session(panel, exporter);

	m_session_map.insert(std::make_pair(panel, session));
}

void dlViewportNsiSceneRenderer::UpdateSessionColorTexture(
	const MString &panel,
	unsigned int w,
	unsigned int h)
{
	/* Get the session from the panel. */
	tStringSessionMap::iterator it = m_session_map.find(panel);
	if (it == m_session_map.end())
	{
		return;
	}

	Session *session = it->second;
	assert(session);

	/* If reset, we release the color texture. */
	MHWRender::MRenderer *the_renderer =
		MHWRender::MRenderer::theRenderer();
	MHWRender::MTextureManager *tex_manager =
		the_renderer->getTextureManager();

	if (session->m_reset)
	{
		tex_manager->releaseTexture(session->m_color_texture);
		session->m_color_texture = NULL;

		session->m_reset = false;
	}

	/* Check if we're resizing the viewport or it's blank at first time. */
	bool is_resizing = false;
	bool is_blank = false;

	if (session->m_color_texture)
	{
		MHWRender::MTextureDescription desc;
		session->m_color_texture->textureDescription(desc);
		if (desc.fWidth != w || desc.fHeight != h)
		{
			is_resizing = true;
		}
	}
	else
	{
		is_blank = true;
	}

	// Stop the restart the rendering.
	//
	if (is_resizing || is_blank)
	{
		/* Notify the 3Delight the new resolution. */
		if (is_resizing)
		{
			assert(session->m_exporter);
			session->m_exporter->UpdateScreen(panel);
		}

		// Create the new texture.
		//
		tex_manager->releaseTexture(session->m_color_texture);
		session->m_color_texture = NULL;

		MHWRender::MTextureDescription desc;
		desc.setToDefault2DTexture();
		desc.fWidth = w;
		desc.fHeight = h;
		desc.fBytesPerRow = desc.fWidth * sizeof(RV_PIXEL);
		desc.fBytesPerSlice = desc.fBytesPerRow * h;
		desc.fFormat = MHWRender::kR32G32B32A32_FLOAT;
		desc.fEnvMapType = MHWRender::kEnvNone;

		session->m_color_texture =
			tex_manager->acquireTexture("", desc, NULL, false);
	}
}

MHWRender::MTexture *
dlViewportNsiSceneRenderer::GetSessionColorTexture(const MString &panel)
{
	MHWRender::MTexture *color_texture = NULL;

	tStringSessionMap::iterator it = m_session_map.find(panel);
	if (it != m_session_map.end())
	{
		color_texture = it->second->m_color_texture;
	}

	return color_texture;
}

void dlViewportNsiSceneRenderer::SetSessionReset(
	const MString &panel,
	bool reset)
{
	tStringSessionMap::iterator it = m_session_map.find(panel);
	if (it == m_session_map.end())
	{
		assert(false);
		return;
	}
	Session *session = it->second;

	session->m_reset = reset;
}

void dlViewportNsiSceneRenderer::DeleteSession(const MString &panel)
{
	MHWRender::MRenderer *the_renderer =
		MHWRender::MRenderer::theRenderer();
	MHWRender::MTextureManager *tex_manager =
		the_renderer->getTextureManager();

	tStringSessionMap::iterator it = m_session_map.find(panel);
	if (it != m_session_map.end())
	{
		Session *session = it->second;
		if (session->m_color_texture)
		{
			tex_manager->releaseTexture(session->m_color_texture);
		}
		delete session;

		m_session_map.erase(it);
	}
}

void dlViewportNsiSceneRenderer::ReleaseResource()
{
	MHWRender::MRenderer *the_renderer =
		MHWRender::MRenderer::theRenderer();
	MHWRender::MTextureManager *tex_manager =
		the_renderer->getTextureManager();

	tStringSessionMap::iterator it = m_session_map.begin();
	for (; it != m_session_map.end(); ++ it)
	{
		Session *session = it->second;
		if (session->m_color_texture)
		{
			tex_manager->releaseTexture(session->m_color_texture);
		}
		delete session;
	}
	m_session_map.clear();
}

//
dlViewportMayaSceneRenderer::dlViewportMayaSceneRenderer(
	const MString &name,
	dlViewportRenderOverride *ro)
:
	MHWRender::MSceneRender(name),
	m_ro(ro),
	m_color_tex(NULL)
{
}

dlViewportMayaSceneRenderer::~dlViewportMayaSceneRenderer()
{
}

MHWRender::MSceneRender::MSceneFilterOption
dlViewportMayaSceneRenderer::renderFilterOverride()
{
#if MAYA_API_VERSION >= 201700
	int x = (MHWRender::MSceneRender::kRenderPreSceneUIItems |
			 MHWRender::MSceneRender::kRenderOpaqueShadedItems |
			 MHWRender::MSceneRender::kRenderTransparentShadedItems |
			 MHWRender::MSceneRender::kRenderShadedItems);
#else
	int x = MHWRender::MSceneRender::kRenderPreSceneUIItems;
#endif
	return static_cast<MHWRender::MSceneRender::MSceneFilterOption>(x);
}

MHWRender::MClearOperation & dlViewportMayaSceneRenderer::clearOperation()
{
#if MAYA_API_VERSION > 201600
	MHWRender::MRenderer* the_renderer = MHWRender::MRenderer::theRenderer();
	mClearOperation.setClearGradient(the_renderer->useGradient());

	const MColor &clear_color_1 = the_renderer->clearColor();
	const MColor &clear_color_2 = the_renderer->clearColor2();

	float clear_color_1_val[4] =
	{
		clear_color_1.r,
		clear_color_1.g,
		clear_color_1.b,
		clear_color_1.a
	};
	float clear_color_2_val[4] =
	{
		clear_color_2.r,
		clear_color_2.g,
		clear_color_2.b,
		clear_color_2.a
	};
	mClearOperation.setClearColor(clear_color_1_val);
	mClearOperation.setClearColor2(clear_color_2_val);
#endif

	mClearOperation.setMask(
		static_cast<unsigned int>(MHWRender::MClearOperation::kClearAll) );

	return mClearOperation;
}

void dlViewportMayaSceneRenderer::postSceneRender(
	const MHWRender::MDrawContext &context )
{
	//
	bool in_color_pass = false;
	bool in_disallowed_pass = false;

	const MHWRender::MPassContext &pass_context = context.getPassContext();
	const MStringArray &semantics = pass_context.passSemantics();
	for (unsigned int i =0; i < semantics.length(); ++ i)
	{
		if (semantics[i] == MHWRender::MPassContext::kColorPassSemantic)
		{
			in_color_pass = true;
		}
		else if ((semantics[i] ==
					MHWRender::MPassContext::kShadowPassSemantic) ||
				(semantics[i] ==
					MHWRender::MPassContext::kDepthPassSemantic) ||
				(semantics[i] ==
					MHWRender::MPassContext::kNormalDepthPassSemantic))
		{
			in_disallowed_pass = true;
		}
	}
	if (! in_color_pass || in_disallowed_pass)
	{
		return;
	}

	//
	m_color_tex = context.copyCurrentColorRenderTargetToTexture();

	dlViewportBlendRenderer *blend_op =
		static_cast<dlViewportBlendRenderer *>(
			m_ro->GetRenderOperation(e_blend_op) );
	blend_op->SetDestinationColorTexture(m_color_tex);
}

void dlViewportMayaSceneRenderer::ReleaseResource()
{
	if (m_color_tex)
	{
		MHWRender::MRenderer* the_renderer =
			MHWRender::MRenderer::theRenderer();
		const MHWRender::MTextureManager *tex_manager =
			the_renderer->getTextureManager();
		tex_manager->releaseTexture(m_color_tex);
	}

	m_color_tex = NULL;
}

//
dlViewportBlendRenderer::dlViewportBlendRenderer(
	const MString &name,
	dlViewportRenderOverride *ro)
:
	MHWRender::MQuadRender(name),
	m_view_rectangle(0.0f, 0.0f, 1.0f, 1.0f),
	m_shader_instance(NULL),
	m_src_color_tex(NULL),
	m_dst_color_tex(NULL),
	m_scale(0.5f)
{
	m_src_color_ta.texture = NULL;
	m_dst_color_ta.texture = NULL;
}

dlViewportBlendRenderer::~dlViewportBlendRenderer()
{
}

const MFloatPoint * dlViewportBlendRenderer::viewportRectangleOverride()
{
	return &m_view_rectangle;
}

const MHWRender::MShaderInstance * dlViewportBlendRenderer::shader()
{
	if( m_shader_instance)
		return m_shader_instance;

	MHWRender::MRenderer *the_renderer = MHWRender::MRenderer::theRenderer();
	if( !the_renderer )
	{
		/* NOTE: do we need an assert here */
		return 0x0;
	}

	/* Create the shipped Blend shader. */
	const MHWRender::MShaderManager *shader_manager =
		the_renderer->getShaderManager();

	if ( !shader_manager )
	{
		/* NOTE: do we need an assert here ? */
		return 0x0;
	}

	/*
	   After 2017 Update 4, Blend.ogsfx added new technique.
	   We try to get the all techniques and check if we have Main or AlphaBlend
	   which is supported after Maya 2016.5+ .

	   This means that blending with Maya backgound is only possible in 2017.
	*/
	MString technique_name("Main");

#if MAYA_API_VERSION >= 201600
	MStringArray technique_names;
	shader_manager->getEffectsTechniques("Blend", technique_names);
	for (unsigned int i = 0; i < technique_names.length(); ++ i)
	{
		if (technique_names[i] == "AlphaBlend")
		{
			technique_name = "AlphaBlend";
		}
	}
#endif

	m_shader_instance =
		shader_manager->getEffectsFileShader("Blend", technique_name);

	assert( m_shader_instance );

	/* Set blending coefficient. */
	m_shader_instance->setParameter("gBlendSrc", m_scale);

	return m_shader_instance;
}

MHWRender::MClearOperation & dlViewportBlendRenderer::clearOperation()
{
	mClearOperation.setMask(static_cast<unsigned int>(
		MHWRender::MClearOperation::kClearAll));
	return mClearOperation;
}

void dlViewportBlendRenderer::SetSourceColorTexture(
	MHWRender::MTexture *src_color_tex)
{
	m_src_color_tex = src_color_tex;
	m_src_color_ta.texture = m_src_color_tex;
	m_shader_instance->setParameter("gSourceTex", m_src_color_ta);
}

void dlViewportBlendRenderer::SetDestinationColorTexture(
	MHWRender::MTexture *dst_color_tex)
{
	m_dst_color_tex = dst_color_tex;
	m_dst_color_ta.texture = m_dst_color_tex;
	m_shader_instance->setParameter("gSourceTex2", m_dst_color_ta);
}

void dlViewportBlendRenderer::SetScale(float scale)
{
	m_scale = scale;
	m_shader_instance->setParameter("gBlendSrc", m_scale);
}

//
dlViewportMayaUIRenderer::dlViewportMayaUIRenderer(const MString &name)
	:
	MHWRender::MSceneRender(name)
{
}

dlViewportMayaUIRenderer::~dlViewportMayaUIRenderer()
{
}

MHWRender::MSceneRender::MSceneFilterOption
dlViewportMayaUIRenderer::renderFilterOverride()
{
	return MHWRender::MSceneRender::kRenderPostSceneUIItems;
}

MHWRender::MSceneRender::MObjectTypeExclusions
dlViewportMayaUIRenderer::objectTypeExclusions()
{
	return static_cast<MObjectTypeExclusions>(uint64_t(
		MHWRender::MSceneRender::kExcludeGrid |
		MHWRender::MSceneRender::kExcludeImagePlane));
}

MHWRender::MClearOperation & dlViewportMayaUIRenderer::clearOperation()
{
	mClearOperation.setMask(
		static_cast<unsigned int>(MHWRender::MClearOperation::kClearNone));
	return mClearOperation;
}

//
dlViewportMayaHudRenderer::dlViewportMayaHudRenderer()
{
}

dlViewportMayaHudRenderer::~dlViewportMayaHudRenderer()
{
}

//
dlViewportPresentTarget::dlViewportPresentTarget(const MString &name)
	:
	MHWRender::MPresentTarget(name)
{
}

dlViewportPresentTarget::~dlViewportPresentTarget()
{
}

//
const MString dlViewportRenderOverride::s_name("NSI");
const MString dlViewportRenderOverride::s_ui_name("3Delight");

dlViewportRenderOverride *dlViewportRenderOverride::s_instance = NULL;

dlViewportRenderOverride::dlViewportRenderOverride(const MString &name)
	:
	MHWRender::MRenderOverride(name),
	m_curr_op(-1)
{
	//
	for (int i = 0 ;i < e_number_of_ops; ++ i)
	{
		m_ops[i] = NULL;
	}

	// Create callbacks for the changing of override.
	//
	if (! m_render_override_changed_cbs.length())
	{
		MStringArray model_panel_names;
		MGlobal::executeCommand("getPanel -type modelPanel", model_panel_names);
		for (unsigned int i = 0; i < model_panel_names.length(); ++ i)
		{
			const MString &model_panel_name = model_panel_names[i];

			MCallbackId render_override_changed_cb =
				MUiMessage::add3dViewRenderOverrideChangedCallback(
					model_panel_name,
					RenderOverrideChangedCallback);

			m_render_override_changed_cbs.append(render_override_changed_cb);
		}
	}
}

dlViewportRenderOverride::~dlViewportRenderOverride()
{
	MHWRender::MRenderer* the_renderer = MHWRender::MRenderer::theRenderer();
	const MHWRender::MRenderTargetManager *target_manager =
		the_renderer->getRenderTargetManager();

	//
	for (int i = 0 ;i < e_number_of_ops; ++ i)
	{
		if (m_ops[i])
		{
			delete m_ops[i];
		}
	}

	//
	if (m_render_override_changed_cbs.length())
	{
		MMessage::removeCallbacks(m_render_override_changed_cbs);
	}
}

MHWRender::DrawAPI dlViewportRenderOverride::supportedDrawAPIs() const
{
	return MHWRender::kAllDevices;
}

bool dlViewportRenderOverride::startOperationIterator()
{
	m_curr_op = 0;

	return true;
}

MHWRender::MRenderOperation * dlViewportRenderOverride::renderOperation()
{
	if (m_curr_op >= 0 && m_curr_op < e_number_of_ops)
	{
		return m_ops[m_curr_op];
	}
	return NULL;
}

bool dlViewportRenderOverride::nextRenderOperation()
{
	++ m_curr_op;
	if (m_curr_op < e_number_of_ops)
	{
		return true;
	}

	return false;
}

MString dlViewportRenderOverride::uiName() const
{
	return s_ui_name;
}

MStatus dlViewportRenderOverride::setup(const MString &panel_name)
{
	//
	MHWRender::MRenderer *the_renderer =
		MHWRender::MRenderer::theRenderer();
	if (! the_renderer)
	{
		return MStatus::kFailure;
	}

	MHWRender::MTextureManager *tex_manager =
		the_renderer->getTextureManager();
	if (! tex_manager)
	{
		return MStatus::kFailure;
	}

	/* If user is changing the size of the viewport,
	   we have to restart the 3Delight.
	*/
	unsigned int output_size_w = 0;
	unsigned int output_size_h = 0;
	the_renderer->outputTargetSize(output_size_w, output_size_h);

	// Create the all ops.
	//
	if (! m_ops[e_present_op])
	{
		MFloatPoint view_rectangle(0.0f, 0.0f, 1.0f, 1.0f);

		// Setup the NSI render.
		//
		static const MString nsi_scene_op_name("__nsi_viewport_nsi_scene_op");
		m_op_names[e_nsi_scene_op] = nsi_scene_op_name;

		dlViewportNsiSceneRenderer *nsi_scene_op =
			new dlViewportNsiSceneRenderer(nsi_scene_op_name);
		m_ops[e_nsi_scene_op] = nsi_scene_op;

		// Setup the original renderer from Maya.
		//
		static const MString maya_scene_op_name("__nsi_viewport_maya_scene_op");
		m_op_names[e_maya_scene_op] = maya_scene_op_name;

		dlViewportMayaSceneRenderer *maya_scene_op =
			new dlViewportMayaSceneRenderer(maya_scene_op_name, this);
		m_ops[e_maya_scene_op] = maya_scene_op;

		// Setup the blend operation.
		//
		static const MString blend_op_name("__nsi_viewport_blend_op");
		m_op_names[e_blend_op] = blend_op_name;

		dlViewportBlendRenderer *blend_op =
			new dlViewportBlendRenderer(blend_op_name, this);
		m_ops[e_blend_op] = blend_op;

		// Setup the Maya UI draw operatiorn for wireframe and manipulator.
		//
		static const MString maya_ui_op_name("__nsi_viewport_maya_ui_op");
		m_op_names[e_maya_ui_op] = maya_ui_op_name;

		dlViewportMayaUIRenderer *maya_ui_op =
			new dlViewportMayaUIRenderer(maya_ui_op_name);
		m_ops[e_maya_ui_op] = maya_ui_op;

		// Setup the HUD operations.
		//
		static const MString maya_hud_op_name("__nsi_viewport_maya_hud_op");
		m_op_names[e_maya_hud_op] = maya_hud_op_name;

		MHWRender::MHUDRender *maya_hud_op = new dlViewportMayaHudRenderer();
		m_ops[e_maya_hud_op] = maya_hud_op;

		// Preset
		//
		static const MString present_op_name("__nsi_viewport_present_op");
		m_op_names[e_present_op] = present_op_name;

		MHWRender::MPresentTarget *present_op =
			new dlViewportPresentTarget(present_op_name);
		m_ops[e_present_op] = present_op;
	}

	return (UpdateRenderOperations(panel_name) ? MS::kSuccess : MS::kFailure);
}

MStatus dlViewportRenderOverride::cleanup()
{
	dlViewportMayaSceneRenderer *maya_scene_op =
		static_cast<dlViewportMayaSceneRenderer *>(m_ops[e_maya_scene_op]);
	if (maya_scene_op)
	{
		maya_scene_op->ReleaseResource();
	}

	return MS::kSuccess;
}

MHWRender::MRenderOperation * dlViewportRenderOverride::GetRenderOperation(
	int i)
{
	if (i >= 0 && i < e_number_of_ops)
	{
		return m_ops[i];
	}
	return NULL;
}

void dlViewportRenderOverride::StartRender(
	const MString &panel,
	NSIViewportExport *exporter)
{
	/* Enqueue the task to start renderon panel from UI thread. */
	tPanelExporterMap::const_iterator it =
		m_create_panel_exporter_map.find(panel);

	if (it == m_create_panel_exporter_map.end())
	{
		m_create_panel_exporter_map.insert(
			std::make_pair(panel, exporter));
	}
}

void dlViewportRenderOverride::StopRender(const MString &panel)
{
	/* Enqueue the task to delete the render on panel in UI thread. */
	m_delete_panels.append(panel);
}

void dlViewportRenderOverride::StopAllRenders()
{
	MStringArray render_panels;
	dlViewportRenderOverride::s_instance->GetRenderPanels(render_panels);

	for(unsigned int i = 0; i < render_panels.length(); ++ i)
	{
		const MString &render_panel = render_panels[i];

		/* Stop the render. */
		dlViewportRenderOverride::s_instance->StopRender(render_panel);

		/* Disable the override. */
		M3dView view;
		CHECK_MSTATUS(M3dView::getM3dViewFromModelPanel(render_panel, view));
		view.setRenderOverrideName("");
	}
}

bool dlViewportRenderOverride::GetRenderPanels(MStringArray &render_panels) const
{
	render_panels.clear();

	MStringArray model_panels;
	MGlobal::executeCommand("getPanel -type modelPanel", model_panels);

	for (unsigned int i = 0; i < model_panels.length(); ++ i)
	{
		const MString &model_panel = model_panels[i];

		M3dView view;
		if (M3dView::getM3dViewFromModelPanel(model_panel, view) != MS::kSuccess)
		{
			render_panels.clear();

			return false;
		}

		MString render_override_name = view.renderOverrideName();
		if (view.isVisible() && render_override_name == s_name)
		{
			render_panels.append(model_panel);
		}
	}

	return render_panels.length() > 0;
}

bool dlViewportRenderOverride::UpdateRenderOperations(const MString &panel)
{
	MHWRender::MRenderer *the_renderer = MHWRender::MRenderer::theRenderer();
	if (! the_renderer)
	{
		return false;
	}

	const MHWRender::MRenderTargetManager *target_manager =
		(the_renderer ? the_renderer->getRenderTargetManager() : NULL);
	if (! target_manager)
	{
		return false;
	}

	// Deal with size.
	//
	unsigned int output_size_w = 0;
	unsigned int output_size_h = 0;
	the_renderer->outputTargetSize(output_size_w, output_size_h);

	/* Update nsi scene op */
	dlViewportNsiSceneRenderer *nsi_scene_op =
		static_cast<dlViewportNsiSceneRenderer *>(m_ops[e_nsi_scene_op]);

	/*
		If the panel stays in the m_delete_renders, we delete the session.
	*/
	for (unsigned int i = 0; i < m_delete_panels.length(); ++ i)
	{
		const MString &delete_panel = m_delete_panels[i];
		nsi_scene_op->DeleteSession(delete_panel);
	}
	m_delete_panels.clear();

	/*
		Check if the panel doesn't exist in the m_create_panel_exporter_map,
		then we create the new session.
	*/
	tPanelExporterMap::iterator it =
		m_create_panel_exporter_map.begin();
	for (; it != m_create_panel_exporter_map.end(); ++ it)
	{
		NSIViewportExport *exporter = it->second;
		nsi_scene_op->CreateSession(panel, exporter);
	}
	m_create_panel_exporter_map.clear();

	nsi_scene_op->UpdateSessionColorTexture(
		panel,
		output_size_w,
		output_size_h);

	/* Update maya scene op */
	dlViewportMayaSceneRenderer *maya_scene_op =
		static_cast<dlViewportMayaSceneRenderer *>(m_ops[e_maya_scene_op]);

	/* Update blend op */
	dlViewportBlendRenderer *blend_op =
		static_cast<dlViewportBlendRenderer *>(m_ops[e_blend_op]);

	blend_op->shader();

	/* Get panel's render target. */
	MHWRender::MTexture *color_texture =
		nsi_scene_op->GetSessionColorTexture(panel);
	blend_op->SetSourceColorTexture(color_texture);

	/* MRenderOverride doesn't have getFrameContext() until 2017. */
	float scale = 1.0f;

#if MAYA_API_VERSION >= 201700
	const MHWRender::MFrameContext *frame_context = getFrameContext();
	if (frame_context)
	{
		const bool is_user_changing_view =
			frame_context->userChangingViewContext();
		if (is_user_changing_view)
		{
			scale = 0.0f;
		}
	}
#endif
	blend_op->SetScale(scale);

	return true;
}

void dlViewportRenderOverride::RenderOverrideChangedCallback(
	const MString &panel,
	const MString &old_renderer,
	const MString &new_renderer,
	void *client_data)
{
	if (new_renderer == dlViewportRenderOverride::s_name)
	{
		/* Start the new render. */
		MString cmd;
		cmd.format("NSI_vpRender(\"^1s\")", panel);
		MGlobal::executeCommandOnIdle(cmd);
	}
	else if (old_renderer == dlViewportRenderOverride::s_name)
	{
		/* Stop the render. */
		MString cmd;
		cmd.format("NSI_vpAbort(\"^1s\")", panel);
		MGlobal::executeCommandOnIdle(cmd);
	}
}

//
MTypeId dlViewportRenderSettings::id(MU_typeIds::DL_VIEWPORTRENDERSETTINGS);

dlViewportRenderSettings::dlViewportRenderSettings()
{
}

dlViewportRenderSettings::~dlViewportRenderSettings()
{
}

MStatus dlViewportRenderSettings::compute(
	const MPlug& plug,
	MDataBlock& data)
{
	return MS::kUnknownParameter;
}

void dlViewportRenderSettings::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

void* dlViewportRenderSettings::creator()
{
	return new dlViewportRenderSettings;
}

MStatus dlViewportRenderSettings::initialize()
{
	return MS::kSuccess;
}

//
dlViewportPanelRenderPass::dlViewportPanelRenderPass(
	const RenderPassInterfaceForScreen &parent_render_pass,
	const MString &panel)
:
	m_parent_render_pass(parent_render_pass),
	m_panel(panel)
{
}

dlViewportPanelRenderPass::~dlViewportPanelRenderPass()
{
}

int dlViewportPanelRenderPass::ResolutionX() const
{
	unsigned pos[2], size[2];
	GetViewport(pos, size);

	return size[0];
}

int dlViewportPanelRenderPass::ResolutionY() const
{
	unsigned pos[2], size[2];
	GetViewport(pos, size);

	return size[1];
}

double dlViewportPanelRenderPass::PixelAspectRatio() const
{
	return m_parent_render_pass.PixelAspectRatio();
}

void dlViewportPanelRenderPass::Crop(
	float top_left[2], float bottom_right[2]) const
{
	// FIXME: Calculate the crop from viewport.

	top_left[0] = 0.0f;
	top_left[1] = 0.0f;

	bottom_right[0] = 1.0f;
	bottom_right[1] = 1.0f;
}

int dlViewportPanelRenderPass::PixelSamples() const
{
	return m_parent_render_pass.PixelSamples();
}

int dlViewportPanelRenderPass::PixelFilterIndex() const
{
	return 0;
}

void dlViewportPanelRenderPass::GetViewport(unsigned pos[2], unsigned size[2]) const
{
	M3dView view;
	CHECK_MSTATUS(M3dView::getM3dViewFromModelPanel(m_panel, view));
	CHECK_MSTATUS(view.viewport(pos[0], pos[1], size[0], size[1]));
}

//
dlViewportRenderPass::dlViewportRenderPass(
	MObject& i_renderPassNode, bool i_canApplyOverrides)
:
	RenderPassInterface(i_renderPassNode, i_canApplyOverrides)
{
}

dlViewportRenderPass::~dlViewportRenderPass()
{
}

int dlViewportRenderPass::ResolutionX( void ) const
{
	assert(false);
	return 0;
}

int dlViewportRenderPass::ResolutionY( void ) const
{
	assert(false);
	return 0;
}

double dlViewportRenderPass::PixelAspectRatio( void ) const
{
	return 1.0;
}

void dlViewportRenderPass::Crop(
	float o_top_left[2], float o_bottom_right[2] ) const
{
	assert(false);

	o_top_left[0] = 0.0f;
	o_top_left[1] = 0.0;

	o_bottom_right[0] = 1.0f;
	o_bottom_right[1] = 1.0f;
}

const char *dlViewportRenderPass::PixelFilter( void ) const
{
	return "sinc";
}

double dlViewportRenderPass::FilterWidth( void ) const
{
	return 2.0;
}

bool dlViewportRenderPass::DisableMotionBlur() const
{
	return true;
}

bool dlViewportRenderPass::DisableDepthOfField() const
{
	return false;
}

std::vector<MObject> dlViewportRenderPass::cameras() const
{
	std::vector<MObject> cameras;

	MStringArray render_panels;
	dlViewportRenderOverride::s_instance->GetRenderPanels(render_panels);

	for (unsigned int i = 0; i < render_panels.length(); ++ i)
	{
		MObject camera_node = GetCameraPath(render_panels[i]).node();
		cameras.push_back(camera_node);
	}
	assert(cameras.size());

	return cameras;
}

int dlViewportRenderPass::ShadingSamples( void ) const
{
	return 4;
}

int dlViewportRenderPass::PixelSamples( void ) const
{
	return 2;
}

int dlViewportRenderPass::PixelFilterIndex() const
{
	return 0;
}

void dlViewportRenderPass::layerIndices(MIntArray& o_indices) const
{
	/* Only single layer */
	o_indices.clear();

	o_indices.append(0);
}

MString dlViewportRenderPass::layerDriver(int i_index) const
{
	/* This function is used by file output but we never generate file.  */
	return MString("null");
}

MString dlViewportRenderPass::layerFilename(int i_index) const
{
	char layerFilename[128];
	snprintf(layerFilename, 127, "<scene>_<pass>_#_%d.nsi_viewport", i_index);

	return MString(layerFilename);
}

void dlViewportRenderPass::layerVariable(
	int i_index,
	MString& o_variable_source,
	MString& o_layer_type,
	int& o_with_alpha,
	MString& o_nsi_name,
	MString& o_token,
	MString& o_label,
	MString* o_description)const
{
	MString selectAov;
	getAttrValue("selectAov", selectAov);

	variableFields(
		selectAov,
		o_variable_source,
		o_layer_type,
		o_with_alpha,
		o_nsi_name,
		o_token,
		o_label);

	if(o_description)
	{
		*o_description = selectAov;
	}
}

void dlViewportRenderPass::layerLightCategories(
	MStringArray& o_categories) const
{
	o_categories.clear();

	/* Use all light. */
	o_categories.append("");
}

bool dlViewportRenderPass::layerIgnoreMatte( void ) const
{
	return true;
}

int dlViewportRenderPass::layerMatteType( void ) const
{
	return 0;
}

void dlViewportRenderPass::MatteSets( std::vector<MObject>& o_sets ) const
{
	/* We don't have any MatteSet. */
	o_sets.clear();
}

MString dlViewportRenderPass::layerScalarFormat(int i_index) const
{
	/* We always uses float since we don't need conversion in memory. */
	return MString("float");
}

//
NSIViewportExport::NSIViewportExport(
	MObject &i_pass,
	bool i_canApplyOverrides)
:
	NSIExport(i_pass, i_canApplyOverrides, 0x0)
{
}

NSIViewportExport::~NSIViewportExport()
{
}

void NSIViewportExport::CreateRenderOutput(const MString &panel)
{
	assert(panel.length());

	RenderPanelData* render_panel_data =
		new RenderPanelData(this, *m_render_pass, panel);
	m_render_panel_data_map.insert(
		std::make_pair(panel, render_panel_data));

	/* Get the panel camera and create the unique handle. */
	MDagPath camera_path = GetCameraPath(panel);
	MFnDagNode dag_node( camera_path );
	MString nsi_handle = NSIExportDelegate::NSIHandle(dag_node, k_live_render);

	/*  Try to find the camera from here. */
	NSIExportDelegate *camera_delegate = GetDelegate( nsi_handle.asChar() );

	if( !camera_delegate )
	{
		assert(!"Unable to find a camera delegate" );
		return;
	}

	/* Stop the current rendering session. */
	NSI::Context nsi(m_context);

	nsi.RenderControl(NSI::CStringPArg("action", "stop"));

	camera_delegate->SetAttributes();

	ExportImageLayer(panel, camera_path);

	NSIRenderControlStart(nsi);
}

void NSIViewportExport::DeleteRenderOutput(const MString &panel)
{
	MString prefix = panel + ":";

	/* Stop the render. */
	NSI::Context nsi(m_context);

	nsi.RenderControl(NSI::CStringPArg("action", "stop"));

	/* Delete the panel's associated screen. */
	MString screen_handle = prefix + "screen";
	nsi.Delete(screen_handle.asChar());

	/* Delete the output layer for the panel. */
	const MString &layer_handle = prefix + "outputlayer";
	nsi.Delete(layer_handle.asChar());

	/* Delete the output driver for the panel. */
	const MString &driver_handle = prefix + "driver";
	nsi.Delete(driver_handle.asChar());

	/* Delete the panel data. */
	tStringRenderPanelDataMap::iterator it =
		m_render_panel_data_map.find(panel);
	assert(it != m_render_panel_data_map.end());
	delete it->second;

	m_render_panel_data_map.erase(it);

	/* Restart the render. */
	NSIRenderControlStart(nsi);
}

void NSIViewportExport::UpdateScreen(const MString& i_panel)
{
	MString prefix = i_panel + ":";
	MString screen_handle = prefix + "screen";

	MFnCamera camera(GetCameraPath(i_panel).node());

	NSI::Context nsi(m_context);
	NSIExportUtilities::ExportScreen(
		m_context,
		GetRenderPass(i_panel),
		camera,
		screen_handle,
		true,
		k_live_render);
	nsi.RenderControl(NSI::CStringPArg("action", "synchronize"));
}

void NSIViewportExport::ExportImageLayers()
{
	/*
		There is nothing to do here, as each panel's screen, output layer and
		output driver is added individually in CreateRenderOutput.
	*/
}

void NSIViewportExport::RenderPassAttributeChanged(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &i_plug,
	MPlug &i_otherPlug )
{
	NSIExport::RenderPassAttributeChanged(i_msg, i_plug, i_otherPlug);

	MStatus status;
	MObject attrib = i_plug.attribute();
	MFnAttribute attribute_fn( attrib, &status );

	/*
		The 'selectAov' attribute is present on the Viewport Settings and
		selects the current AOV to render. Any output driver modification
		needs a start/stop event in NSI.
	*/
	if( attribute_fn.name() == "selectAov" )
	{
		NSI::Context nsi( m_context );

		MString variable_source, layer_type, aov, aov_token, aov_label, description;
		int with_alpha;
		m_render_pass->layerVariable(
			0,
			variable_source,
			layer_type,
			with_alpha,
			aov,
			aov_token,
			aov_label);

		MString select_aov(i_plug.asString());

		/* Set the attribute for the "outputlayer" with special name. */
		nsi.RenderControl( NSI::CStringPArg( "action", "stop") );

		for(tStringRenderPanelDataMap::iterator p = m_render_panel_data_map.begin();
			p != m_render_panel_data_map.end();
			p++)
		{
			MString layer_handle = p->second->Name() + ":outputlayer";

			nsi.SetAttribute( layer_handle.asChar(),
				(
					NSI::StringArg( "variablename", aov.asChar() ),
					NSI::StringArg( "variablesource", variable_source.asChar() ),
					NSI::StringArg( "layertype", layer_type.asChar() ),
					NSI::IntegerArg( "withalpha", with_alpha ) )
				);
		}

		NSIRenderControlStart(nsi);
	}
}

NSIViewportExport::RenderPanelData::RenderPanelData(
	NSIViewportExport *exporter,
	RenderPassInterfaceForScreen &parent_render_pass,
	const MString &panel)
	:	m_exporter(exporter)
{
	/* Create the instance of dlViewportPanelRenderPass. */
	m_render_pass.reset(new dlViewportPanelRenderPass(
		parent_render_pass,
		panel));

	/* Create the callbacks for this panel. */

	MCallbackId panel_camera_changed_id =
		MUiMessage::addCameraChangedCallback(
			panel,
			PanelCameraChangedCallback,
			exporter);

	m_callback_ids.append(panel_camera_changed_id);


	MObject camera = GetCameraPath(Name()).node();

	MCallbackId camera_attribute_changed_id =
	MNodeMessage::addAttributeChangedCallback(
		camera,
		&CameraAttributeChangedCallback,
		this);

	m_callback_ids.append(camera_attribute_changed_id);
}

NSIViewportExport::RenderPanelData::~RenderPanelData()
{
	if (m_callback_ids.length())
	{
		MMessage::removeCallbacks(m_callback_ids);
	}
}

void NSIViewportExport::ExportImageLayer(
	const MString &panel,
	const MDagPath &camera_path)
{
	const int layer_index = 0;

	MString variable_source, layer_type, variable_name, aov_token, aov_label;
	int with_alpha;
	m_render_pass->layerVariable(
		layer_index,
		variable_source,
		layer_type,
		with_alpha,
		variable_name,
		aov_token,
		aov_label);
	MString scalar_format = m_render_pass->layerScalarFormat(layer_index);

	MString prefix = panel + ":";
	MString layer_handle = prefix + "outputlayer";
	MString screen_handle = prefix + "screen";

	MFnCamera camera(camera_path.node());

	NSIExportUtilities::ExportScreen(
		m_context,
		GetRenderPass(panel),
		camera,
		screen_handle,
		true,
		k_live_render);

	MString driver_handle = prefix + "driver";
	unsigned sort_key = layer_index;
	static const MString display_driver("nsi_viewport");

	NSI::Context nsi(m_context);

	nsi.Create(driver_handle.asChar(), "outputdriver");
	nsi.SetAttribute(driver_handle.asChar(),
		(
			NSI::StringArg("drivername", display_driver.asChar()),
			NSI::StringArg("renderpanel", panel.asChar())
		));

	ExportOneOutputLayer(
		layer_handle,
		layer_index,
		variable_source,
		layer_type,
		variable_name,
		with_alpha,
		scalar_format,
		screen_handle,
		MObject(),
		driver_handle,
		display_driver,
		sort_key );
}

dlViewportPanelRenderPass&
NSIViewportExport::GetRenderPass(const MString& i_panel)const
{
	tStringRenderPanelDataMap::const_iterator it =
		m_render_panel_data_map.find(i_panel);
	assert(it != m_render_panel_data_map.end());

	RenderPanelData *render_panel_data = it->second;
	assert(render_panel_data);
	return *render_panel_data->m_render_pass;
}

void NSIViewportExport::RenderPanelData::PanelCameraChangedCallback(
	const MString &panel,
	MObject&/*node*/,
	void *data)
{
	NSIViewportExport *exporter =
		reinterpret_cast<NSIViewportExport *>(data);

	/* Return if we don't have this panel with NSI render override. */
	M3dView view;
	CHECK_MSTATUS(M3dView::getM3dViewFromModelPanel(panel, view));
	if (view.renderOverrideName() != dlViewportRenderOverride::s_name)
	{
		return;
	}

	/* Terminate the render on that panel, delete the panel's camera and layer. */
	dlViewportRenderOverride::s_instance->StopRender(panel);

	exporter->DeleteRenderOutput(panel);

	/* Create the new camera+layer nodes and render. */
	exporter->CreateRenderOutput(panel);

	dlViewportRenderOverride::s_instance->StartRender(panel, exporter);
}

void NSIViewportExport::RenderPanelData::CameraAttributeChangedCallback(
	MNodeMessage::AttributeMessage i_msg,
	MPlug& i_plug,
	MPlug&,
	void* i_data)
{
	MString name = NSIExportUtilities::AttributeName( i_plug );

	/*
		Those two attributes seem to have no effect on the NSI screen node, so
		we can probably safely ignore them, especially since they're likely to
		be modified often using the mouse. For the other attributes, we will
		re-output the screen's attributes and require a restart of the render.
		This could be overkill, but seems safer and simpler to maintain than
		having a long list of attributes to ignore.
	*/
	if(name == "focalLength" || name == "centerOfInterest")
	{
		return;
	}

	RenderPanelData* panel_data = reinterpret_cast<RenderPanelData*>(i_data);
	assert(panel_data);
	assert(panel_data->m_exporter);
	panel_data->m_exporter->UpdateScreen(panel_data->Name());
}
