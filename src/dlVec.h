#pragma once

namespace dl
{

	/// Geometric D-dimension vector type with simple operators.
	template<int D, typename T>
	struct Vec
	{
		Vec() {}
		Vec(T a, T b) { v[0] = a; v[1] = b; }
		Vec(T a, T b, T c) { v[0] = a; v[1] = b; v[2] = c; }

		const Vec<D, T>& operator+=(const Vec<D, T>& a)
		{
			for(int i = 0; i < D; i++) v[i] += a.v[i];
			return *this;
		}

		const Vec<D, T>& operator-=(const Vec<D, T>& a)
		{
			for(int i = 0; i < D; i++) v[i] -= a.v[i];
			return *this;
		}

		const Vec<D, T>& operator*=(T s)
		{
			for(int i = 0; i < D; i++) v[i] *= s;
			return *this;
		}

		const T& operator[](unsigned i)const { return v[i]; }
		T& operator[](unsigned i) { return v[i]; }

		T v[D];
	};


	template<int D, typename T>
	Vec<D, T> operator+(const Vec<D, T>& a, const Vec<D, T>& b)
	{
		Vec<D, T> c = a;
		return c += b;
	}

	template<int D, typename T>
	Vec<D, T> operator-(const Vec<D, T>& a, const Vec<D, T>& b)
	{
		Vec<D, T> c = a;
		return c -= b;
	}

	template<int D, typename T>
	Vec<D, T> operator*(const Vec<D, T>& a, T b)
	{
		Vec<D, T> c = a;
		return c *= b;
	}


	typedef Vec<2, float> V2f;
	typedef Vec<3, float> V3f;
	typedef Vec<2, int> V2i;

}
