/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#include "DL_extensionAttrCmd.h"

#include <maya/MArgParser.h>

#include "DL_errors.h"
#include "DL_extensionAttrRegistry.h"


bool
DL_extensionAttrCmd::isUndoable() const
{
	return false;
}

void*
DL_extensionAttrCmd::creator()
{
	return new DL_extensionAttrCmd;
}

MStatus
DL_extensionAttrCmd::doIt(const MArgList& i_args)
{
	MStatus status = MStatus::kSuccess;

	// Validate the argument list
	//
	MArgParser argParser(syntax(), i_args, &status);
	RETURN_ON_FAILED_MSTATUS(status);

	bool do_listGroups = argParser.isFlagSet("-listGroups");
	bool nodeTypeSpecified = argParser.isFlagSet("-nodeType");
	bool do_addGroup = argParser.isFlagSet("-addGroup");
	bool do_removeGroup = argParser.isFlagSet("-removeGroup");

	if((int)do_listGroups + (int)do_addGroup + (int)do_removeGroup != 1)
	{
		MString error = _3DFM_ERR_PREFIX(DL_EXTENSIONATTR_CMD_STR) +
			MString("Must specify precisely one of -listGroups, -addGroup ") +
			MString(" or -removeGroup.");

		displayError(error);
		return MStatus::kFailure;		
	}

	if(!nodeTypeSpecified && !do_listGroups)
	{
		MString error = _3DFM_ERR_PREFIX(DL_EXTENSIONATTR_CMD_STR) +
		MString("Must specify a node type name with -nodeType.");

		displayError(error);
		return MStatus::kFailure;
	}

	// Execute the requested action.
	//
	MString nodeType;
	if(nodeTypeSpecified)
	{
		status = argParser.getFlagArgument("-nodeType", 0, nodeType);
		RETURN_ON_FAILED_MSTATUS(status);
	}

	MString error;

	if(do_listGroups)
	{
		MStringArray groups;

		if(nodeTypeSpecified)
		{
			groups = 
				DL_extensionAttrRegistry::getInstance().getAttributeGroupNames(nodeType);
		}
		else
		{
			groups =
				DL_extensionAttrRegistry::getInstance().getAttributeGroupNames();
		}

		status = MStatus::kSuccess;
		error = MString("");

		setResult(groups);
	}
	else if(do_addGroup)
	{
		MString groupName;
		status = argParser.getFlagArgument("-addGroup", 0, groupName);
		RETURN_ON_FAILED_MSTATUS(status);

		status = DL_extensionAttrRegistry::getInstance().addAttributeGroup(
			nodeType, 
			groupName);

		if(status == MStatus::kInvalidParameter)
		{
			error = _3DFM_ERR_PREFIX(DL_EXTENSIONATTR_CMD_STR) +
				MString("'" + nodeType + "' : invalid node type.");
		}
		else if(status == MStatus::kUnknownParameter)
		{
			error = _3DFM_ERR_PREFIX(DL_EXTENSIONATTR_CMD_STR) +
				MString("'" + groupName + "' : invalid attribute group name.");		
		}
		else if(status == MStatus::kFailure)
		{
			error = _3DFM_ERR_PREFIX(DL_EXTENSIONATTR_CMD_STR) +
				MString("attribute group '" + groupName + "' already exists for node ") +
				MString("type '" + nodeType + "'.");
		}
	}
	else if(do_removeGroup)
	{
		MString groupName;
		status = argParser.getFlagArgument("-removeGroup", 0, groupName);
		RETURN_ON_FAILED_MSTATUS(status);

		status = DL_extensionAttrRegistry::getInstance().removeAttributeGroup(
			nodeType, 
			groupName);

		if(status == MStatus::kInvalidParameter)
		{
			error = _3DFM_ERR_PREFIX(DL_EXTENSIONATTR_CMD_STR) +
			MString("'" + nodeType + "' : invalid node type.");
		}
		else if(status == MStatus::kFailure)
		{
			error = _3DFM_ERR_PREFIX(DL_EXTENSIONATTR_CMD_STR) +
				MString("type '" + nodeType + "' has no '" + groupName) + 
				MString("' attribute group.");
		}
	}

	if(error != MString(""))
	{
		displayError(error);
	}

	return status;
}

MSyntax
DL_extensionAttrCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("-nt", "-nodeType", MSyntax::kString);
	syntax.addFlag("-ls", "-listGroups");
	syntax.addFlag("-add", "-addGroup", MSyntax::kString);
	syntax.addFlag("-rm", "-removeGroup", MSyntax::kString);

	return syntax;
}
