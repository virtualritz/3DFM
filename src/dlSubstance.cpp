#include "dlSubstance.h"

#include <maya/MFloatVector.h>
#include <maya/MObjectArray.h>

#include "DL_autoLoadOSL.h"
#include "DL_utils.h"
#include "OSLUtils.h"

#include <cassert>

MObject dlSubstance::s_color;
MObject dlSubstance::s_outColor;

void* dlSubstance::creator()
{
	return new dlSubstance();
}

MStatus dlSubstance::initialize()
{
	MStringArray shaderPaths = OSLUtils::GetBuiltInSearchPaths();
	MString shaderName( "dlSubstance" );
	DlShaderInfo *info;

	OSLUtils::OpenShader( shaderName, shaderPaths, info );

	MObjectArray objects;
	MStringArray objectNames;

	DL_OSLShadingNode::CreateAttributesFromShaderParameters(
		info, 0x0, &objects, &objectNames );

	if(
		!DL_OSLShadingNode::FindAttribute(
			"color", s_color, objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"outColor", s_outColor, objects, objectNames) )
	{
		assert(false);
		// It makes no sense to return success here, but if we don't, Maya crashes.
		return MStatus::kSuccess;
	}

	for( unsigned i = 0; i < objects.length(); i++ )
	{
		addAttribute( objects[ i ] );
	}

	attributeAffects( s_color, s_outColor );

	MString name = info->shadername().c_str();
	MString niceName = OSLUtils::GetShaderNiceName( info );
	DL_OSLShadingNode::DefineShaderNiceName( name, niceName );

	return MStatus::kSuccess;
}

void dlSubstance::postConstructor()
{
	MFnDependencyNode dep_node(thisMObject());

	/* Set the default node name */
	MString defaultname = typeName() + "#";

	/* Save the digit in the node name */
	if( isdigit( defaultname.asChar()[0] ) )
	{
		defaultname = "_" + defaultname;
	}

	dep_node.setName(defaultname);

	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlSubstance::compute(
    const MPlug& i_plug,
    MDataBlock& i_block )
{
	if ((i_plug != s_outColor) && (i_plug.parent() != s_outColor))
	{
		return MS::kUnknownParameter;
	}

	/* Just transfer s_color to s_outColor */
	MFloatVector& color = i_block.inputValue( s_color ).asFloatVector();

	/* set ouput color attribute */
  	MDataHandle outColorHandle = i_block.outputValue( s_outColor );
	MFloatVector& outColor = outColorHandle.asFloatVector();
	outColor = color;
	outColorHandle.setClean();

	return MS::kSuccess;
}
