#include <ri.h>

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

#include "DL_RiProcedural.h"

#include <string.h>
#include <stdlib.h>

MSyntax
DL_RiProcedural::newSyntax()
{
  MSyntax syntax;

  syntax.addFlag("-l", "-libraryName", MSyntax::kString);
  syntax.addFlag("-an", "-archiveName", MSyntax::kString);
  syntax.addFlag("-pn", "-programName", MSyntax::kString);
  syntax.addFlag("-b", "-bound",
    MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble, 
    MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble );
  syntax.addFlag("-p", "-param", MSyntax::kString);
  syntax.addFlag("-d", "-detail", MSyntax::kDouble);

  return syntax;
}

static RtPointer
AllocStrings( const char *i_string1, const char *i_string2 )
{
  RtString* array = (RtString*) malloc(2 * sizeof(RtString));
  array[0] = strdup(i_string1);
  array[1] = strdup(i_string2);
  
  return (RtPointer)array;
}

static RtVoid
DeallocStrings( RtPointer i_data )
{
  char** array = (char**) i_data;
  free(array[0]);
  free(array[1]);
  free(array);
}

MStatus
DL_RiProcedural::doIt(const MArgList& args)
{
  MStatus status;
  MArgDatabase argData(syntax(), args, &status);

  if( !status )
    return status;

  if( m_binding == dynamicLoad )
  {
    MGlobal::displayWarning(
      "RiDynamicLoad is deprecated. Use RiProcedural instead." );
  }
  else if( m_binding == delayedReadArchive )
  {
    MGlobal::displayWarning(
      "RiDelayedReadArchive is deprecated. Use RiProcedural instead." );
  }

  RtBound bound;
  double detail;
  bool immediate = false;
  if( argData.isFlagSet("-b") )
  {
    double xmin, xmax, ymin, ymax, zmin, zmax;

    argData.getFlagArgument("-b", 0, xmin);
    argData.getFlagArgument("-b", 1, xmax);
    argData.getFlagArgument("-b", 2, ymin);
    argData.getFlagArgument("-b", 3, ymax);
    argData.getFlagArgument("-b", 4, zmin);
    argData.getFlagArgument("-b", 5, zmax);

    bound[0] = xmin; bound[1] = xmax;
    bound[2] = ymin; bound[3] = ymax;
    bound[4] = zmin; bound[5] = zmax;
  }
  else if( argData.isFlagSet("-d") )
  {
    immediate = true;
    argData.getFlagArgument("-d", 0, detail);
  }
  else
  {
    MGlobal::displayError(
      "need a bound specified with -b"
      " or detail specified with -d for immediate call" );
    return MS::kFailure;
  }

  bool dynamicLoad = argData.isFlagSet("-l");
  bool delayedReadArchive = argData.isFlagSet("-an");
  bool runProgram = argData.isFlagSet("-pn");

  if( int(dynamicLoad) + int(delayedReadArchive) + int(runProgram) != 1 )
  {
    MGlobal::displayError(
      "need one (and only one) of -archiveName (for RiProcDelayedReadArchive),"
      " -libraryName (for RiProcDynamicLoad) or"
      " -programName (for RiProcRunProgram)");
    return MS::kFailure;
  }

  MString params;
  if (argData.isFlagSet("-p"))
  {
    if( delayedReadArchive )
    {
      MGlobal::displayError("-param should not be used with -archiveName");
      return MS::kFailure;
    }

    argData.getFlagArgument("-p", 0, params);
  }

  if( dynamicLoad )
  {
    MString lib_name;
    argData.getFlagArgument("-l", 0, lib_name);
    
    RtPointer data = AllocStrings( lib_name.asChar(), params.asChar() );
    if( immediate )
    {
      RiProcDynamicLoad( data, detail );
      DeallocStrings( data );
    }
    else
    {
      RiProcedural( data, bound, RiProcDynamicLoad, DeallocStrings );
    }
  }
  else if( delayedReadArchive )
  {
    MString archive_name;
    argData.getFlagArgument("-an", 0, archive_name);
    
    RtPointer data = AllocStrings( archive_name.asChar(), "" /* unused */ );
    if( immediate )
    {
      /* Pointless (just call RiReadArchive) but here for completeness. */
      RiProcDelayedReadArchive( data, detail );
      DeallocStrings( data );
    }
    else
    {
      RiProcedural( data, bound, RiProcDelayedReadArchive, DeallocStrings );
    }
  }
  else if( runProgram )
  {
    MString program_name;
    argData.getFlagArgument("-pn", 0, program_name);
    
    RtPointer data = AllocStrings( program_name.asChar(), params.asChar() );
    if( immediate )
    {
      RiProcRunProgram( data, detail );
      DeallocStrings( data );
    }
    else
    {
      RiProcedural( data, bound, RiProcRunProgram, DeallocStrings );
    }
  }

  return status;
}

void DL_RiProcedural::CallDynamicLoad(
  const MString &i_lib,
  const MString &i_param,
  float i_bbox[6] )
{
    RiProcedural(
      AllocStrings( i_lib.asChar(), i_param.asChar() ),
      i_bbox, RiProcDynamicLoad, DeallocStrings );
}

