#include "dlOpenVDBShader.h"

#include <maya/MFnStringData.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MRampAttribute.h>

#include "DL_utils.h"

MObject dlOpenVDBShader::m_shaderFilename;

MObject dlOpenVDBShader::m_scatteringDensity;
MObject dlOpenVDBShader::m_scatteringColor;
MObject dlOpenVDBShader::m_scatteringAnisotropy;
MObject dlOpenVDBShader::m_multipleScattering;
MObject dlOpenVDBShader::m_multipleScatteringIntensity;
MObject dlOpenVDBShader::m_densityRemapEnable;
MObject dlOpenVDBShader::m_densityRemapRange;
MObject dlOpenVDBShader::m_densityRemapRangeStart;
MObject dlOpenVDBShader::m_densityRemapRangeEnd;
MObject dlOpenVDBShader::m_densityRemapCurve;

MObject dlOpenVDBShader::m_transparencyColor;
MObject dlOpenVDBShader::m_transparencyScale;

MObject dlOpenVDBShader::m_incandescence;

MObject dlOpenVDBShader::m_emissionIntensityScale;
MObject dlOpenVDBShader::m_emissionIntensityGridEnable;
MObject dlOpenVDBShader::m_emissionIntensityRange;
MObject dlOpenVDBShader::m_emissionIntensityRangeStart;
MObject dlOpenVDBShader::m_emissionIntensityRangeEnd;
MObject dlOpenVDBShader::m_emissionIntensityCurve;

MObject dlOpenVDBShader::m_blackbodyIntensity;
MObject dlOpenVDBShader::m_blackbodyMode;
MObject dlOpenVDBShader::m_blackbodyKelvin;
MObject dlOpenVDBShader::m_blackbodyTint;
MObject dlOpenVDBShader::m_blackbodyRange;
MObject dlOpenVDBShader::m_blackbodyRangeStart;
MObject dlOpenVDBShader::m_blackbodyRangeEnd;
MObject dlOpenVDBShader::m_blackbodyTemperatureCurve;

MObject dlOpenVDBShader::m_emissionRampIntensity;
MObject dlOpenVDBShader::m_emissionRampTint;
MObject dlOpenVDBShader::m_emissionRampRangeStart;
MObject dlOpenVDBShader::m_emissionRampRangeEnd;
MObject dlOpenVDBShader::m_emissionRampRange;
MObject dlOpenVDBShader::m_emissionRampColorCurve;

MObject dlOpenVDBShader::m_emissionGridIntensity;
MObject dlOpenVDBShader::m_emissionGridTint;

MObject dlOpenVDBShader::m_scatteringColorCorrectGamma;
MObject dlOpenVDBShader::m_scatteringColorCorrectHueShift;
MObject dlOpenVDBShader::m_scatteringColorCorrectSaturation;
MObject dlOpenVDBShader::m_scatteringColorCorrectVibrance;
MObject dlOpenVDBShader::m_scatteringColorCorrectContrast;
MObject dlOpenVDBShader::m_scatteringColorCorrectContrastPivot;
MObject dlOpenVDBShader::m_scatteringColorCorrectGain;
MObject dlOpenVDBShader::m_scatteringColorCorrectOffset;

MObject dlOpenVDBShader::m_outColor;
MObject dlOpenVDBShader::m_outTransparency;

void* dlOpenVDBShader::creator()
{
	return new dlOpenVDBShader;
}

MStatus dlOpenVDBShader::initialize()
{
	using namespace Utilities;

  /*
  	Add shaderFilename string attribute with a default value of "vdbVolume".
  	This is used by the shader data cache when figuring out the OSL shader file
  	to open for a given shading node.
  */
  MFnStringData defaultShaderFilenameData;
  MObject defaultShaderFilename =
  	defaultShaderFilenameData.create( "vdbVolume" );

	MFnTypedAttribute typedAttrFn;
	m_shaderFilename =
		typedAttrFn.create( "shaderFilename", "file", MFnData::kString );

	typedAttrFn.setConnectable( false );
	typedAttrFn.setHidden( true );
	typedAttrFn.setDefault( defaultShaderFilename );
	addAttribute( m_shaderFilename );

	MFnNumericAttribute numAttrFn;

	// Density & Scattering attributes
	m_scatteringDensity = numAttrFn.create(
		"scatteringDensity",
		"scatDens",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Scattering Density" );
	addAttribute( m_scatteringDensity );

	m_scatteringColor = numAttrFn.createColor(
		"scatteringColor",
		"scatCol" );
	numAttrFn.setDefault( 1.0, 1.0, 1.0 );
	numAttrFn.setNiceNameOverride( "Scattering Color" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_scatteringColor );

	m_scatteringAnisotropy = numAttrFn.create(
		"scatteringAnisotropy",
		"scatAniso",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Scattering Anisotropy" );
	numAttrFn.setMin( -1.0 );
	numAttrFn.setMax( 1.0 );
	addAttribute( m_scatteringAnisotropy );

	m_multipleScattering = numAttrFn.create(
		"multipleScattering",
		"multScat",
		MFnNumericData::kBoolean );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Multiple Scattering Enable" );
	numAttrFn.setDefault( true );
	addAttribute( m_multipleScattering );

	m_multipleScatteringIntensity = numAttrFn.create(
		"multipleScatteringIntensity",
		"msInt",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Multiple Scattering" );
	numAttrFn.setMin( 0.0 );
	numAttrFn.setMax( 1.0 );
	addAttribute( m_multipleScatteringIntensity );

	// Density remap attributes
	m_densityRemapEnable = numAttrFn.create(
		"densityRemapEnable",
		"drEnb",
		MFnNumericData::kBoolean );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Enable Density Ramp" );
	numAttrFn.setDefault( false );
	addAttribute( m_densityRemapEnable );

	m_densityRemapRangeStart = numAttrFn.create(
		"densityRemapRangeStart",
		"drRgS",
		MFnNumericData::kDouble,
		0.0 );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Density Range Start" );
	addAttribute( m_densityRemapRangeStart );

	m_densityRemapRangeEnd = numAttrFn.create(
		"densityRemapRangeEnd",
		"drRgE",
		MFnNumericData::kDouble,
		1.0 );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Density Range End" );
	addAttribute( m_densityRemapRangeEnd );

	m_densityRemapRange = numAttrFn.create(
		"densityRemapRange",
		"drRg",
		m_densityRemapRangeStart,
		m_densityRemapRangeEnd );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Density Range" );
	addAttribute( m_densityRemapRange );

	m_densityRemapCurve = MRampAttribute::createCurveRamp(
		"densityRemapCurve",
		"drRmp" );
	addAttribute( m_densityRemapCurve );

	// transparency attributes
	m_transparencyColor = numAttrFn.createColor(
		"transparencyColor",
		"transpCol" );
	numAttrFn.setDefault( 0.0, 0.0, 0.0 );
	numAttrFn.setNiceNameOverride( "Transparency Color" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_transparencyColor );

	m_transparencyScale = numAttrFn.create(
		"transparencyScale",
		"transpSc",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Transparency Scale" );
	numAttrFn.setMin( 0.0 );
	numAttrFn.setSoftMax( 4.0 );
	addAttribute( m_transparencyScale );

	// incandescence attributes
	m_incandescence = numAttrFn.createColor(
		"incandescence",
		"incan" );
	numAttrFn.setDefault( 1.0, 1.0, 1.0 );
	numAttrFn.setHidden( true );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_incandescence );

	// emission intensity attributes
	m_emissionIntensityScale = numAttrFn.create(
		"emissionIntensityScale",
		"emIntSc",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Intensity Scale" );
	numAttrFn.setMin( 0.0 );
	numAttrFn.setSoftMax( 4.0 );
	addAttribute( m_emissionIntensityScale );

	m_emissionIntensityGridEnable = numAttrFn.create(
		"emissionIntensityGridEnable",
		"emIntEnb",
		MFnNumericData::kBoolean );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Enable Emission Intensity Grid" );
	numAttrFn.setDefault( false );
	addAttribute( m_emissionIntensityGridEnable );

	m_emissionIntensityRangeStart = numAttrFn.create(
		"emissionIntensityRangeStart",
		"emIntRgS",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Intensity Range Start" );
	addAttribute( m_emissionIntensityRangeStart );

	m_emissionIntensityRangeEnd = numAttrFn.create(
		"emissionIntensityRangeEnd",
		"emIntRgE",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Intensity Range End" );
	addAttribute( m_emissionIntensityRangeEnd );

	m_emissionIntensityRange = numAttrFn.create(
		"emissionIntensityRange",
		"emIntRg",
		m_emissionIntensityRangeStart,
		m_emissionIntensityRangeEnd );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Intensity Range" );
	addAttribute( m_emissionIntensityRange );

	m_emissionIntensityCurve = MRampAttribute::createCurveRamp(
		"emissionIntensityCurve",
		"emIntRmp");
	addAttribute( m_emissionIntensityCurve	);

	// blackbody attributes
	m_blackbodyIntensity = numAttrFn.create(
		"blackbodyIntensity",
		"bbInt",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Intensity" );
	addAttribute( m_blackbodyIntensity );

	m_blackbodyMode = numAttrFn.create(
		"blackbodyMode",
		"bbMode",
		MFnNumericData::kDouble,
		2.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Intensity Mode" );
	addAttribute( m_blackbodyMode );

	m_blackbodyKelvin = numAttrFn.create(
		"blackbodyKelvin",
		"bbKelvin",
		MFnNumericData::kDouble,
		5000.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Kelvin" );
	numAttrFn.setMin( 0 );
	numAttrFn.setSoftMin( 1500.0 );
	numAttrFn.setSoftMax( 10000.0 );
	addAttribute( m_blackbodyKelvin );

	m_blackbodyTint = numAttrFn.createColor(
		"blackbodyTint",
		"bbTint" );
		numAttrFn.setDefault( 1.0, 1.0, 1.0 );
		numAttrFn.setNiceNameOverride( "Blackbody Tint" );
		makeShaderInputAttribute( numAttrFn );
		addAttribute( m_blackbodyTint );

	m_blackbodyRangeStart = numAttrFn.create(
		"blackbodyRangeStart",
		"bbTempRgS",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Temperature Range Start" );
	addAttribute( m_blackbodyRangeStart );

	m_blackbodyRangeEnd = numAttrFn.create(
		"blackbodyRangeEnd",
		"bbTempRgE",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Temperature Range End" );
	addAttribute( m_blackbodyRangeEnd );

	m_blackbodyRange = numAttrFn.create(
		"blackbodyRange",
		"bbTempRg",
		m_blackbodyRangeStart,
		m_blackbodyRangeEnd );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Blackbody Temperature Range" );
	addAttribute( m_blackbodyRange );

	m_blackbodyTemperatureCurve = MRampAttribute::createCurveRamp(
		"blackbodyTemperatureCurve",
		"bbTempC");
	addAttribute( m_blackbodyTemperatureCurve	);

	// emission ramp attributes
	m_emissionRampIntensity = numAttrFn.create(
		"emissionRampIntensity",
		"emRampInt",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Ramp Intensity" );
	addAttribute( m_emissionRampIntensity );

	m_emissionRampTint = numAttrFn.createColor(
		"emissionRampTint",
		"emRampTint" );
		numAttrFn.setDefault( 1.0, 1.0, 1.0 );
		numAttrFn.setNiceNameOverride( "Emission Ramp Tint" );
		makeShaderInputAttribute( numAttrFn );
		addAttribute( m_emissionRampTint );

	m_emissionRampRangeStart = numAttrFn.create(
		"emissionRampRangeStart",
		"emRampTempRgS",
		MFnNumericData::kDouble,
		0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Ramp Temperature Range Start" );
	addAttribute( m_emissionRampRangeStart );

	m_emissionRampRangeEnd = numAttrFn.create(
		"emissionRampRangeEnd",
		"emRampTempRgE",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Ramp Temperature Range End" );
	addAttribute( m_emissionRampRangeEnd );

	m_emissionRampRange = numAttrFn.create(
		"emissionRampRange",
		"emRampTempRg",
		m_emissionRampRangeStart,
		m_emissionRampRangeEnd );
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Ramp Temperature Range" );
	addAttribute( m_emissionRampRange );

	m_emissionRampColorCurve = MRampAttribute::createColorRamp(
		"emissionRampColorCurve",
		"emRampC");
	addAttribute( m_emissionRampColorCurve );

	m_outColor = numAttrFn.createColor( "outColor", "oc" );
	makeOutputAttribute( numAttrFn );
	addAttribute( m_outColor );

	m_outTransparency = numAttrFn.createColor( "m_outTransparency", "ot" );
	makeOutputAttribute( numAttrFn );
	addAttribute( m_outTransparency );

	// Emission Grid attributes
	m_emissionGridIntensity = numAttrFn.create(
		"emissionGridIntensity",
		"emGridInt",
		MFnNumericData::kDouble,
		1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Emission Grid Intensity" );
	addAttribute( m_emissionGridIntensity );

	m_emissionGridTint = numAttrFn.createColor(
		"emissionGridTint",
		"emGridTint" );
	numAttrFn.setDefault( 1.0, 1.0, 1.0 );
	numAttrFn.setNiceNameOverride( "Emission Grid Tint" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_emissionGridTint );

	// color correct attributes
	m_scatteringColorCorrectGamma = numAttrFn.create(
		"scatteringColorCorrectGamma", "scatteringCCG", MFnNumericData::kDouble, 1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Gamma" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 5 );
	addAttribute( m_scatteringColorCorrectGamma );

	m_scatteringColorCorrectHueShift = numAttrFn.create(
		"scatteringColorCorrectHueShift", "scatteringCCHS", MFnNumericData::kDouble, 0.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Hue Shift" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 1 );
	addAttribute( m_scatteringColorCorrectHueShift );

	m_scatteringColorCorrectSaturation = numAttrFn.create(
		"scatteringColorCorrectSaturation", "scatteringCCS", MFnNumericData::kDouble, 1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Saturation" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 5 );
	addAttribute( m_scatteringColorCorrectSaturation );

	m_scatteringColorCorrectVibrance = numAttrFn.create(
		"scatteringColorCorrectVibrance", "scatteringCCV", MFnNumericData::kDouble, 1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Vibrance" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 5 );
	addAttribute( m_scatteringColorCorrectVibrance );

	m_scatteringColorCorrectContrast = numAttrFn.create(
		"scatteringColorCorrectContrast", "scatteringCCC", MFnNumericData::kDouble, 1.0);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Contrast" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 5 );
	addAttribute( m_scatteringColorCorrectContrast );

	m_scatteringColorCorrectContrastPivot = numAttrFn.create(
		"scatteringColorCorrectContrastPivot", "scatteringCCCP", MFnNumericData::kDouble, 0.18);
	makeShaderInputAttribute( numAttrFn );
	numAttrFn.setNiceNameOverride( "Contrast" );
	numAttrFn.setMin( 0 ); numAttrFn.setMax( 1 );
	addAttribute( m_scatteringColorCorrectContrastPivot );

	m_scatteringColorCorrectGain = numAttrFn.createColor(
		"scatteringColorCorrectGain", "scatteringCCGain" );
	numAttrFn.setDefault( 1.0, 1.0, 1.0 );
	numAttrFn.setNiceNameOverride( "Gain" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_scatteringColorCorrectGain );

	m_scatteringColorCorrectOffset = numAttrFn.createColor(
		"scatteringColorCorrectOffset", "scatteringCCO" );
	numAttrFn.setDefault( 0.0, 0.0, 0.0 );
	numAttrFn.setNiceNameOverride( "Offset" );
	makeShaderInputAttribute( numAttrFn );
	addAttribute( m_scatteringColorCorrectOffset );

	return MStatus::kSuccess;
}

void dlOpenVDBShader::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlOpenVDBShader::compute( const MPlug& i_plug, MDataBlock& i_dataBlock )
{
	if ((i_plug != m_outColor) && (i_plug.parent() != m_outColor))
	{
		return MS::kUnknownParameter;
	}

	/* Just transfer s_color to s_outColor */
	MFloatVector& color =
		i_dataBlock.inputValue( m_scatteringColor ).asFloatVector();

	/* set ouput color attribute */
	MDataHandle outColorHandle = i_dataBlock.outputValue( m_outColor );
	MFloatVector& outColor = outColorHandle.asFloatVector();
	outColor = color;
	outColorHandle.setClean();

	return MStatus::kSuccess;
}
