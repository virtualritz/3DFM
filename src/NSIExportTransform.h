#ifndef __NSIExportTransform_h
#define __NSIExportTransform_h

#include "NSIExportDelegate.h"

/**
	\sa NSIExportDelegate
*/
class NSIExportTransform : public NSIExportDelegate
{
public:
	NSIExportTransform(
		MFnDagNode &i_dag,
		const NSIExportDelegate::Context& i_context )
	:
		NSIExportDelegate( i_dag, i_context )
	{
	}

	virtual void Create();
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );

private:
};
#endif
