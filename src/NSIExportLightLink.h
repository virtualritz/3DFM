#ifndef __NSIExportLightLink_h
#define __NSIExportLightLink_h

#include "NSIExportDelegate.h"

#include <maya/MNodeMessage.h>
#include <maya/MObject.h>
#include <maya/MFnDependencyNode.h>

/**
	\sa NSIExportDelegate
*/
class NSIExportLightLink : public NSIExportDelegate
{
public:
	NSIExportLightLink(
		MFnDependencyNode &dep,
		MObject &obj,
		const NSIExportDelegate::Context& i_context );

	virtual void Create();
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual void Connect( const DelegateTable * );
	virtual bool RegisterCallbacks( void );

	void attributeChangedCB(
		MNodeMessage::AttributeMessage i_msg,
		MPlug &i_plug, MPlug &i_otherPlug );

private:
	static void static_attributeChangedCB(
		MNodeMessage::AttributeMessage i_msg,
		MPlug &i_plug, MPlug &i_otherPlug,
		void *i_data );

	void DoInterObjectVisibility(
		const DelegateTable *, const MPlug &i_link, bool i_visibility );
	int GetPriority( bool light_is_set, bool object_is_set );

private:
	MObject m_object_node, m_light_node;
};
#endif
