#ifndef __NSIExportOpenVDB_h
#define __NSIExportOpenVDB_h

#include "NSIExportDelegate.h"

#include <nsi.hpp>

#include <maya/MNodeMessage.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MPlug;
#endif

#include "MU_typeIds.h"

class NSIExportOpenVDB : public NSIExportDelegate
{
public:
	static int MayaDAGTypeID() { return MU_typeIds::DLOPENVDBSHAPE; }

	NSIExportOpenVDB(
		MObject i_object,
		const NSIExportDelegate::Context& i_context );

	~NSIExportOpenVDB();

	virtual void Create();
	virtual void SetAttributes();
	virtual void SetAttributesAtTime( double i_time, bool i_no_motion );
	static bool has_vdb_light_layer(MObject i_object);
};

#endif
