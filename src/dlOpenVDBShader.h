#ifndef __dlOpenVDBShader_H__
#define __dlOpenVDBShader_H__

#include <maya/MPxNode.h>

class dlOpenVDBShader : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug&, MDataBlock& );

private:
	static MObject m_shaderFilename;

	static MObject m_scatteringDensity;
	static MObject m_scatteringColor;
	static MObject m_scatteringAnisotropy;
	static MObject m_multipleScattering;
	static MObject m_multipleScatteringIntensity;
	static MObject m_densityRemapEnable;
	static MObject m_densityRemapRange;
	static MObject m_densityRemapRangeStart;
	static MObject m_densityRemapRangeEnd;
	static MObject m_densityRemapCurve;

	static MObject m_transparencyColor;
	static MObject m_transparencyScale;

	static MObject m_incandescence;

	static MObject m_emissionIntensityScale;
	static MObject m_emissionIntensityGridEnable;
	static MObject m_emissionIntensityRange;
	static MObject m_emissionIntensityRangeStart;
	static MObject m_emissionIntensityRangeEnd;
	static MObject m_emissionIntensityCurve;

	static MObject m_blackbodyIntensity;
	static MObject m_blackbodyMode;
	static MObject m_blackbodyKelvin;
	static MObject m_blackbodyTint;
	static MObject m_blackbodyRange;
	static MObject m_blackbodyRangeStart;
	static MObject m_blackbodyRangeEnd;
	static MObject m_blackbodyTemperatureCurve;

	static MObject m_emissionRampIntensity;
	static MObject m_emissionRampTint;
	static MObject m_emissionRampRangeStart;
	static MObject m_emissionRampRangeEnd;
	static MObject m_emissionRampRange;
	static MObject m_emissionRampColorCurve;

	static MObject m_emissionGridIntensity;
	static MObject m_emissionGridTint;

	static MObject m_scatteringColorCorrectGamma;
	static MObject m_scatteringColorCorrectHueShift;
	static MObject m_scatteringColorCorrectSaturation;
	static MObject m_scatteringColorCorrectVibrance;
	static MObject m_scatteringColorCorrectContrast;
	static MObject m_scatteringColorCorrectContrastPivot;
	static MObject m_scatteringColorCorrectGain;
	static MObject m_scatteringColorCorrectOffset;

	static MObject m_outColor;
	static MObject m_outTransparency;
};

#endif

