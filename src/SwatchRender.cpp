
// #define OUTPUT_TO_IDISPLAY

#include "DelegateTable.h"
#include "DL_renderState.h"
#include "DL_utils.h"
#include "nsi.hpp"
#include "NSIErrorHandler.h"
#include "NSIExportShader.h"

#include <maya/MFnDependencyNode.h>
#include <maya/MItDependencyGraph.h>
#include <maya/MGlobal.h>
#include <maya/MPlug.h>

#include <string.h>
#include <assert.h>

#include "SwatchRender.h"

#if MAYA_API_VERSION >= 201500

static const bool k_live_render = false;

MSwatchRenderBase* SwatchRender::creator(
	MObject swatchObj, MObject renderObj, int res )
{
	return new SwatchRender( swatchObj, renderObj, res );
}

SwatchRender::SwatchRender( MObject swatchObj, MObject renderObj, int i_res )
:
	MSwatchRenderBase(swatchObj, renderObj, i_res),
	m_nsi(NSI_BAD_CONTEXT),
	m_color_space("linear")
{
	image().create( i_res, i_res );
	image().setRGBA( true );
}

SwatchRender::~SwatchRender()
{
	cancelParallelRendering();
}


/**
	\see startRender
*/
void SwatchRender::Stopped(
	void* data,
	NSIContext_t i_context,
	int i_status)
{
	if( i_status == NSIRenderCompleted )
	{
		assert( data );
		SwatchRender* swatch_render = (SwatchRender*)data;

		assert( i_context == swatch_render->m_nsi );
		NSI::Context nsi( swatch_render->m_nsi );
		nsi.End();
		swatch_render->m_nsi = NSI_BAD_CONTEXT;
		swatch_render->finishParallelRender();
	}
}

/**
	\brief The renderer scene has been exported using the MaterialViewer. It's
	nicely lit using area lights and will do the job.

	The camera is already positioned and it's name is swatch_camera

	NOTE/TODO?: There is no environment in the scene.
*/
void SwatchRender::ExportScene( void )
{
	assert( m_nsi != NSI_BAD_CONTEXT );
	NSI::Context nsi( m_nsi );

	MString scene = getDelightPath();
	scene += "/maya/swatch/scene.nsi";

	nsi.Evaluate(
		(
			NSI::StringArg("type", "apistream"),
			NSI::StringArg("filename", scene.asChar())
		) );

	OutputDisplayChain( "swatch_camera" );
	OutputShadingNetwork( "shading_network_head" );
}
/**
	Because renderParallel() == true, this function apparently needs to behave
	differently. Maya will call it only once. Returning true seem to flag the
	swatch for destruction.

	We should check the value returned by renderQuality() to adjust the swatch
	rendering, but the doc is very vague. So far, I've seen this function
	returning 0 while dragging a slider and 1 for regular attr change.
*/
bool SwatchRender::doIteration()
{
	return !startRender();
}

bool SwatchRender::renderParallel()
{
	return true;
}

void SwatchRender::cancelParallelRendering()
{
	if( m_nsi != NSI_BAD_CONTEXT )
	{
		NSI::Context nsi( m_nsi );
		nsi.RenderControl( NSI::StringArg("action", "stop") );
		nsi.End();
		m_nsi = NSI_BAD_CONTEXT;
		finishParallelRender();
	}
}


/**
	\brief Export scene and start NSI render in a parallel thread.

	** WARNING **

	Shader export HAS TO BE DONE IN THIS THREAD or you will have very weird
	Python errors.
*/
bool SwatchRender::startRender()
{
	MStatus status;
	MFnDependencyNode dep_node( node() );

	if( status != MStatus::kSuccess )
	{
		assert( false );
		return false;
	}

	if( m_nsi != NSI_BAD_CONTEXT )
	{
		return false;
	}

	NSIErrorHandler_t eh = NSIErrorHandlerMaya;

	NSIParam_t param[2];
	param[0].name = "errorhandler";
	param[0].data = &eh;
	param[0].type = NSITypePointer; param[0].count = 1;
	param[0].flags = 0; param[0].arraylength = 0;

	const char *user_data = "Swatch Render";
	param[1].name = "errorhandlerdata";
	param[1].data = &user_data;
	param[1].type = NSITypePointer; param[1].count = 1;
	param[1].flags = 0; param[1].arraylength = 0;

	m_nsi = NSIBegin( 2, param );

	NSI::Context nsi( m_nsi );

	ExportScene();

	if( renderQuality() == 0 )
	{
		nsi.SetAttribute( NSI_SCENE_GLOBAL,
			NSI::IntegerArg("quality.shadingsamples", 4) );
	}

	/*
		Use only one thread for rendering.
	*/
	nsi.SetAttribute( NSI_SCENE_GLOBAL,
		NSI::IntegerArg("numberofthreads", 1) );

	assert(!k_live_render);
	nsi.RenderControl(
		(
			NSI::CStringPArg("action", "start"),
			NSI::PointerArg( "stoppedcallback", (void*)SwatchRender::Stopped ),
			NSI::PointerArg( "stoppedcallbackdata", this )
		) );

	return true;
}

void SwatchRender::OutputShadingNetwork( 
	const char *shading_network_head ) const
{
	assert( m_nsi != NSI_BAD_CONTEXT );
	assert( shading_network_head );

	NSI::Context nsi( m_nsi );
	DelegateTable delegates;
	MObject obj = node();

	MItDependencyGraph graphIt(
		obj,
		MFn::kInvalid,
		MItDependencyGraph::kUpstream );

	for(; !graphIt.isDone(); graphIt.next()  )
	{
		if( NSIExportUtilities::IsShadingNode( graphIt.currentItem() ) )
		{
			NSIExportDelegate::Context export_context(m_nsi, k_live_render);
			NSIExportShader* delegate =
				new NSIExportShader( graphIt.currentItem(), export_context );

			delegate->Create();
			delegates.emplace(
				delegate->Handle(),
				std::shared_ptr<NSIExportDelegate>(delegate));
		}
	}

	for( const auto &item : delegates )
	{
		item.second->SetAttributes();
		item.second->SetAttributesAtTime(0, true);
	}

	for( const auto &item : delegates )
	{
		item.second->Connect( &delegates );
	}

	MString handle = NSIExportDelegate::NSIShaderHandle( obj, k_live_render );
	nsi.Connect( handle.asChar(), "outColor", shading_network_head, "i_surface" );

	for( const auto &item : delegates )
	{
		item.second->Finalize();
	}

	delegates.clear();
}

void SwatchRender::OutputDisplayChain( const char *i_camera )
{
	assert( m_nsi != NSI_BAD_CONTEXT );
	assert( i_camera );

	NSI::Context nsi( m_nsi );

	const char *k_output_layer = "Bob";
	const char *k_output_driver = "Dale";
	const char *k_screen = "Gloves";

	char image_pointer[64];
	snprintf(image_pointer, sizeof(image_pointer), "%p", image().pixels() );

	nsi.Create( k_output_driver, "outputdriver" );
	nsi.SetAttribute( k_output_driver,
		(
			NSI::StringArg("drivername",  "memory"),
			NSI::StringArg("imagefilename", image_pointer),
			NSI::StringArg("colorprofile", m_color_space.asChar())
		) );

#ifdef OUTPUT_TO_IDISPLAY
	const char *k_output_driver2 = "AI";
	nsi.Create( k_output_driver2, "outputdriver" );
	nsi.SetAttribute( k_output_driver2,
		(
			NSI::StringArg("drivername",  "idisplay"),
			NSI::StringArg("imagefilename", "shader ball"),
			NSI::StringArg("colorprofile", m_color_space.asChar()),
			NSI::FloatArg("aspectratio", 1.0f)
		) );
#endif

	nsi.Create( k_output_layer, "outputlayer" );
	nsi.SetAttribute( k_output_layer,
		(
			NSI::StringArg( "variablename", "Ci"),
			NSI::StringArg( "channelname", "beauty"),
			NSI::StringArg( "scalarformat", "uint8"),
			NSI::IntegerArg( "withalpha", 1 )
		) );

	nsi.Create( k_screen, "screen" );

	nsi.Connect(
		k_output_driver, "",
		k_output_layer, "outputdrivers" );

#ifdef OUTPUT_TO_IDISPLAY
	nsi.Connect(
		k_output_driver2, "",
		k_output_layer, "outputdrivers" );
#endif

	nsi.Connect(
		k_output_layer, "",
		k_screen, "outputlayers" );

	nsi.Connect(
		k_screen, "",
		i_camera, "screens" );

	/* Finally, set the resolution and oversampling on the screen */
	int res[2] =
	{
		resolution(),
		resolution(),
	};

	NSI::ArgumentList arg;
	arg.Add(
		NSI::Argument::New( "resolution" )
		->SetArrayType( NSITypeInteger, 2 )
		->CopyValue( res, sizeof(res) ) );

	int oversampling = 16;
	arg.Add(
		NSI::Argument::New( "oversampling" )
		->SetType( NSITypeInteger )
		->CopyValue( &oversampling, sizeof(oversampling) ) );

	nsi.SetAttribute( k_screen, arg );
}


#endif  // MAYA_API_VERSION
