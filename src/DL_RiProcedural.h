#ifndef __DL_RIPROCEDURAL_H__
#define __DL_RIPROCEDURAL_H__

#include <maya/MPxCommand.h>

class DL_RiProcedural : public MPxCommand
{
public:
  /* This is used only to warn the user that RiDynamicLoad and
     RiDelayedReadArchive are deprecated bindings. */
  enum eBinding { procedural, dynamicLoad, delayedReadArchive };
  DL_RiProcedural( eBinding i_b = procedural ) : m_binding(i_b) {}

  virtual MStatus doIt(const MArgList& args);

  virtual bool isUndoable() const { return false; }

  static void* creator() { return new DL_RiProcedural; }
  static void* creatorDynamicLoad()
    { return new DL_RiProcedural( dynamicLoad ); }
  static void* creatorDelayedReadArchive()
    { return new DL_RiProcedural( delayedReadArchive ); }

  static MSyntax newSyntax();

  static void CallDynamicLoad(
    const MString &i_lib,
    const MString &i_param,
    float i_bbox[6] );

private:
  eBinding m_binding;
};

#endif
