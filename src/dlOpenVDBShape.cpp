/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#include "dlOpenVDBShape.h"
#include "OpenVDBCache.h"

#include <maya/MArrayDataBuilder.h>
#include <maya/MBoundingBox.h>
#include <maya/MDrawData.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MHWGeometry.h>
#include <maya/MHWGeometryUtilities.h>
#include <maya/MPoint.h>
#include <maya/MShaderManager.h>

#include "DL_utils.h"
#include "VDBQuery.h"

#include <algorithm>

MString dlOpenVDBShape::drawDbClassification("drawdb/geometry/dlOpenVDBShape");
MString dlOpenVDBShape::drawRegistrantId("dlOpenVDBShapePlugin");

MObject dlOpenVDBShape::m_time;
DL_fileSequence dlOpenVDBShape::m_fileSequence;

MObject dlOpenVDBShape::m_filename;
MObject dlOpenVDBShape::m_smokeGrid;
MObject dlOpenVDBShape::m_smokeColorGrid;
MObject dlOpenVDBShape::m_temperatureGrid;
MObject dlOpenVDBShape::m_emissionIntenistyGrid;
MObject dlOpenVDBShape::m_emissionTintGrid;
MObject dlOpenVDBShape::m_velocityGrid;
MObject dlOpenVDBShape::m_velocityScale;

MObject dlOpenVDBShape::m_drawPoints;

MObject dlOpenVDBShape::m_visibilityDiffuse;

MObject dlOpenVDBShape::m_bboxMin;
MObject dlOpenVDBShape::m_bboxMax;
MObject dlOpenVDBShape::m_outFilename;

class dlOpenVDBShapeGeo
{
public:
	dlOpenVDBShapeGeo();

	void clear();

	double m_minX, m_minY, m_minZ;
	double m_maxX, m_maxY, m_maxZ;
	double m_scale;

	bool m_display_points;

	MString m_current_file;
	MString m_current_grid;

	std::vector<float> m_vertices;
	std::vector<float> m_colors;
};

/* Scales bounding box */
static void
getScaledBBox(
	const MPoint& min,
	const MPoint& max,
	double scale,
	MPoint& scaledMin,
	MPoint& scaledMax)
{
	MPoint halfSize = (max - min) / 2;
	MPoint center = min + halfSize;
	MPoint scaledHalfSize(
		scale * halfSize.x, scale * halfSize.y, scale * halfSize.z);

	scaledMin = center - scaledHalfSize;
	scaledMax = center + scaledHalfSize;
}

/*
	Returns a version of i_filename where the sequence number is replaced by <f>.

	It is assumed that the sequence number is specified right before the file
	extension.
*/
static MString getFormattedFilename( const MString& i_filename )
{
	const char* filename_cstr = i_filename.asChar();
	MString formatted_filename = i_filename;

	/*
		Starting for the end of the string, find the '.' that preceedes the file
		extension.
	*/

	int i = i_filename.rindex('.');
	if( i < 0 )
	{
		// No '.' was found
		i = i_filename.length();
	}

	// This is the last index of a possible frame number
	i--;
	int frame_number_start_index = i;
	int frame_number_end_index = i;
	/*
		Continue backwards in the string until the current character is not a
		digit.
	*/
	for( ; i >= 0; i-- )
	{
		if( filename_cstr[i] < '0' || filename_cstr[i] > '9' )
		{
			frame_number_start_index = i + 1;
			break;
		}
		else
		{
			frame_number_start_index = i;
		}
	}

	//We may want to consider that the frame number might even be one digit.
	if( frame_number_start_index > frame_number_end_index )
		return i_filename;

	if (frame_number_start_index > 0)
	{
		formatted_filename = i_filename.substring( 0, frame_number_start_index - 1 );
	}

	formatted_filename += "<f>";
	formatted_filename =
		formatted_filename +
		i_filename.substring( frame_number_end_index + 1, i_filename.length() );

	return formatted_filename;
}

dlOpenVDBShapeGeo::dlOpenVDBShapeGeo() :
	m_minX(-1.0), m_minY(-1.0), m_minZ(-1.0),
	m_maxX( 1.0), m_maxY( 1.0), m_maxZ( 1.0),
	m_scale(1.0),
	m_display_points(false)
{
}

void
dlOpenVDBShapeGeo::clear()
{
	m_vertices.clear();
	m_colors.clear();

	m_current_file = "";
	m_current_grid = "";
}

dlOpenVDBShape::dlOpenVDBShape()
	: MPxSurfaceShape(), m_shape_geo(0)
{
	/* Pointer because it's necessary to change the content in const methods.
	   For example for automatic reload the vertex buffer. */
	m_shape_geo = new dlOpenVDBShapeGeo();
}

dlOpenVDBShape::~dlOpenVDBShape()
{
	delete m_shape_geo;
}

void dlOpenVDBShape::postConstructor()
{
	setRenderable( true );

	/*
		Change render stats attribute values for nodes that are not created during
		a Maya scene file reading.

		This is as close we can get to altering the render stats default values
	*/
	if( !Utilities::isReadingSceneFile() )
	{
		MFnDependencyNode depFn( thisMObject() );

		MPlug reflect = depFn.findPlug( "visibleInReflections", true );
		reflect.setBool( true );

		MPlug refract = depFn.findPlug( "visibleInRefractions", true );
		refract.setBool( true );
	}
}

bool dlOpenVDBShape::isBounded() const
{
	return true;
}

MBoundingBox dlOpenVDBShape::boundingBox() const
{
	updateGeo();

	MPoint min(m_shape_geo->m_minX, m_shape_geo->m_minY, m_shape_geo->m_minZ);
	MPoint max(m_shape_geo->m_maxX, m_shape_geo->m_maxY, m_shape_geo->m_maxZ);
	MPoint scaledMin;
	MPoint scaledMax;

	getScaledBBox(min, max, m_shape_geo->m_scale, scaledMin, scaledMax);

	return MBoundingBox(scaledMin, scaledMax);
}

// Horribly abuse the purpose of this method to notify the Viewport 2.0
// renderer that something about this shape has changed and that it should
// be retranslated.
MStatus
dlOpenVDBShape::setDependentsDirty(const MPlug& plug, MPlugArray& affected)
{
	if (plug == m_time || plug == m_filename || plug == m_smokeGrid ||
		plug == m_drawPoints ||
		plug == m_velocityScale ||
		plug == m_fileSequence.m_fileSequenceMode ||
		plug == m_fileSequence.m_firstIndex ||
		plug == m_fileSequence.m_lastIndex ||
		plug == m_fileSequence.m_offset ||
		plug == m_fileSequence.m_increment ||
		plug == m_fileSequence.m_outsideRangeMode ||
		plug == m_fileSequence.m_frameRangeMode ||
		plug == m_fileSequence.m_frameRangeStart ||
		plug == m_fileSequence.m_frameRangeEnd)
	{
		MHWRender::MRenderer::setGeometryDrawDirty(thisMObject());

		return MS::kSuccess;
	}

	return MPxSurfaceShape::setDependentsDirty(plug, affected);
}

MSelectionMask
dlOpenVDBShape::getShapeSelectionMask() const
{
	return MSelectionMask(MSelectionMask::kSelectLocators);
}

const dlOpenVDBShapeGeo* dlOpenVDBShape::getCurrentGeo() const
{
	return m_shape_geo;
}

MStatus dlOpenVDBShape::compute(const MPlug& i_plug, MDataBlock& i_data)
{
	bool computeBbox =
		i_plug.parent() == m_bboxMin ||
		i_plug.parent() == m_bboxMax;

	// We only know how to compute m_outFilename and the bbox.
	if ( i_plug != m_outFilename && !computeBbox )
	{
		return MS::kUnknownParameter;
	}

	MString filename("");
	MDataHandle handle;

	// To compute m_outFilename, we obviously need to figure the filename.
	bool needFilename = i_plug == m_outFilename;

	MString smoke_grid("");
	MVector min(-1.0, -1.0, -1.0);
	MVector max( 1.0,  1.0,  1.0);

	/*
		When computing the bbox, we need the filename only when a smoke grid is set.
	*/
	if( computeBbox )
	{
		handle = i_data.inputValue(m_smokeGrid);
		smoke_grid = handle.asString();

		if( smoke_grid.length() > 0 )
		{
			needFilename = true;
		}
	}

	// Figure the expanded VDB file, if needed
	if( needFilename )
	{
		handle = i_data.inputValue( m_filename );
		filename = handle.asString();

		handle = i_data.inputValue( m_fileSequence.m_fileSequenceMode );
		int seq_mode = handle.asInt();
		if( seq_mode > 0 && filename.length() > 0 )
		{
			filename = getFormattedFilename( filename );
		}

		handle = i_data.inputValue( m_time );
		MTime time = handle.asTime();
		double frame = time.value();
		frame = m_fileSequence.getSequenceNumber( thisMObject(), frame );

		char expanded_text[2048];
		expandEnvVars(
			expanded_text,
			sizeof( expanded_text )/sizeof( expanded_text[0] ),
			const_cast<char*>( filename.asChar() ),
			frame );

		filename = expanded_text;
		filename = Utilities::expandFToken( filename, frame );
	}

	// Compute plug values
	if( i_plug == m_outFilename )
	{
		handle = i_data.outputValue( m_outFilename );
		handle.set( filename );
		handle.setClean();
	}
	else if( computeBbox )
	{
		if( needFilename && filename.length() > 0 )
		{
			double bbox[6];
			if( DlVDBGetFileBBox( filename.asChar(), bbox ) )
			{
				min = MVector( bbox[0], bbox[1], bbox[2] );
				max = MVector( bbox[3], bbox[4], bbox[5] );
			}
			else
			{
				min = max = MVector();
			}
		}

		handle = i_data.outputValue( m_bboxMin );
		handle.set3Float( float(min.x), float(min.y), float(min.z) );
		handle.setClean();

		handle = i_data.outputValue( m_bboxMax );
		handle.set3Float( float(max.x), float(max.y), float(max.z) );
		handle.setClean();
	}

	i_data.setClean( i_plug );

	return MS::kSuccess;
}

void* dlOpenVDBShape::creator()
{
	return new dlOpenVDBShape;
}

MStatus dlOpenVDBShape::initialize()
{
	using namespace Utilities;

	m_fileSequence.initialize();

	MFnUnitAttribute unitAttrFn;
	m_time = unitAttrFn.create( "time", "tm", MFnUnitAttribute::kTime, 0.0 );
	makeInputAttribute( unitAttrFn );
	addAttribute( m_time );

	MFnTypedAttribute file_attr;
	m_filename = file_attr.create( "filename", "file", MFnData::kString );
	makeInputAttribute( file_attr );
	file_attr.setUsedAsFilename( true );
	addAttribute( m_filename );

	MFnTypedAttribute typedAttrFn;
	m_smokeGrid = typedAttrFn.create( "smokeGrid", "sg", MFnData::kString );
	makeInputAttribute( typedAttrFn );
	addAttribute( m_smokeGrid );

	m_smokeColorGrid =
		typedAttrFn.create( "smokeColorGrid", "scg", MFnData::kString );
	makeInputAttribute( typedAttrFn );
	addAttribute( m_smokeColorGrid );

	m_temperatureGrid =
		typedAttrFn.create( "temperatureGrid", "tg", MFnData::kString );
	makeInputAttribute( typedAttrFn );
	addAttribute( m_temperatureGrid );

	m_emissionIntenistyGrid =
		typedAttrFn.create( "emissionIntensityGrid", "eig", MFnData::kString );
	makeInputAttribute( typedAttrFn );
	addAttribute( m_emissionIntenistyGrid );

	m_emissionTintGrid =
		typedAttrFn.create( "emissionTintGrid", "emissiontg", MFnData::kString );
	makeInputAttribute( typedAttrFn );
	addAttribute( m_emissionTintGrid );

	m_velocityGrid =
		typedAttrFn.create( "velocityGrid", "vg", MFnData::kString );
	makeInputAttribute( typedAttrFn );
	addAttribute( m_velocityGrid );

	MFnNumericAttribute numAttrFn;
	m_velocityScale =
		numAttrFn.create( "velocityScale", "vs", MFnNumericData::kDouble, 1.0 );
	numAttrFn.setMin( 0.0 );
	makeInputAttribute( numAttrFn );
	addAttribute( m_velocityScale );

	m_drawPoints = numAttrFn.create("drawPoints", "dp", MFnNumericData::kBoolean, 0);
	makeInputAttribute( numAttrFn );
	addAttribute( m_drawPoints );

	/*
		This must be identical to the related extension attribute defined in
		DL_extensionAttrRegistry.
	*/
	m_visibilityDiffuse = numAttrFn.create(
		"_3delight_visibilityDiffuse",
		"_3delight_visibilityDiffuse",
		MFnNumericData::kBoolean,
		0.0 );

	numAttrFn.setNiceNameOverride( MString("Visible in Diffuse") );
	numAttrFn.setDefault( true );
	makeInputAttribute( numAttrFn );
	addAttribute( m_visibilityDiffuse );

	m_bboxMin =	numAttrFn.createPoint( "bboxMin", "min" );
	numAttrFn.setDefault(-1.0, -1.0, -1.0);
	makeOutputAttribute( numAttrFn );
	addAttribute( m_bboxMin );

	m_bboxMax =	numAttrFn.createPoint( "bboxMax", "max" );
	numAttrFn.setDefault(1.0, 1.0, 1.0);
	makeOutputAttribute( numAttrFn );
	addAttribute( m_bboxMax );

	m_outFilename = typedAttrFn.create( "outFilename", "ofile", MFnData::kString );
	makeOutputAttribute( typedAttrFn );
	addAttribute( m_outFilename );

	attributeAffects(m_time, m_bboxMin);
	attributeAffects(m_time, m_bboxMax);
	attributeAffects(m_time, m_outFilename);

	attributeAffects(m_filename, m_bboxMin);
	attributeAffects(m_filename, m_bboxMax);
	attributeAffects(m_filename, m_outFilename);

	attributeAffects(m_fileSequence.m_fileSequenceMode, m_bboxMin);
	attributeAffects(m_fileSequence.m_fileSequenceMode, m_bboxMax);
	attributeAffects(m_fileSequence.m_fileSequenceMode, m_outFilename);
	attributeAffects(m_fileSequence.m_firstIndex, m_bboxMin);
	attributeAffects(m_fileSequence.m_firstIndex, m_bboxMax);
	attributeAffects(m_fileSequence.m_firstIndex, m_outFilename);
	attributeAffects(m_fileSequence.m_lastIndex, m_bboxMin);
	attributeAffects(m_fileSequence.m_lastIndex, m_bboxMax);
	attributeAffects(m_fileSequence.m_lastIndex, m_outFilename);
	attributeAffects(m_fileSequence.m_offset, m_bboxMin);
	attributeAffects(m_fileSequence.m_offset, m_bboxMax);
	attributeAffects(m_fileSequence.m_offset, m_outFilename);
	attributeAffects(m_fileSequence.m_increment, m_bboxMin);
	attributeAffects(m_fileSequence.m_increment, m_bboxMax);
	attributeAffects(m_fileSequence.m_increment, m_outFilename);
	attributeAffects(m_fileSequence.m_outsideRangeMode, m_bboxMin);
	attributeAffects(m_fileSequence.m_outsideRangeMode, m_bboxMax);
	attributeAffects(m_fileSequence.m_outsideRangeMode, m_outFilename);
	attributeAffects(m_fileSequence.m_frameRangeMode, m_bboxMin);
	attributeAffects(m_fileSequence.m_frameRangeMode, m_bboxMax);
	attributeAffects(m_fileSequence.m_frameRangeMode, m_outFilename);
	attributeAffects(m_fileSequence.m_frameRangeStart, m_bboxMin);
	attributeAffects(m_fileSequence.m_frameRangeStart, m_bboxMax);
	attributeAffects(m_fileSequence.m_frameRangeStart, m_outFilename);
	attributeAffects(m_fileSequence.m_frameRangeEnd, m_bboxMin);
	attributeAffects(m_fileSequence.m_frameRangeEnd, m_bboxMax);
	attributeAffects(m_fileSequence.m_frameRangeEnd, m_outFilename);

	attributeAffects(m_smokeGrid, m_bboxMin);
	attributeAffects(m_smokeGrid, m_bboxMax);

	return MStatus::kSuccess;
}

/*
	\brief Updates the "m_shape_geo" struct members with the current maya
	attribute values
*/
void dlOpenVDBShape::updateGeo() const
{
	MObject node = thisMObject();
	MPlug bboxMinPlug(node, m_bboxMin);
	MPlug bboxMaxPlug(node, m_bboxMax);
//	MPlug bboxScalePlug(node, m_velocityScale);

	MFloatPoint min, max;
//	double scale;

	bboxMinPlug.child(0).getValue(min.x);
	bboxMinPlug.child(1).getValue(min.y);
	bboxMinPlug.child(2).getValue(min.z);
	bboxMaxPlug.child(0).getValue(max.x);
	bboxMaxPlug.child(1).getValue(max.y);
	bboxMaxPlug.child(2).getValue(max.z);

//	bboxScalePlug.getValue(scale);

	m_shape_geo->m_minX = std::min(min.x, max.x);
	m_shape_geo->m_minY = std::min(min.y, max.y);
	m_shape_geo->m_minZ = std::min(min.z, max.z);
	m_shape_geo->m_maxX = std::max(min.x, max.x);
	m_shape_geo->m_maxY = std::max(min.y, max.y);
	m_shape_geo->m_maxZ = std::max(min.z, max.z);
//	m_shape_geo->m_scale = scale;

	MPlug drawPointsPlug(node, m_drawPoints);
	drawPointsPlug.getValue(m_shape_geo->m_display_points);

	MPlug filePlug(node, m_filename);
	MPlug gridPlug(node, m_smokeGrid);
	MPlug timePlug(node, m_time);

	MString vdb_file;
	filePlug.getValue(vdb_file);

	MString vdb_grid;
	gridPlug.getValue(vdb_grid);

	if( m_shape_geo->m_display_points &&
		vdb_file.length()>0 && vdb_grid.length()>0 )
	{
		/* Resolve the frame number */
		MPlug seqModePlug(node, m_fileSequence.m_fileSequenceMode);
		int seqMode = 0;
		seqModePlug.getValue( seqMode );

		if( seqMode > 0 )
		{
			vdb_file = getFormattedFilename( vdb_file );
		}

		MTime time;
		timePlug.getValue(time);
		double frame = time.value();
		frame = m_fileSequence.getSequenceNumber(thisMObject(), frame);

		char expanded_text[2048];
		expandEnvVars(
			expanded_text,
			sizeof(expanded_text)/sizeof(expanded_text[0]),
			const_cast<char*>(vdb_file.asChar()),
			frame);

		vdb_file = expanded_text;
		vdb_file = Utilities::expandFToken( vdb_file, frame );


		if( vdb_file!=m_shape_geo->m_current_file ||
			vdb_grid!=m_shape_geo->m_current_grid)
		{
			/* Reload */
			m_shape_geo->m_current_file = vdb_file;
			m_shape_geo->m_current_grid = vdb_grid;

			const std::vector<float> *vertices = NULL;
			const std::vector<float> *colors = NULL;

			OpenVDBCache::GetGridVertices(
				vdb_file.asChar(), vdb_grid.asChar(), &vertices, &colors);

			if(vertices)
			{
				m_shape_geo->m_vertices = *vertices;

				if(colors && vertices->size()==colors->size())
				{
					m_shape_geo->m_colors = *colors;
				}
			}
		}
	}
}

// Geometry Override
//

// Render item names
const MString sBoundingBoxItemName = "VDBShapeBBox";
const MString sPointsItemName = "VDBShapePoints";

MHWRender::MPxGeometryOverride*
dlOpenVDBShapeGeometryOverride::Creator(const MObject& obj)
{
	return new dlOpenVDBShapeGeometryOverride(obj);
}

dlOpenVDBShapeGeometryOverride::dlOpenVDBShapeGeometryOverride(
	const MObject& obj) :
		MHWRender::MPxGeometryOverride(obj),
		m_shape(NULL)
{
	m_cb_id = MEventMessage::addEventCallback(
		"timeChanged", timeChangedCB, this);

	MStatus status;
	MFnDependencyNode node(obj, &status);
	if (status)
		m_shape = dynamic_cast<dlOpenVDBShape*>(node.userNode());
}

dlOpenVDBShapeGeometryOverride::~dlOpenVDBShapeGeometryOverride()
{
	m_shape = NULL;

	if (m_cb_id != 0)
	{
		MMessage::removeCallback(m_cb_id);
		m_cb_id = 0;
	}
}

// For some reason, when the shape's time has a connection from the time
// node, setDependentsDirty() is not always called. Use a timeChanged
// callback to get the Viewport 2.0 renderer to redraw the shape when
// changing the frame.
//
void
dlOpenVDBShapeGeometryOverride::timeChangedCB(void* data)
{
	dlOpenVDBShapeGeometryOverride* ovr =
		static_cast<dlOpenVDBShapeGeometryOverride*>(data);

	if (ovr && ovr->m_shape)
	{
		MHWRender::MRenderer::setGeometryDrawDirty(ovr->m_shape->thisMObject());
	}
}

MHWRender::DrawAPI
dlOpenVDBShapeGeometryOverride::supportedDrawAPIs() const
{
	return MHWRender::kAllDevices;
}

void
dlOpenVDBShapeGeometryOverride::updateDG()
{
	if (!m_shape)
		return;

	m_shape->updateGeo();
	const dlOpenVDBShapeGeo* shapeGeo = m_shape->getCurrentGeo();

	if (shapeGeo->m_minX != m_minX ||
		shapeGeo->m_minY != m_minY ||
		shapeGeo->m_minZ != m_minZ ||
		shapeGeo->m_maxX != m_maxX ||
		shapeGeo->m_maxY != m_maxY ||
		shapeGeo->m_maxZ != m_maxZ ||
		shapeGeo->m_scale != m_scale)
	{
		m_bbox_dirty = true;
	}

	m_minX = shapeGeo->m_minX;
	m_minY = shapeGeo->m_minY;
	m_minZ = shapeGeo->m_minZ;

	m_maxX = shapeGeo->m_maxX;
	m_maxY = shapeGeo->m_maxY;
	m_maxZ = shapeGeo->m_maxZ;

	m_scale = shapeGeo->m_scale;

	m_display_points = shapeGeo->m_display_points;
	if (m_display_points)
	{
		if (shapeGeo->m_current_file != m_current_file ||
			shapeGeo->m_current_grid != m_current_grid)
		{
			m_display_points = m_display_points &&
				!shapeGeo->m_vertices.empty();
			m_points_dirty = m_display_points;
		}

		m_current_file = shapeGeo->m_current_file;
		m_current_grid = shapeGeo->m_current_grid;
	}
}

bool
dlOpenVDBShapeGeometryOverride::isIndexingDirty(
	const MHWRender::MRenderItem& item)
{
	if (item.name() == sPointsItemName)
		return m_points_dirty;

	return false;
}

bool
dlOpenVDBShapeGeometryOverride::isStreamDirty(
	const MHWRender::MVertexBufferDescriptor& desc)
{
	if (desc.name() == sBoundingBoxItemName)
		return m_bbox_dirty;

	if (desc.name() == sPointsItemName)
		return m_points_dirty;

	return false;
}

void
dlOpenVDBShapeGeometryOverride::updateRenderItems(
	const MDagPath& path, MHWRender::MRenderItemList& list)
{
	MHWRender::MRenderer* renderer = MHWRender::MRenderer::theRenderer();
	if (!renderer) return;

	const MHWRender::MShaderManager* shaderMgr = renderer->getShaderManager();
	if (!shaderMgr) return;

	MHWRender::MShaderInstance* shader;

	MHWRender::MRenderItem* boundingBoxItem;
	int index = list.indexOf(sBoundingBoxItemName);

	if (index < 0)
	{
		boundingBoxItem = MHWRender::MRenderItem::Create(
			sBoundingBoxItemName,
			MHWRender::MRenderItem::DecorationItem,
			MHWRender::MGeometry::kLines);
		boundingBoxItem->setDrawMode(MHWRender::MGeometry::kAll);
		boundingBoxItem->depthPriority(5);

		list.append(boundingBoxItem);

		shader = shaderMgr->getStockShader(MHWRender::MShaderManager::k3dSolidShader);
		if (shader)
		{
			boundingBoxItem->setShader(shader, &sBoundingBoxItemName);
			shaderMgr->releaseShader(shader);
		}
	}
	else
	{
		boundingBoxItem = list.itemAt(index);
	}

	shader = boundingBoxItem->getShader();
	if (shader)
	{
		MColor color = MHWRender::MGeometryUtilities::wireframeColor(path);
		float wireframeColor[4] = {color.r, color.g, color.b, 1.0f};
		shader->setParameter("solidColor", wireframeColor);
		shaderMgr->releaseShader(shader);
	}

	boundingBoxItem->enable(true);

	MHWRender::MRenderItem* pointsItem;
	index = list.indexOf(sPointsItemName);

	if (index < 0)
	{
		pointsItem = MHWRender::MRenderItem::Create(
			sPointsItemName,
			MHWRender::MRenderItem::DecorationItem,
			MHWRender::MGeometry::kPoints);
		pointsItem->setDrawMode(
			(MHWRender::MGeometry::DrawMode) (MHWRender::MGeometry::kAll & ~MHWRender::MGeometry::kBoundingBox));
		pointsItem->depthPriority(5);

		list.append(pointsItem);

		shader = shaderMgr->getStockShader(MHWRender::MShaderManager::k3dCPVSolidShader);
		if (shader)
		{
			pointsItem->setShader(shader, &sPointsItemName);
			shaderMgr->releaseShader(shader);
		}
	}
	else
	{
		pointsItem = list.itemAt(index);
	}

	pointsItem->enable(m_display_points);
}

void
dlOpenVDBShapeGeometryOverride::populateGeometry(
	const MHWRender::MGeometryRequirements& requirements,
	const MHWRender::MRenderItemList& renderItems,
	MHWRender::MGeometry& data)
{
	const dlOpenVDBShapeGeo* shapeGeo = m_shape->getCurrentGeo();

	unsigned int numPoints = shapeGeo->m_vertices.size() / 3;
	unsigned int numColors = shapeGeo->m_colors.size() / 3;

	MHWRender::MVertexBuffer* bboxVertsBuffer = NULL;
	float* bboxVerts = NULL;

	MHWRender::MVertexBuffer* pointsVertsBuffer = NULL;
	float* pointsVerts = NULL;

	MHWRender::MVertexBuffer* pointsColorsBuffer = NULL;
	float* pointsColors = NULL;

	const MHWRender::MVertexBufferDescriptorList& descList =
		requirements.vertexRequirements();
	int numVertRequirements = descList.length();

	MHWRender::MVertexBufferDescriptor desc;
	for (int reqNum = 0; reqNum < numVertRequirements; ++reqNum)
	{
		if (!descList.getDescriptor(reqNum, desc))
			continue;

		switch (desc.semantic())
		{
			case MHWRender::MGeometry::kPosition:
			{
				if (desc.name() == sBoundingBoxItemName)
				{
					if (!bboxVertsBuffer)
					{
						bboxVertsBuffer = data.createVertexBuffer(desc);
						if (bboxVertsBuffer)
							bboxVerts = (float*) bboxVertsBuffer->acquire(8, true);
					}
				}
				else if (desc.name() == sPointsItemName)
				{
					if (!pointsVertsBuffer)
					{
						pointsVertsBuffer = data.createVertexBuffer(desc);
						if (pointsVertsBuffer)
							pointsVerts = (float*) pointsVertsBuffer->acquire(numPoints, true);
					}
				}
			}
			break;
			case MHWRender::MGeometry::kColor:
			{
				if (desc.name() == sPointsItemName)
				{
					if (!pointsColorsBuffer)
					{
						pointsColorsBuffer = data.createVertexBuffer(desc);
						if (pointsColorsBuffer)
							pointsColors = (float*) pointsColorsBuffer->acquire(numColors, true);
					}
				}
			}
			break;
			default:
				// do nothing for stuff we don't understand
				break;
		}
	}

	if (bboxVertsBuffer && bboxVerts)
	{
		MPoint scaledMin, scaledMax;
		getScaledBBox(
			MPoint(m_minX, m_minY, m_minZ),
			MPoint(m_maxX, m_maxY, m_maxZ),
			m_scale,
			scaledMin,
			scaledMax);

		double sMin[3] = {scaledMin.x, scaledMin.y, scaledMin.z};
		double sMax[3] = {scaledMax.x, scaledMax.y, scaledMax.z};

		int v = 0;
		bboxVerts[v++] = sMin[0]; bboxVerts[v++] = sMin[1]; bboxVerts[v++] = sMin[2];
		bboxVerts[v++] = sMax[0]; bboxVerts[v++] = sMin[1]; bboxVerts[v++] = sMin[2];
		bboxVerts[v++] = sMax[0]; bboxVerts[v++] = sMax[1]; bboxVerts[v++] = sMin[2];
		bboxVerts[v++] = sMin[0]; bboxVerts[v++] = sMax[1]; bboxVerts[v++] = sMin[2];
		bboxVerts[v++] = sMin[0]; bboxVerts[v++] = sMin[1]; bboxVerts[v++] = sMax[2];
		bboxVerts[v++] = sMax[0]; bboxVerts[v++] = sMin[1]; bboxVerts[v++] = sMax[2];
		bboxVerts[v++] = sMax[0]; bboxVerts[v++] = sMax[1]; bboxVerts[v++] = sMax[2];
		bboxVerts[v++] = sMin[0]; bboxVerts[v++] = sMax[1]; bboxVerts[v++] = sMax[2];

		bboxVertsBuffer->commit(bboxVerts);
	}

	if (pointsVertsBuffer && pointsVerts)
	{
		std::copy(
			shapeGeo->m_vertices.begin(), shapeGeo->m_vertices.end(),
			pointsVerts);
		pointsVertsBuffer->commit(pointsVerts);
	}

	if (pointsColorsBuffer && pointsColors)
	{
		std::size_t c = 0;
		for (std::size_t i = 0; i < shapeGeo->m_colors.size(); i += 3)
		{
			pointsColors[c++] = shapeGeo->m_colors[i];
			pointsColors[c++] = shapeGeo->m_colors[i+1];
			pointsColors[c++] = shapeGeo->m_colors[i+2];
			pointsColors[c++] = 1.0f;
		}

		pointsColorsBuffer->commit(pointsColors);
	}

	for (int j = 0; j < renderItems.length(); ++j)
	{
		const MHWRender::MRenderItem* item = renderItems.itemAt(j);

		if (!item)
			continue;

		if (item->name() == sBoundingBoxItemName)
		{
			MHWRender::MIndexBuffer* bboxIndexBuffer =
				data.createIndexBuffer(MHWRender::MGeometry::kUnsignedInt32);
			unsigned int* bboxIndices =
				(unsigned int*) bboxIndexBuffer->acquire(24, true);

			int i = 0;

			// front face
			bboxIndices[i++] = 0; bboxIndices[i++] = 1;
			bboxIndices[i++] = 1; bboxIndices[i++] = 2;
			bboxIndices[i++] = 2; bboxIndices[i++] = 3;
			bboxIndices[i++] = 3; bboxIndices[i++] = 0;

			// back face
			bboxIndices[i++] = 4; bboxIndices[i++] = 5;
			bboxIndices[i++] = 5; bboxIndices[i++] = 6;
			bboxIndices[i++] = 6; bboxIndices[i++] = 7;
			bboxIndices[i++] = 7; bboxIndices[i++] = 4;

			bboxIndices[i++] = 0; bboxIndices[i++] = 4;
			bboxIndices[i++] = 3; bboxIndices[i++] = 7;
			bboxIndices[i++] = 2; bboxIndices[i++] = 6;
			bboxIndices[i++] = 1; bboxIndices[i++] = 5;

			bboxIndexBuffer->commit(bboxIndices);
			item->associateWithIndexBuffer(bboxIndexBuffer);
		}
		else if (item->name() == sPointsItemName)
		{
			MHWRender::MIndexBuffer* pointsIndexBuffer =
				data.createIndexBuffer(MHWRender::MGeometry::kUnsignedInt32);
			unsigned int* pointsIndices =
				(unsigned int*) pointsIndexBuffer->acquire(numPoints, true);

			for (unsigned int i = 0; i < numPoints; ++i)
				pointsIndices[i] = i;

			pointsIndexBuffer->commit(pointsIndices);
			item->associateWithIndexBuffer(pointsIndexBuffer);
		}
	}
}

void
dlOpenVDBShapeGeometryOverride::cleanUp()
{
	m_bbox_dirty = false;
	m_points_dirty = false;
}
