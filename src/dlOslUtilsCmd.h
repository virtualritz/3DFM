#ifndef __dlOslUtilsCmd_h__
#define __dlOslUtilsCmd_h__

#include <maya/MPxCommand.h>

#define DLOSLUTILSCMD_STR "dlOslUtils"

class dlOslUtilsCmd : public MPxCommand
{
public:
	virtual MStatus doIt( const MArgList& i_argList );

	virtual bool isUndoable() const { return false; }

	static void* creator() { return new dlOslUtilsCmd; }

	static MSyntax newSyntax();
};

#endif
