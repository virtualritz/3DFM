#include <nsi.h>
#include <string>
#include <maya/MGlobal.h>

/**
	We don't print the error code in the error message. Useless for the user.
*/
void NSIErrorHandlerMaya(
	void *userdata, int level, int code, const char *message )
{
	const char *pre = (const char *)userdata;
	std::string buffer( "3Delight" );

	if( pre )
	{
		buffer += " (" + std::string(pre) + "): ";
	}
	else
		buffer += ": ";

	buffer += message;

	switch( level )
	{
		case NSIErrMessage:
			MGlobal::displayInfo( buffer.c_str() );
			break;
		case NSIErrInfo:
			MGlobal::displayInfo( buffer.c_str() );
			break;
		case NSIErrWarning:
			MGlobal::displayWarning( buffer.c_str() );
			break;
		default:
		case NSIErrError:
			MGlobal::displayError( buffer.c_str() );
			break;
	}
}


