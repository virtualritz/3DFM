/*
  Copyright (c) 2012 soho vfx inc.
  Copyright (c) 2012 The 3Delight Team.
*/

#ifndef __dlEnvironmentShape_h__
#define __dlEnvironmentShape_h__

#include <maya/MPxDrawOverride.h>
#include <maya/MPxSurfaceShape.h>

// Shape class - defines the non-UI part of a shape node
class dlEnvironmentShape : public MPxSurfaceShape
{
  public:
    dlEnvironmentShape();
    virtual ~dlEnvironmentShape(); 

    virtual void postConstructor();
    virtual MStatus compute( const MPlug&, MDataBlock& );

    virtual bool setInternalValue(
        const MPlug&,
        const MDataHandle& );

    virtual bool isBounded() const;
    virtual MBoundingBox boundingBox() const; 

    static  void* creator();
    static  MStatus initialize();

    static MTypeId id;

    static MString drawDbClassification;
    static MString drawRegistrantId;

  private:
    friend class dlEnvironmentShapeDrawOverride;

    // Attributes
    static MObject m_mappingObj;
    static MObject m_textureObj;
    static MObject m_radiusObj;
    static MObject m_intensityObj;
    static MObject m_exposureObj;
    static MObject m_tintObj;
    static MObject m_useBackgroundTextureObj;
    static MObject m_backgroundTextureObj;
    static MObject m_visibleToCameraObj;
    static MObject m_prelitObj;
    static MObject m_diffuseContrObj;
    static MObject m_specularContrObj;
    static MObject m_hairContrObj;
    static MObject m_volumeContrObj;

    unsigned m_textureVersion;

	/* We keep an internal copy of this. Makes returning bbox easier. */
    float m_radius;
};

// Shape in VP2
class dlEnvironmentShapeDrawOverride : public MHWRender::MPxDrawOverride
{
public:
    static MHWRender::MPxDrawOverride* creator(const MObject& i_obj);

    virtual ~dlEnvironmentShapeDrawOverride();

    virtual MHWRender::DrawAPI supportedDrawAPIs() const;

    virtual MMatrix transform(const MDagPath &i_objPath,
        const MDagPath &i_cameraPath) const;

    virtual MBoundingBox boundingBox(const MDagPath &i_objPath,
      const MDagPath &i_cameraPath) const;

    virtual bool isBounded(const MDagPath &i_objPath,
        const MDagPath &i_cameraPath) const;

    virtual MUserData* prepareForDraw(const MDagPath &i_objPath,
        const MDagPath &i_cameraPath,
        const MHWRender::MFrameContext &i_frameContext,
        MUserData *i_oldData);

	virtual bool hasUIDrawables() const;

	virtual void addUIDrawables (
		const MDagPath &objPath,
		MUIDrawManager &drawManager,
		const MFrameContext &frameContext,
		const MUserData *data);

private:
	void addSphere(
		MUIDrawManager &drawManager,
		float i_radius,
		int i_numSubdiv,
		bool i_isLightProbe);

private:
    dlEnvironmentShapeDrawOverride(const MObject &i_obj);
};

#endif

