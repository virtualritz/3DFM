#ifndef __NSIExportInstancer_h
#define __NSIExportInstancer_h

#include "NSIExportDelegate.h"

#include <maya/MObject.h>
#include <maya/MIntArray.h>
#include <maya/MMatrixArray.h>
#include <maya/MDagPathArray.h>
#include <maya/MDagPath.h>

#include <string>

class NSIExportTransform;

/**
	\sa NSIExportDelegate
*/
class NSIExportInstancer : public NSIExportDelegate
{
public:
	NSIExportInstancer(
		MDagPath &,
		MFnDagNode &,
		const NSIExportDelegate::Context& i_context );
	~NSIExportInstancer();
	virtual const char *NSINodeType( void ) const;
	virtual void Create( void );
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual bool IsDeformed( void ) const { return true; }

private:
	MDagPath m_path;

	/* A delegate to export instancers main transform. */
	NSIExportTransform *m_transform;

	std::string m_instance_handle;
};

#endif
