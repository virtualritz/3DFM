#ifndef __DL_STRINGINTMAP_H__
#define __DL_STRINGINTMAP_H__

#include <map>

#include "DL_mstringSTLutils.h"

#include <maya/MStringArray.h>

// this is a hash table containing <MString,int> maps
class DL_stringIntMap
{
public:
  DL_stringIntMap();
  ~DL_stringIntMap();

  // clears all the maps but doesn't remove them
  void    clearAllMaps();

  // clears and removes all maps
  void    removeAllMaps();

  // clear the given map
  bool    clearMap(const MString& map_name);

  // clear and remove the given map
  bool    removeMap(const MString& map_name);

  bool    addMap(const MString& map_name);
  bool    containsMap(const MString& map_name);


  bool    set(const MString& map_name, const MString& key, int value);
  bool    set(const MString& map_name, const MString& key,const MString& value);
  bool set(
    const MString& map_name, const MString& key, const MStringArray& value);
  bool    get(int& value, const MString& map_name, const MString& key);
  bool    get(MString& value, const MString& map_name, const MString& key);
  bool    get(MStringArray& value, const MString& map_name, const MString& key);
  bool    mapContains(const MString& map_name, const MString& key);
  bool    removeKeyInt(const MString& map_name, const MString& key);
  bool    removeKeyString(const MString& map_name, const MString& key);

  bool    getAllKeys(MStringArray& keys, const MString& map_name);

private:

  typedef std::map<MString, int, delight_mstringLessThan> t_intMapType;
  typedef std::map<MString, MString, delight_mstringLessThan> t_stringMapType;
  typedef std::map<MString, MStringArray, delight_mstringLessThan> t_arrayMapType;

  struct combinedMaps
  {
    t_intMapType m_ints;
    t_stringMapType m_strings;
    t_arrayMapType m_arrays;
  };

  typedef std::map<MString, combinedMaps, delight_mstringLessThan> t_tableType;

  t_tableType m_hashTable;

};
#endif
