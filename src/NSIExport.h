/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#ifndef __NSIEXPORT_H__
#define __NSIEXPORT_H__

#include "NSIExportDelegate.h"
#include "NSIDelegatesContainer.h"
#include "RenderPassInterface.h"

#include <maya/MBoundingBox.h>
#include <maya/MDagPath.h>
#include <maya/MString.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MMessage.h>
#include <maya/MDagMessage.h>
#include <maya/MNodeMessage.h>
#include <maya/MObjectArray.h>
#include <maya/MEventMessage.h>
#include <maya/MObjectHandle.h>
#include <vector>
#include <map>
#include <memory>

#include <nsi.hpp>

#include "DL_mstringSTLutils.h"

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MComputation;
class MDagPath;
class MFnCamera;
class MFnDagNode;
class MFloatArray;
class MIntArray;
#endif
struct CachedExporterData;
class NSIExportMesh;
class NSIExportCamera;
class RenderPassInterface;

class NSIExport
{
public:
	enum ExportMode
	{
		e_invalid,
		e_maya_batch,
		e_maya_batch_sequence,
		e_interactive,
		e_live,
		e_live_in_vp2,
		e_export_scene,
		e_export_archive,
		e_interactive_frame_sequence
	};

public:
	NSIExport(
		MObject &i_pass,
		bool i_canApplyOverrides,
		MComputation* i_computation );

	virtual ~NSIExport();

	/**
		\brief Export the scene as specified in the render pass (could be
		multiple frames)
	*/
	void Export( ExportMode i_mode, const MDagPath& i_root );


	/**
		\brief Stops a render. Only valid during an ongoing render.
	*/
	void StopRender( void );

	bool HasMotionBlur( void ) const { return m_motion_blur; }

	/**
		\brief Get the export model.
	*/
	ExportMode GetMode() const { return m_export_mode; }

	static const char* TransparentSurfaceHandle();

private:

	void InitiateRender();

	/**
		\brief Export one frame of the scene at the specified time

		\param i_time
			The time (frame number) to export.

		\param i_lastFrame
			When true, indicates that this is the last frame to render. In
			interactive mode, this means we will not wait for its completion to
			return.
	*/
	void ExportFrame( double i_time );

	/**
		\brief Issues NSI's begin and end commands
	*/
	void Begin( double i_time );
	void End();

	/// Exports a file containing additional information for an NSI archive.
	void ExportArchiveSidecar()const;

	/**
		\brief Scans the DAG and producdes the delegates for each known Maya
		object.

		\param i_filter
			A list of MFn::Types. Those recognized Maya nodes that we will be
			scanning for.

		\param i_first_time
			=true if we are calling this method the first time. This is only
			relevant for IPR operation where the ScanDAG can be called more
			that one time.
	*/
	void ScanDAG( MIntArray &i_filter, bool i_first_time = true );

	void ScanDependencyNodes( void );

	/**
		\brief Export NSI nodes that contain geometric object attributes.

		Those nodes are meant to be connected to the objects that
		have the attributes enabled.
	*/
	void ExportObjectAttributeNodes();

	/**
		\brief Connect the specified object to relevant object attribute nodes.
	*/
	void ConnectToObjectAttributesNodes( MFnDagNode& i_dagNode );

	/**
		\brief Geometric Object attributes types

		These attributes are handled by a connection to global
		attribute nodes that define the non-default NSI value.
	*/
	enum GeoAttribute
	{
		e_matte = 0,
		e_prelit,
		e_visCamera,
		e_visShadow,
		e_visDiffuse,
		e_visReflection,
		e_visRefraction,
		e_invalidAttribute
	};

	/**
		\brief The NSI handle of the global attribute node
	*/
	static const char* GeoAttributeNodeHandle( GeoAttribute i_type );
	/**
		\brief The name of the NSI attribute
	*/
	static const char* GeoAttributeNSIName( GeoAttribute i_type );
	/**
		\brief The name of the Maya attribute
	*/
	static const char* GeoAttributeMayaName( GeoAttribute i_type );
	/**
		\brief The attribute's default NSI value
	*/
	static bool GeoAttributeNSIDefaultValue( GeoAttribute i_type );
	/**
		\brief Returns true if the Maya attribute has its default value
	*/
	static bool GeoAttributeHasMayaDefaultValue(
		GeoAttribute i_type,
		MPlug i_attr );
	/**
		\brief Connect or disconnect the speficied object to the attribute node
	*/
	static void AdjustGeoAttributeConnection(
		NSIContext_t i_nsi,
		MFnDagNode& i_dagNode,
		GeoAttribute i_type,
		bool i_first_time,
		bool i_live);

	void ExportTransparentSurface();

	/**
		\brief Returns the handle of the atmosphere attributes node.
	*/
	MString AtmosphereAttributesNodeHandle();

	NSIExportDelegate *ExportAtmosphere();

	void ExportMatteObjects( bool i_first_time ) const;

	void ExportRenderNotes( double i_time ) const;

	/**
		\brief Call the finalize function on each custom exporter we know about.
	*/
	void FinalizeCustomExporters();

protected:

	/*
		\brief Returns true if we are in a live session.
	*/
	bool IsLive( void ) const
	{
		return m_export_mode == e_live ||
			m_export_mode == e_live_in_vp2;
	}

	/*
		\returns
			true if we are doing command line batch render.
	*/
	bool IsBatch() const
	{
		return m_export_mode == e_maya_batch ||
			m_export_mode == e_maya_batch_sequence;
	}

	/*
		\returns
			true if we are doing a sequence render (batch or interactive).
	*/
	bool IsSequence() const
	{
		return m_export_mode == e_maya_batch_sequence ||
			m_export_mode == e_interactive_frame_sequence;
	}

	/**
		\brief Output the camera with light catagories and the output drivers.
	*/
	virtual void ExportImageLayers( void );

	/**
		\brief Produce an outputlayer NSI node.

		\param i_handle
			The NSI handle of the outputlayer node to be created.
		\param i_index
			The index of the related render pass image layer
		\param i_variable_source
			The "variablesource" attribute for the NSI "outputlayer" node
			("attribute", "shader" or "builtin").
		\param i_layer_type
			The "layertype" attribute for the NSI "outputlayer" node ("color",
			"vector" or "scalar").
		\param i_aov
			The aov
		\param i_alpha
			Indicates if the layer has alpha
		\param i_format
			The scalar format string
		\param i_colorprofile
			The color profile to use. empty if no color profile.
		\param i_camera
			The layer camera
		\param i_lightCategory
			The layer light category
		\param i_driver_handle
			The NSI handle of the output driver to which the layer should be
			connected.
		\param i_driver_name
			The type of the output driver (ie : the file format or framebuffer
			type).
		\param io_sort_key
			The layer's order, will be incremented automatically according to
			the number of layers actually produced.
	*/
	void ExportOneOutputLayer(
		const MString& i_handle,
		int i_index,
		const MString& i_variable_source,
		const MString& i_layer_type,
		const MString& i_aov,
		int i_alpha,
		const MString& i_format,
		const MString& i_screen,
		const MObject& i_lightCategory,
		const MString& i_driver_handle,
		const MString& i_driver_name,
		unsigned& io_sort_key );

private:

	/**
		\brief Produce a NSI handle for an outputdriver.

		\param i_filename
			The filename of the output driver
		\param i_drivername
			The name of the driver.
		\param o_first_request
			Will be set to true if this is the first time a given handle is
			returned.
	*/
	MString OutputDriverHandle(
		const MString& i_filename,
		const MString& i_drivername,
		bool& o_first_request );

	/**
		\brief Export the outputlayer feedback data.

		\param i_handle
			The outputlayer's NSI handle
		\param i_lightCategory
			The current light category.
	*/
	void ExportLayerFeedbackData(
		const MString& i_handle,
		const MString& i_lightCategory ) const;

	/**
		\brief Converts a light category to a string appropriate for <light>
		token substitution.

		This should probably go elsewhere and have a proper C++ implementation.

		\param A layer's light category.
	*/
	MString LightTokenFromLightCategory(const MString& i_category) const;

	/**
		\brief Returns the name of the framebuffer driver to use.
	*/
	MString FramebufferDriver() const;

	/**
		\brief Returns if the specified driver supports multiple layers in a file.
	*/
	bool DriverSupportsMultipleLayers(const MString& i_driver ) const;

	/**
		\brief Export quality related attributes
	*/
	void ExportQualityGroup( void ) const;

	/**
		\brief Export the frame number.
	*/
	void ExportFrameID( double i_time ) const;

	/**
		\brief Export overrides related attributes
	*/
	void ExportOverridesGroup( void ) const;

	/**
		\brief Export crop related attributes for prioritywindow in live
	*/
	void ExportCropGroup(const MString& i_screen_handle) const;

	/**
		\brief Registers necessary callbacks for IPR functioning
	*/
	void RegisterIPRCallbacks( void );
	static void RegisterDelegateCallbacks(
		NSIExportDelegate *i_delegate,
		NSIExport* i_exporter );

	static void GetArchivableObjectTypes(MIntArray& o_fnTypes);
	/**
		\brief Return the renderable object types.
	*/
	static void GetRenderableObjectTypes(MIntArray& o_fnTypes);

	static bool IsObjectVisible(MFnDependencyNode& i_depNode);
	static bool IsObjectRenderable(MFnDagNode& i_depNode);
	static bool IsObjectUsedByInstancer(MFnDagNode& i_dagNode);

	bool CreateDelegate(
		MDagPath &,
		MFnDagNode& i_dagNode, NSIExportDelegate *&o_delegate,
		bool i_live=false );

	NSIExportDelegate* CreateDelegate(MObject& node);

	/**
		\brief Called by CreateDelegate when a custom NSI delegate may be used
	*/
	NSIExportDelegate* CreateCustomDelegate( MObject& node );

	/**
		\brief Calls SetAttributes on all delegates
	*/
	void SetDelegateAttributes();

	/**
		\brief Calls SetDelegateAttributesAtTime on all delegates.
	*/
	void SetDelegateAttributesAtTime( double i_time );

	/**
		\brief Calls the Finalize on all delegates.
	*/
	void SetDelegateFinalized();

protected:

	/**
		\brief Return the delegate instead of using m_delegates directly.
	*/
	NSIExportDelegate* GetDelegate( const MString& handle );

	/**
		\brief A live render callback for renderpass's attribute changes
	*/
	virtual void RenderPassAttributeChanged(
		MNodeMessage::AttributeMessage i_msg,
		MPlug &i_plug,
		MPlug &i_otherPlug );

private:

	/**
		\brief Connects or disconnects a node to/from its parent
		\param i_dagNode
			The node to be connected/disconnected
		\param i_connect
			If true makes a NSIConnect, if false make a NSIDisconnect
	*/
	void connectToParent(
		NSIContext_t, MFnDagNode& i_dagNode, bool i_connect = true );

	void clearFlag(unsigned int i_flag);

	/**
		\brief A live render callback for renderpass's attribute changes
	*/
	static void RenderPassAttributeChangedCallback(
		MNodeMessage::AttributeMessage i_msg,
		MPlug &i_plug,
		MPlug &/*i_otherPlug*/,
		void *i_data );

	static void ObjectsToRenderCB(
		MNodeMessage::AttributeMessage, MPlug &, MPlug &, void * );

	static void LightsToRenderCB(
		MNodeMessage::AttributeMessage, MPlug &, MPlug &, void * );

	/**
		\brief A live render callback for node's attribute changes
	*/
	static void AttributeChangedCallback(
		MNodeMessage::AttributeMessage i_msg,
		MPlug &i_plug,
		MPlug &/*i_otherPlug*/,
		void *i_data );

	static void ChildRemovedCB(
		MDagPath &i_child, MDagPath &i_parent, void *i_exporter );
	static void NodeAddedCB( MObject &, void *i_pass );
	static void NodeRemovedCB( MObject &, void *i_pass );
	static void NodeDirtyPlugCB( MObject &, MPlug& i_plug, void *i_pass );
	static void InstanceCB(
			MDagMessage::DagMessage msgType,
			MDagPath &child, MDagPath &parent, void *clientData );

	/**
		\brief Creates a context for sequence rendering.
	*/
	void CreateSequenceContext();

	/**
		\brief Callback for NSIRenderControl::stoppedcallback
	*/
	static void RenderStoppedCallback(
		void* i_data,
		NSIContext_t i_context,
		int i_status );

	void ReIssueDAGObjects( MFn::Type i_type );

	/**
		\brief Alters the delegates number of motion samples for members of NSI
		sets that have the relevant override enabled.
	*/
	void OverrideNumMotionSamples();

	void RegisterDlSets();
	void RegisterDlSetToOneDelegate( MObject &setfn, bool i_edit );

	/// Returns true when the given object is a light source.
	bool IsLight( const MFnDependencyNode& i_object );

	/// Returns true if the given object is a geometric object.
	bool IsGeo( const MFnDagNode& i_object );

	/// Returns true when there is a progress bar & the user pressed ESC
	bool CancelRequested();

	/// Sets the progress bar value
	void SetProgressValue( int i_value );

	/// Returns the current progress bar value
	int GetProgressValue();

	/// Set the nice name for all nodes coming from a reference
	void SetReferencesNiceName();

protected:

	/* Container for delegates */
	NSIDelegatesContainer m_delegates;

	/** The NSI context where the scene is exported */
	NSIContext_t m_context;

	/* The render pass being rendered */
	std::unique_ptr<RenderPassInterface> m_render_pass;
private:

	ExportMode m_export_mode;

	/// The bounding box of all objects exported to an archive
	MBoundingBox m_archive_bbox;

	/* == true if motion blur enabled */
	bool m_motion_blur;

	/* Time at which the scene is to be exported . */
	double m_export_time;
	double m_shutter_angle;

	/* Maximum number of motion samples throughout the entire scene, be it
	   transformation or defrormation. */
	int m_max_motion_steps;

	/* Flags attached to dag nodes */
	unsigned int m_isOutput;

	/** callbacks for not addition and removal */
	MCallbackId m_node_added_id, m_node_removed_id, m_instance_added_id,
		m_instance_removed_id;

	/** Render pass attributes callback */
	MCallbackId m_render_pass_id;

	/** A callback on the set the defines the objects to render */
	MCallbackId m_objects_to_render_id;

	/** A callback on the set the defines the lights to render */
	MCallbackId m_lights_to_render_id;

	/* map of output drivers */
	std::map<MString, MString, delight_mstringLessThan> m_output_driver_map;

	/// The root of the sub-scene to export
	MDagPath m_root;

	/**
		Map to Maya node type names to their custom delegate creator function.
		A null value means that we already checked for such a function and none
		was found.
	*/
	std::map<MString, std::shared_ptr<CachedExporterData>,
		delight_mstringLessThan> m_customExporters;

	/**
		Then temporary directoiry we create to to write out NSI files
		prior to running renderdl
	*/
	MString m_export_directory;

	/** The context which controls rendering sequences. */
	std::shared_ptr<NSI::Context> m_sequence_context;

	/**
		The optional progress bar indicator
	*/
	MComputation* m_computation;
	int m_frameProgressIncrement;

	/**
		Accumulates reference nodes so they can be processed once all relevant
		dependency nodes have a delegate.
	*/
	MObjectArray m_references;
};

#endif
