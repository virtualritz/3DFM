/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/
#include "MaterialViewer.h"
#include "nsi.hpp"

#include "NSIErrorHandler.h"
#include "NSIExportCamera.h"
#include "NSIExportMaterialViewerLight.h"
#include "NSIExportMesh.h"
#include "NSIExportShader.h"
#include "DL_utils.h"
#include "OSLUtils.h"

#if MAYA_API_VERSION >= 201600
#include <maya/MColor.h>
#include <maya/MDagPath.h>
#include <maya/MFnAreaLight.h>
#include <maya/MFnCamera.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnDagNode.h>
#include <maya/MObjectArray.h>
#include <maya/MItDependencyGraph.h>

#include <string.h>
#include <stdlib.h>

// #define DEBUG_OUTPUT

/**
	MaterialViewer
	A MPxRenderer implementation. Currently only used for the rendering of
	images displayed in the Hyperhsade's Material Viewer panel, which is a
	fancy shader preview.

	Flow of events:
	- MaterialViewer object created upon plug-in load.
	- When the Hypershade is opened, an initial scene preparation occurs:
		- beginSceneUpdate()
		- camera definition update:
			- translateCamera()
			- translateTransform() (for the camera transform)
		- primitive definition update:
			- translateMesh() (the currently selected primitive)
			- translateTransform() (for the mesh)
			- setProperty swatchObjectId int (the ID of the selected primitive)
		- environment, when one is selected:
			- translateEnvironment()
			- setProperty "string" "imagefile" on the environment object
			- setProperty "bool" "visible" on the environment object
		- light sources: a set of 3 area lights: MayaMtlView_KeyLightShape1,
		  MayaMtlView_FillLightShape1 and MayaMtlView_RimLightShape1. For each
		  light:
			-translateLightSource()
			-translateTransform() (for the light)
		- endSceneUpdate()
		- startAsync()
		- possibly another scene update block for the mesh, swatchObjectID
		  property, and camera;
	- Note that at this point there still is nothing to render since no shader
		is selected.
	- When selecting a primitive in the option menu, a scene update block occurs
		- beginSceneUpdate()
		- translateMesh(), translateTransform(), setProperty swatchObjectID
		- translateCamera(), translateTransfrom()
		- endSceneUpdate()
	- When selecting an environment menu item:
		- beginSceneUpdate()
		- translateEnvironment(), setProperty imageFile
		- endSceneUpdate()
	- When finally a shader is selected:
		- beginSceneUpdate()
		- translateShader()
		- setShader()
		- endSceneUpdate()
		- beginSceneUpdate()
		- setResolution()
		- endSceneUpdate()
*/

namespace
{

/** \see translateEnvironment, \see setProperty */
const char *k_environment = "IBLenvironment";
const char *k_attributes = "IBLattributes";

const int k_xres = 640;
const int k_yres = 480;

const bool k_live_render = true;

}


MaterialViewer::MaterialViewer() :
	m_nsi( NSI_BAD_CONTEXT ),
	m_numReceivedPixels(0),
	m_numExpectedPixels(0),
	m_started(false),
	m_camera_alread_output(false)
{
}

MaterialViewer::~MaterialViewer()
{
	assert( m_nsi == NSI_BAD_CONTEXT );
	if( m_nsi != NSI_BAD_CONTEXT )
	{
		destroyScene();
	}
}

void* MaterialViewer::creator()
{
	return new MaterialViewer();
}

/**
*/
MStatus MaterialViewer::startAsync( const JobParams& i_params )
{
	assert( m_nsi != NSI_BAD_CONTEXT );

#ifdef DEBUG_OUTPUT
	cout << "startAsync" << endl;
#endif

	NSI::Context nsi( m_nsi );

	MString scanning =
		MGlobal::optionVarStringValue( "_3delight_scanning" );
	scanning = scanning.length() ? scanning.toLowerCase() : "circle";

	nsi.SetAttribute( NSI_SCENE_GLOBAL,
		(
			NSI::IntegerArg("numberofthreads", i_params.maxThreads),
			NSI::CStringPArg("bucketorder", scanning.asChar())
		) );

	assert(k_live_render);
	nsi.RenderControl(
		(
			NSI::CStringPArg("action", "start"),
			NSI::IntegerArg("interactive", 1)
		) );
	m_started = true;

	return MStatus::kSuccess;
}

/**
	Scene state is not destroyed here so we stop rendering without destroying
	the context
	\sa MaterialViewer::destroyScene
*/
MStatus MaterialViewer::stopAsync()
{

#ifdef DEBUG_OUTPUT
	cout << "stopAsync" << endl;
#endif

	assert( m_nsi != NSI_BAD_CONTEXT );

	NSI::Context nsi( m_nsi );
	nsi.RenderControl( NSI::CStringPArg( "action", "stop") );
	nsi.RenderControl( NSI::CStringPArg( "action", "wait") );
	m_started = false;

	return MStatus::kSuccess;
}

/**
	Not sure why Maya needs this: it knows itself if a render is running
	or not. Probably it wants to know if the render is really finished and
	just waiting for new scene updates. So we probably should update this
	when NSI provides us with a way to know that.
*/
bool MaterialViewer::isRunningAsync()
{
	return m_nsi != NSI_BAD_CONTEXT;
}

/**
	In NSI, there is not need to do anything in particular to make scene
	changes in live render mode but the first time this function is called
	we need to create the NSI context. We will take the occasion to build
	all the display-chain nodes.
*/
MStatus MaterialViewer::beginSceneUpdate()
{
	if( m_nsi != NSI_BAD_CONTEXT )
	{
		return MStatus::kSuccess;
	}

#if 0
	const char *output = "/tmp/shaderball.nsi";
	NSIParam_t streamParam;
	streamParam.name = "streamfilename";
	streamParam.data = &output;
	streamParam.type = NSITypeString;
	streamParam.count = 1;
	streamParam.flags = 0;
	m_nsi = NSIBegin( 1, &streamParam );
#else
	NSIErrorHandler_t eh = NSIErrorHandlerMaya;

	NSIParam_t param[2];
	param[0].name = "errorhandler";
	param[0].data = &eh;
	param[0].type = NSITypePointer; param[0].count = 1;
	param[0].flags = 0; param[0].arraylength = 0;

	const char *user_data = "MaterialViewer";
	param[1].name = "errorhandlerdata";
	param[1].data = &user_data;
	param[1].type = NSITypePointer; param[1].count = 1;
	param[1].flags = 0; param[1].arraylength = 0;

	m_nsi = NSIBegin( 2, param );
#endif
	NSIExportShader::CreateTextureUVCoordNode( m_nsi );

	return MStatus::kSuccess;
}

MStatus MaterialViewer::translateMesh(
	const MUuid& i_id, const MObject& i_node)
{
	assert( m_nsi != NSI_BAD_CONTEXT );
	assert( i_node.hasFn(MFn::kMesh) );

#ifdef DEBUG_OUTPUT
	cout << "translateMesh: " << i_id.asString() << endl;
#endif

	MFnDagNode dag_node(i_node);

	NSIExportDelegate::Context export_context(m_nsi, k_live_render);
	NSIExportMesh mesh( dag_node, export_context );
	mesh.Create();
	mesh.SetAttributes();
	mesh.SetAttributesAtTime( 0, true );
	mesh.Connect( 0x0 );
	mesh.Finalize();

	MString trsHandle = mesh.Handle() + MString( " transform" );

	NSI::Context nsi(m_nsi);
	nsi.Create( trsHandle.asChar(), "transform" );
	nsi.Connect( mesh.Handle(), "", trsHandle.asChar(), "objects" );

	return MStatus::kSuccess;
}

/**
*/
MStatus MaterialViewer::translateLightSource(
	const MUuid& i_id, const MObject& i_node )
{
	assert( m_nsi != NSI_BAD_CONTEXT );

	MStatus status;
	MFnDagNode dag_node( i_node, &status );

	if( status != MStatus::kSuccess )
		return MStatus::kFailure;

	NSIExportDelegate::Context export_context(m_nsi, k_live_render);
	NSIExportMaterialViewerLight light( dag_node, export_context );
	light.Create();
	light.SetAttributes();
	light.SetAttributesAtTime( 0, true );
	light.Connect( 0x0 );
	light.Finalize();

#ifdef DEBUG_OUTPUT
	printf( "translateLightSource %s %s %s\n", i_id.asString().asChar(), dag_node.fullPathName().asChar(), light.Handle() );
#endif


	MString trsHandle = light.Handle() + MString( " transform" );

	NSI::Context nsi( m_nsi );

	nsi.Create( trsHandle.asChar(), "transform" );
	nsi.Connect( light.Handle(), "", trsHandle.asChar(), "objects" );

	return MStatus::kSuccess;
}

/**
*/
MStatus MaterialViewer::translateCamera(
	const MUuid& i_id, const MObject& i_node )
{
	assert( m_nsi != NSI_BAD_CONTEXT );
	NSI::Context nsi( m_nsi );

	if( m_camera_alread_output )
		return MStatus::kSuccess;

	m_camera_alread_output = true;

	MiniRenderPass minipass(k_xres, k_yres);

	MFnDagNode dag_node( i_node );
	NSIExportDelegate::Context export_context(m_nsi, k_live_render);
	NSIExportCamera camera( minipass, dag_node, export_context );

#ifdef DEBUG_OUTPUT
	printf( "translateCamaera %s %s\n", i_id.asString().asChar(), camera.Handle() );
#endif

	camera.Create();
	camera.SetAttributes();
	camera.SetAttributesAtTime( 0, true );
	camera.Connect( 0x0 );
	camera.Finalize();

	MString screenid = i_id.asString() + "|screen";

	MFnCamera cam( i_node );
	NSIExportUtilities::ExportScreen(
		m_nsi, minipass, cam, screenid.asChar(),
		false /* apply display */, k_live_render );

	OutputDisplayChain( screenid.asChar() );

	nsi.Connect(
		screenid.asChar(), "",
		camera.Handle(), "screens" );

	/* This should be connected to the .root later by translateTransform */
	MString trsHandle = camera.Handle() + MString( " transform" );

	nsi.Create( trsHandle.asChar(), "transform" );
	nsi.Connect( camera.Handle(), "", trsHandle.asChar(), "objects" );

	return MStatus::kSuccess;
}

MStatus MaterialViewer::translateEnvironment(
	const MUuid& i_id, EnvironmentType i_type )
{
	if( i_type != IBL_ENVIRONMENT )
	{
		return MStatus::kNotImplemented;
	}

#ifdef DEBUG_OUTPUT
	cout << "translateEnvironment: " << i_id.asString() << endl;
#endif

	NSI::Context nsi( m_nsi );

	nsi.Create( k_environment, "environment" );
	nsi.Create( i_id.asString().asChar(), "shader" );
	nsi.Create( k_attributes, "attributes" );

	nsi.Connect( i_id.asString().asChar(), "", k_attributes, "surfaceshader" );
	nsi.Connect( k_attributes, "", k_environment, "geometryattributes" );

	MString shader = OSLUtils::FindBuiltinShader( "environmentLight" );
	if( shader == "" )
	{
		assert( false );
		return MStatus::kNotImplemented;
	}

	nsi.SetAttribute( i_id.asString().asChar(),
		NSI::StringArg( "shaderfilename", shader.asChar()) );

	return MStatus::kSuccess;
}

/**
	We connect the transform to the root and connect the child to
	the tranform:

		.root <- i_id <- i_child_id

	The i_child_id is probably a geometry.
*/
MStatus MaterialViewer::translateTransform(
	const MUuid& i_id,
	const MUuid& i_child_id,
	const MMatrix& i_matrix )
{
	assert( m_nsi != NSI_BAD_CONTEXT );

	NSI::Context nsi( m_nsi );

	const char* handle = ::strdup( i_id.asString().asChar() );

#ifdef DEBUG_OUTPUT
	fprintf( stderr, "translateTransform: %s %s\n", i_id.asString().asChar(),
		i_child_id.asString().asChar() );
#endif

	MString childHandle = i_child_id.asString() + MString(" transform" );

	nsi.Create( handle, "transform" );
	nsi.Connect( handle, "", NSI_SCENE_ROOT, "objects" );
	nsi.Connect( childHandle.asChar(), "", handle, "objects" );

	nsi.SetAttribute( handle,
	NSI::DoubleMatrixArg( "transformationmatrix", &i_matrix.matrix[0][0]) );

	free( (void *)handle );
	return MStatus::kSuccess;
}

/**
	Create an attributes node named after the i_id and connect the shading
	network to it. Note that we first delete the entire subtree to try
	to keep the NSI space clean.

	\sa MaterialViewer::setShader
*/
MStatus MaterialViewer::translateShader(
	const MUuid& i_id, const MObject& i_node )
{
#ifdef DEBUG_OUTPUT
	cout << "translateShader: " << i_id.asString().asChar() << endl;
#endif

	assert( m_nsi != NSI_BAD_CONTEXT );

	NSI::Context nsi( m_nsi );

	const char *k_shader_head = "shading_network_head";

	nsi.Create( i_id.asString().asChar(), "attributes" );
	nsi.Disconnect(NSI_ALL_NODES,"",i_id.asString().asChar(), "surfaceshader" );

	NSIExportDelegate::Context export_context(m_nsi, k_live_render);

	MItDependencyGraph itDep(
		*const_cast<MObject*>(&i_node),
		MFn::kInvalid,  MItDependencyGraph::kUpstream);
	std::vector<NSIExportShader*> shaders;
	for ( ; ! itDep.isDone(); itDep.next() )
	{
		if(! NSIExportUtilities::IsShadingNode( itDep.currentItem() ) )
			continue;

		NSIExportShader* delegate =
			new NSIExportShader( itDep.currentItem(), export_context );

		delegate->Delete();
		delegate->Create();
		shaders.push_back(delegate);
	}

	for( NSIExportShader *shader : shaders )
	{
		shader->SetAttributes();

		if (!shader->Exist())
			continue;

		shader->SetAttributesAtTime(0, true);
		shader->Connect( 0x0 );
		shader->Finalize();
	}

	if( !shaders.empty() && shaders.front()->Exist() )
	{
		MString shaderfilename =
			OSLUtils::FindBuiltinShader( "shadingEngine_surface" );

		if( shaderfilename == "" )
		{
			assert( false );
		}

		nsi.Create( k_shader_head, "shader" );
		nsi.SetAttribute( k_shader_head,
			NSI::StringArg("shaderfilename", shaderfilename.asChar()) );

		nsi.Connect(
			shaders.front()->Handle(), "outColor", k_shader_head, "i_surface" );

		nsi.Connect( k_shader_head, "", i_id.asString().asChar(), "surfaceshader" );
	}

	// free memory
	while(!shaders.empty())
	{
		delete shaders.back();
		shaders.pop_back();
	}

	return MStatus::kSuccess;
}

MStatus MaterialViewer::setShader(
	const MUuid& i_id, const MUuid& i_shaderId )
{
#ifdef DEBUG_OUTPUT
	cout << "setShader: " <<
		i_id.asString() << ", " << i_shaderId.asString() << endl;
#endif

	NSI::Context nsi( m_nsi );

	nsi.Connect( i_shaderId.asString().asChar(), "",
		i_id.asString().asChar(), "geometryattributes" );

	return MStatus::kSuccess;
}

MStatus MaterialViewer::setProperty(
	const MUuid& i_id, const MString& i_name, bool i_value)
{
	assert( m_nsi != NSI_BAD_CONTEXT );
	return setProperty( i_id, i_name, int(i_value) );
}

MStatus MaterialViewer::setProperty(
	const MUuid& i_id, const MString& i_name, int i_value)
{
	assert( m_nsi != NSI_BAD_CONTEXT );
	return MStatus::kSuccess;
}

MStatus MaterialViewer::setProperty(
	const MUuid& i_id, const MString& i_name, float i_value)
{
	assert( m_nsi != NSI_BAD_CONTEXT );
	return MStatus::kSuccess;
}

/**
	We only recognized "imageFile" set on a environment shader. When empty,
	disconnect the environment from .root so not to spend time render a black
	environment map.
*/
MStatus MaterialViewer::setProperty(
	const MUuid& i_id, const MString& i_name, const MString& i_value )
{
	assert( m_nsi != NSI_BAD_CONTEXT );

#ifdef DEBUG_OUTPUT
	cout << "setProperty(string) : " << i_id.asString() << ", " <<
		i_name << ", " << i_value << endl;
#endif

	if( i_name == "imageFile" )
	{
		NSI::Context nsi( m_nsi );

		if( i_value == "" )
		{
			nsi.Disconnect( k_environment, "", NSI_SCENE_ROOT, "objects" );
		}
		else
		{

			nsi.SetAttribute(
				i_id.asString().asChar(),
				NSI::StringArg( "image.meta.colorspace", "linear" ) );

			nsi.SetAttribute(
				i_id.asString().asChar(),
				NSI::StringArg( "image", i_value.asChar() ) );

			nsi.Connect( k_environment, "", NSI_SCENE_ROOT, "objects" );
		}
		return MStatus::kSuccess;
	}

	return MStatus::kNotImplemented;
}

/**
	FIXME: we need  NSI support for stop & go.
*/
MStatus MaterialViewer::setResolution(unsigned i_width, unsigned i_height)
{
	return MStatus::kSuccess;
}

/**
	Nothing to do really.
*/
MStatus MaterialViewer::endSceneUpdate()
{
	if( m_nsi != NSI_BAD_CONTEXT && m_started )
	{
		NSI::Context nsi( m_nsi );
		nsi.RenderControl( NSI::StringArg("action","synchronize") );
	}

	return MStatus::kSuccess;
}

/**
	Note that we don't reset m_width and m_height here since stupid Maya
	doesn't resend this on new scene.
*/
MStatus MaterialViewer::destroyScene()
{
	if( m_nsi == NSI_BAD_CONTEXT )
		return MStatus::kSuccess;

	NSIEnd( m_nsi );
	m_nsi = NSI_BAD_CONTEXT;
	m_started = false;
	m_camera_alread_output = false;

	// This is expected by the MPxRenderer API at this point
	ProgressParams param;
	param.progress = -1.0;
	progress( param );

	m_numReceivedPixels = m_numExpectedPixels = 0;

	return MStatus::kSuccess;
}

bool MaterialViewer::isSafeToUnload()
{
	return m_nsi == NSI_BAD_CONTEXT;
}

void MaterialViewer::OutputDisplayChain( const char *i_screen ) const
{
	NSI::Context nsi( m_nsi );

	const char *k_output_layer = "Bob";
	const char *k_output_driver = "Dale";

	char image_pointer[256];
	sprintf(image_pointer, "%llu", (unsigned long long)(size_t)this);

	nsi.Create( k_output_driver, "outputdriver" );

	nsi.SetAttribute( k_output_driver,
		(
			NSI::StringArg("drivername",  "maya_material_viewer"),
			NSI::StringArg("imagefilename", image_pointer),
			NSI::StringArg("colorprofile", "linear"),
			NSI::FloatArg("aspectratio", 1.0f)
		) );

#ifdef OUTPUT_TO_IDISPLAY
	const char *k_output_driver2 = "AI";
	nsi.Create( k_output_driver2, "outputdriver" );
	nsi.SetAttribute( k_output_driver2,
		(
			NSI::StringArg("drivername",  "idisplay"),
			NSI::StringArg("imagefilename", "shader ball"),
			NSI::StringArg("colorprofile", "linear"),
			NSI::FloatArg("aspectratio", 1.0f)
		) );
#endif

	nsi.Create( k_output_layer, "outputlayer" );
	nsi.SetAttribute( k_output_layer,
		(
			NSI::StringArg( "variablename", "Ci"),
			NSI::StringArg( "channelname", "beauty"),
			NSI::StringArg( "scalarformat", "float"),
			NSI::IntegerArg( "withalpha", 1 )
		) );

	nsi.Connect(
		k_output_driver, "",
		k_output_layer, "outputdrivers" );

#ifdef OUTPUT_TO_IDISPLAY
	nsi.Connect(
		k_output_driver2, "",
		k_output_layer, "outputdrivers" );
#endif

	nsi.Connect(
		k_output_layer, "",
		i_screen, "outputlayers" );
}

void MaterialViewer::receiveBucket(
	int i_xmin, int i_xmax_plus_one,
	int i_ymin, int i_ymax_plus_one,
	int i_entrySize,
	void* i_data)
{
	m_numReceivedPixels += (i_xmax_plus_one-i_xmin) * (i_ymax_plus_one-i_ymin);

	RefreshParams params;
	params.width = k_xres;
	params.height = k_yres;

	params.left = i_xmin;
	params.right = i_xmax_plus_one - 1;
	params.bottom = params.height - i_ymax_plus_one;
	params.top = params.height - i_ymin - 1;

	params.channels = 4;
	params.bytesPerChannel = sizeof(float);
	params.data = i_data;

	refresh( params );

	if( m_numExpectedPixels > 0 )
	{
		ProgressParams param;
		param.progress = m_numReceivedPixels;
		param.progress /= m_numExpectedPixels;
		progress(param);
	}
}

void MaterialViewer::setNumPasses(int i_numPasses)
{
	m_numExpectedPixels = k_xres * k_yres * 4 * i_numPasses;
}

#endif
