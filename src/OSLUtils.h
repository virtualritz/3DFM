#ifndef __OSLUTILS_H__
#define __OSLUTILS_H__

#include <maya/MStringArray.h>
#include <3Delight/ShaderQuery.h>

namespace OSLUtils
{

	// Returns the array of shader paths for shaders that are provided with
	// 3Delight.
	//
	MStringArray GetBuiltInSearchPaths();

	// Returns the array of shader paths for user-defined shaders.
	MStringArray GetUserSearchPaths();

	/** returns full path to a given shader or "" if not found */
	MString FindBuiltinShader( const MString& i_shaderName );

	// Search for a shader in a list of search paths. If the shader is found,
	// its metadata is returned in o_info and its path is returned. An empty
	// string is returned if the shader is not found.
	//
	// The returned path is a "fixed" (as in DL_utils.h's getFixedPath(), i.e.
	// with forward slashes on Windows) version of the related search path array
	// element. The path designates the directory that contains the shader file.
	//
	MString OpenShader(
			const MString& i_shaderName,
			const MStringArray& i_paths,
			DlShaderInfo *&o_info);

	// Same as above, uses built-in paths + user paths as search paths, in that
	// order.
	//
	MString OpenShader(
		const MString& i_shaderName, DlShaderInfo *&o_info );

	// Opens the specified shader and returns the full path name, with forward
	// slashes on all platforms, of the specified shader.
	//
	MString GetFullpathname(const MString& i_shaderName);

	/*
		Returns the string defined for "string niceName" metadata.
		If you need other shader level meta-data, it is probably more efficient
		to iterate yourself through them.
	*/
	MString GetShaderNiceName( DlShaderInfo *i_shaderInfo );
}

#endif
