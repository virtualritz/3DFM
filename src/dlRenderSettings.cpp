/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#include "MU_typeIds.h"

#include "dlRenderSettings.h"

MTypeId dlRenderSettings::id(MU_typeIds::DL_RENDERSETTINGS);

void
dlRenderSettings::postConstructor()
{
  setExistWithoutInConnections(true);
  setExistWithoutOutConnections(true);
}

void*
dlRenderSettings::creator()
{
  return new dlRenderSettings();
}

MStatus
dlRenderSettings::initialize()
{
  return MS::kSuccess;
}

MStatus
dlRenderSettings::compute( const MPlug& /*out_plug*/, MDataBlock& /*data*/)
{
  return MS::kUnknownParameter;
}
