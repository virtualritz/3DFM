#include "dlTriplanar.h"

#include <maya/MFloatVector.h>
#include <maya/MObjectArray.h>

#include "DL_autoLoadOSL.h"
#include "DL_utils.h"
#include "OSLUtils.h"

#include <cassert>

MObject dlTriplanar::s_colorTexture;
MObject dlTriplanar::s_floatTexture;
MObject dlTriplanar::s_heightTexture;
MObject dlTriplanar::s_outColor;
MObject dlTriplanar::s_outFloat;
MObject dlTriplanar::s_outHeight;

void* dlTriplanar::creator()
{
	return new dlTriplanar();
}

MStatus dlTriplanar::initialize()
{
	MStringArray shaderPaths = OSLUtils::GetBuiltInSearchPaths();
	MString shaderName( "dlTriplanar" );
	DlShaderInfo *info;

	OSLUtils::OpenShader( shaderName, shaderPaths, info );

	MObjectArray objects;
	MStringArray objectNames;

	DL_OSLShadingNode::CreateAttributesFromShaderParameters(
		info, 0x0, &objects, &objectNames );

	if(
		!DL_OSLShadingNode::FindAttribute(
			"colorTexture", s_colorTexture, objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"floatTexture", s_floatTexture, objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"heightTexture", s_heightTexture, objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"outColor", s_outColor, objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"outFloat", s_outFloat, objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"outHeight", s_outHeight, objects, objectNames) )
	{
		assert(false);
		// It makes no sense to return success here, but if we don't, Maya crashes.
		return MStatus::kSuccess;
	}

	for( unsigned i = 0; i < objects.length(); i++ )
	{
		addAttribute( objects[ i ] );
	}

	attributeAffects( s_colorTexture, s_outColor );
	attributeAffects( s_floatTexture, s_outFloat );
	attributeAffects( s_heightTexture, s_outHeight );

	MString name = info->shadername().c_str();
	MString niceName = OSLUtils::GetShaderNiceName( info );
	DL_OSLShadingNode::DefineShaderNiceName( name, niceName );

	return MStatus::kSuccess;
}

void dlTriplanar::postConstructor()
{
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlTriplanar::compute(
	const MPlug& i_plug,
	MDataBlock& i_block )
{
	return MS::kSuccess;
}
