/*
  Copyright (c) 2017 The 3Delight Team.
*/

#include "dlDecayFilterShape.h"
#include "DL_errors.h"
#include "DL_utils.h"

#include "MU_typeIds.h"

#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MRampAttribute.h>

MTypeId dlDecayFilterShape::id( MU_typeIds::DL_DECAYFILTER );

MObject dlDecayFilterShape::s_decayType;
MObject dlDecayFilterShape::s_decayRange;
MObject dlDecayFilterShape::s_decayRangeStart;
MObject dlDecayFilterShape::s_decayRangeEnd;
MObject dlDecayFilterShape::s_decayCurve;
MObject dlDecayFilterShape::s_filter_output;

dlDecayFilterShape::dlDecayFilterShape()
{
}

dlDecayFilterShape::~dlDecayFilterShape()
{
}

void dlDecayFilterShape::postConstructor()
{
  setExistWithoutInConnections(true);
  setExistWithoutOutConnections(true);
}

MStatus dlDecayFilterShape::compute(
  const MPlug& /*plug*/,
  MDataBlock& /*datablock*/ )
{
  return MS::kUnknownParameter;
}

MStatus dlDecayFilterShape::shouldSave(const MPlug& i_plug, bool& o_isSaving)
{
  if(i_plug == s_decayCurve)
  {
    o_isSaving = true;
    return MStatus::kSuccess;
  }

  return MPxSurfaceShape::shouldSave(i_plug, o_isSaving);
}

void* dlDecayFilterShape::creator()
{
  return new dlDecayFilterShape();
}

MStatus dlDecayFilterShape::initialize()
{
  MFnCompoundAttribute compAttr;
  MFnEnumAttribute enumAttr;
  MFnNumericAttribute numAttr;

  s_decayType = enumAttr.create("decayType", "dt");
  enumAttr.addField("Distance from light", 0);
  enumAttr.addField("Distance form light plane", 1);
  enumAttr.addField("Angle from axis", 2);
  enumAttr.addField("Distance from axis", 3);
  Utilities::makeInputAttribute(enumAttr, true);
  enumAttr.setNiceNameOverride(MString("Type"));
  addAttribute(s_decayType);

  s_decayRangeStart =
    numAttr.create( "decayRangeStart", "drs", MFnNumericData::kDouble, 0.0f );
  Utilities::makeInputAttribute(numAttr, true);
  numAttr.setNiceNameOverride(MString("Range Start"));
  addAttribute(s_decayRangeStart);

  s_decayRangeEnd =
    numAttr.create( "decayRangeEnd", "dre", MFnNumericData::kDouble, 10.0f );
  Utilities::makeInputAttribute(numAttr, true);
  numAttr.setNiceNameOverride(MString("Range End"));
  addAttribute(s_decayRangeEnd);

  s_decayRange = 
    numAttr.create("decayRange", "dr", s_decayRangeStart, s_decayRangeEnd);
  numAttr.setNiceNameOverride(MString("Range"));
  Utilities::makeInputAttribute(numAttr, true);
  addAttribute(s_decayRange);

  s_decayCurve = MRampAttribute::createCurveRamp("decayCurve", "dc");
  addAttribute(s_decayCurve);

  s_filter_output = numAttr.createColor( "filter_output", "fo" );
  Utilities::makeOutputAttribute(numAttr);
  addAttribute(s_filter_output);

  attributeAffects( s_decayType, s_filter_output );
  attributeAffects( s_decayRangeStart, s_filter_output );
  attributeAffects( s_decayRangeEnd, s_filter_output );
  attributeAffects( s_decayCurve, s_filter_output );

  return MS::kSuccess;
}
