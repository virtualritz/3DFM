#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MAnimControl.h>
#include <maya/MTime.h>

#include <math.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "DL_renderState.h"
#include "DL_utils.h"
#include "DL_expandVariables.h"

MSyntax
DL_expandVariables::newSyntax()
{
  MSyntax syntax;

  syntax.addFlag("-t", "-text", MSyntax::kString);

  return syntax;
}

MStatus
DL_expandVariables::doIt(const MArgList& args)
{
  MStatus         status;
  MArgDatabase    argData(syntax(), args, &status);

  if (status == MS::kSuccess)
  {
    if (argData.isFlagSet("-t"))
    {
      DL_renderState& render_state = DL_renderState::getRenderState();
      MString         filename;
      MString         result;

      char              expanded_text[2048];

      argData.getFlagArgument("-t", 0, filename);

      float current_frame = 0;
      if (render_state.getRenderNode() == MString(""))
      {
        current_frame = MAnimControl::currentTime().value();
      }
      else
      {
        current_frame = render_state.getCurrentFrame();
      }
      
      expandEnvVars(expanded_text,
                    2048,
                    const_cast<char*>(filename.asChar()),
                    current_frame);

      result = expanded_text;

      setResult(result);

      status = MS::kSuccess;
    }
    else
    {
      if (!argData.isFlagSet("-t"))
      {
        MGlobal::displayError("need text specified with -t");
        status = MS::kFailure;
      }
    }
  }

  return status;
}
