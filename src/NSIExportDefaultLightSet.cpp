#include "DelegateTable.h"
#include "NSIExportDefaultLightSet.h"
#include "NSIExportPriorities.h"

#include <maya/MNodeMessage.h>
#include <maya/MFnSet.h>
#include <maya/MPlug.h>
#include <maya/MSelectionList.h>
#include <maya/MItSelectionList.h>
#include <maya/MDagPath.h>
#include <nsi.hpp>

#include <assert.h>

NSIExportDefaultLightSet::NSIExportDefaultLightSet(
	MFnDependencyNode &i_dag,
	MObject &i_object,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate(i_dag, i_object, i_context)
{
}

/**
	\brief Iterate through all the sets and connect the members.
*/
void NSIExportDefaultLightSet::Connect( const DelegateTable *i_table )
{
	assert( Object().hasFn(MFn::kSet) );

	MFnSet set( Object() );

	MSelectionList list;
	MStatus status = set.getMembers( list, false /* flatten */ );

	if( status != MStatus::kSuccess )
		return;

	NSI::Context nsi( m_nsi );
	MItSelectionList iter( list );
	for ( ; !iter.isDone(); iter.next() )
	{
		MDagPath item;
		MObject component;
		iter.getDagPath( item, component );

		MFnDagNode dag_node( item, &status );
		if( status != MStatus::kSuccess )
			continue;

		MString destination_handle = NSIHandle( dag_node, m_live );

		if( i_table && !i_table->contains(destination_handle) )
		{
			continue;
		}

		nsi.Connect( Handle(), "",
			destination_handle.asChar(), "geometryattributes" );
	}
}

/**
	FIXME: this should really be a "set" but NSI doesn't allow it for now.
*/
void NSIExportDefaultLightSet::Create()
{
	NSICreate( m_nsi, m_nsi_handle, "attributes", 0, 0x0 );
}

void NSIExportDefaultLightSet::SetAttributes( void )
{
	NSI::Context nsi( m_nsi );
	nsi.SetAttribute( Handle(),
		(
			NSI::IntegerArg("visibility", 1),
			NSI::IntegerArg("visibility.priority", NSI_LIGHTSET_PRIORITY)
		) );
}

void NSIExportDefaultLightSet::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
}

/**
	\brief List to connection made / broken messages.
*/
void NSIExportDefaultLightSet::attributeChangedCB(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &i_plug, MPlug &i_otherPlug,
	void *i_data )
{
	if( !(i_msg &
		(MNodeMessage::kConnectionMade | MNodeMessage::kConnectionBroken)) )
	{
		return;
	}
	NSIExportDefaultLightSet *delegate = (NSIExportDefaultLightSet *)i_data;

	MString set_element = NSIHandle( i_otherPlug.node(), true );
	NSI::Context nsi( delegate->NSIContext() );

	if( i_msg & MNodeMessage::kConnectionBroken )
	{
		nsi.Disconnect(
			delegate->Handle(),  "",
			NSIHandle(i_otherPlug.node(), true).asChar(),
			"geometryattributes" );
	}
	else
	{
		assert( i_msg & MNodeMessage::kConnectionMade );
		nsi.Connect(
			delegate->Handle(), "",
			NSIHandle(i_otherPlug.node(), true).asChar(),
			"geometryattributes" );
	}

	nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
}

/**
	\brief Register a callback to monitor changes in set membership
*/
bool NSIExportDefaultLightSet::RegisterCallbacks( void )
{
	MObject &object = (MObject &)Object();
	MCallbackId id = MNodeMessage::addAttributeChangedCallback(
		object, attributeChangedCB, (void *)this );
	AddCallbackId( id );

	return true;
}


