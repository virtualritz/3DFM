#ifndef __dlSubstance_H__
#define __dlSubstance_H__

#include <maya/MPxNode.h>

class dlSubstance : public MPxNode
{
public:
	static void* creator();
	static MStatus initialize();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug&, MDataBlock& );

private:
	static MObject s_color;
	static MObject s_outColor;
};

#endif
