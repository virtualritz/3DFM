#ifndef __dlThin_H__
#define __dlThin_H__

#include <maya/MPxNode.h>

class dlThin : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static MObject s_color;
	static MObject s_outColor;
};

#endif
