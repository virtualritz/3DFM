#!/usr/bin/perl

# This is a script to generate Makefile dependencies on systems where the C
# compiler is unable to do the job. It does not understand defines so it simply
# checks out all the include directives. This should, at worst, be a little
# excessive in a few cases.
#
# invocation is:
# mkdep.pl [--ignore regexp] objectfile.o srcfile [COMPILER_OPTIONS]
#
# The .o suffix for the object file is mandatory; it is replaced by .d to form
# the dependency file name. srcfile is the source file to scan for includes.
# COMPILER_OPTIONS should be all the options normally passed to the compiler.
# This script only interprets -I and /I options.
#
# The --ignore option can be used to ignore some files when scanning for
# dependencies. For example, one might specify --ignore "/usr/include/". The
# regexp may match either the name in the source of the full path.

use strict;

my @ignore_patterns = ();
my $cygpath = 0;

while( 1 )
{
	if( $ARGV[0] eq "--ignore" )
	{
		shift @ARGV;
		push( @ignore_patterns, shift @ARGV );
		next;
	}

	if( $ARGV[0] eq "--cygpath" )
	{
		shift @ARGV;
		$cygpath = 1;
		next;
	}

	last;
}

my $object_file = shift @ARGV;
my $src_file = shift @ARGV;

my @include_paths = ( "." );

while( $#ARGV >= 0 )
{
	my $arg = shift @ARGV;
	if( $arg =~ /[\/-]I(.*)/ )
	{
		if( not &IsIgnored( $1 ) )
		{
			push( @include_paths, $1 );
		}
	}
}

#print( "paths: " . join( " ", @include_paths ) . "\n" );

$object_file =~ /(.*)o/;
my $d_file = "$1d";

my %all_dependencies;
my %readdir_cache;

&ScanFile( $src_file );

open( DEP, ">$d_file" )
	or die "can't write to $d_file";

print DEP "$object_file:";
foreach my $key (keys %all_dependencies)
{
	my $unix_path = $key;

	if( $cygpath && $unix_path =~ /(\w):(.*)/ )
	{
		$unix_path = "/cygdrive/$1$2";
	}

	print DEP " \\\n\t$unix_path";
}
print DEP "\n";

close( DEP );

#
# ScanFile( filename )
#
sub ScanFile
{
	my $filename = $_[0];

	if( $all_dependencies{$filename} == 1 )
	{
		return;
	}

	$all_dependencies{$filename} = 1;

	# localize filehandle glob; see http://perl.plover.com/local.html
	local *INCLUDE;

	open( INCLUDE, "<$filename" )
		or die "can't read $filename";

	while( <INCLUDE> )
	{
		# Check both "" and <>. This is a bit more relaxed than the standard
		# but this script is not intended to cope with invalid source files.
		if( /^\s*#\s*include\s*[<"](.*)[>"]/ )
		{
			my $included = $1;

			if( &IsIgnored( $included ) )
			{
				next;
			}

			# TODO: For improved performance, we could skip "known" standard
			# includes such as stdio.h, stdlib.h, etc.
			foreach my $path (@include_paths)
			{
				my $final_path = "$path/$included";

				# ./file.h should work but it's prettier not to have the ./
				if( $path eq "." )
				{
					$final_path = $included;
				}

				# Early check for a file we've already seen. This does not
				# change the output but it reduces filesystem access and makes
				# things faster, especially on windows.
				if( $all_dependencies{$final_path} == 1 )
				{
					last;
				}

				if( &FileExists( "$path/$included" ) )
				{
					if( &IsIgnored( "$path/$included" ) )
					{
						last;
					}

					&ScanFile( $final_path );

					last;
				}
			}
		}
	}

	close( INCLUDE )
}

#
# IsIgnored( filename )
#
sub IsIgnored
{
	my $filename = $_[0];

	foreach my $patt (@ignore_patterns)
	{
		if( $filename =~ m,$patt, )
		{
			return 1;
		}
	}

	return 0;
}

#
# FileExists( filepath )
#
# if( &FileExists( $file ) )    is equivalent to   if( -e $file )
#
# The purpose of this function is that it caches the contents of directories to
# reduce the effect of a high latency network on file tests. This makes the
# whole script up to 4-5 times faster on windows.
#
sub FileExists
{
	my( $filepath ) = @_;

	my $path;
	my $file;

	if( $filepath =~ /(.*)\/([^\/]+)/ )
	{
		$path = $1;
		$file = $2;
	}
	else
	{
		$path = ".";
		$file = $filepath;
	}

	if( not defined $readdir_cache{$path} )
	{
		my @contents;
		if( opendir( my $dir, $path ) )
		{
			@contents = readdir( $dir );
			closedir( $dir );
		}
		$readdir_cache{$path} = \@contents;
	}

	my $dir_contents = $readdir_cache{$path};

	for my $f (@{$dir_contents})
	{
		if( $f eq $file )
		{
			return 1;
		}
	}

	return 0;
}

