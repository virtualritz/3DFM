#include <maya/MGlobal.h>
#include <maya/MArgDatabase.h>
#include <maya/MColor.h>
#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MObjectArray.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MSelectionList.h>
#include <maya/MSyntax.h>
#include <maya/MVector.h>

#include <maya/shaveAPI.h>

#include "DLSH_cacheShave.h"
#include "DL_motionCache.h"
#include "DL_RiBasisNode.h"
#include "DL_RiCurvesNode.h"
#include "DL_RiPrimitive.h"
#include "DL_utils.h"

#include <assert.h>

typedef DL_motionCache<DL_RiPrimitive*> t_shaveCache;

static t_shaveCache&
DL_getShaveCache()
{
  static t_shaveCache shave_cache;
  return shave_cache;
}

static void
getHairVertex(MPoint& vertex, shaveAPI::HairInfo& hair_info, int vertex_index)
{
  vertex.x = hair_info.vertices[hair_info.hairVertices[vertex_index]].x;
  vertex.y = hair_info.vertices[hair_info.hairVertices[vertex_index]].y;
  vertex.z = hair_info.vertices[hair_info.hairVertices[vertex_index]].z;
}

static void
getSkinNormal(MVector& normal, shaveAPI::HairInfo& hair_info, int hair_num)
{
  normal.x = hair_info.surfaceNormals[hair_num].x;
  normal.y = hair_info.surfaceNormals[hair_num].y;
  normal.z = hair_info.surfaceNormals[hair_num].z;
}

static void
getHairColors(MColor& root_color,
              MColor& tip_color,
              shaveAPI::HairInfo& hair_info,
              int hair_num)
{
  root_color.r = hair_info.rootColors[hair_num].r;
  root_color.g = hair_info.rootColors[hair_num].g;
  root_color.b = hair_info.rootColors[hair_num].b;
  
  tip_color.r = hair_info.tipColors[hair_num].r;
  tip_color.g = hair_info.tipColors[hair_num].g;
  tip_color.b = hair_info.tipColors[hair_num].b;
}

static void
getColorValue(
    MFnDependencyNode& i_dep_fn,
    MString i_parameter,
    MColor& o_color)
{
  MString r_param = i_parameter + "R";
  MString g_param = i_parameter + "G";
  MString b_param = i_parameter + "B";

  MPlug r_plug = i_dep_fn.findPlug( r_param );
  MPlug g_plug = i_dep_fn.findPlug( g_param );
  MPlug b_plug = i_dep_fn.findPlug( b_param );

  r_plug.getValue( o_color.r );
  g_plug.getValue( o_color.g );
  b_plug.getValue( o_color.b );
}

struct HairInfoPatch : public shaveAPI::HairInfo
{
	/* This is to prevent the stupid plugin from screwing up our stack. */
  // shaveAPI.h was updated for v8 with the missing float*; this is probably
  // not needed anymore.
  //
	unsigned padding1, padding2;
};

static MStatus
shaveToRiCurves(DL_RiPrimitive& new_prim, MDagPath& hair_path)
{
  MFnDependencyNode   hair_node_fn(hair_path.node());
  MObjectArray        shave_nodes;
  MStatus             status;
  //shaveAPI::HairInfo  hair_info;
  HairInfoPatch hair_info;
  
  shave_nodes.append(hair_path.node());
  
  status = shaveAPI::exportHair(shave_nodes, &hair_info);
  
  if (status)
  {
    DL_RiBasisNode*   basis_node;
    DL_RiCurvesNode*  curves_node;
    
    basis_node = new DL_RiBasisNode;
    curves_node = new DL_RiCurvesNode;
    
    basis_node->setUbasis(DL_RiBasisNode::DL_CATMULL_ROM_BASIS);
    basis_node->setVbasis(DL_RiBasisNode::DL_CATMULL_ROM_BASIS);
    
    curves_node->setCurveType(DL_RiCurvesNode::DL_CUBIC);
    curves_node->setWrapMode(DL_RiCurvesNode::DL_NONPERIODIC);
    
    int num_hairs = hair_info.numHairs;
    
    curves_node->setNumCurves(num_hairs);
    
    DL_primVarList& hair_prim_vars = *curves_node->getPrimVarList();
    DL_primVar*     p_prim_var = NULL;
    DL_primVar*     width_prim_var = NULL;
    DL_primVar*     opacity_prim_var = NULL;
    DL_primVar*     skin_normal_prim_var = NULL;
    DL_primVar*     root_color_prim_var = NULL;
    DL_primVar*     tip_color_prim_var = NULL;
    DL_primVar*     SHAVEambdiff_prim_var = NULL;
    DL_primVar*     SHAVEopacity_prim_var = NULL;
    DL_primVar*     SHAVEspec_prim_var = NULL;
    DL_primVar*     SHAVEgloss_prim_var = NULL;
    DL_primVar*     SHAVEselfshad_prim_var = NULL;
    DL_primVar*     SHAVEspec_color_prim_var = NULL;
    DL_primVar*     SHAVEspec_color2_prim_var = NULL;
    DL_primVar*     w_prim_var = NULL;
    DL_primVar*     cs_prim_var = NULL;
    DL_primVar*     s_prim_var = NULL;
    DL_primVar*     t_prim_var = NULL;
    DL_primVar*     index_prim_var = NULL;
    
    int            num_vertex_values;
    int            num_varying_values;
    
    bool            success;
    
    // first we need to figure out the total number of varying values required
    num_varying_values = 0;
    
    for (int hair_num = 0; hair_num < num_hairs; ++hair_num)
    {
      int  curr_num_vertices;
      int  curr_start_index;
      int  curr_end_index;
      
      curr_start_index = hair_info.hairStartIndices[hair_num];
      curr_end_index = hair_info.hairEndIndices[hair_num];
      
      curr_num_vertices = curr_end_index - curr_start_index;
      
      num_varying_values += curr_num_vertices;
    }
    
    // then the number of vertex values
    num_vertex_values = num_varying_values + 2 * num_hairs;
    
    // Retrieve what should be exported as per the Shave Globals node
    MSelectionList selectionList;
    status = selectionList.add("shaveGlobals");
    
    if(status != MStatus::kSuccess || selectionList.length() < 1)
    {
      MGlobal::displayError("Can't find shave globals");
      return MStatus::kFailure;
    }
    
    MObject depNodeObj;
    status = selectionList.getDependNode(0, depNodeObj);
    assert(status == MStatus::kSuccess);
    MFnDependencyNode shaveGlobalsDepFn(depNodeObj, &status);
    assert(status == MStatus::kSuccess);
  
    bool exportNormals = false;
    MPlug exportNormalsPlug = shaveGlobalsDepFn.findPlug("ribNormals");
    exportNormalsPlug.getValue(exportNormals);
    
    bool exportOpacities = false;
    MPlug exportOpacitiesPlug = shaveGlobalsDepFn.findPlug("ribOpacities");
    exportOpacitiesPlug.getValue(exportOpacities);

    bool exportRootTipColors = false;
    MPlug exportRootTipColorsPlug = 
      shaveGlobalsDepFn.findPlug("ribRootTipColors");
    exportRootTipColorsPlug.getValue(exportRootTipColors);
    
    bool exportVertexColors = false;
    MPlug exportVertexColorsPlug = 
      shaveGlobalsDepFn.findPlug("ribVertexColors");
    exportVertexColorsPlug.getValue(exportVertexColors);
    
    bool exportWcoords = false;
    MPlug exportWcoordsPlug = shaveGlobalsDepFn.findPlug("ribWCoords");
    exportWcoordsPlug.getValue(exportWcoords);
    
    bool exportUvs = false;
    MPlug exportUvsPlug = shaveGlobalsDepFn.findPlug("ribUVs");
    exportUvsPlug.getValue(exportUvs);

    // Get the per-shaveShape "tip fade" attribute
    bool tipFade = false;
    MPlug tipFadePlug = hair_node_fn.findPlug("tipFade");
    tipFadePlug.getValue(tipFade);
    
    // Do we export the primvars to mimic what is done by shaveWriteRib
    // or the old fashioned, 3dfmShave way?
    char* oldPrimVarNames = getenv("_3DFM_SHAVE_OLD_PRIMVARS");
    
    // now we can declare our primitive variables
    p_prim_var = hair_prim_vars.addPrimVar("P",
                                           "vertex point",
                                           num_vertex_values,
                                           success);
    
    width_prim_var = hair_prim_vars.addPrimVar("width",
                                               "varying float",
                                               num_varying_values,
                                               success);
    
    if(exportNormals)
    {
      const char* primVarName = oldPrimVarNames ? "skin_normal" : "N_Srf";
      
      skin_normal_prim_var = hair_prim_vars.addPrimVar(primVarName,
                                                       "uniform normal",
                                                       num_hairs,
                                                       success);
    }
    
    if(exportOpacities)
    {
      if(oldPrimVarNames)
      {
        opacity_prim_var = hair_prim_vars.addPrimVar(
          "hair_opacity",
          "uniform float",
          num_hairs,
          success);
      }
      else
      {
        opacity_prim_var = hair_prim_vars.addPrimVar(
          "Os", 
          "varying color", 
          num_varying_values, 
          success);
      }
    }
    
    if(exportRootTipColors)
    {
      root_color_prim_var = hair_prim_vars.addPrimVar("rootcolor",
                                                      "uniform color",
                                                      num_hairs,
                                                      success);
      
      tip_color_prim_var = hair_prim_vars.addPrimVar("tipcolor",
                                                     "uniform color",
                                                     num_hairs,
                                                     success);

      SHAVEambdiff_prim_var =
        hair_prim_vars.addPrimVar(
            "SHAVEambdiff",
            "uniform float",
            num_hairs,
            success);

      SHAVEopacity_prim_var =
        hair_prim_vars.addPrimVar(
            "SHAVEopacity",
            "uniform float",
            num_hairs,
            success);

      SHAVEspec_prim_var =
        hair_prim_vars.addPrimVar(
            "SHAVEspec",
            "uniform float",
            num_hairs,
            success);

      SHAVEgloss_prim_var =
        hair_prim_vars.addPrimVar(
            "SHAVEgloss",
            "uniform float",
            num_hairs,
            success);

      /* Get constant values from the shape */
      SHAVEselfshad_prim_var =
        hair_prim_vars.addPrimVar(
            "SHAVEselfshad",
            "constant float",
            num_hairs,
            success);
      float selfShadow = 1.0f;
      MPlug selfShadow_plug = hair_node_fn.findPlug("selfShadow");
      selfShadow_plug.getValue(selfShadow);
      SHAVEselfshad_prim_var->setFloatValue(selfShadow, 0);

      SHAVEspec_color_prim_var =
        hair_prim_vars.addPrimVar(
            "SHAVEspec_color",
            "constant color",
            num_hairs,
            success);
      MColor specularTint( 1.0f );
      getColorValue(hair_node_fn, "specularTint", specularTint);
      SHAVEspec_color_prim_var->setColorValue(specularTint, 0);

      SHAVEspec_color2_prim_var =
        hair_prim_vars.addPrimVar(
            "SHAVEspec_color2",
            "constant color",
            num_hairs,
            success);
      MColor specularTint2( 0.0f );
      getColorValue(hair_node_fn, "specularTint2", specularTint2);
      SHAVEspec_color2_prim_var->setColorValue(specularTint2, 0);
    }
    
    if(exportVertexColors)
    {
      cs_prim_var = hair_prim_vars.addPrimVar(
        "Cs",
        "varying color",
        num_vertex_values,
        success);
    }
    
    if(exportUvs)
    {
      s_prim_var = hair_prim_vars.addPrimVar(
        "s",
        "uniform float",
        num_hairs,
        success);
      
      t_prim_var = hair_prim_vars.addPrimVar(
        "t",
        "uniform float",
        num_hairs,
        success);
    }
    
    if(exportWcoords)
    {
      w_prim_var = hair_prim_vars.addPrimVar(
        "w", 
        "varying float",
        num_varying_values,
        success);
    }
    
    index_prim_var = hair_prim_vars.addPrimVar(
      "index", 
      "uniform float", 
      num_hairs, 
      success);
    
    MPoint  curr_vertex;
    MColor  curr_root_color;
    MColor  curr_tip_color;
    MVector curr_skin_normal;
    float   curr_root_width;
    float   curr_tip_width;
    float   width_delta;
    float   curr_width;
    int    curr_num_vertices;
    int    curr_start_index;
    int    curr_end_index;
    int    vertex_index;
    int    varying_index;
    
    vertex_index = 0;
    varying_index = 0;
    
    for (int hair_num = 0; hair_num < num_hairs; ++hair_num)
    {
      index_prim_var->setFloatValue(hair_num, hair_num);

      curr_start_index = hair_info.hairStartIndices[hair_num];
      curr_end_index = hair_info.hairEndIndices[hair_num];
      
      curr_num_vertices = curr_end_index - curr_start_index;
      
      // +2 because we need to repeat the first and last vertices for the
      // catmull-rom basis
      curves_node->setNumVertices(hair_num, curr_num_vertices + 2);
      
      // now do the uniform (per-hair) variables
      
      // opacity
      if(exportOpacities && oldPrimVarNames)
        opacity_prim_var->setFloatValue(hair_info.opacities[hair_num], hair_num);
      
      // root and tip colors
      getHairColors(curr_root_color, curr_tip_color, hair_info, hair_num);

      if(exportRootTipColors)
      {
        root_color_prim_var->setColorValue(curr_root_color, hair_num);
        tip_color_prim_var->setColorValue(curr_tip_color, hair_num);

        SHAVEambdiff_prim_var->setFloatValue(
            hair_info.ambDiffs[hair_num], hair_num);
        SHAVEopacity_prim_var->setFloatValue(
            hair_info.opacities[hair_num], hair_num);
        SHAVEspec_prim_var->setFloatValue(
            hair_info.speculars[hair_num], hair_num);
        SHAVEgloss_prim_var->setFloatValue(
            hair_info.glosses[hair_num], hair_num);
      }
      
      // skin normal
      if(exportNormals)
      {
        getSkinNormal(curr_skin_normal, hair_info, hair_num);
        skin_normal_prim_var->setNormalValue(curr_skin_normal, hair_num);
      }
      
      // uvs
      if(exportUvs)
      {
        s_prim_var->setFloatValue(
          hair_info.uvws[hair_info.hairVertices[curr_start_index]].x,
          hair_num);

        t_prim_var->setFloatValue(
          hair_info.uvws[hair_info.hairVertices[curr_start_index]].y,
          hair_num);
      }
              
      // now figure out the width values
      curr_root_width = hair_info.rootRadii[hair_num] * 2;
      curr_tip_width = hair_info.tipRadii[hair_num] * 2;
      
      width_delta = (curr_tip_width - curr_root_width)/(curr_num_vertices - 1);
      
      // Cs values are interpolated color values from root and tip colors
      MColor curr_cs_delta(0, 0, 0);
      
      if(exportVertexColors)
      {
        curr_cs_delta.r = (curr_tip_color.r - curr_root_color.r) / 
          (curr_num_vertices - 1);

        curr_cs_delta.g = (curr_tip_color.g - curr_root_color.g) / 
          (curr_num_vertices - 1);

        curr_cs_delta.b = (curr_tip_color.b - curr_root_color.b) / 
          (curr_num_vertices - 1);
      }
      
      // Os values are hair opacities * possibly an interpolation between an 
      // opaque root and a transparent tip.
      //
      float root_os = hair_info.opacities[hair_num];
      float curr_os_delta = tipFade ? -root_os / (curr_num_vertices - 1) : 0;

      // we need to repeat the first and last vertices for the catmull-rom basis
      // for "vertex" primvars
      getHairVertex(curr_vertex, hair_info, curr_start_index);
      p_prim_var->setPointValue(curr_vertex, vertex_index);
      vertex_index++;
      
      curr_width = curr_root_width;
      for(int shave_vertex_index = curr_start_index;
          shave_vertex_index < curr_end_index;
          shave_vertex_index++)
      {
        unsigned curr_iter = shave_vertex_index - curr_start_index;
        
        getHairVertex(curr_vertex, hair_info, shave_vertex_index);
        p_prim_var->setPointValue(curr_vertex, vertex_index);
               
        width_prim_var->setFloatValue(curr_width, varying_index);
       
        if(exportVertexColors)
        {
          MColor curr_color = curr_root_color + curr_iter * curr_cs_delta;
          cs_prim_var->setColorValue(
            curr_color, 
            varying_index);
        }
        
        if(exportOpacities && !oldPrimVarNames)
        {
          float curr_os = root_os + curr_iter * curr_os_delta;
          opacity_prim_var->setColorValue(
            MColor(curr_os, curr_os, curr_os),
            varying_index);
        }
        
        if(exportWcoords)
        {
          w_prim_var->setFloatValue(
            hair_info.uvws[hair_info.hairVertices[shave_vertex_index]].z,
            varying_index);
        }

        curr_width += width_delta;
        varying_index++;
        vertex_index++;
      }

      // we need to repeat the first and last vertices for the catmull-rom basis
      getHairVertex(curr_vertex, hair_info, curr_end_index - 1);
      p_prim_var->setPointValue(curr_vertex, vertex_index);
      vertex_index++;
    }
    
    new_prim.addPrimitiveNode(basis_node);
    new_prim.addPrimitiveNode(curves_node);
  }
  
  return status ? MS::kSuccess : MS::kFailure;
}
                                 
MSyntax
DLSH_cacheShave::newSyntax()
{
  MSyntax syntax;

  syntax.addFlag("-st", "-sampleTime", MSyntax::kDouble);

  syntax.addFlag("-a", "-addstep");
  syntax.addFlag("-r", "-remove");
  syntax.addFlag("-e", "-emit");
  syntax.addFlag("-f", "-flush");

  syntax.addFlag("-c", "-contains");
  syntax.addFlag("-l", "-list");

  syntax.setObjectType(MSyntax::kSelectionList, 0, 1);

  return syntax;
}

MStatus
DLSH_cacheShave::doAdd(MDagPath& selected_path, float sample_time)
{
  MStatus status = MS::kSuccess;
  MString path_string = selected_path.fullPathName();

  if (!DL_getShaveCache().contains(path_string, sample_time))
  {
    DL_RiPrimitive* primitive = new DL_RiPrimitive;
    status = shaveToRiCurves(*primitive, selected_path);
    
    if (status == MS::kSuccess)
    {
      DL_getShaveCache().addData(path_string, sample_time, primitive);
    }
    else
    {
      MGlobal::displayError(
        "Conversion to RiCurves failed for object:" + 
        path_string);
      
      delete primitive;
      primitive = 0;
      
      status = MS::kFailure;
    }
  }
  else
  {
    MGlobal::displayError(
      "Object " +
      path_string + 
      " already cached at sample " +
      sample_time);
    status = MS::kFailure;
  }
  
  return status;
}

MStatus
DLSH_cacheShave::doRemove(MDagPath& selected_path)
{
  MStatus status = MS::kSuccess;
  MString path_string = selected_path.fullPathName();
    
  if (!DL_getShaveCache().remove(path_string))
  {
    MGlobal::displayError("Object was not in cache: " + path_string);
    status = MS::kFailure;
  }

  return status;
}

MStatus
DLSH_cacheShave::doEmit(MDagPath& selected_path, float sample_time)
{
  MStatus status = MS::kSuccess;
  MString path_string = selected_path.fullPathName();

  if (DL_getShaveCache().contains(path_string, sample_time))
  {
    DL_RiPrimitive* primitive = 
      DL_getShaveCache().getData(path_string, sample_time);
    
    if (primitive)
    {
      primitive->emitRIB();
      status = MS::kSuccess;
    }
    else
    {
      // TODO: do we need this branch since we're calling contains()
      // first? Should we just assert?
      MGlobal::displayError("HT error");
      status = MS::kFailure;
    }
  }
  else
  {
    MGlobal::displayError(
      "Object: " + 
      path_string +
      " and/or sample: " + 
      sample_time + 
      " not in cache");
    status = MS::kFailure;
  }
  
  return status;
}

MStatus
DLSH_cacheShave::doEmit(MDagPath& selected_path)
{
  MStatus status = MS::kSuccess;
  MString path_string = selected_path.fullPathName();

  if (DL_getShaveCache().contains(path_string))
  {
    std::vector<float> sample_times;
    DL_getShaveCache().getAllSampleTimes(sample_times, path_string);
    
    std::vector<DL_RiPrimitive*> samples;
    
    for (unsigned i = 0; i < sample_times.size(); i++)
    {
      DL_RiPrimitive* cur_sample = 
        DL_getShaveCache().getData(path_string, sample_times[i]);
    
      samples.push_back(cur_sample);
    }

    if (sample_times.size() == 1)
    {
      samples[0]->emitRIB();
    }
    else
    {
      // Output sample times relative to current frame time
      //
      std::vector<float> sample_offsets(sample_times);
      double curr_frame = 0.0;
      status = MGlobal::executeCommand("delightRenderState -qf", curr_frame);
      
      if (status != MStatus::kSuccess)
      {
        MGlobal::displayError("delightRenderState command failed.");
        return status;
      }
      
      for(unsigned i = 0; i < sample_offsets.size(); i++)
      {
        sample_offsets[i] = roundTimeStep( sample_offsets[i] - curr_frame );
      }

      // foreach of the primitive nodes that make up the geometry, loop over
      // all of the samples and output the RIB
      for (unsigned node_num = 0; 
           node_num < samples[0]->getNumPrimitiveNodes();
           ++node_num)
      {
        if (samples[0]->nodeSupportsMotionBlur(node_num))
        {
          RiMotionBeginV(sample_offsets.size(), &sample_offsets[0]);
          
          for (unsigned sample_num = 0;
               sample_num <  sample_times.size();
               ++sample_num)
          {
            samples[sample_num]->emitRIB(node_num);
          }
          
          RiMotionEnd();
        }
        else
        {
          samples[0]->emitRIB(node_num);
        }
      }
    }
  }
  else
  {
    MGlobal::displayError("Object not in cache: " + path_string);
    status = MS::kFailure;
  }
  
  return status;
}

MStatus
DLSH_cacheShave::doIt(const MArgList& args)
{
  MStatus status = MStatus::kFailure;
  MArgDatabase argData(syntax(), args, &status);
  
  if (status == MS::kSuccess)
  {
    // doing an XOR here
    bool do_emit = argData.isFlagSet("-e");
    bool do_add = argData.isFlagSet("-a");
    bool do_remove = argData.isFlagSet("-r");
    bool do_flush = argData.isFlagSet("-f");
    bool do_list = argData.isFlagSet("-l");
    bool do_contains = argData.isFlagSet("-c");
    
    if (do_list)
    {
      MStringArray  all_objects;
      
      DL_getShaveCache().getAllObjectNames(all_objects);

      setResult(all_objects);
    }
    else
    {
      if((int)do_emit + (int)do_add + (int)do_remove + (int)do_flush + 
        (int) do_contains == 1)
      {
        if (do_flush)
        {
          DL_getShaveCache().clear();
        }
        else
        {
          MSelectionList selection;
          argData.getObjects(selection);
          
          // TODO: check that there's an object specified
          // make sure that it's a shape node and not a transform
          
          // our syntax only allows one object so we can hard-code things a bit
          MObject selected_component;
          MDagPath selected_path;          
          selection.getDagPath(0, selected_path, selected_component);
          
          double sample_time = 0.0;
          bool is_sample_specified = argData.isFlagSet("-st");
          
          if (is_sample_specified)
            argData.getFlagArgument("-st", 0, sample_time);
            
          if (do_add)
          {
            if (is_sample_specified)
              doAdd(selected_path, sample_time);
            else
            {
              MGlobal::displayError("Need to specify a sample num with -st");
              status = MS::kFailure;
            }
          }
          else if (do_remove)
          {
            doRemove(selected_path);
          }
          else if (do_emit)
          {
            if (is_sample_specified)
              doEmit(selected_path, sample_time);
            else
              doEmit(selected_path);
          }
          else if (do_contains)
          {
            MString path_string(selected_path.fullPathName());

            if (is_sample_specified)
              setResult(DL_getShaveCache().contains(path_string, sample_time));
            else 
              setResult(DL_getShaveCache().contains(path_string));
          }
        }
      }
      else
      {
        MGlobal::displayError(
                              "Need to specify precisely one of -add, " \
                              "-remove, -emit, -flush or -contains.");
        status = MS::kFailure;
      }
    }
  }
  
  return status;
}
