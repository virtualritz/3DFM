Create "*uvCoord" "shader" 
SetAttribute "*uvCoord" 
  "shaderfilename" "string" 1 "${DELIGHT}/maya/osl/uvCoord"

Create "swatch_camera" "perspectivecamera"
SetAttribute "swatch_camera"
  "depthoffield.enable" "int" 1 0
  "clippingrange" "double" 2 [ 0.01 1e4 ]
  "shutterrange" "double" 2 [ -0.20000000000000001 0.20000000000000001 ]
  "fov" "float" 1 37.84929

Create "E4A51507-CF41-7E09-990A-CCAB64D3DAFC" "transform"
Connect "swatch_camera" "" "E4A51507-CF41-7E09-990A-CCAB64D3DAFC" "objects"
Create "31F66CF7-114A-A5AA-5698-679FF5D82014" "transform"
Connect "31F66CF7-114A-A5AA-5698-679FF5D82014" "" ".root" "objects"
Connect "E4A51507-CF41-7E09-990A-CCAB64D3DAFC" "" "31F66CF7-114A-A5AA-5698-679FF5D82014" "objects"
SetAttribute "31F66CF7-114A-A5AA-5698-679FF5D82014"
  "transformationmatrix" "doublematrix" 1 [ 0.99847870448060538 0 -0.055138704180746345 0 -0.015242945820073597 0.96102907920973835 -0.27602674058143112 0 0.052989898107640807 0.27644729861816769 0.95956706997752872 0 0.21199999999999999 1.1060000000000001 3.839 1 ]


Create "|shaderBallGeom1|shaderBallGeomShape1" "particles"
SetAttribute "|shaderBallGeom1|shaderBallGeomShape1"
	"P" "point"  1 [0 0 0 ]
	"width" "float" 1 [2]

Create "1219F7E4-A34E-51C1-EC40-8EBCE65E054B" "transform"
Connect "|shaderBallGeom1|shaderBallGeomShape1" "" "1219F7E4-A34E-51C1-EC40-8EBCE65E054B" "objects"
Create "E0510B6B-3341-CAF8-BE25-B3A9CC1ED01D" "transform"
Connect "E0510B6B-3341-CAF8-BE25-B3A9CC1ED01D" "" ".root" "objects"
Connect "1219F7E4-A34E-51C1-EC40-8EBCE65E054B" "" "E0510B6B-3341-CAF8-BE25-B3A9CC1ED01D" "objects"
SetAttribute "E0510B6B-3341-CAF8-BE25-B3A9CC1ED01D"
  "transformationmatrix" "doublematrix" 1 [ 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 ]

Create "B69AF183-AF5F-4EC6-B9B0-4B2F1D688223" "shader"
Create "IBLattributes" "attributes"
Connect "B69AF183-AF5F-4EC6-B9B0-4B2F1D688223" "" "IBLattributes" "surfaceshader"
SetAttribute "B69AF183-AF5F-4EC6-B9B0-4B2F1D688223"
  "shaderfilename" "string" 1 "${DELIGHT}/maya/osl/environmentLight"
Create "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1|shader" "shader"
SetAttribute "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1|shader"
  "shaderfilename" "string" 1 "${DELIGHT}/maya/osl/areaLight"
Create "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1" "mesh"
SetAttribute "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1"
  "P" "point" 4 [ -1 -1 0 -1 1 0 1 1 0 1 -1 0 ]
  "nvertices" "int" 1 4
Create "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1|attributes" "attributes"
SetAttribute "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1|attributes"
  "visibility.camera" "int" 1 -1
SetAttribute "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1|shader"
  "intensity" "float" 1 10
  "i_color" "color" 1 [ 1 1 1 ]
  "normalize_area" "int" 1 0
  "twosided" "int" 1 1
  "decayRate" "int" 1 2
Connect "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1|shader" "" "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1|attributes" "surfaceshader"
Connect "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1|attributes" "" "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1" "geometryattributes"
Create "7313CDB7-C340-64EB-77DF-EB8B660EF2CA" "transform"
Connect "|MayaMtlView_KeyLight1|MayaMtlView_KeyLightShape1" "" "7313CDB7-C340-64EB-77DF-EB8B660EF2CA" "objects"
Create "49DDEB67-9549-C8ED-70A4-90B5B4AB92BD" "transform" 
Connect "49DDEB67-9549-C8ED-70A4-90B5B4AB92BD" "" ".root" "objects" 
Connect "7313CDB7-C340-64EB-77DF-EB8B660EF2CA" "" "49DDEB67-9549-C8ED-70A4-90B5B4AB92BD" "objects" 
SetAttribute "49DDEB67-9549-C8ED-70A4-90B5B4AB92BD" 
  "transformationmatrix" "doublematrix" 1 [ -0.77902662611819107 0 -0.84594179220494126 0 -1.1429452494102248 -1.0935459085825521 1.05253669896729 0 -0.42337582872526541 0.81776625452644192 0.38988621495122078 0 -3.8149999999999999 2.9780000000000002 2.0390000000000001 1 ] 
Create "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1|shader" "shader" 
SetAttribute "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1|shader" 
  "shaderfilename" "string" 1 "${DELIGHT}/maya/osl/areaLight"
Create "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1" "mesh" 
SetAttribute "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1" 
  "P" "point" 4 [ -1 -1 0 -1 1 0 1 1 0 1 -1 0 ] 
  "nvertices" "int" 1 4 
Create "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1|attributes" "attributes" 
SetAttribute "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1|attributes" 
  "visibility.camera" "int" 1 -1 
SetAttribute "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1|shader" 
  "intensity" "float" 1 10 
  "i_color" "color" 1 [ 1 1 1 ] 
  "normalize_area" "int" 1 0 
  "twosided" "int" 1 1 
  "decayRate" "int" 1 2 
Connect "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1|shader" "" "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1|attributes" "surfaceshader" 
Connect "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1|attributes" "" "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1" "geometryattributes" 
Create "1AAD7F0C-4043-F9FB-5188-6293A2E2FEB1" "transform" 
Connect "|MayaMtlView_FillLight1|MayaMtlView_FillLightShape1" "" "1AAD7F0C-4043-F9FB-5188-6293A2E2FEB1" "objects" 
Create "FDDBE8CE-5944-7BDA-DE22-FC9A83E26EC4" "transform" 
Connect "FDDBE8CE-5944-7BDA-DE22-FC9A83E26EC4" "" ".root" "objects" 
Connect "1AAD7F0C-4043-F9FB-5188-6293A2E2FEB1" "" "FDDBE8CE-5944-7BDA-DE22-FC9A83E26EC4" "objects" 
SetAttribute "FDDBE8CE-5944-7BDA-DE22-FC9A83E26EC4" 
  "transformationmatrix" "doublematrix" 1 [ 0.023902599375901779 0 0.99971429205702322 0 0.033613440849478537 0.99943458550100472 -0.00080367822752385972 0 -0.99914903910144148 0.033623047221136702 0.023889084499650992 0 3.9100000000000001 0.84299999999999997 -3.0609999999999999 1 ] 
Create "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1|shader" "shader" 
SetAttribute "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1|shader" 
  "shaderfilename" "string" 1 "${DELIGHT}/maya/osl/areaLight"
Create "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1" "mesh" 
SetAttribute "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1" 
  "P" "point" 4 [ -1 -1 0 -1 1 0 1 1 0 1 -1 0 ] 
  "nvertices" "int" 1 4 
Create "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1|attributes" "attributes" 
SetAttribute "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1|attributes" 
  "visibility.camera" "int" 1 -1 
SetAttribute "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1|shader" 
  "intensity" "float" 1 10 
  "i_color" "color" 1 [ 1 1 1 ] 
  "normalize_area" "int" 1 0 
  "twosided" "int" 1 1 
  "decayRate" "int" 1 2 
Connect "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1|shader" "" "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1|attributes" "surfaceshader" 
Connect "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1|attributes" "" "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1" "geometryattributes" 
Create "4D0D00F1-7146-E687-D206-BF97CE703922" "transform" 
Connect "|MayaMtlView_RimLight1|MayaMtlView_RimLightShape1" "" "4D0D00F1-7146-E687-D206-BF97CE703922" "objects" 
Create "0B1F1D51-044A-E13F-0FB5-1C90979D60B6" "transform" 
Connect "0B1F1D51-044A-E13F-0FB5-1C90979D60B6" "" ".root" "objects" 
Connect "4D0D00F1-7146-E687-D206-BF97CE703922" "" "0B1F1D51-044A-E13F-0FB5-1C90979D60B6" "objects" 
SetAttribute "0B1F1D51-044A-E13F-0FB5-1C90979D60B6" 
  "transformationmatrix" "doublematrix" 1 [ 0.56993414928192632 0 -0.82169037081025054 0 0.15006962547335559 -0.98318073914358184 0.10409006526737703 0 -0.80787014612038588 -0.18263524899941969 -0.56034827815417287 0 -1.339 3.7440000000000002 6.8109999999999999 1 ] 
Create "lambert1|shader" "shader"
SetAttribute "lambert1|shader"
  "shaderfilename" "string" 1 "${DELIGHT}/maya/osl/lambert"
  "refractiveIndex" "float" 1 1
  "transparency" "color" 1 [ 0 0 0 ]
  "i_color" "color" 1 [ 0.5 0.5 0.5 ]
  "refractions" "int" 1 0
  "incandescence" "color" 1 [ 0 0 0 ]
  "i_diffuse" "float" 1 0.8
Create "shading_network_head" "shader"
SetAttribute "shading_network_head"
  "shaderfilename" "string" 1 "${DELIGHT}/maya/osl/shadingEngine_surface"
Connect "lambert1|shader" "outColor" "shading_network_head" "i_surface" 
Create "surface_attributes" "attributes"
Connect "shading_network_head" "" "surface_attributes" "surfaceshader" 
Connect "surface_attributes" "" "E0510B6B-3341-CAF8-BE25-B3A9CC1ED01D" "geometryattributes" 
