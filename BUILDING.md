# Building 3Delight for Maya

## Requirements

- Clone the [**3Delight for Maya repository**](https://gitlab.com/3Delight/3DFM)
  on *GitLab*. Usually this will be checked out to a folder called `3DFM`.

- Download & install [**3Delight|NSI**](https://www.3delight.com/download).

- Downloand & install **Maya**.

- Download the
  [**Maya devkit**](https://www.autodesk.com/developer-network/platform-technologies/maya).

- Download the **`legacy_include` 3Delight** directory.

A clone of the **`osl_shaders`** repository is not mandatory but you will not be
able to compile the OSL shaders of *3Delight for Maya* without it.

### C++ Compiler

On **macOS**: **XCode** or a standalone **Clang** installation (via **Homebrew**,
**MacPorts** etc.).

on **Linux**: **gcc** or **Clang**.

On **Windows**: **Visual C++**. At the time of writing the [**Visual Studio
Community Edition**](https://visualstudio.microsoft.com/vs/community/) is
recommended.

Make sure to select the **Windows SDK** during setup it as this seems to trigger
the installation of the 64-bit version of the compiler.

### Windows Only

- The **`depwrap`** executable.

- Downloadn & install [**Cygwin**](https://www.cygwin.com/setup-x86_64.exe).

  The makefiles require **GNU make** and rely on the `cygpath` utility.

  During the installation, once prompted to select packages, adding the `make`
  package to the default selection is all that is needed. Once setup has
  completed, add `c:\cygwin64\bin` to your `PATH`.

- Download & install [**DirectX 11**](https://www.microsoft.com/en-us/download/details.aspx?displaylang=en&id=6812).

## Prerequisites

### 3Delight

The shell in which you will compile *3Delight for Maya* should have the proper
environment set to run `renderdl`, `oslc`, etc.

Refer to the *3Delight|NSI* installation instructions for details.

### Maya

Any of the supported *Maya* versions can be installed (2018--2023).

One way to select one for compilation is to define the `MAYA_LOCATION`
environment variable to point to the ***Maya* installation dir**.

### Maya Devkit

After unpacking, copy the contents of the **`devkitBase`** folder to
**`$MAYA_LOCATION`**, overwriting exisiting files.

### Other Resources

These are the `legacy_include` folder and the `depwrap` executable when
building on *Windows*.

Although the specific locations of these elements can be defined by more
environment variables, the makefiles will find them automatically if they are
placed as follows:

- Next to your `3DFM` (repository) directory, create a `3delight-resources`
  directory.

- Place the required elements like this:

  `3delight-resources/legacy_include`

  and, **if you are on *Windows***:

  `3delight-resources/building/depwrap/win64-x64/depwrap.exe`

- If you have the `osl_shaders` repository, place your clone next to the
  `3DfM` directory.

## Compilation

### Shell Environment

As stated above, you will need to have valid paths set in `DELIGHT` and
`MAYA_LOCATION` environment variables.

On Windows, make sure that your `MAYA_LOCATION` does not contain double quotes.

Change the current directory to the `3FDM` (repository) directory.

### macOS & Linux

For **bash** and similar shells (e.g. **zsh**):

```sh
source setup.bash
```

Or, for **tcsh**:

```sh
source setup.bash
```

Then:

```sh
make
```

### Windows

assuming you are compiling with *Visual C++ 2022*:

```cmd
winenv.bat /VS2022
```

```cmd
make
```

## Using The Compiled Plug-In

Once the compilation has completed, start *Maya* from the current shell or
command prompt.

The initialization scripts also define the proper environment variables so that
*Maya* will find & use the plug-in, shaders & script files you just built.
