test "$shell" != "/bin/tcsh" && source setup.sh
test "$shell" != "/bin/tcsh" && return

if(-e ./setup.custom) then
	source ./setup.custom
endif

# Set DEVROOT to (hopefully) the location of this file. (TODO: improve)
if (! $?DEVROOT) then
	echo "Error: 3DFM build environment: DEVROOT variable not set";
	setenv DEVROOT `pwd`
	echo "Error: 3DFM build environment: DEVROOT set to $DEVROOT";
endif

# Set PLAT to something platform-specific (matches 3Delight's setting)
if( ! $?PLAT ) then
	setenv PLAT `uname -s`-`uname -m | sed -e's/ /-/g'`
	echo "PLAT variable set to" $PLAT
endif

# Also set OS
if( ! $?OS ) then
	setenv OS `uname`
	echo "OS variable set to" $OS
endif

# 3Delight home directory where various stuff is needed for the build.
if( ! $?HOME_3DELIGHT ) then
	setenv HOME_3DELIGHT /net1/3delight
endif

setenv PATH /usr/local/gcc332/bin:$DEVROOT/bin:${PATH}

# Set Maya environment (only needed to run maya, not for compiling)
if( $?MAYA_LOCATION ) then
	set VERSION=`echo $MAYA_LOCATION | sed -e 's/.*maya\([[:digit:]]\{4,\}\(\.[[:digit:]]\{1,\}\)\{0,1\}\).*/\1/'`
	if( $?MAYA_PLUG_IN_PATH ) then
		setenv MAYA_PLUG_IN_PATH ${DEVROOT}/build/$PLAT/$VERSION/plugins:${MAYA_PLUG_IN_PATH}
	else
		setenv MAYA_PLUG_IN_PATH ${DEVROOT}/build/$PLAT/$VERSION/plugins 
	endif
else
	# If MAYA_LOCATION is not set, then add all possible plug-in paths; this will
	# not work with plug-in auto-load.
	#
	if( $?MAYA_PLUG_IN_PATH ) then
		setenv MAYA_PLUG_IN_PATH ${DEVROOT}/build/$PLAT/2022/plugins:${MAYA_PLUG_IN_PATH}
	else
		setenv MAYA_PLUG_IN_PATH ${DEVROOT}/build/$PLAT/2022/plugins
	endif
	setenv MAYA_PLUG_IN_PATH ${DEVROOT}/build/$PLAT/2020/plugins:${MAYA_PLUG_IN_PATH}
	setenv MAYA_PLUG_IN_PATH ${DEVROOT}/build/$PLAT/2019/plugins:${MAYA_PLUG_IN_PATH}
	setenv MAYA_PLUG_IN_PATH ${DEVROOT}/build/$PLAT/2018/plugins:${MAYA_PLUG_IN_PATH}
endif

if( $?MAYA_SCRIPT_PATH ) then
	setenv MAYA_SCRIPT_PATH ${DEVROOT}/build/$PLAT/scripts:${MAYA_SCRIPT_PATH}
else
	setenv MAYA_SCRIPT_PATH ${DEVROOT}/build/$PLAT/scripts	
endif

if( $?PYTHONPATH ) then
	setenv PYTHONPATH ${DEVROOT}/build/$PLAT/scripts:${PYTHONPATH}
else
	setenv PYTHONPATH ${DEVROOT}/build/$PLAT/scripts
endif

if( $?MAYA_RENDER_DESC_PATH ) then
	setenv MAYA_RENDER_DESC_PATH ${DEVROOT}/render_desc:${MAYA_RENDER_DESC_PATH}
else
	setenv MAYA_RENDER_DESC_PATH ${DEVROOT}/render_desc
endif

if( $OS == "Darwin" ) then
	if( $?XBMLANGPATH ) then
		setenv XBMLANGPATH ${DEVROOT}/icons/:${XBMLANGPATH}
	else
		setenv XBMLANGPATH ${DEVROOT}/icons/
	endif
else
	if( $?XBMLANGPATH ) then
		setenv XBMLANGPATH ${DEVROOT}/icons/%B:${XBMLANGPATH}
	else
		setenv XBMLANGPATH ${DEVROOT}/icons/%B
	endif
endif

if( $?MAYA_PRESET_PATH ) then
	setenv MAYA_PRESET_PATH ${DEVROOT}/presets:${MAYA_PRESET_PATH}
else
	setenv MAYA_PRESET_PATH ${DEVROOT}/presets
endif

if( $?MAYA_CUSTOM_TEMPLATE_PATH ) then
	setenv MAYA_CUSTOM_TEMPLATE_PATH ${DEVROOT}/build/$PLAT/templates:${MAYA_CUSTOM_TEMPLATE_PATH}
else
	setenv MAYA_CUSTOM_TEMPLATE_PATH ${DEVROOT}/build/$PLAT/templates
endif

setenv _3DFM_OSL_PATH ${DEVROOT}/build/$PLAT/osl

if(! $?_3DFM_USER_FRAGMENT_PATH ) then
	setenv _3DFM_USER_FRAGMENT_PATH ${DEVROOT}/shaderFragments
endif
